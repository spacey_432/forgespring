import './init';

import DiaryOfAn8BitMage from '../assets/fonts/DiaryOfAn8BitMage.png?url';
import fonts$info from '../assets/fonts/index.json';

import { Filter, Graphics, Rectangle } from 'pixi.js';
import forgespring_sources, { forgespringMap } from './forgespring/sources';
import {
   content,
   context,
   filters,
   inventories,
   musicConvolver,
   musicFilter,
   musicMixer,
   musicRegistry,
   soundMixer
} from './systems/assets';
import {
   OutertaleChildAnimationDecorator,
   OutertaleChildBarrierDecorator,
   OutertaleChildObject,
   OutertaleChildScriptDecorator,
   OutertaleEditorContainer,
   OutertaleLayerKey,
   OutertaleMap,
   OutertaleParentObject,
   OutertaleParentObjectDecorator,
   OutertaleRoom,
   OutertaleRoomDecorator,
   OutertaleSpriteDecorator
} from './systems/outertale';
import {
   CosmosAnimation,
   CosmosAnimationResources,
   CosmosAsset,
   CosmosAtlas,
   CosmosDirection,
   CosmosEventHost,
   CosmosHitbox,
   CosmosImage,
   CosmosInstance,
   CosmosInventory,
   CosmosKeyboardInput,
   CosmosNavigator,
   CosmosObject,
   CosmosPoint,
   CosmosRectangle,
   CosmosRegion,
   CosmosRegistry,
   CosmosRenderer,
   CosmosSprite,
   CosmosText,
   CosmosTextUtils,
   CosmosUtils,
   CosmosValue
} from './systems/storyteller';
import './types.d';

import.meta.hot?.on('vite:beforeFullReload', () => {
   throw '';
});

const text = {
   delete: 'delete',
   property: {
      active: 'active',
      anchor: 'anchor',
      args: 'args',
      background: 'background',
      filter: 'filter',
      filters: 'filters',
      frames: 'frames',
      gain: 'gain',
      layer: 'layer',
      music: 'music',
      name: 'name',
      neighbors: 'neighbors',
      objects: 'objects',
      position: 'position',
      preload: 'preload',
      rate: 'rate',
      regionMax: 'region-max',
      regionMin: 'region-min',
      resources: 'resources',
      reverb: 'reverb',
      rotation: 'rotation',
      score: 'score',
      size: 'size',
      spawn: 'spawn',
      steps: 'steps',
      tags: 'tags',
      type: 'type'
   },
   room: 'Room: $(x)',
   status: {
      creating: 'creating',
      modifying: 'modifying',
      selecting: 'selecting'
   },
   type: {
      object: 'object',
      room: 'room',
      subObject: 'sub-object'
   }
};

function atlasInput (key = null as string | null) {
   return game.input || [ 'ArrowLeft', 'ArrowRight' ].includes(key!);
}

function backSprite (map: OutertaleMap, name: string) {
   const handler = {
      priority: -Infinity,
      listener (this: CosmosSprite) {
         const {
            area: { x, y, width, height },
            offset
         } = map.value![`r:${name}`];
         this.crop.top = y;
         this.crop.left = x;
         this.crop.right = -x - width;
         this.crop.bottom = -y - height;
         this.position.set(offset);
         this.off('tick', handler);
      }
   };
   return new CosmosSprite({ frames: [ map.image ] }).on('tick', handler);
}

function easyRoom (name: string, map: OutertaleMap, decorator: OutertaleRoomDecorator) {
   const extras: CosmosAsset[] = [];
   rooms.register(
      name,
      new OutertaleRoom({
         decorator,
         face: decorator.face as CosmosDirection,
         layers: Object.fromEntries(
            Object.entries(decorator.layers ?? {}).map(([ layer, objects = [] ]) => [
               layer,
               [
                  ...(layer === decorator.background ? [ backSprite(map, name) ] : []),
                  ...(layer in (decorator.mixins ?? {}) ? [ backSprite(maps.of(decorator.mixins![layer]!), name) ] : []),
                  ...objects.map(decorator => {
                     const {
                        attachments = [],
                        barriers = [],
                        filters: filterList = [],
                        interacts = [],
                        position,
                        rotation,
                        tags = [],
                        triggers = []
                     } = decorator;
                     return new CosmosObject({
                        filters: filterList
                           .filter(filter => filter in filters)
                           .map(filter => filters[filter as keyof typeof filters]),
                        metadata: { class: 'object', decorator, tags: tags.slice() },
                        position,
                        rotation,
                        objects: [
                           ...attachments.map(decorator => {
                              const {
                                 anchor,
                                 active = false,
                                 filters: filterList = [],
                                 frames = [],
                                 position,
                                 resources,
                                 rotation,
                                 steps: duration,
                                 type
                              } = decorator;
                              if (type === 'sprite') {
                                 return new CosmosSprite({
                                    anchor,
                                    active,
                                    filters: filterList
                                       .filter(filter => filter in filters)
                                       .map(filter => filters[filter as keyof typeof filters]),
                                    frames: frames.map(frame => {
                                       const extra = content[frame as keyof typeof content] as CosmosImage;
                                       extras.push(extra);
                                       return extra;
                                    }),
                                    metadata: { class: 'attachment', decorator },
                                    position,
                                    rotation,
                                    duration
                                 });
                              } else {
                                 const extra = content[resources as keyof typeof content] as CosmosAnimationResources;
                                 extras.push(extra);
                                 return new CosmosAnimation({
                                    anchor,
                                    active,
                                    filters: filterList
                                       .filter(filter => filter in filters)
                                       .map(filter => filters[filter as keyof typeof filters]),
                                    metadata: { class: 'attachment', decorator },
                                    resources: extra,
                                    position,
                                    rotation
                                 });
                              }
                           }),
                           ...barriers.map(decorator => {
                              const { anchor, position, rotation, size } = decorator;
                              return new CosmosHitbox({
                                 anchor,
                                 metadata: { barrier: true, class: 'barrier', decorator },
                                 position,
                                 rotation,
                                 size
                              });
                           }),
                           ...interacts.map(decorator => {
                              const { anchor, args = [], name, position, rotation, size } = decorator;
                              return new CosmosHitbox({
                                 anchor,
                                 metadata: {
                                    args: args.slice(),
                                    class: 'interact',
                                    decorator,
                                    interact: true,
                                    name
                                 },
                                 position,
                                 rotation,
                                 size
                              });
                           }),
                           ...triggers.map(decorator => {
                              const { anchor, args = [], name, position, rotation, size } = decorator;
                              return new CosmosHitbox({
                                 anchor,
                                 metadata: { args, class: 'trigger', decorator, name, trigger: true },
                                 position,
                                 rotation,
                                 size
                              });
                           })
                        ]
                     });
                  })
               ]
            ])
         ),
         metadata: decorator.metadata,
         neighbors: decorator.neighbors?.slice(),
         preload: new CosmosInventory(
            ...([
               ...[ ...new Set([ map, ...Object.values(decorator.mixins ?? {}).map(key => maps.of(key!)) ]) ],
               ...extras,
               ...(decorator.preload ?? []).map(asset =>
                  asset[0] === '#'
                     ? maps.get(asset.slice(1))
                     : content[asset as keyof typeof content] || inventories[asset as keyof typeof inventories]
               )
            ].filter(asset => asset instanceof CosmosAsset) as CosmosAsset[])
         ),
         region: decorator.region?.slice() as CosmosRegion | [],
         score: decorator.score,
         spawn: decorator.spawn
      })
   );
}

function editorProperty (property: string) {
   if (atlas.target === 'editorObject' || property !== editor.property) {
      return '§fill:#fff§';
   } else if (atlas.target === 'editorProperty') {
      return '§fill:#0ff§';
   } else {
      return '§fill:#ff0§';
   }
}

function editorValue (property: string, index: number) {
   if (atlas.target === 'editorValue' && property === editor.property) {
      if (index === editor.index) {
         return '§fill:#f00§';
      } else {
         return '§fill:#ff0§';
      }
   } else {
      return editorProperty(property);
   }
}

function param (key: string, value: string | null = null) {
   value === null ? params.delete(key) : params.set(key, value);
   history.replaceState(null, '', `${location.origin + location.pathname}?${params.toString()}${location.hash}`);
}

async function reload (fast = false, invisible = fast) {
   param('fast', fast ? '' : null);
   param('invisible', invisible ? '' : null);
   musicMixer.output.disconnect(context.destination);
   soundMixer.output.disconnect(context.destination);
   renderer.stop();
   renderer.application.destroy(true, true);
   for (const value of Object.values(content)) {
      value.destroy();
   }
   await context.close();
   backend?.exec('reload') ?? location.reload();
}

async function teleport (dest: string) {
   const next = rooms.of(dest);
   const score = next.score;
   const prev = rooms.of(game.room);
   if (score.music !== prev.score.music) {
      game.music?.stop();
   }
   renderer.clear('below', 'main', 'above');
   await next.preload.load();
   for (const key in next.layers) {
      renderer.attach(key as keyof typeof next.layers, ...(next.layers[key as keyof typeof next.layers] ?? []));
   }
   prev !== next && prev.preload.unload();
   const prevkey = game.room;
   game.room = dest;
   await renderer.on('render');
   events.fire('teleport', prevkey, dest);
   renderer.region[0].x = next.region[0]?.x ?? renderer.region[0].x;
   renderer.region[0].y = next.region[0]?.y ?? renderer.region[0].y;
   renderer.region[1].x = next.region[1]?.x ?? renderer.region[1].x;
   renderer.region[1].y = next.region[1]?.y ?? renderer.region[1].y;
   renderer.position.set(renderer.position.clamp(...renderer.region));
   if (score.music !== prev.score.music) {
      const daemon = typeof score.music === 'string' ? musicRegistry.of(score.music) : null;
      game.music = daemon?.instance(renderer) ?? null;
   }
   musicFilter.value = score.filter;
   game.music && (game.music.gain.value = game.music.daemon.gain * score.gain);
   game.music && (game.music.rate.value = game.music.daemon.rate * score.rate);
   musicConvolver.value = score.reverb;
}

const atlas = new CosmosAtlas();
const backend = typeof ___spacetime___ === 'object' ? ___spacetime___ : null;
const dragger = { state: false, origin: { x: 0, y: 0 }, offset: { x: 0, y: 0 } };
const events = new CosmosEventHost<{ teleport: [string, string] }>();

const fontLoader = CosmosTextUtils.import(
   DiaryOfAn8BitMage,
   fonts$info.find(font => font.name === 'DiaryOfAn8BitMage')!
).then(({ name, font }) => {
   CosmosTextUtils.register(name, font);
});

const game = {
   input: true,
   music: null as CosmosInstance | null,
   resize () {
      const width = 640;
      const height = 480;
      const frame = document.querySelector('#frame') as HTMLElement;
      const wrapper = document.querySelector('#wrapper') as HTMLElement;
      let ratio: number;
      if (frame.clientWidth / frame.clientHeight < width / height) {
         ratio = frame.clientWidth / width;
      } else {
         ratio = frame.clientHeight / height;
      }
      wrapper.style.width = `${width}px`;
      wrapper.style.height = `${height}px`;
      wrapper.style.transform = `scale(${ratio})`;
   },
   room: ''
};

const keys = {
   downKey: new CosmosKeyboardInput(null, 'ArrowDown', 'KeyS'),
   freecamKey: new CosmosKeyboardInput(null, 'Tab', ''),
   interactKey: new CosmosKeyboardInput(null, 'Enter', 'Space'),
   leftKey: new CosmosKeyboardInput(null, 'ArrowLeft', 'KeyA'),
   rightKey: new CosmosKeyboardInput(null, 'ArrowRight', 'KeyD'),
   specialKey: new CosmosKeyboardInput(null, 'Backspace', 'Escape', 'ShiftLeft', 'ShiftRight'),
   upKey: new CosmosKeyboardInput(null, 'ArrowUp', 'KeyW')
};

const maps = new CosmosRegistry<string, OutertaleMap>(new OutertaleMap('', new CosmosImage('')));
const params = new URLSearchParams(location.search);

const renderer = new CosmosRenderer<OutertaleLayerKey>({
   area: new Rectangle(0, 0, 640, 480),
   wrapper: '#wrapper',
   layers: { base: [ 'fixed' ], below: [], main: [ 'vertical' ], above: [], menu: [ 'fixed' ] },
   width: 640,
   height: 480,
   scale: 2
});

const rooms = new CosmosRegistry<string, OutertaleRoom>(new OutertaleRoom());
const zoomFactor = 2 ** (1 / 4);

maps.register('forgespring', forgespringMap);

for (const [ key, value ] of Object.entries(forgespring_sources)) {
   easyRoom(key, forgespringMap, value);
}

const editor = {
   associations: new Map<OutertaleParentObject | OutertaleChildObject, CosmosRectangle>(),
   box (parent: OutertaleParentObject, child?: OutertaleChildObject) {
      if (child) {
         editor.associations.has(child) ||
            editor.associations.set(
               child,
               new CosmosRectangle({
                  alpha: 1,
                  fill: { attachment: '#ffffff3f', barrier: '#ff00003f', interact: '#00ff003f', trigger: '#0000ff3f' }[
                     child.metadata.class
                  ],
                  metadata: {
                     display: false,
                     graphics: new Graphics()
                  },
                  stroke: {
                     attachment: '#ffffffdf',
                     barrier: '#ff0000df',
                     interact: '#00ff00df',
                     trigger: '#0000ffdf'
                  }[child.metadata.class]
               }).on('tick', function () {
                  this.alpha.value = editor.target?.object === child ? 1 : 0.6;
                  this.anchor.set(child.anchor);
                  this.position.set(parent.position.add(child));
                  this.size.set(child.compute());
                  this.rotation.value = child.rotation.value;
                  if (editor.target?.object === child) {
                     const graphics = this.metadata.graphics.clear().lineStyle({
                        width: 1.5,
                        color: { attachment: 0xffffff, barrier: 0xff3f3f, interact: 0x3fff3f, trigger: 0x3f3fff }[
                           child.metadata.class
                        ],
                        alpha: 0x6f / 0xff
                     });
                     const size = child.compute();
                     const half = new CosmosPoint(size?.x || 0, size?.y || 0).divide(2);
                     const diff = half.add(half.multiply(child.anchor));
                     for (const [ dx, dy ] of [
                        [ 0, 0 ],
                        [ size?.x ?? 0, 0 ],
                        [ 0, size?.y ?? 0 ],
                        [ size?.x ?? 0, size?.y ?? 0 ]
                     ]) {
                        graphics
                           .beginFill()
                           .moveTo(dx - diff.x, dy - diff.y)
                           .lineTo(0, 0)
                           .endFill();
                     }
                     graphics.beginFill().arc(0, 0, 1.5, 0, 360).endFill();
                     if (!this.metadata.display) {
                        this.metadata.display = true;
                        this.container.addChild(graphics);
                     }
                  } else if (this.metadata.display) {
                     this.metadata.display = false;
                     this.container.removeChild(this.metadata.graphics);
                  }
               })
            );
         return editor.associations.get(child)!;
      } else {
         editor.associations.has(parent) ||
            editor.associations.set(
               parent,
               new CosmosRectangle().on('tick', function () {
                  this.alpha.value = editor.target?.object === parent ? 1 : 0.6;
                  this.position = (renderer.freecam ? renderer.position : renderer.position.clamp(...renderer.region))
                     .multiply(-renderer.zoom.value)
                     .add(160, 120);
                  this.rotation.value = parent.rotation.value;
                  this.scale.set(renderer.zoom.value);
               })
            );
         return editor.associations.get(parent)!;
      }
   },
   containers: [] as OutertaleEditorContainer[],
   depth: -1,
   destination: params.get('room') ?? 'f_hub',
   editTime: Infinity,
   get index () {
      return atlas.navigators.of('editorValue').position.x;
   },
   set index (value) {
      atlas.navigators.of('editorValue').position.x = value;
   },
   generate () {
      editor.wrapper.objects.splice(0, editor.wrapper.objects.length);
      editor.containers = (
         editor.layers
            .flatMap(key => renderer.layers[key].objects)
            .filter(object => object.metadata.class === 'object') as OutertaleParentObject[]
      ).map(parent => {
         const box = editor.box(parent);
         const container = {
            box,
            children: [] as {
               box: CosmosRectangle;
               object: OutertaleChildObject;
               parent: OutertaleEditorContainer;
            }[],
            object: parent
         };
         box.objects.splice(0, box.objects.length);
         box.attach(
            ...parent.objects.map((child: OutertaleChildObject) => {
               const box = editor.box(parent, child);
               container.children.push({ box, object: child, parent: container });
               return box;
            })
         );
         editor.wrapper.attach(box);
         return container;
      });
   },
   get group () {
      const property = editor.property;
      for (const key in editor.groups) {
         if (editor.groups[key as keyof typeof editor.groups].includes(property)) {
            return key;
         }
      }
   },
   groups: {
      array: [ 'tags', 'frames', 'args', 'preload', 'neighbors', 'filters' ],
      boolean: [ 'active' ],
      number: [ 'steps', 'gain', 'reverb', 'filter', 'rate', 'rotation' ],
      string: [ 'resources', 'name', 'music' ],
      vector: [ 'position', 'anchor', 'size', 'region-min', 'region-max', 'spawn' ]
   },
   layers: [ 'below', 'main', 'above' ] as OutertaleLayerKey[],
   get parent () {
      return editor.containers[editor.pos.x];
   },
   pos: { x: 0, y: 0 },
   get property () {
      return atlas.navigators.of('editorProperty').selection() as string;
   },
   get room () {
      return rooms.of(game.room).decorator ?? {};
   },
   get target () {
      if (editor.depth === -1) {
         return null;
      } else if (editor.depth > 0) {
         return editor.parent.children[editor.pos.y];
      } else if (editor.containers.length > 0) {
         return editor.parent;
      } else {
         return null;
      }
   },
   write () {
      backend?.writeRoom(game.room, CosmosUtils.serialize(rooms.of(game.room).decorator, true));
   },
   text: new CosmosRectangle({
      position: { x: 2.5, y: 240 - 2.5 },
      anchor: { y: 1 },
      fill: '#0007',
      fontName: 'DiaryOfAn8BitMage',
      fontSize: 10,
      priority: Infinity,
      objects: [
         new CosmosText({
            anchor: { y: 1 },
            fill: '#fff',
            position: { x: 2.5, y: -2.5 },
            spacing: { x: -0.5 }
         }).on('tick', function () {
            if (editor.depth === -2) {
               this.content = text.room.replace('$(x)', editor.destination);
            } else if (editor.depth === -1) {
               this.content = `${text.status.modifying} - ${text.type.room}\n---\n${editorProperty('preload')}> ${
                  text.property.preload
               }: [${CosmosTextUtils.format(
                  (editor.room.preload || [])
                     .map((arg, index) => `${editorValue('preload', index)}"${arg}"${editorProperty('preload')}`)
                     .join(', '),
                  120,
                  true
               )}]\n${editorProperty('background')}> ${text.property.background}: "${
                  editor.room.background
               }"\n${editorProperty('neighbors')}> ${text.property.neighbors}: [${(editor.room.neighbors || [])
                  .map((arg, index) => `${editorValue('neighbors', index)}"${arg}"${editorProperty('neighbors')}`)
                  .join(', ')}]\n${editorProperty('region-min')}> ${text.property.regionMin}: ${
                  editor.room.region?.[0].x ?? 0
               }x ${editor.room.region?.[0].y ?? 0}y\n${editorProperty('region-max')}> ${text.property.regionMax}: ${
                  editor.room.region?.[1].x ?? 0
               }x ${editor.room.region?.[1].y ?? 0}y\n${editorProperty('spawn')}> ${text.property.spawn}: ${
                  editor.room.spawn?.x ?? 0
               }x ${editor.room.spawn?.y ?? 0}y\n${editorProperty('music')}> ${text.property.music}: "${
                  editor.room.score?.music ?? ''
               }"\n${editorProperty('gain')}> ${text.property.gain}: ${editor.room.score?.gain ?? 1}\n${editorProperty(
                  'reverb'
               )}> ${text.property.reverb}: ${editor.room.score?.reverb ?? 0}\n${editorProperty('filter')}> ${
                  text.property.filter
               }: ${editor.room.score?.filter ?? 0}\n${editorProperty('rate')}> ${text.property.rate}: ${
                  editor.room.score?.rate ?? 1
               }\n${editorProperty('objects')}> ${text.property.objects}: [${editor.containers.length}]`;
            } else if (atlas.target === 'editorAdd') {
               this.content = `${text.status.creating} - ${
                  editor.depth > 0 ? text.type.subObject : text.type.object
               }\n§fill:#ff0§> ${editor.depth > 0 ? text.property.type : text.property.layer}: ${
                  (editor.depth > 0 ? editor.types : editor.layers)[atlas.navigators.of('editorAdd').position.y]
               }`;
            } else {
               const target = editor.target;
               if (target) {
                  const object = target.object;
                  const meta = object.metadata;
                  this.content = `${
                     atlas.target === 'editorObject' ? text.status.selecting : text.status.modifying
                  } - ${meta.class}\n---\n${editorProperty('position')}> ${text.property.position}: ${
                     meta.decorator.position?.x ?? 0
                  }x ${meta.decorator.position?.y ?? 0}y\n${
                     meta.class === 'object'
                        ? `${editorProperty('filters')}> ${text.property.filters}: [${(meta.decorator.filters || [])
                             .map(
                                (frame, index) =>
                                   `${editorValue('filters', index)}"${frame}"${editorProperty('filters')}`
                             )
                             .join(', ')}]\n${editorProperty('tags')}> ${text.property.tags}: [${(
                             meta.decorator.tags || []
                          )
                             .map((tag, index) => `${editorValue('tags', index)}"${tag}"${editorProperty('tags')}`)
                             .join(', ')}]\n${editorProperty('objects')}> ${text.property.objects}: [${
                             object.objects.length
                          }]`
                        : `${editorProperty('anchor')}> ${text.property.anchor}: ${meta.decorator.anchor?.x ?? -1}x ${
                             meta.decorator.anchor?.y ?? -1
                          }y\n${
                             meta.class === 'attachment'
                                ? `${editorProperty('active')}> ${text.property.active}: ${
                                     meta.decorator.active || false
                                  }\n${editorProperty('filters')}> ${text.property.filters}: [${(
                                     meta.decorator.filters || []
                                  )
                                     .map(
                                        (frame, index) =>
                                           `${editorValue('filters', index)}"${frame}"${editorProperty('filters')}`
                                     )
                                     .join(', ')}]\n${
                                     meta.decorator.type === 'sprite'
                                        ? `${editorProperty('frames')}> ${text.property.frames}: [${(
                                             meta.decorator.frames || []
                                          )
                                             .map(
                                                (frame, index) =>
                                                   `${editorValue('frames', index)}"${frame}"${editorProperty(
                                                      'frames'
                                                   )}`
                                             )
                                             .join(', ')}]\n${editorProperty('steps')}> ${text.property.steps}: ${
                                             meta.decorator.steps || 0
                                          }`
                                        : `${editorProperty('resources')}> ${text.property.resources}: "${
                                             meta.decorator.resources || ''
                                          }"`
                                  }`
                                : `${editorProperty('size')}> ${text.property.size}: ${meta.decorator.size?.x ?? 0}x ${
                                     meta.decorator.size?.y ?? 0
                                  }y${
                                     meta.class === 'barrier'
                                        ? ''
                                        : `\n${editorProperty('name')}> ${text.property.name}: "${
                                             meta.decorator.name || ''
                                          }"\n${editorProperty('args')}> ${text.property.args}: [${(
                                             meta.decorator.args || []
                                          )
                                             .map(
                                                (arg, index) =>
                                                   `${editorValue('args', index)}"${arg}"${editorProperty('args')}`
                                             )
                                             .join(', ')}]`
                                  }`
                          }`
                  }\n${editorProperty('rotation')}> ${text.property.rotation}: ${
                     meta.decorator.rotation ?? 0
                  }\n${editorProperty('delete')}> ${text.delete}`;
               }
            }
         })
      ]
   }).on('tick', function () {
      this.size.set((this.objects[0] as CosmosText).compute().add(5, 7.5));
   }),
   types: [ 'animation', 'sprite', 'barrier', 'interact', 'trigger' ],
   waiting: false,
   wrapper: new CosmosObject({ priority: -1 })
};

atlas.navigators.register({
   editorAdd: new CosmosNavigator({
      flip: true,
      grid: () => [ editor.depth > 0 ? editor.types : editor.layers ],
      next (self) {
         const selection = self.selection() as string;
         if (editor.depth > 0) {
            let object: CosmosObject;
            const parent = editor.parent.object;
            switch (selection) {
               case 'animation': {
                  const decorator = { type: 'animation' } as OutertaleChildAnimationDecorator;
                  object = new CosmosAnimation({ metadata: { class: 'attachment', decorator } });
                  (parent.metadata.decorator.attachments ??= []).push(decorator);
                  break;
               }
               case 'sprite': {
                  const decorator = { type: 'sprite' } as OutertaleSpriteDecorator;
                  object = new CosmosSprite({ metadata: { class: 'attachment', decorator } });
                  (parent.metadata.decorator.attachments ??= []).push(decorator);
                  break;
               }
               case 'barrier': {
                  const decorator = {} as OutertaleChildBarrierDecorator;
                  object = new CosmosHitbox({ metadata: { barrier: true, class: 'barrier', decorator } });
                  (parent.metadata.decorator.barriers ??= []).push(decorator);
                  break;
               }
               default: {
                  const decorator = {} as OutertaleChildScriptDecorator;
                  object = new CosmosHitbox({
                     metadata: { [selection]: true, args: [], class: selection, decorator, name: '' }
                  });
                  (parent.metadata.decorator[`${selection as 'interact' | 'trigger'}s`] ??= []).push(decorator);
                  break;
               }
            }
            parent.attach(object);
            editor.generate();
            editor.write();
            for (const [ index, child ] of editor.parent.children.entries()) {
               if (child.object === object) {
                  editor.pos.y = index;
                  break;
               }
            }
         } else {
            const decorator = {} as OutertaleParentObjectDecorator;
            const object = new CosmosObject({ metadata: { class: 'object', decorator, tags: [] as string[] } });
            ((rooms.of(game.room).decorator!.layers ??= {})[selection as OutertaleLayerKey] ??= []).push(decorator);
            renderer.attach(selection as OutertaleLayerKey, object);
            const room = game.room;
            events.on('teleport', (from, to) => {
               if (to === room) {
                  renderer.attach(selection as OutertaleLayerKey, object);
               } else if (from === room) {
                  renderer.detach(selection as OutertaleLayerKey, object);
               }
            });
            editor.generate();
            editor.write();
            for (const [ index, parent ] of editor.containers.entries()) {
               if (parent.object === object) {
                  editor.pos.x = index;
                  break;
               }
            }
         }
         atlas.navigators.of('editorProperty').position = { x: 0, y: 0 };
         return 'editorProperty';
      },
      prev () {
         if (editor.depth > 0) {
            editor.parent.children.length === 0 && (editor.depth = 0);
            return 'editorObject';
         } else if (editor.containers.length > 0) {
            return 'editorObject';
         } else {
            editor.depth = -1;
            atlas.navigators.of('editorProperty').position.y = CosmosUtils.provide(
               atlas.navigators.of('editorProperty').grid
            )[0].indexOf('objects');
            return 'editorProperty';
         }
      }
   }).on('from', function () {
      this.position = { x: 0, y: 0 };
   }),
   editorObject: new CosmosNavigator<string>({
      flip: true,
      grid () {
         if (editor.depth > 0) {
            return [ [ ...editor.parent.children.keys() ] ];
         } else {
            return [ [ ...editor.containers.keys() ] ];
         }
      },
      next () {
         atlas.navigators.of('editorProperty').position.y = 0;
         return 'editorProperty';
      },
      prev () {
         editor.depth--;
         atlas.navigators.of('editorProperty').position.y = CosmosUtils.provide(
            atlas.navigators.of('editorProperty').grid
         )[0].indexOf('objects');
         return 'editorProperty';
      }
   })
      .on('from', function () {
         this.position.y = editor.depth > 0 ? editor.pos.y : editor.pos.x;
      })
      .on('change', function () {
         if (editor.depth > 0) {
            editor.pos.y = this.position.y;
         } else {
            editor.pos.x = this.position.y;
         }
      }),
   editorProperty: new CosmosNavigator({
      grid () {
         if (editor.depth === -1) {
            return [
               [
                  'preload',
                  'background',
                  'neighbors',
                  'region-min',
                  'region-max',
                  'spawn',
                  'music',
                  'gain',
                  'reverb',
                  'filter',
                  'rate',
                  'objects'
               ]
            ];
         } else {
            const meta = editor.target!.object.metadata;
            switch (meta.class) {
               case 'attachment':
                  if (meta.decorator.type === 'sprite') {
                     return [ [ 'position', 'anchor', 'active', 'filters', 'frames', 'steps', 'rotation', 'delete' ] ];
                  } else {
                     return [ [ 'position', 'anchor', 'active', 'filters', 'resources', 'rotation', 'delete' ] ];
                  }
               case 'barrier':
                  return [ [ 'position', 'anchor', 'size', 'rotation', 'delete' ] ];
               case 'interact':
               case 'trigger':
                  return [ [ 'position', 'anchor', 'size', 'name', 'args', 'rotation', 'delete' ] ];
               case 'object':
                  return [ [ 'position', 'filters', 'tags', 'objects', 'rotation', 'delete' ] ];
            }
         }
      },
      next (self) {
         if (editor.depth === -2) {
            if (!editor.waiting) {
               editor.waiting = true;
               param('room', editor.destination);
               teleport(editor.destination).then(() => {
                  editor.generate();
                  renderer.attach('menu', editor.wrapper);
                  editor.depth = -1;
                  editor.waiting = false;
               });
            }
            return;
         }
         const property = self.selection();
         if (property === 'objects') {
            if (editor.depth === -1) {
               editor.depth = 0;
               editor.pos.x = 0;
               return editor.containers.length > 0 ? 'editorObject' : 'editorAdd';
            } else {
               const target = editor.target as OutertaleEditorContainer;
               editor.depth = 1;
               editor.pos.y = 0;
               return target.children.length > 0 ? 'editorObject' : 'editorAdd';
            }
         } else if (property === 'delete') {
            const target = editor.target!;
            if (target.object.metadata.class === 'object') {
               for (const layer of editor.layers) {
                  if (renderer.layers[layer].objects.includes(target.object)) {
                     renderer.detach(layer, target.object);
                     const list = rooms.of(game.room).decorator!.layers![layer]!;
                     list.splice(list.indexOf(target.object.metadata.decorator as any), 1);
                     break;
                  }
               }
               editor.generate();
               editor.write();
               if (editor.containers.length > 0) {
                  editor.pos.x = Math.min(editor.pos.x, editor.containers.length - 1);
                  return 'editorObject';
               } else {
                  editor.depth = -1;
                  atlas.navigators.of('editorProperty').position.y = CosmosUtils.provide(
                     atlas.navigators.of('editorProperty').grid
                  )[0].indexOf('objects');
                  return 'editorProperty';
               }
            } else {
               const parent = editor.parent;
               parent.object.objects.splice(parent.object.objects.indexOf(target.object), 1);
               const collection = parent.object.metadata.decorator;
               for (const key in collection) {
                  if (key !== 'tags' && key !== 'position') {
                     const list = collection[key as keyof typeof collection] as (
                        | OutertaleChildAnimationDecorator
                        | OutertaleSpriteDecorator
                        | OutertaleChildBarrierDecorator
                        | OutertaleChildScriptDecorator
                     )[];
                     if (list.includes(target.object.metadata.decorator)) {
                        list.splice(list.indexOf(target.object.metadata.decorator as any), 1);
                        break;
                     }
                  }
               }
               editor.generate();
               editor.write();
               for (const [ index, other ] of editor.containers.entries()) {
                  if (other.object === parent.object) {
                     editor.pos.x = index;
                     break;
                  }
               }
               if (parent.children.length > 0) {
                  editor.pos.y = Math.min(editor.pos.y, parent.children.length - 1);
                  return 'editorObject';
               } else {
                  editor.depth = 0;
                  atlas.navigators.of('editorProperty').position.y = CosmosUtils.provide(
                     atlas.navigators.of('editorProperty').grid
                  )[0].indexOf('objects');
                  return 'editorProperty';
               }
            }
         } else {
            switch (editor.group) {
               case 'array':
               case 'boolean':
                  editor.index = 0;
                  atlas.navigators.of('editorValue').position.y = 0;
                  break;
               case 'number':
                  editor.index = 1;
                  atlas.navigators.of('editorValue').position.y = 0;
                  break;
               case 'vector':
                  editor.index = 1;
                  atlas.navigators.of('editorValue').position.y = 1;
                  break;
            }
            editor.editTime = renderer.time + 50;
            if (editor.group === 'array' || editor.group === 'string') {
               game.input = false;
            }
            return 'editorValue';
         }
      },
      prev () {
         if (editor.depth === -1) {
            editor.depth = -2;
            renderer.detach('menu', editor.wrapper);
            atlas.navigators.of('editorProperty').position = { x: 0, y: 0 };
         } else if (editor.depth !== -2) {
            return 'editorObject';
         }
      }
   }),
   editorValue: new CosmosNavigator<string>({
      grid () {
         switch (editor.group) {
            case 'array':
               if (editor.depth === -1) {
                  return [ ...(editor.room[editor.property as 'preload' | 'neighbors'] || []).keys() ].map(value => [
                     value
                  ]);
               } else {
                  //@ts-expect-error
                  return [ ...editor.target.object.metadata[editor.property].keys() ].map(value => [ value ]);
               }
            case 'boolean':
               return [ [ 0 ], [ 1 ] ];
            case 'number':
               return [ [ 0 ], [ 1 ], [ 2 ] ];
            case 'vector':
               return [
                  [ 0, 1, 2 ],
                  [ 3, 4, 5 ],
                  [ 6, 7, 8 ]
               ];
            default:
               return [ [] ];
         }
      },
      prev: 'editorProperty'
   })
      .on('change', () => {
         if (editor.depth === -1) {
            const room = editor.room;
            const position = atlas.navigators.of('editorValue').position;
            const property = editor.property;
            switch (editor.group) {
               case 'number': {
                  const rate = property === 'rate';
                  if (position.x === 0) {
                     (room.score ??= {})[property as 'gain' | 'reverb' | 'filter' | 'rate'] =
                        Math.round(
                           Math.min(
                              Math.max(
                                 (room.score[property as 'gain' | 'reverb' | 'filter' | 'rate'] ??= [
                                    'gain',
                                    'rate'
                                 ].includes(property)
                                    ? 1
                                    : 0) - (keys.interactKey.active() ? 0.01 : 0.2),
                                 0
                              ),
                              rate ? Infinity : 1
                           ) * 100
                        ) / 100;
                     position.x = 1;
                  } else if (position.x === 2) {
                     (room.score ??= {})[property as 'gain' | 'reverb' | 'filter' | 'rate'] =
                        Math.round(
                           Math.min(
                              Math.max(
                                 (room.score[property as 'gain' | 'reverb' | 'filter' | 'rate'] ??= [
                                    'gain',
                                    'rate'
                                 ].includes(property)
                                    ? 1
                                    : 0) + (keys.interactKey.active() ? 0.01 : 0.2),
                                 0
                              ),
                              rate ? Infinity : 1
                           ) * 100
                        ) / 100;
                     position.x = 1;
                  }
                  editor.write();
                  break;
               }
               case 'vector': {
                  const amount = keys.interactKey.active() ? 20 : 1;
                  if (position.x === 0) {
                     (property === 'spawn'
                        ? (room.spawn ??= { x: 0, y: 0 })
                        : (room.region ??= [
                             { x: 0, y: 0 },
                             { x: 0, y: 0 }
                          ])[property === 'region-min' ? 0 : 1]
                     ).x -= amount;
                     position.x = 1;
                  } else if (position.x === 2) {
                     (property === 'spawn'
                        ? (room.spawn ??= { x: 0, y: 0 })
                        : (room.region ??= [
                             { x: 0, y: 0 },
                             { x: 0, y: 0 }
                          ])[property === 'region-min' ? 0 : 1]
                     ).x += amount;
                     position.x = 1;
                  } else if (position.y === 0) {
                     (property === 'spawn'
                        ? (room.spawn ??= { x: 0, y: 0 })
                        : (room.region ??= [
                             { x: 0, y: 0 },
                             { x: 0, y: 0 }
                          ])[property === 'region-min' ? 0 : 1]
                     ).y -= amount;
                     position.y = 1;
                  } else if (position.y === 2) {
                     (property === 'spawn'
                        ? (room.spawn ??= { x: 0, y: 0 })
                        : (room.region ??= [
                             { x: 0, y: 0 },
                             { x: 0, y: 0 }
                          ])[property === 'region-min' ? 0 : 1]
                     ).y += amount;
                     position.y = 1;
                  }
                  if (property === 'spawn') {
                     const spawn = rooms.of(game.room).spawn;
                     spawn.x = room.spawn?.x;
                     spawn.y = room.spawn?.y;
                  } else {
                     rooms.of(game.room).region = [
                        { x: room.region![0].x, y: room.region![0].y },
                        { x: room.region![1].x, y: room.region![1].y }
                     ];
                     renderer.region = [
                        { x: room.region![0].x, y: room.region![0].y },
                        { x: room.region![1].x, y: room.region![1].y }
                     ];
                  }
                  editor.write();
                  break;
               }
            }
         } else {
            const object = editor.target!.object as any;
            const position = atlas.navigators.of('editorValue').position;
            const property = editor.property;
            switch (editor.group) {
               case 'boolean': {
                  (object.metadata.decorator[property] = object[property] = !object[property])
                     ? object.enable()
                     : object.reset();
                  editor.write();
                  break;
               }
               case 'number': {
                  const prop = object[property];
                  const amount = keys.interactKey.active() ? 15 : 1;
                  if (position.x === 0) {
                     if (prop instanceof CosmosValue) {
                        prop.value -= amount;
                     } else {
                        object[property] -= amount;
                     }
                     position.x = 1;
                  } else if (position.x === 2) {
                     if (prop instanceof CosmosValue) {
                        prop.value += amount;
                     } else {
                        object[property] += amount;
                     }
                     position.x = 1;
                  }
                  object.metadata.decorator[property] = prop instanceof CosmosValue ? prop.value : object[property];
                  editor.write();
                  break;
               }
               case 'vector': {
                  const anchor = property === 'anchor';
                  const amount = (keys.interactKey.active() ? (anchor ? 0.05 : 20) : 1) * (anchor ? -1 : 1);
                  if (position.x === 0) {
                     object[property].x -= amount;
                     position.x = 1;
                  } else if (position.x === 2) {
                     object[property].x += amount;
                     position.x = 1;
                  } else if (position.y === 0) {
                     object[property].y -= amount;
                     position.y = 1;
                  } else if (position.y === 2) {
                     object[property].y += amount;
                     position.y = 1;
                  }
                  object.metadata.decorator[property] = object[property].value();
                  editor.write();
                  break;
               }
            }
         }
      })
      .on('to', () => {
         game.input = true;
      })
});

keys.downKey.on('down', () => atlasInput() && atlas.seek('down'));
keys.interactKey.on('down', () => game.input && atlas.next());
keys.leftKey.on('down', key => atlasInput(key) && atlas.seek('left'));
keys.rightKey.on('down', key => atlasInput(key) && atlas.seek('right'));
keys.specialKey.on('down', () => game.input && atlas.prev());
keys.upKey.on('down', () => atlasInput() && atlas.seek('up'));

keys.downKey.on('down', key => {
   if (!game.input && key === 'ArrowDown') {
      const property = editor.property;
      if (editor.group === 'array') {
         if (editor.depth === -1) {
            const array = editor.room[property as 'preload' | 'neighbors'];
            if (array?.length ?? 0 > 0) {
               array?.splice(editor.index, 1);
               editor.index = Math.min(Math.max(editor.index, 0), (array?.length ?? 0) - 1);
            }
         } else {
            const object = editor.target!.object as CosmosObject;
            const meta = object.metadata as any;
            if (meta.decorator[property]?.length ?? 0 > 0) {
               if (property === 'filters') {
                  object.container.filters?.splice(editor.index, 1);
               } else {
                  meta[property].splice(editor.index, 1);
               }
               meta.decorator[property].splice(editor.index, 1);
               editor.index = Math.min(Math.max(editor.index, 0), Math.max(meta.decorator[property].length - 1, 0));
            }
         }
      }
   }
});

keys.freecamKey.on('down', () => {
   if (game.input) {
      renderer.freecam = !renderer.freecam;
      renderer.zoom.value = 1;
      renderer.position.set(renderer.position.clamp(...renderer.region));
   }
});

keys.upKey.on('down', key => {
   if (!game.input) {
      if (key === 'ArrowUp') {
         const property = editor.property;
         if (editor.group === 'array') {
            if (editor.depth === -1) {
               const room = editor.room;
               (room[property as 'preload' | 'neighbors'] ??= []).push('');
            } else {
               const object = editor.target!.object as any;
               if (property === 'filters') {
                  (object.container.filters ??= []).push(new Filter());
               } else if (property === 'frames') {
                  object.frames.push(null);
               } else {
                  object.metadata[property].push('');
               }
               (object.metadata.decorator[property] ??= []).push('');
            }
         }
      }
   } else if (atlas.target === 'editorObject') {
      atlas.switch('editorAdd');
   }
});

addEventListener('keydown', async event => {
   switch (event.code) {
      case 'F4':
      case 'F11':
         if (backend === null) {
            document.fullscreenElement ? document.exitFullscreen() : document.body.requestFullscreen();
         } else {
            event.preventDefault();
         }
         return;
      case 'F12':
         backend?.exec('devtools', true) ?? event.preventDefault();
         return;
      case 'KeyR':
         event.preventDefault();
         if (event.ctrlKey) {
            reload(event.shiftKey);
            return;
         }
         break;
      default:
         event.preventDefault();
         break;
   }
   if (editor.depth === -2) {
      switch (event.key) {
         case 'Backspace':
            editor.destination.length > 0 && (editor.destination = editor.destination.slice(0, -1));
            break;
         default:
            event.key.length === 1 && (editor.destination += event.key);
      }
   } else if (!game.input && editor.editTime <= renderer.time) {
      let value = '';
      const property = editor.property;
      if (editor.depth === -1) {
         const room = editor.room;
         switch (editor.group) {
            case 'array':
               if (room[property as 'preload' | 'neighbors']?.length ?? 0 > 0) {
                  value = room[property as 'preload' | 'neighbors']![editor.index];
               } else {
                  return;
               }
               break;
            case 'string':
               value = room.score?.music ?? '';
               break;
            default:
               return;
         }
      } else {
         if (event.key === 'Enter' || event.key === 'Escape') {
            atlas.prev();
            return;
         }
         const object = editor.target!.object as any;
         switch (editor.group) {
            case 'array':
               if (object.metadata.decorator[property]?.length ?? 0 > 0) {
                  value = object.metadata.decorator[property][editor.index];
               } else {
                  return;
               }
               break;
            case 'string':
               value = object.metadata.decorator[property] || '';
               break;
            default:
               return;
         }
      }
      if (event.key === 'Backspace') {
         value = value.slice(0, -1);
      } else if (event.key.length === 1) {
         value += event.key;
      }
      if (editor.depth === -1) {
         const room = editor.room;
         switch (editor.group) {
            case 'array':
               room[property as 'preload' | 'neighbors']![editor.index] = value;
               editor.write();
               if (property === 'preload') {
                  if (value[0] === '#') {
                     maps.get(value.slice(1))?.load();
                  } else if (value in content) {
                     const resource = content[value as keyof typeof content];
                     resource.load();
                  } else if (value in inventories) {
                     const resource = inventories[value as keyof typeof inventories];
                     resource.load();
                  }
               }
               break;
            case 'string':
               (room.score ??= {}).music = value;
               editor.write();
               break;
         }
      } else {
         const object = editor.target!.object as any;
         switch (editor.group) {
            case 'array':
               if (property === 'filters') {
                  if (value in filters) {
                     object.container.filters[editor.index] = filters[value as keyof typeof filters];
                  }
               } else if (property === 'frames') {
                  if (value in content) {
                     const frame = content[value as keyof typeof content];
                     if (frame instanceof CosmosImage) {
                        frame.load().then(() => {
                           object.frames[editor.index] = frame;
                        });
                     }
                  }
               } else {
                  object.metadata[property][editor.index] = value;
               }
               object.metadata.decorator[property][editor.index] = value;
               editor.write();
               break;
            case 'string':
               if (property === 'resources') {
                  if (value in content) {
                     const resources = content[value as keyof typeof content];
                     if (resources instanceof CosmosAnimationResources) {
                        resources.load().then(() => {
                           object.resources = resources;
                        });
                     }
                  }
               } else {
                  object.metadata.name = value;
               }
               object.metadata.decorator[property] = value;
               editor.write();
               break;
         }
      }
   }
});

addEventListener('resize', () => {
   game.resize();
});

renderer.wrapper!.addEventListener('mousedown', event => {
   if (!dragger.state) {
      dragger.origin.x = renderer.position.x;
      dragger.origin.y = renderer.position.y;
      dragger.state = true;
      dragger.offset.x = event.offsetX;
      dragger.offset.y = event.offsetY;
   }
});

renderer.wrapper!.addEventListener('mousemove', event => {
   if (dragger.state) {
      const zoom = renderer.scale.multiply(renderer.zoom.value);
      const position = new CosmosPoint(
         dragger.origin.x + (dragger.offset.x - event.offsetX) / zoom.x,
         dragger.origin.y + (dragger.offset.y - event.offsetY) / zoom.y
      );
      renderer.position.set(renderer.freecam ? position : position.clamp(...renderer.region));
   }
});

renderer.wrapper!.addEventListener('mouseup', () => {
   dragger.state = false;
});

renderer.wrapper!.addEventListener(
   'wheel',
   event => {
      if (renderer.freecam) {
         if (event.deltaY < 0) {
            renderer.zoom.value *= zoomFactor;
         } else {
            renderer.zoom.value /= zoomFactor;
         }
      }
   },
   { passive: true }
);

(async () => {
   game.resize();
   document.querySelector('#splash')!.remove();
   await Promise.all([ fontLoader, content.ieSplashBackground.load() ]);
   await teleport(editor.destination);
   renderer.attach('base', new CosmosSprite({ frames: [ content.ieSplashBackground ], tint: 0x7f7f7f, scale: 0.5 }));
   editor.generate();
   atlas.switch('editorProperty');
   renderer.attach('menu', editor.wrapper, editor.text);
})();
