import './init';

import * as api from './api';

import * as developer from './developer';

import {
   atlas,
   backend,
   dialog,
   events,
   fullscreen,
   game,
   keys,
   launch,
   param,
   params,
   reload,
   renderer,
   spawn
} from './systems/core';
import { gamepadder, keyState, saver } from './systems/framework';
import { SAVE } from './systems/save';
import { CosmosUtils } from './systems/storyteller';
import text from './systems/text';
import './types.d';

export type OutertaleMod = (mod: string, api: typeof import('./api')) => any;

const inputState = { code: '', hyper: () => keys.altKey.active() || (!SAVE.ready && keys.menuKey.active()) };

async function fileOpen () {
   const content = await dialog.open(text.dialog.dialog_open);
   if (typeof content === 'string') {
      try {
         const entries = SAVE.parse(content);
         const { room } = SAVE.strings(entries.find(entry => entry[0] === SAVE.namespace)?.[1]);
         if (
            (await dialog.message(false, 'confirm', {
               buttons: text.dialog.message_confirm,
               title: text.dialog.dialog_open.title,
               message: `${text.dialog.prompt_open}\n${text.general.name} @ ${saver.locations.of(room || spawn).name}`
            })) === 1
         ) {
            for (const key of SAVE.keys()) {
               key.startsWith(SAVE.safespace) || SAVE.manager.removeItem(key);
            }
            for (const [ key, value ] of entries) {
               key.startsWith(SAVE.safespace) || SAVE.manager.setItem(key, value);
            }
            backend?.writeSave(SAVE.serialize());
            param('namespace');
            reload();
         }
      } catch {
         dialog.message(true, 'alert', {
            buttons: text.dialog.message_alert,
            title: text.dialog.dialog_open.title,
            message: text.dialog.error_load
         });
      }
   }
}

async function fileReset () {
   const { room } = SAVE.strings(SAVE.manager.getItem(SAVE.namespace));
   if (
      (await dialog.message(false, 'confirm', {
         buttons: text.dialog.message_confirm,
         title: text.dialog.dialog_clear_title,
         message: `${text.dialog.prompt_clear}\n${text.general.name} @ ${saver.locations.of(room || spawn).name}`
      })) === 1
   ) {
      for (const key of SAVE.keys()) {
         key.startsWith(SAVE.safespace) || SAVE.manager.removeItem(key);
      }
      backend?.writeSave(SAVE.serialize());
      param('namespace');
      reload();
   }
}

async function fileSave () {
   const { room } = SAVE.strings(SAVE.manager.getItem(SAVE.namespace));
   if (
      (await dialog.message(false, 'confirm', {
         buttons: text.dialog.message_confirm,
         title: text.dialog.dialog_save.title,
         message: `${text.dialog.prompt_save}\n${text.general.name} @ ${saver.locations.of(room || spawn).name}`
      })) === 1
   ) {
      dialog.save(SAVE.serialize(true), text.dialog.dialog_save);
   }
}

function inputHandler (key: string) {
   if (inputState.hyper()) {
      switch ((inputState.code += key)) {
         case 'll':
            developer.panel.activate();
            return false;
         case 'rr':
            reload(true, false);
            return false;
         case 'uu':
            fileOpen();
            return false;
         case 'ud':
            fileReset();
            return false;
         case 'dd':
            fileSave();
            return false;
      }
      return false;
   } else {
      return game.input && game.interact && atlas.target !== null && !game.movement;
   }
}

Object.assign(globalThis, api, developer);

keys.altKey.on('up', () => (inputState.code = ''));
keys.downKey.on('down', () => inputHandler('d') && atlas.seek('down'));
keys.interactKey.on('down', () => inputHandler('a') && atlas.next());
keys.leftKey.on('down', () => inputHandler('l') && atlas.seek('left'));
keys.menuKey.on('up', () => SAVE.ready || (inputState.code = ''));
keys.openKey.on('down', () => inputState.hyper() && (keys.shiftKey.active() ? fileReset() : fileOpen()));
keys.rightKey.on('down', () => inputHandler('r') && atlas.seek('right'));
keys.saveKey.on('down', () => inputState.hyper() && fileSave());
keys.specialKey.on('down', () => inputHandler('b') && atlas.prev());
keys.upKey.on('down', () => inputHandler('u') && atlas.seek('up'));

renderer.on('tick', {
   priority: Number.MIN_SAFE_INTEGER,
   listener () {
      keyState.down = keys.downKey.active();
      keyState.interact = keys.interactKey.active();
      keyState.left = keys.leftKey.active();
      keyState.menu = keys.menuKey.active();
      keyState.right = keys.rightKey.active();
      keyState.special = keys.specialKey.active();
      keyState.up = keys.upKey.active();
   }
});

renderer.on('pre-render', {
   priority: Number.MIN_SAFE_INTEGER,
   listener () {
      this.freecam || this.position.set(game.camera);
   }
});

addEventListener('keydown', async event => {
   switch (event.code) {
      case 'F4':
      case 'F11':
         if (backend === null) {
            fullscreen();
            break;
         }
      default:
         event.preventDefault();
         break;
   }
});

addEventListener('resize', () => {
   game.resize();
});

(async () => {
   await Promise.all(
      [
         ...CosmosUtils.parse<string[]>(
            params.has('modscript')
               ? decodeURIComponent(params.get('modscript') ?? '')
               : SAVE.manager.getItem('MODSCRIPT'),
            []
         ).map((script, index) => [ `modscript:${index}`, `data:text/javascript,${script}` ]),
         ...(backend?.mods() ?? []).map(name => [ `mods:${name}`, `mods:${name}/index.js?${Math.random()}` ])
      ].map(async ([ name, source ]) => {
         try {
            return [ name, await import(/* @vite-ignore */ source) ] as [string, any];
         } catch (error) {
            console.trace(error);
            backend?.exec('devtools', true);
            dialog.message(true, 'alert', {
               buttons: text.dialog.message_alert,
               message: `${text.dialog.error_mod.replace('$(x)', name)}\n${error}`,
               title: text.dialog.dialog_error_title
            });
            return [ name, null ] as [string, null];
         }
      })
   ).then(entries =>
      Promise.all(
         entries.map(async ([ name, value ]) => {
            if (value) {
               try {
                  await value.default?.(name, api);
                  await Promise.all(events.fire('loaded-mod', name));
               } catch (error) {
                  console.trace(error);
                  backend?.exec('devtools', true);
                  dialog.message(true, 'alert', {
                     buttons: text.dialog.message_alert,
                     message: `${text.dialog.error_mod.replace('$(x)', name)}\n${error}`,
                     title: text.dialog.dialog_error_title
                  });
               }
            }
         })
      )
   );
   await Promise.all(events.fire('modded'));
   document.querySelector('#splash')?.remove();
   game.resize();
   const gp = [ ...navigator.getGamepads() ].find(value => value instanceof Gamepad);
   gp !== void 0 && gp !== null && gamepadder.connect(gp);
   param('namespace', SAVE.namespace);
   SAVE.state = CosmosUtils.parse(SAVE.manager.getItem(SAVE.namespace), {});
   SAVE.ready = true;
   await Promise.all(events.fire('loaded'));
   launch.intro && (await Promise.all(events.fire('init-intro')));
   await Promise.all(events.fire('init-between'));
   launch.overworld && (await Promise.all(events.fire('init-overworld')));
   game.input = true;
   game.active = true;
   game.timer = true;
   await Promise.all(events.fire('ready'));
   CosmosUtils.status(`OUTERTALE INITIALIZED (${Math.floor(performance.now()) / 1000})`, { color: '#fff' });
})();

CosmosUtils.status(`LOAD MODULE: INDEX (${Math.floor(performance.now()) / 1000})`, { color: '#07f' });
