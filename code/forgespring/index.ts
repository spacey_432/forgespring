import './bootstrap';

import { content, inventories, music, sounds } from '../systems/assets';
import { atlas, events, fontLoader, game, init, items, launch, renderer, rng, spawn } from '../systems/core';
import {
   dialogue,
   dialogue_primitive,
   fastAssets,
   frontEnder,
   hashes,
   heal,
   instance,
   player,
   quickresume,
   saver,
   shopper,
   talkFinder,
   teleport,
   teleporter,
   temporary,
   tracker,
   trivia,
   ultraPosition,
   updateArmor,
   world
} from '../systems/framework';
import { SAVE } from '../systems/save';
import {
   CosmosAnimation,
   CosmosAsset,
   CosmosCharacter,
   CosmosDirection,
   CosmosHitbox,
   CosmosKeyed,
   CosmosMath,
   CosmosObject,
   CosmosSprite,
   CosmosUtils
} from '../systems/storyteller';
import text from './text';
import sources from './sources';
import { OutertaleShop } from '../systems/outertale';

export type ForgespringRoomKey = keyof typeof sources;

export type ForgespringDefinedRS = {
   f_hub: {};
};

export type AerialisRS = {
   [k in ForgespringRoomKey]: k extends keyof ForgespringDefinedRS ? Partial<ForgespringDefinedRS[k]> : {};
};

export const characters = (() => {
   const napstablook = (() => {
      return {
         down: new CosmosSprite({
            anchor: { x: 0, y: 1 },
            frames: [ content.iocNapstablookDown ],
            metadata: { time: 0 }
         }).on('tick', function () {
            this.position.y = CosmosMath.wave(((this.metadata.time += CosmosMath.FRAME) % 4000) / 4000) * -2;
         }),
         left: new CosmosSprite({
            anchor: { x: 0, y: 1 },
            frames: [ content.iocNapstablookLeft ],
            metadata: { time: 0 }
         }).on('tick', function () {
            this.position.y = CosmosMath.wave(((this.metadata.time += CosmosMath.FRAME) % 4000) / 4000) * -2;
         }),
         right: new CosmosSprite({
            anchor: { x: 0, y: 1 },
            frames: [ content.iocNapstablookRight ],
            metadata: { time: 0 }
         }).on('tick', function () {
            this.position.y = CosmosMath.wave(((this.metadata.time += CosmosMath.FRAME) % 4000) / 4000) * -2;
         }),
         up: new CosmosSprite({ anchor: { x: 0, y: 1 }, frames: [ content.iocNapstablookUp ], metadata: { time: 0 } }).on(
            'tick',
            function () {
               this.position.y = CosmosMath.wave(((this.metadata.time += CosmosMath.FRAME) % 4000) / 4000) * -2;
            }
         )
      };
   })();

   const mettaton = {
      down: new CosmosAnimation({ anchor: { x: 0, y: 1 }, resources: content.iocMettatonDown }),
      left: new CosmosAnimation({ anchor: { x: 0, y: 1 }, resources: content.iocMettatonLeft }),
      right: new CosmosAnimation({ anchor: { x: 0, y: 1 }, resources: content.iocMettatonRight }),
      up: new CosmosAnimation({ anchor: { x: 0, y: 1 }, resources: content.iocMettatonUp })
   };

   const nobody = {
      down: new CosmosSprite(),
      left: new CosmosSprite(),
      right: new CosmosSprite(),
      up: new CosmosSprite()
   };

   return {
      mettaton: {
         walk: mettaton,
         talk: mettaton
      },
      napstablook: {
         walk: napstablook,
         talk: napstablook
      },
      none: {
         walk: nobody,
         talk: nobody
      }
   };
})();

export const napstar = new CosmosCharacter({
   preset: characters.napstablook,
   key: 'napstablook',
   metadata: { override: false, static: false, move: 0 }
}).on('tick', function () {
   if (this.metadata.override) {
      this.metadata.move = 0;
      return;
   }
   if (player.metadata.moved) {
      this.metadata.move < 4 && this.metadata.move++;
   } else {
      this.metadata.move > 0 && this.metadata.move--;
   }
   const animate = !this.talk && !this.metadata.static;
   const [ face, to ] = tracker.interpolate(7 + this.metadata.move);
   if (this.position.x !== to.x || this.position.y !== to.y) {
      this.position.set(to);
      this.face = face;
      if (animate) {
         for (const sprite of Object.values(this.sprites)) {
            sprite.duration = 5;
         }
         this.sprite.enable();
      }
   } else if (animate) {
      this.sprite.reset();
   }
});

export const hapstar = new CosmosCharacter({
   preset: characters.mettaton,
   key: 'mettaton',
   metadata: { override: false, static: false, move: 0 }
}).on('tick', function () {
   if (this.metadata.override) {
      this.metadata.move = 0;
      return;
   }
   if (player.metadata.moved) {
      this.metadata.move < 8 && this.metadata.move++;
   } else {
      this.metadata.move > 0 && this.metadata.move--;
   }
   const animate = !this.talk && !this.metadata.static;
   const [ face, to ] = tracker.interpolate(14 + this.metadata.move);
   if (this.position.x !== to.x || this.position.y !== to.y) {
      this.position.set(to);
      this.face = face;
      if (animate) {
         for (const sprite of Object.values(this.sprites)) {
            sprite.duration = 5;
         }
         this.sprite.enable();
      }
   } else if (animate) {
      this.sprite.reset();
   }
});

export const frisk = {
   down: new CosmosAnimation({ anchor: { x: 0, y: 1 }, resources: content.iocFriskDown }),
   left: new CosmosAnimation({ anchor: { x: 0, y: 1 }, resources: content.iocFriskLeft }),
   right: new CosmosAnimation({ anchor: { x: 0, y: 1 }, resources: content.iocFriskRight }),
   up: new CosmosAnimation({ anchor: { x: 0, y: 1 }, resources: content.iocFriskUp })
};

export const lazyLoader = events.on('modded').then(async () => {
   await (launch.overworld && inventories.lazyAssets.load());
});

export const menuLoader = events.on('loaded').then(async () => {
   await (launch.intro && Promise.all([ inventories.menuAssets.load(), frontEnder.menuMusicResource.asset.load() ]));
});

export const queue = {
   assets: [] as CosmosAsset[],
   load () {
      return Promise.all([
         lazyLoader,
         ...fastAssets().map(asset => asset.load()),
         ...queue.assets.map(asset => asset.load())
      ]);
   }
};

export const queueLoader = events.on('loaded').then(async () => {
   await (launch.overworld && queue.load());
});

export async function endCall (t: string) {
   await dialogue(t, text.c_call_common.end);
}

export function genCB () {
   const cornerbarrier = instance('below', 'cornerbarrier');
   if (cornerbarrier) {
      const truebarrier = temporary(new CosmosObject({ position: cornerbarrier.object.position }), 'below');
      for (const object of cornerbarrier.object.objects as CosmosHitbox[]) {
         const px = object.position.add(object.size.x, 0);
         const py = object.position.add(0, object.size.y);
         const md = px.add(py).divide(2);
         const ps = truebarrier.position.add(object);
         truebarrier.attach(
            new CosmosHitbox({
               anchor: { x: 0 },
               rotation: md.angleFrom(object) - 90,
               position: object,
               size: { x: px.extentOf(py), y: md.extentOf(object) }
            }).on('tick', function () {
               let barrier = true;
               if (object.size.x > 0) {
                  player.position.x < ps.x && (barrier = false);
               } else {
                  player.position.x > ps.x && (barrier = false);
               }
               if (object.size.y > 0) {
                  player.position.y < ps.y && (barrier = false);
               } else {
                  player.position.y > ps.y && (barrier = false);
               }
               this.metadata.barrier = barrier;
            })
         );
      }
   }
}

export const forgespringStates = {
   rooms: {} as Partial<CosmosKeyed<CosmosKeyed<any>>>,
   scripts: {} as Partial<CosmosKeyed<CosmosKeyed<any>>>
};

export const areaF = {
   tick () {},
   scripts: {
      async npc (roomState, scriptState, key) {
         if (!game.movement) {
            return;
         }
         await instance('main', key)?.talk(
            'a',
            talkFinder(),
            'auto',
            ...CosmosUtils.provide(text.a_forgespring.npc[key as keyof typeof text.a_forgespring.npc])
         );
      }
   } as Partial<CosmosKeyed<(roomState: any, scriptState: any, ...args: string[]) => any>>,
   tickers: {} as { [k in ForgespringRoomKey]?: (roomState: AerialisRS[k], ...args: string[]) => any },
   teleports: {} as { [k in ForgespringRoomKey]?: (roomState: AerialisRS[k], from: string) => any }
};

export const forgespringScript = async (subscript: string, ...args: string[]): Promise<any> => {
   const roomState = (forgespringStates.rooms[game.room] ??= {});
   if (subscript === 'tick') {
      areaF.tickers[game.room as ForgespringRoomKey]?.(roomState);
   } else {
      areaF.scripts[subscript]?.(roomState, (forgespringStates.scripts[subscript] ??= {}), ...args);
   }
};

export async function quickCall (end: boolean, ...lines: string[]) {
   atlas.switch('dialoguerBottom');
   await dialogue_primitive(text.c_call_common.start, ...lines);
   if (end) {
      await dialogue_primitive(text.c_call_common.end);
   }
   atlas.switch(null);
   atlas.detach(renderer, 'menu', 'sidebar');
   game.movement = true;
}

// intro init script
events.on('init-intro', async () => {
   await Promise.all([ fontLoader, menuLoader ]);
   init();
   frontEnder.index = 1;
   if (frontEnder.nostory) {
      atlas.switch('frontEndLoad');
   } else {
      atlas.switch('frontEnd');
      await Promise.race([
         atlas.navigators.of('frontEndLoad').on('from'),
         atlas.navigators.of('frontEndStart').on('from')
      ]);
   }
   frontEnder.impactNoise?.stop();
   frontEnder.menuMusic = frontEnder.menuMusicResource.daemon.instance(renderer);
   await events.on('spawn');
   game.input = false;
   renderer.alpha.value = 0;
   atlas.switch(null);
   inventories.menuAssets.unload();
   content.amMenu.unload();
});

events.on('init-between', async () => {
   await Promise.all([ fontLoader, queueLoader ]);
   init();
   updateArmor(SAVE.data.s.armor || 'spacesuit');
   SAVE.data.n.hp ||= 20;
   SAVE.data.s.weapon ||= 'spanner';
   rng.dialogue.value = SAVE.data.n.base_dialogue || hashes.of(Math.random().toString());
   rng.overworld.value = SAVE.data.n.base_overworld || hashes.of(rng.dialogue.value.toString());
   game.room = SAVE.data.s.room || spawn;
   game.camera = player;
});

events.on('init-overworld', async () => {
   renderer.alpha.value = 0;
   const entities = [ player, napstar, hapstar ];
   for (const entity of entities) {
      entity.fire('tick');
   }
   renderer.attach('main', ...entities);
   const ultra = ultraPosition(game.room);
   teleporter.forcemove = true;
   await teleport(game.room, ultra.face, ultra.position.x, ultra.position.y, { fast: true });
   renderer.alpha.modulate(renderer, 300, 1);
   world.cutscene() || quickresume(true);
});

events.on('script', async (name, ...args) => {
   if (!game.movement) {
      return;
   }
   switch (name) {
      case 'save':
         game.movement = false;
         heal();
         const lines = CosmosUtils.provide(saver.locations.of(game.room).text);
         if (lines.length > 0) {
            atlas.switch('dialoguerBottom');
            await dialogue_primitive(...lines);
         }
         atlas.switch('save');
         break;
      case 'teleport':
         await teleport(args[0] as string, args[1] as CosmosDirection, +args[2], +args[3], world);
         break;
      case 'trivia':
         if (game.movement && game.room[0] === 'f') {
            trivia(
               ...CosmosUtils.provide(text.a_forgespring.trivia[args[0] as keyof typeof text.a_forgespring.trivia])
            );
         }
         break;
      case 'shop':
         switch (args[0]) {
            case 'masa':
               shopper.open(shops.masa, args[1] as CosmosDirection, +args[2], +args[3]);
               break;
         }
         break;
      case 'forgespring':
         forgespringScript(args[0], ...args.slice(1));
         break;
   }
});

events.on('teleport', (from, to) => {
   genCB();
   const roomState = (forgespringStates.rooms[to] ??= {});
   areaF.teleports[to as ForgespringRoomKey]?.(roomState, from);
});

events.on('tick', () => {
   game.movement && game.room[0] === 'f' && forgespringScript('tick');
});

player.on('tick', function () {
   if (!this.metadata.cache.init) {
      this.metadata.cache.init = true;
      this.sprites = frisk;
   }
});

const shops = {
   masa: new OutertaleShop({
      background: new CosmosSprite({ frames: [ content.ismBackground ] }),
      async handler () {
         if (atlas.target === 'shop') {
            if (shopper.index === 1) {
               if (shops.masa.vars.sell) {
                  await shopper.text(...text.n_shop_masa.sell2);
               } else {
                  shops.masa.vars.sell = true;
                  await shopper.text(...text.n_shop_masa.sell1);
               }
            } else if (shopper.index === 3) {
               atlas.switch('shopText');
               await dialogue_primitive(...text.n_shop_masa.exit);
               const mus = shops.masa.music?.instances.slice(-1)[0];
               await Promise.all([
                  renderer.alpha.modulate(renderer, 300, 0),
                  mus?.gain.modulate(renderer, 300, 0).then(() => {
                     mus.stop();
                  })
               ]);
               atlas.switch(null);
               renderer.alpha.modulate(renderer, 300, 1);
            } else {
               atlas.switch('shopList');
            }
         } else if (atlas.target === 'shopList') {
            if (shopper.listIndex === 4) {
               atlas.switch('shop');
            } else if (shopper.index === 0) {
               atlas.switch('shopPurchase');
            } else {
               await shopper.text(...text.n_shop_masa.talkText[shopper.listIndex]());
            }
         }
      },
      keeper: new CosmosAnimation({
         anchor: { x: 0, y: 1 },
         position: { x: 160, y: 120 },
         resources: content.ismKeeper
      }),
      music: music.forgespring,
      options () {
         if (atlas.target === 'shop') {
            return text.n_shop_masa.menu;
         } else if (shopper.index === 0) {
            return text.n_shop_masa.item;
         } else {
            return text.n_shop_masa.talk();
         }
      },
      preset (index) {
         shops.masa.keeper.index = index;
      },
      price () {
         return [ 10000, 77777, 90000, 99999 ][shopper.listIndex];
      },
      prompt () {
         return text.n_shop_masa.itemPurchasePrompt;
      },
      purchase (buy) {
         let success = false;
         if (buy) {
            if (SAVE.storage.inventory.size < 8) {
               const price = CosmosUtils.provide(shops.masa.price);
               if (SAVE.data.n.g < price) {
                  shops.masa.vars.purchase = 3;
               } else {
                  shops.masa.vars.purchase = 1;
                  SAVE.data.n.g -= price;
                  success = true;
               }
            } else {
               shops.masa.vars.purchase = 4;
            }
         } else {
            shops.masa.vars.purchase = 2;
         }
         if (success) {
            sounds.purchase.instance(renderer);
         }
      },
      size () {
         if (atlas.target === 'shop') {
            return 4;
         } else {
            return 5;
         }
      },
      status () {
         if (shops.masa.vars.purchase || 0 > 0) {
            const purchaseValue = shops.masa.vars.purchase as number;
            shops.masa.vars.purchase = 0;
            return text.n_shop_masa.itemPurchase[purchaseValue - 1];
         } else if (atlas.target === 'shop') {
            if (shops.masa.vars.idle) {
               return text.n_shop_masa.menuPrompt2;
            } else {
               shops.masa.vars.idle = true;
               return text.n_shop_masa.menuPrompt1;
            }
         } else if (shopper.index === 0) {
            return text.n_shop_masa.itemPrompt;
         } else {
            return text.n_shop_masa.talkPrompt;
         }
      },
      tooltip () {
         if ([ 'shopList', 'shopPurchase' ].includes(atlas.target!) && shopper.index === 0) {
            if (shopper.listIndex === 4) {
               return null;
            } else {
               const info = items.of([ 'masa1', 'masa2', 'masa3', 'masa4' ][shopper.listIndex]);
               const calc =
                  info.value -
                  (info.type === 'consumable' || info.type === 'special' ? 0 : items.of(SAVE.data.s[info.type]).value);
               return text.n_shop_masa.itemInfo[shopper.listIndex].replace('$(x)', `${calc < 0 ? '' : '+'}${calc}`);
            }
         } else {
            return null;
         }
      },
      vars: {}
   })
};

export default shops;

CosmosUtils.status(`LOAD MODULE: FORGESPRING AREA (${Math.floor(performance.now()) / 1000})`, { color: '#07f' });
