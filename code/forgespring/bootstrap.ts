import { content, context, convolver, effectSetup, soundOpts, soundRouter } from '../systems/assets';
import { atlas, maps, speech } from '../systems/core';
import { easyRoom, keyring, phone, portraits, saver } from '../systems/framework';
import { OutertaleSpeechPreset } from '../systems/outertale';
import { CosmosDaemon, CosmosEffect, CosmosUtils } from '../systems/storyteller';
import sources, { forgespringMap } from './sources';
import text from './text';

export const faces = {};

phone.register('keyring', {
   display () {
      return [ ...keyring.values() ].filter(value => value.display()).length > 0;
   },
   trigger () {
      atlas.switch('sidebarCellKey');
   },
   priority: -1000,
   name: text.c_name_common.keyring
});

portraits.register(faces);

speech.presets.register({
   chara: new OutertaleSpeechPreset({
      faces: [ null ],
      interval: 2,
      voices: [ [ new CosmosDaemon(content.avChara, soundOpts(1)) ] ]
   }),
   event: new OutertaleSpeechPreset({ faces: [ null ], interval: 2, voices: [ null ] }),
   frisk: new OutertaleSpeechPreset({
      faces: [ null ],
      interval: 2,
      voices: [ [ new CosmosDaemon(content.avFrisk, soundOpts(0.2)) ] ]
   }),
   basic: new OutertaleSpeechPreset({
      faces: [ null ],
      interval: 2,
      voices: [ [ new CosmosDaemon(content.avNarrator, soundOpts(1)) ] ]
   }),
   mettaton: new OutertaleSpeechPreset({
      chunksize: 2,
      interval: 2,
      voices: [
         [
            new CosmosDaemon(content.avMettaton1, soundOpts()),
            new CosmosDaemon(content.avMettaton2, soundOpts()),
            new CosmosDaemon(content.avMettaton3, soundOpts()),
            new CosmosDaemon(content.avMettaton4, soundOpts()),
            new CosmosDaemon(content.avMettaton5, soundOpts()),
            new CosmosDaemon(content.avMettaton6, soundOpts()),
            new CosmosDaemon(content.avMettaton7, soundOpts()),
            new CosmosDaemon(content.avMettaton8, soundOpts()),
            new CosmosDaemon(content.avMettaton9, soundOpts())
         ]
      ],
      threshold: 0.8
   }),
   napstablook: new OutertaleSpeechPreset({
      faces: [ null ],
      interval: 2,
      voices: [
         [
            new CosmosDaemon(content.avNapstablook, {
               context,
               gain: 0.6,
               router: effectSetup(new CosmosEffect(context, convolver, 0.6), soundRouter)
            })
         ]
      ]
   }),
   twinkly: new OutertaleSpeechPreset({
      faces: [ null ],
      interval: 2,
      voices: [
         [
            new CosmosDaemon(content.avTwinkly1, {
               context,
               gain: 0.6,
               router: soundRouter
            })
         ],
         [
            new CosmosDaemon(content.avTwinkly2, {
               context,
               gain: 0.6,
               router: soundRouter
            })
         ]
      ]
   })
});

maps.register('forgespring', forgespringMap);

saver.locations.register(text.s_save_forgespring);

for (const [ key, value ] of Object.entries(sources)) {
   easyRoom(key, forgespringMap, value);
}

CosmosUtils.status(`LOAD MODULE: FORGESPRING BOOTSTRAP (${Math.floor(performance.now()) / 1000})`, { color: '#07f' });
