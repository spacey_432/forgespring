import imForgespring$info from '../../assets/images/maps/forgespring.json?url';
import imForgespring from '../../assets/images/maps/forgespring.png?url';
import f_cutscene from '../../assets/rooms/f_cutscene.json';
import f_dark from '../../assets/rooms/f_dark.json';
import f_gazebo from '../../assets/rooms/f_gazebo.json';
import f_graveyard from '../../assets/rooms/f_graveyard.json';
import f_hub from '../../assets/rooms/f_hub.json';
import f_shop from '../../assets/rooms/f_shop.json';

import { OutertaleMap } from '../systems/outertale';
import { CosmosImage, CosmosUtils } from '../systems/storyteller';

export const forgespringMap = new OutertaleMap(imForgespring$info, new CosmosImage(imForgespring));
forgespringMap.name = 'maps::forgespring';

const sources = {
   f_cutscene,
   f_dark,
   f_gazebo,
   f_graveyard,
   f_hub,
   f_shop
};

export default sources;

CosmosUtils.status(`LOAD MODULE: FORGESPRING SOURCES (${Math.floor(performance.now()) / 1000})`, { color: '#07f' });
