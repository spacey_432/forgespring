import { pager } from '../systems/framework';
import { CosmosUtils } from '../systems/storyteller';
import { SAVE } from '../systems/save';

// START-TRANSLATE
const text = {
   a_forgespring: {
      npc: {
         f_beanboi: pager.create(0, [
            "<32>{#npc/a}{#p/basic}* I hope you're happy.\n* Thanks to your show, the whole town's turned upside down...",
            '<25>{#npc}{#p/mettaton}* WELL, DARLING, IT WAS URGENT INFORMATION!',
            "<25>* AND IF I DIDN'T REPORT THE HOTTEST FACTS, I'D BE AN AWFUL ANCHORBOT!",
            "<32>{#npc/a}{#p/basic}* ... I just hope it quiets down.\n* I'd LIKE to continue reading...",
            "<32>{#npc}{#p/frisk}* So this whole human mess doesn't bother you?",
            "<32>{#npc/a}{#p/basic}* I'll leave that to the adults.\n* After all, this town's full of guards."
         ])
      },
      trivia: {
         f_statue: pager.create(0, [
            "<32>{#p/basic}* (It's a statue.)\n* (There's something engraved at its base.)",
            '<32>* ("We built this town to honor the three brave heroes who guided us out of despair.")',
            '<32>* ("We shall not allow our loss to hinder our progression or crush our spirits.")',
            '<99>* ("We will build an even greater\n  Waterfall, for those lost and\n  for future generations!")',
            "<25>{#p/napstablook}* it's funny...\n* one of those heroes lives nearby...",
            "<25>* and whenever he visits, he's treated like a huge celebrity.",
            '<25>{#p/mettaton}* WELL, TO BE FAIR, HE -WAS- A HERO IN THE HUMAN AND MONSTER WAR.',
            '<25>* THOUGH I SUPPOSE TOO MUCH LIMELIGHT COULD BE A -LITTLE- EXHAUSTING.',
            '<25>* NO WONDER HE LIVES ON THE OUTSKIRTS OF TOWN!',
            "<25>{#p/frisk}* ... So, who's the hooded dude?",
            '<25>{#p/mettaton}* ... OH.',
            '<25>{#p/napstablook}* ... um, well, most people just call them "the mage."',
            '<25>{#p/mettaton}* IN LIGHT OF RECENT EVENTS, MOST REFUSE TO CONFRONT THE, ERM...',
            '<25>* "ELEPHANT" IN THE ROOM.',
            '<25>{#p/frisk}* Meaning?',
            "<25>{#p/mettaton}* SIMPLY PUT, YOU'RE NOT THE ONLY ONE USING A HOOD TO HIDE.",
            "<32>{#p/basic}* (Upon closer inspection, the mage's face appears weathered by time.)",
            "<32>* (It's strangely featureless.)"
         ])
      }
   },

   c_name_common: {
      keyring: 'Keyring'
   },

   c_call_common: {
      start: '<32>{#s/phone}{#p/event}* Dialing...',
      end: '<32>{#s/equip}{#p/event}* Click...',
      nobody1: [ '<32>{#p/basic}* (No response.)' ]
   },
   
   n_shop_masa: {
      menu: [ 'Buy', 'Sell', 'Talk', 'Exit' ],
      menuPrompt1:
         "<23>{#p/basic}{#k/3}* HAAAHAHAHA!\n* Welcome to my shop!\n* I am Masa, you have money, so let's do business!",
      menuPrompt2: '<23>{#p/basic}{#k/0}* Take your time.',
      item: [
         '10000G - Pricklythorn Plate',
         '77777G - Duster Dirk',
         '90000G - Hal of the Pebble',
         '99999G - End of Tales',
         'Exit'
      ],
      itemPrompt: '<09>{#p/basic}{#k/0}Want to do some damage, or a lot?',
      itemInfo: [
         'Armor: 90DF\n($(x) DF)\nStings to\nthe touch.',
         'Weapon: 77AT\n($(x) AT)\nFit for a\nsoldier.',
         'Armor: 99DF\n($(x) DF)\nBecome\nunstoppable.',
         'Weapon: 999AT\n($(x) AT)\nA fitting\ncloser.'
      ],
      itemPurchase: [
         '<09>{#p/basic}{#k/0}bepis',
         '<09>{#p/basic}{#k/0}Want to do some damage, or a lot?',
         '<09>{#p/basic}{#k/0}Not enough cash, kiddo!',
         "<09>{#p/human}(You're carrying too much.)"
      ],
      itemPurchasePrompt: 'Buy it for\n$(x)G?',
      sell1: [ '<30>{#p/basic}{#k/0}* sell text 1' ],
      sell2: [ '<30>{#p/basic}{#k/0}* sell text 2' ],
      talk: () => [
         'Forgespring',
         'Fort Aquarius',
         'Waterfall Incident',
         [
            'About Undyne',
            '\xa7fill:#ff0\xa7About Undyne (NEW)',
            '\xa7fill:#ff0\xa7About Mettaton (NEW)',
            'About Mettaton'
         ][Math.min(SAVE.data.n.shop_masa_undyne, 3)],
         'Exit'
      ],
      talkPrompt: "<09>{#p/basic}{#k/1}Sure! I'm up for a friendly chat!",
      talkText: [
         () => [
            "<32>{#p/basic}{#k/0}* Ain't that much to tell about 'er.\n* Just a neat little town that sprung up back in the day.",
            '<32>{#k/0}* We needed a place to rest and organize while building Ft. Aquarius, so we set an encampment nearby.',
            "<32>{#k/1}* And y'know, with people comes business.\n* Plenty of people came around to sell to the workers and soldiers and stuff.",
            "<33>{#k/0}* Those people stuck around, cuz, y'know, maintaining a huge\n  fort is a lot easier with an established supply line and plenty of able-bodied monsters.",
            '<32>{#k/6}* I should thank my grand dad for sticking around.\n* Business is the BEST here!'
         ],
         () => [
            "<32>{#p/basic}{#k/1}* It's one of the most fortified, heavily defended, and advanced bases of the Underground!",
            "<32>{#k/6}* They're also the reason I can afford to put my kids through college.\n* So, y'know, can't complain!",
            '<32>{#k/0}* At first, people were anxious about it.',
            "<32>{#k/1}* But everybody feels safer knowing there's a box full of angry people with swords ready to fight for you nearby!",
            '<32>{#k/5}* And good thing, too.\n* From what I hear, everybody was apprehensive after the Waterfall Incident.'
         ],
         () => [
            '<32>{#p/basic}{#k/5}* Ah, the Waterfall Incident...',
            "<32>{#k/2}* Man, I wasn't there, but from my grand dad's stories, I think that's for the best.",
            '<32>{#k/5}* One day, a human fell down, and, well... did what humans do best, I suppose.',
            '<32>{#k/2}* An entire platoon of soldiers, grinded to dust by a single human... truly scary stuff.',
            "<32>{#k/5}* If it wasn't for the queen and the old timer, we might've lost an entire friggin' army!",
            "<32>{#k/5}* Was pretty costly, though.\n* I've seen the old timer shamble about...",
            '<32>{#k/4}* Looked like he got an early retirement to the knee thanks to that little devil...'
         ],
         () => [
            [
               "<32>{#p/basic}{#k/1}* Ah, Undyne!\n* Now she's an inspiration to us all.",
               "<32>{#k/6}* Why, she's just half my age, yet has made it all the way to head scientist!",
               "<32>{#k/1}* And I'm not surprised.\n* She's so passionate and FIERY about her work!\n* If anyone can get us outta this ditch, it's her!",
               "<32>{#k/6}* Heck, even if she can't, she sure makes ME believe it!"
            ],
            [
               '<32>{#p/basic}{#k/1}* Ha!\n* Would you believe that people think Undyne and I are related?',
               "<32>{#k/3}* I wish!\n* Maybe then I'd be able to shed those couple extra pounds, huh?\n* HAAAA HAHAHA!"
            ],
            [
               "<32>{#p/basic}{#k/4}* Ugh, that quadrangular wastrel?\n* I don't get why people water at the mouth for him!\n* All he does is shamble about and screech at a microphone!",
               '<32>{#k/2}* Big deal!\n* Gimme a couple of pints and a mic night and I can do the same just fine!',
               '<32>{#k/5}* But... he does seem to keep the morale up, and he does bring in more business, so... all right, I guess.',
               "<32>{#k/4}* Really wish he'd put those robo-servos to soldering a girder or something else more productive.",
               '<32>{#p/mettaton}* YOU DO KNOW I CAN HEAR YOU, RIGHT?',
               '<32>{#p/basic}{#k/4}* Good!'
            ],
            [
               "<32>{#p/basic}{#k/4}* Ugh, that quadrangular wastrel?\n* I don't get why people water at the mouth for him!\n* All he does is shamble about and screech at a microphone!",
               '<32>{#k/2}* Big deal!\n* Gimme a couple of pints and a mic night and I can do the same just fine!',
               '<32>{#k/5}* But... he does seem to keep the morale up, and he does bring in more business, so... all right, I guess.',
               "<32>{#k/4}* Really wish he'd put those robo-servos to soldering a girder or something else more productive."
            ]
         ][Math.min(SAVE.data.n.shop_masa_undyne++, 3)]
      ],
      exit: [ '<32>{#p/basic}{#k/0}* leave text' ]
   },

   s_save_forgespring: {
      f_hub: {
         name: 'Forgespring Hub',
         text: [
            '<32>{#p/basic}* (The laid-back atmosphere of this quiet water town...)',
            '<32>* (It fills you with determination.)'
         ]
      }
   }
};
// END-TRANSLATE

export default text;

CosmosUtils.status(`LOAD MODULE: FORGESPRING TEXT (${Math.floor(performance.now()) / 1000})`, { color: '#07f' });
