import amForgespring from '../../assets/audio/music/forgespring.mp3?url';
import amMenu from '../../assets/audio/music/menu.mp3?url';
import amStory from '../../assets/audio/music/story.mp3?url';
import asAppear from '../../assets/audio/sounds/appear.mp3?url';
import asApplause from '../../assets/audio/sounds/applause.mp3?url';
import asArrow from '../../assets/audio/sounds/arrow.mp3?url';
import asAsrielSparkle from '../../assets/audio/sounds/asriel_sparkle.mp3?url';
import asBahbye from '../../assets/audio/sounds/bahbye.mp3?url';
import asBark from '../../assets/audio/sounds/bark.mp3?url';
import asBell from '../../assets/audio/sounds/bell.mp3?url';
import asBoing from '../../assets/audio/sounds/boing.mp3?url';
import asBomb from '../../assets/audio/sounds/bomb.mp3?url';
import asBombfall from '../../assets/audio/sounds/bombfall.mp3?url';
import asBookspin from '../../assets/audio/sounds/bookspin.mp3?url';
import asBoom from '../../assets/audio/sounds/boom.mp3?url';
import asBreak from '../../assets/audio/sounds/break.mp3?url';
import asBuhbuhbuhdaadodaa from '../../assets/audio/sounds/buhbuhbuhdaadodaa.mp3?url';
import asBurst from '../../assets/audio/sounds/burst.mp3?url';
import asCackle from '../../assets/audio/sounds/cackle.mp3?url';
import asCast from '../../assets/audio/sounds/cast.mp3?url';
import asClap from '../../assets/audio/sounds/clap.mp3?url';
import asComputer from '../../assets/audio/sounds/computer.mp3?url';
import asCrit from '../../assets/audio/sounds/crit.mp3?url';
import asCymbal from '../../assets/audio/sounds/cymbal.mp3?url';
import asDeeploop2 from '../../assets/audio/sounds/deeploop2.mp3?url';
import asDepower from '../../assets/audio/sounds/depower.mp3?url';
import asDestroyed from '../../assets/audio/sounds/destroyed.mp3?url';
import asDimbox from '../../assets/audio/sounds/dimbox.mp3?url';
import asDog from '../../assets/audio/sounds/dog.mp3?url';
import asDogsword from '../../assets/audio/sounds/dogsword.mp3?url';
import asDoor from '../../assets/audio/sounds/door.mp3?url';
import asDoorClose from '../../assets/audio/sounds/doorClose.mp3?url';
import asDrumroll from '../../assets/audio/sounds/drumroll.mp3?url';
import asDununnn from '../../assets/audio/sounds/dununnn.mp3?url';
import asElectrodoor from '../../assets/audio/sounds/electrodoor.mp3?url';
import asElevator from '../../assets/audio/sounds/elevator.mp3?url';
import asEquip from '../../assets/audio/sounds/equip.mp3?url';
import asFear from '../../assets/audio/sounds/fear.mp3?url';
import asFrypan from '../../assets/audio/sounds/frypan.mp3?url';
import asGlassbreak from '../../assets/audio/sounds/glassbreak.mp3?url';
import asGonerCharge from '../../assets/audio/sounds/goner_charge.mp3?url';
import asGoodbye from '../../assets/audio/sounds/goodbye.mp3?url';
import asGrab from '../../assets/audio/sounds/grab.mp3?url';
import asGunshot from '../../assets/audio/sounds/gunshot.mp3?url';
import asHeal from '../../assets/audio/sounds/heal.mp3?url';
import asHero from '../../assets/audio/sounds/hero.mp3?url';
import asHit from '../../assets/audio/sounds/hit.mp3?url';
import asHurt from '../../assets/audio/sounds/hurt.mp3?url';
import asImpact from '../../assets/audio/sounds/impact.mp3?url';
import asIndicator from '../../assets/audio/sounds/indicator.mp3?url';
import asJetpack from '../../assets/audio/sounds/jetpack.mp3?url';
import asKnock from '../../assets/audio/sounds/knock.mp3?url';
import asLanding from '../../assets/audio/sounds/landing.mp3?url';
import asMenu from '../../assets/audio/sounds/menu.mp3?url';
import asMeow from '../../assets/audio/sounds/meow.mp3?url';
import asMetapproach from '../../assets/audio/sounds/metapproach.mp3?url';
import asMusMtYeah from '../../assets/audio/sounds/mus_mt_yeah.mp3?url';
import asMusOhyes from '../../assets/audio/sounds/mus_ohyes.mp3?url';
import asNode from '../../assets/audio/sounds/node.mp3?url';
import asNoise from '../../assets/audio/sounds/noise.mp3?url';
import asNote from '../../assets/audio/sounds/note.mp3?url';
import asNotify from '../../assets/audio/sounds/notify.mp3?url';
import asPathway from '../../assets/audio/sounds/pathway.mp3?url';
import asPhone from '../../assets/audio/sounds/phone.mp3?url';
import asPrebomb from '../../assets/audio/sounds/prebomb.mp3?url';
import asPunch1 from '../../assets/audio/sounds/punch1.mp3?url';
import asPunch2 from '../../assets/audio/sounds/punch2.mp3?url';
import asPurchase from '../../assets/audio/sounds/purchase.mp3?url';
import asRain from '../../assets/audio/sounds/rain.mp3?url';
import asRainbowbeam from '../../assets/audio/sounds/rainbowbeam.mp3?url';
import asRetract from '../../assets/audio/sounds/retract.mp3?url';
import asRimshot from '../../assets/audio/sounds/rimshot.mp3?url';
import asRotate from '../../assets/audio/sounds/rotate.mp3?url';
import asRun from '../../assets/audio/sounds/run.mp3?url';
import asRustle from '../../assets/audio/sounds/rustle.mp3?url';
import asSaber3 from '../../assets/audio/sounds/saber3.mp3?url';
import asSave from '../../assets/audio/sounds/save.mp3?url';
import asSega from '../../assets/audio/sounds/sega.mp3?url';
import asSelect from '../../assets/audio/sounds/select.mp3?url';
import asShake from '../../assets/audio/sounds/shake.mp3?url';
import asShakein from '../../assets/audio/sounds/shakein.mp3?url';
import asShatter from '../../assets/audio/sounds/shatter.mp3?url';
import asShock from '../../assets/audio/sounds/shock.mp3?url';
import asSingBad1 from '../../assets/audio/sounds/sing_bad1.mp3?url';
import asSingBad2 from '../../assets/audio/sounds/sing_bad2.mp3?url';
import asSingBad3 from '../../assets/audio/sounds/sing_bad3.mp3?url';
import asSingBass1 from '../../assets/audio/sounds/sing_bass1.mp3?url';
import asSingBass2 from '../../assets/audio/sounds/sing_bass2.mp3?url';
import asSingTreble1 from '../../assets/audio/sounds/sing_treble1.mp3?url';
import asSingTreble2 from '../../assets/audio/sounds/sing_treble2.mp3?url';
import asSingTreble3 from '../../assets/audio/sounds/sing_treble3.mp3?url';
import asSingTreble4 from '../../assets/audio/sounds/sing_treble4.mp3?url';
import asSingTreble5 from '../../assets/audio/sounds/sing_treble5.mp3?url';
import asSingTreble6 from '../../assets/audio/sounds/sing_treble6.mp3?url';
import asSlidewhistle from '../../assets/audio/sounds/slidewhistle.mp3?url';
import asSparkle from '../../assets/audio/sounds/sparkle.mp3?url';
import asSpecin from '../../assets/audio/sounds/specin.mp3?url';
import asSpecout from '../../assets/audio/sounds/specout.mp3?url';
import asSpeed from '../../assets/audio/sounds/speed.mp3?url';
import asSpiderLaugh from '../../assets/audio/sounds/spider_laugh.mp3?url';
import asSplash from '../../assets/audio/sounds/splash.mp3?url';
import asSpooky from '../../assets/audio/sounds/spooky.mp3?url';
import asSqueak from '../../assets/audio/sounds/squeak.mp3?url';
import asStab from '../../assets/audio/sounds/stab.mp3?url';
import asStarfall from '../../assets/audio/sounds/starfall.mp3?url';
import asStatic from '../../assets/audio/sounds/static.mp3?url';
import asStep from '../../assets/audio/sounds/step.mp3?url';
import asStomp from '../../assets/audio/sounds/stomp.mp3?url';
import asSwallow from '../../assets/audio/sounds/swallow.mp3?url';
import asSwipe from '../../assets/audio/sounds/swipe.mp3?url';
import asSwitch from '../../assets/audio/sounds/switch.mp3?url';
import asSword from '../../assets/audio/sounds/sword.mp3?url';
import asTarget from '../../assets/audio/sounds/target.mp3?url';
import asTextnoise from '../../assets/audio/sounds/textnoise.mp3?url';
import asTwinklyLaugh from '../../assets/audio/sounds/twinkly_laugh.mp3?url';
import asUpgrade from '../../assets/audio/sounds/upgrade.mp3?url';
import asWhimper from '../../assets/audio/sounds/whimper.mp3?url';
import asWind from '../../assets/audio/sounds/wind.mp3?url';
import avAlphys from '../../assets/audio/voices/alphys.mp3?url';
import avAsgore from '../../assets/audio/voices/asgore.mp3?url';
import avChara from '../../assets/audio/voices/chara.mp3?url';
import avFrisk from '../../assets/audio/voices/frisk.mp3?url';
import avMettaton1 from '../../assets/audio/voices/mettaton1.mp3?url';
import avMettaton2 from '../../assets/audio/voices/mettaton2.mp3?url';
import avMettaton3 from '../../assets/audio/voices/mettaton3.mp3?url';
import avMettaton4 from '../../assets/audio/voices/mettaton4.mp3?url';
import avMettaton5 from '../../assets/audio/voices/mettaton5.mp3?url';
import avMettaton6 from '../../assets/audio/voices/mettaton6.mp3?url';
import avMettaton7 from '../../assets/audio/voices/mettaton7.mp3?url';
import avMettaton8 from '../../assets/audio/voices/mettaton8.mp3?url';
import avMettaton9 from '../../assets/audio/voices/mettaton9.mp3?url';
import avNapstablook from '../../assets/audio/voices/napstablook.mp3?url';
import avNarrator from '../../assets/audio/voices/narrator.mp3?url';
import avPapyrus from '../../assets/audio/voices/papyrus.mp3?url';
import avSans from '../../assets/audio/voices/sans.mp3?url';
import avTwinkly1 from '../../assets/audio/voices/twinkly1.mp3?url';
import avTwinkly2 from '../../assets/audio/voices/twinkly2.mp3?url';
import ieSOUL from '../../assets/images/extras/SOUL.png?url';
import ieButtonC from '../../assets/images/extras/buttonC.png?url';
import ieButtonF from '../../assets/images/extras/buttonF.png?url';
import ieButtonM from '../../assets/images/extras/buttonM.png?url';
import ieButtonX from '../../assets/images/extras/buttonX.png?url';
import ieButtonZ from '../../assets/images/extras/buttonZ.png?url';
import ieSplashBackground from '../../assets/images/extras/splash-background.png?url';
import ieSplashForeground from '../../assets/images/extras/splash-foreground.png?url';
import ieStory$info from '../../assets/images/extras/story.json?url';
import ieStory from '../../assets/images/extras/story.png?url';
import iocFriskDown$info from '../../assets/images/overworldCharacters/frisk/down.json?url';
import iocFriskDown from '../../assets/images/overworldCharacters/frisk/down.png?url';
import iocFriskLeft$info from '../../assets/images/overworldCharacters/frisk/left.json?url';
import iocFriskLeft from '../../assets/images/overworldCharacters/frisk/left.png?url';
import iocFriskRight$info from '../../assets/images/overworldCharacters/frisk/right.json?url';
import iocFriskRight from '../../assets/images/overworldCharacters/frisk/right.png?url';
import iocFriskUp$info from '../../assets/images/overworldCharacters/frisk/up.json?url';
import iocFriskUp from '../../assets/images/overworldCharacters/frisk/up.png?url';
import iocMettatonDown$info from '../../assets/images/overworldCharacters/mettaton/down.json?url';
import iocMettatonDown from '../../assets/images/overworldCharacters/mettaton/down.png?url';
import iocMettatonLeft$info from '../../assets/images/overworldCharacters/mettaton/left.json?url';
import iocMettatonLeft from '../../assets/images/overworldCharacters/mettaton/left.png?url';
import iocMettatonRight$info from '../../assets/images/overworldCharacters/mettaton/right.json?url';
import iocMettatonRight from '../../assets/images/overworldCharacters/mettaton/right.png?url';
import iocMettatonUp$info from '../../assets/images/overworldCharacters/mettaton/up.json?url';
import iocMettatonUp from '../../assets/images/overworldCharacters/mettaton/up.png?url';
import iocNapstablookDown from '../../assets/images/overworldCharacters/napstablook/down.png?url';
import iocNapstablookLeft from '../../assets/images/overworldCharacters/napstablook/left.png?url';
import iocNapstablookRight from '../../assets/images/overworldCharacters/napstablook/right.png?url';
import iocNapstablookUp from '../../assets/images/overworldCharacters/napstablook/up.png?url';
import ionFBeanboi$info from '../../assets/images/overworldNPCs/forgespring/beanboi.json?url';
import ionFBeanboi from '../../assets/images/overworldNPCs/forgespring/beanboi.png?url';
import iooFBeamHorizontal$info from '../../assets/images/overworldObjects/forgespring/f_beam-horizontal.json?url';
import iooFBeamHorizontal from '../../assets/images/overworldObjects/forgespring/f_beam-horizontal.png?url';
import iooFBeamVertical$info from '../../assets/images/overworldObjects/forgespring/f_beam-vertical.json?url';
import iooFBeamVertical from '../../assets/images/overworldObjects/forgespring/f_beam-vertical.png?url';
import iooFBench from '../../assets/images/overworldObjects/forgespring/f_bench.png?url';
import iooFChair from '../../assets/images/overworldObjects/forgespring/f_chair.png?url';
import iooFEchoFlower from '../../assets/images/overworldObjects/forgespring/f_echo-flower.png?url';
import iooFGazebo from '../../assets/images/overworldObjects/forgespring/f_gazebo.png?url';
import iooFHouseCrustyBlue from '../../assets/images/overworldObjects/forgespring/f_house-crusty-blue.png?url';
import iooFHouseCrustyRed from '../../assets/images/overworldObjects/forgespring/f_house-crusty-red.png?url';
import iooFHouseSparkleBlue from '../../assets/images/overworldObjects/forgespring/f_house-sparkle-blue.png?url';
import iooFHouseSparkleRed from '../../assets/images/overworldObjects/forgespring/f_house-sparkle-red.png?url';
import iooFHouseTall from '../../assets/images/overworldObjects/forgespring/f_house-tall.png?url';
import iooFHouseUndyne from '../../assets/images/overworldObjects/forgespring/f_house-undyne.png?url';
import iooFLampBlue from '../../assets/images/overworldObjects/forgespring/f_lamp-blue.png?url';
import iooFLampRed from '../../assets/images/overworldObjects/forgespring/f_lamp-red.png?url';
import iooFMessign from '../../assets/images/overworldObjects/forgespring/f_messign.png?url';
import iooFMushroom from '../../assets/images/overworldObjects/forgespring/f_mushroom.png?url';
import iooFOneshroom from '../../assets/images/overworldObjects/forgespring/f_oneshroom.png?url';
import iooFPlant from '../../assets/images/overworldObjects/forgespring/f_plant.png?url';
import iooFShopFish from '../../assets/images/overworldObjects/forgespring/f_shop-fish.png?url';
import iooFShopSpider from '../../assets/images/overworldObjects/forgespring/f_shop-spider.png?url';
import iooFSign from '../../assets/images/overworldObjects/forgespring/f_sign.png?url';
import iooFSmallshroom from '../../assets/images/overworldObjects/forgespring/f_smallshroom.png?url';
import iooFStatue from '../../assets/images/overworldObjects/forgespring/f_statue.png?url';
import iooFStrutBackLeft from '../../assets/images/overworldObjects/forgespring/f_strut-back-left.png?url';
import iooFStrutBackRight from '../../assets/images/overworldObjects/forgespring/f_strut-back-right.png?url';
import iooFStrutLeft from '../../assets/images/overworldObjects/forgespring/f_strut-left.png?url';
import iooFStrutMidLeft from '../../assets/images/overworldObjects/forgespring/f_strut-mid-left.png?url';
import iooFStrutMidRight from '../../assets/images/overworldObjects/forgespring/f_strut-mid-right.png?url';
import iooFStrutRight from '../../assets/images/overworldObjects/forgespring/f_strut-right.png?url';
import iooFTeleporter from '../../assets/images/overworldObjects/forgespring/f_teleporter.png?url';
import ismBackground from '../../assets/images/shops/masa/background.png?url';
import ismKeeper$info from '../../assets/images/shops/masa/keeper.json?url';
import ismKeeper from '../../assets/images/shops/masa/keeper.png?url';

import { AdvancedBloomFilter } from '@pixi/filter-advanced-bloom';
import { CRTFilter } from '@pixi/filter-crt';
import { GlitchFilter } from '@pixi/filter-glitch';
import { GlowFilter } from '@pixi/filter-glow';
import { OutlineFilter } from '@pixi/filter-outline';
import {
   CosmosAnimationResources,
   CosmosAudio,
   CosmosAudioUtils,
   CosmosColor,
   CosmosDaemon,
   CosmosDaemonRouter,
   CosmosData,
   CosmosEffect,
   CosmosImage,
   CosmosInventory,
   CosmosMath,
   CosmosMixer,
   CosmosRegistry,
   CosmosUtils
} from './storyteller';

export type BackdropSpec = { header: string; content: string[] };

export const ratio01 = 0x55 / 0xff;
export const ratio02 = 0x75 / 0xff;

export const contrast = (color: CosmosColor): CosmosColor => [
   CosmosMath.bezier(color[0] / 256, 0, 0, 256, 256),
   CosmosMath.bezier(color[1] / 256, 0, 0, 256, 256),
   CosmosMath.bezier(color[2] / 256, 0, 0, 256, 256),
   color[3]
];

export const dark01 = (color: CosmosColor) =>
   [ color[0] * ratio01, color[1] * ratio01, color[2] * ratio01, color[3] ] as CosmosColor;

export const dark02 = (color: CosmosColor) =>
   [ color[0] * ratio02, color[1] * ratio02, color[2] * ratio02, color[3] ] as CosmosColor;

export const asrielTrueColorFilter = (color: CosmosColor) =>
   color[0] === 0 || color[0] === 255 || color[3] === 0 ? color : ([ 0, 0, 0, 1 ] as CosmosColor);

export const content = {
   amForgespring: new CosmosAudio(amForgespring),
   amMenu: new CosmosAudio(amMenu),
   amStory: new CosmosAudio(amStory),
   asAppear: new CosmosAudio(asAppear),
   asApplause: new CosmosAudio(asApplause),
   asArrow: new CosmosAudio(asArrow),
   asAsrielSparkle: new CosmosAudio(asAsrielSparkle),
   asBahbye: new CosmosAudio(asBahbye),
   asBark: new CosmosAudio(asBark),
   asBell: new CosmosAudio(asBell),
   asBoing: new CosmosAudio(asBoing),
   asBomb: new CosmosAudio(asBomb),
   asBombfall: new CosmosAudio(asBombfall),
   asBookspin: new CosmosAudio(asBookspin),
   asBoom: new CosmosAudio(asBoom),
   asBreak: new CosmosAudio(asBreak),
   asBuhbuhbuhdaadodaa: new CosmosAudio(asBuhbuhbuhdaadodaa),
   asBurst: new CosmosAudio(asBurst),
   asCackle: new CosmosAudio(asCackle),
   asCast: new CosmosAudio(asCast),
   asClap: new CosmosAudio(asClap),
   asComputer: new CosmosAudio(asComputer),
   asCrit: new CosmosAudio(asCrit),
   asCymbal: new CosmosAudio(asCymbal),
   asDeeploop2: new CosmosAudio(asDeeploop2),
   asDepower: new CosmosAudio(asDepower),
   asDestroyed: new CosmosAudio(asDestroyed),
   asDimbox: new CosmosAudio(asDimbox),
   asDog: new CosmosAudio(asDog),
   asDogsword: new CosmosAudio(asDogsword),
   asDoor: new CosmosAudio(asDoor),
   asDoorClose: new CosmosAudio(asDoorClose),
   asDrumroll: new CosmosAudio(asDrumroll),
   asDununnn: new CosmosAudio(asDununnn),
   asElectrodoor: new CosmosAudio(asElectrodoor),
   asElevator: new CosmosAudio(asElevator),
   asEquip: new CosmosAudio(asEquip),
   asFear: new CosmosAudio(asFear),
   asFrypan: new CosmosAudio(asFrypan),
   asGlassbreak: new CosmosAudio(asGlassbreak),
   asGonerCharge: new CosmosAudio(asGonerCharge),
   asGoodbye: new CosmosAudio(asGoodbye),
   asGrab: new CosmosAudio(asGrab),
   asGunshot: new CosmosAudio(asGunshot),
   asHeal: new CosmosAudio(asHeal),
   asHero: new CosmosAudio(asHero),
   asHit: new CosmosAudio(asHit),
   asHurt: new CosmosAudio(asHurt),
   asImpact: new CosmosAudio(asImpact),
   asIndicator: new CosmosAudio(asIndicator),
   asJetpack: new CosmosAudio(asJetpack),
   asKnock: new CosmosAudio(asKnock),
   asLanding: new CosmosAudio(asLanding),
   asMenu: new CosmosAudio(asMenu),
   asMeow: new CosmosAudio(asMeow),
   asMetapproach: new CosmosAudio(asMetapproach),
   asMusMtYeah: new CosmosAudio(asMusMtYeah),
   asMusOhyes: new CosmosAudio(asMusOhyes),
   asNode: new CosmosAudio(asNode),
   asNoise: new CosmosAudio(asNoise),
   asNote: new CosmosAudio(asNote),
   asNotify: new CosmosAudio(asNotify),
   asPathway: new CosmosAudio(asPathway),
   asPhone: new CosmosAudio(asPhone),
   asPrebomb: new CosmosAudio(asPrebomb),
   asPunch1: new CosmosAudio(asPunch1),
   asPunch2: new CosmosAudio(asPunch2),
   asPurchase: new CosmosAudio(asPurchase),
   asRain: new CosmosAudio(asRain),
   asRainbowbeam: new CosmosAudio(asRainbowbeam),
   asRetract: new CosmosAudio(asRetract),
   asRimshot: new CosmosAudio(asRimshot),
   asRotate: new CosmosAudio(asRotate),
   asRun: new CosmosAudio(asRun),
   asRustle: new CosmosAudio(asRustle),
   asSaber3: new CosmosAudio(asSaber3),
   asSave: new CosmosAudio(asSave),
   asSega: new CosmosAudio(asSega),
   asSelect: new CosmosAudio(asSelect),
   asShake: new CosmosAudio(asShake),
   asShakein: new CosmosAudio(asShakein),
   asShatter: new CosmosAudio(asShatter),
   asShock: new CosmosAudio(asShock),
   asSingBad1: new CosmosAudio(asSingBad1),
   asSingBad2: new CosmosAudio(asSingBad2),
   asSingBad3: new CosmosAudio(asSingBad3),
   asSingBass1: new CosmosAudio(asSingBass1),
   asSingBass2: new CosmosAudio(asSingBass2),
   asSingTreble1: new CosmosAudio(asSingTreble1),
   asSingTreble2: new CosmosAudio(asSingTreble2),
   asSingTreble3: new CosmosAudio(asSingTreble3),
   asSingTreble4: new CosmosAudio(asSingTreble4),
   asSingTreble5: new CosmosAudio(asSingTreble5),
   asSingTreble6: new CosmosAudio(asSingTreble6),
   asSlidewhistle: new CosmosAudio(asSlidewhistle),
   asSparkle: new CosmosAudio(asSparkle),
   asSpecin: new CosmosAudio(asSpecin),
   asSpecout: new CosmosAudio(asSpecout),
   asSpeed: new CosmosAudio(asSpeed),
   asSpiderLaugh: new CosmosAudio(asSpiderLaugh),
   asSplash: new CosmosAudio(asSplash),
   asSpooky: new CosmosAudio(asSpooky),
   asSqueak: new CosmosAudio(asSqueak),
   asStab: new CosmosAudio(asStab),
   asStarfall: new CosmosAudio(asStarfall),
   asStatic: new CosmosAudio(asStatic),
   asStep: new CosmosAudio(asStep),
   asStomp: new CosmosAudio(asStomp),
   asSwallow: new CosmosAudio(asSwallow),
   asSwipe: new CosmosAudio(asSwipe),
   asSwitch: new CosmosAudio(asSwitch),
   asSword: new CosmosAudio(asSword),
   asTarget: new CosmosAudio(asTarget),
   asTextnoise: new CosmosAudio(asTextnoise),
   asTwinklyLaugh: new CosmosAudio(asTwinklyLaugh),
   asUpgrade: new CosmosAudio(asUpgrade),
   asWhimper: new CosmosAudio(asWhimper),
   asWind: new CosmosAudio(asWind),
   avAlphys: new CosmosAudio(avAlphys),
   avAsgore: new CosmosAudio(avAsgore),
   avChara: new CosmosAudio(avChara),
   avFrisk: new CosmosAudio(avFrisk),
   avMettaton1: new CosmosAudio(avMettaton1),
   avMettaton2: new CosmosAudio(avMettaton2),
   avMettaton3: new CosmosAudio(avMettaton3),
   avMettaton4: new CosmosAudio(avMettaton4),
   avMettaton5: new CosmosAudio(avMettaton5),
   avMettaton6: new CosmosAudio(avMettaton6),
   avMettaton7: new CosmosAudio(avMettaton7),
   avMettaton8: new CosmosAudio(avMettaton8),
   avMettaton9: new CosmosAudio(avMettaton9),
   avNapstablook: new CosmosAudio(avNapstablook),
   avNarrator: new CosmosAudio(avNarrator),
   avPapyrus: new CosmosAudio(avPapyrus),
   avSans: new CosmosAudio(avSans),
   avTwinkly1: new CosmosAudio(avTwinkly1),
   avTwinkly2: new CosmosAudio(avTwinkly2),
   ieButtonC: new CosmosImage(ieButtonC),
   ieButtonF: new CosmosImage(ieButtonF),
   ieButtonM: new CosmosImage(ieButtonM),
   ieButtonX: new CosmosImage(ieButtonX),
   ieButtonZ: new CosmosImage(ieButtonZ),
   ieSOUL: new CosmosImage(ieSOUL),
   ieSplashBackground: new CosmosImage(ieSplashBackground),
   ieSplashForeground: new CosmosImage(ieSplashForeground),
   ieStory: new CosmosAnimationResources(new CosmosImage(ieStory), new CosmosData(ieStory$info)),
   iocFriskDown: new CosmosAnimationResources(new CosmosImage(iocFriskDown), new CosmosData(iocFriskDown$info)),
   iocFriskLeft: new CosmosAnimationResources(new CosmosImage(iocFriskLeft), new CosmosData(iocFriskLeft$info)),
   iocFriskRight: new CosmosAnimationResources(new CosmosImage(iocFriskRight), new CosmosData(iocFriskRight$info)),
   iocFriskUp: new CosmosAnimationResources(new CosmosImage(iocFriskUp), new CosmosData(iocFriskUp$info)),
   iocMettatonDown: new CosmosAnimationResources(
      new CosmosImage(iocMettatonDown),
      new CosmosData(iocMettatonDown$info)
   ),
   iocMettatonLeft: new CosmosAnimationResources(
      new CosmosImage(iocMettatonLeft),
      new CosmosData(iocMettatonLeft$info)
   ),
   iocMettatonRight: new CosmosAnimationResources(
      new CosmosImage(iocMettatonRight),
      new CosmosData(iocMettatonRight$info)
   ),
   iocMettatonUp: new CosmosAnimationResources(new CosmosImage(iocMettatonUp), new CosmosData(iocMettatonUp$info)),
   iocNapstablookDown: new CosmosImage(iocNapstablookDown),
   iocNapstablookLeft: new CosmosImage(iocNapstablookLeft),
   iocNapstablookRight: new CosmosImage(iocNapstablookRight),
   iocNapstablookUp: new CosmosImage(iocNapstablookUp),
   ionFBeanboi: new CosmosAnimationResources(new CosmosImage(ionFBeanboi), new CosmosData(ionFBeanboi$info)),
   iooFBeamHorizontal: new CosmosAnimationResources(
      new CosmosImage(iooFBeamHorizontal),
      new CosmosData(iooFBeamHorizontal$info)
   ),
   iooFBeamVertical: new CosmosAnimationResources(
      new CosmosImage(iooFBeamVertical),
      new CosmosData(iooFBeamVertical$info)
   ),
   iooFBench: new CosmosImage(iooFBench),
   iooFChair: new CosmosImage(iooFChair),
   iooFEchoFlower: new CosmosImage(iooFEchoFlower),
   iooFGazebo: new CosmosImage(iooFGazebo),
   iooFHouseCrustyBlue: new CosmosImage(iooFHouseCrustyBlue),
   iooFHouseCrustyRed: new CosmosImage(iooFHouseCrustyRed),
   iooFHouseSparkleBlue: new CosmosImage(iooFHouseSparkleBlue),
   iooFHouseSparkleRed: new CosmosImage(iooFHouseSparkleRed),
   iooFHouseTall: new CosmosImage(iooFHouseTall),
   iooFHouseUndyne: new CosmosImage(iooFHouseUndyne),
   iooFLampBlue: new CosmosImage(iooFLampBlue),
   iooFLampRed: new CosmosImage(iooFLampRed),
   iooFMessign: new CosmosImage(iooFMessign),
   iooFMushroom: new CosmosImage(iooFMushroom),
   iooFOneshroom: new CosmosImage(iooFOneshroom),
   iooFPlant: new CosmosImage(iooFPlant),
   iooFShopFish: new CosmosImage(iooFShopFish),
   iooFShopSpider: new CosmosImage(iooFShopSpider),
   iooFSign: new CosmosImage(iooFSign),
   iooFSmallshroom: new CosmosImage(iooFSmallshroom),
   iooFStatue: new CosmosImage(iooFStatue),
   iooFStrutBackLeft: new CosmosImage(iooFStrutBackLeft),
   iooFStrutBackRight: new CosmosImage(iooFStrutBackRight),
   iooFStrutLeft: new CosmosImage(iooFStrutLeft),
   iooFStrutMidLeft: new CosmosImage(iooFStrutMidLeft),
   iooFStrutMidRight: new CosmosImage(iooFStrutMidRight),
   iooFStrutRight: new CosmosImage(iooFStrutRight),
   iooFTeleporter: new CosmosImage(iooFTeleporter),
   ismBackground: new CosmosImage(ismBackground),
   ismKeeper: new CosmosAnimationResources(new CosmosImage(ismKeeper), new CosmosData(ismKeeper$info))
};

for (const [ key, value ] of Object.entries(content)) {
   value.name = `content::${key}`;
   if (value instanceof CosmosAnimationResources) {
      value.data.name = `content::${key}`;
      value.image.name = `content::${key}`;
   }
}

export const filters = {
   // unused in code, but very much used in rooms (don't delete it)
   bloom: new AdvancedBloomFilter({ threshold: 0.8, bloomScale: 1, quality: 10, brightness: 1 }),
   bloomX: new AdvancedBloomFilter({ threshold: 0, bloomScale: 1, quality: 10, brightness: 1 }),
   crt: new CRTFilter({
      curvature: 0,
      lineContrast: 0.15,
      lineWidth: 5,
      noise: 0.15,
      noiseSize: 1.5,
      vignetting: 0.1,
      vignettingAlpha: 0.25,
      vignettingBlur: 0.75
   }),
   glitch: new GlitchFilter({ slices: 50, offset: 5 }),
   glow: new GlowFilter({ color: 0xffffff, innerStrength: 0, outerStrength: 0, quality: 1 }),
   outline: new OutlineFilter(2, 0, 0.1, 1, false)
};

export const inventories = {
   avMettaton: new CosmosInventory(
      content.avMettaton1,
      content.avMettaton2,
      content.avMettaton3,
      content.avMettaton4,
      content.avMettaton5,
      content.avMettaton6,
      content.avMettaton7,
      content.avMettaton8,
      content.avMettaton9
   ),
   iocNapstablook: new CosmosInventory(
      content.iocNapstablookDown,
      content.iocNapstablookLeft,
      content.iocNapstablookRight,
      content.iocNapstablookUp
   ),
   lazyAssets: new CosmosInventory(
      content.asDimbox,
      content.asEquip,
      content.asHeal,
      content.asMenu,
      content.asNoise,
      content.asPhone,
      content.asSelect,
      content.asSwallow,
      content.avAsgore,
      content.avNarrator,
      content.avPapyrus,
      content.iocFriskDown,
      content.iocFriskLeft,
      content.iocFriskRight,
      content.iocFriskUp,
      content.iocMettatonDown,
      content.iocMettatonLeft,
      content.iocMettatonRight,
      content.iocMettatonUp,
      content.iocNapstablookDown,
      content.iocNapstablookLeft,
      content.iocNapstablookRight,
      content.iocNapstablookUp,
      content.ieSOUL,
      content.avFrisk,
      content.avChara,
      content.avNapstablook,
      content.avMettaton1,
      content.avMettaton2,
      content.avMettaton3,
      content.avMettaton4,
      content.avMettaton5,
      content.avMettaton6,
      content.avMettaton7,
      content.avMettaton8,
      content.avMettaton9
   ),
   menuAssets: new CosmosInventory(
      content.amStory,
      content.ieStory,
      content.asImpact,
      content.asCymbal,
      content.ieSplashForeground,
      content.avTwinkly1,
      content.avTwinkly2
   ),
   mobileAssets: new CosmosInventory(
      content.ieButtonC,
      content.ieButtonF,
      content.ieButtonM,
      content.ieButtonX,
      content.ieButtonZ
   ),
   splashAssets: new CosmosInventory(content.asSplash, content.ieSplashForeground),
   twinklyAssets: new CosmosInventory(content.avTwinkly1, content.avTwinkly2)
};

for (const [ key, value ] of Object.entries(inventories)) {
   value.name = `inventories::${key}`;
}

export const context = new AudioContext();
CosmosAudioUtils.context = context;

export const convolver = CosmosAudioUtils.convolver(context, 0.8);

export const musicConvolver = new CosmosEffect(context, CosmosAudioUtils.convolver(context, 1.2), 0);
export const musicFilter = new CosmosEffect(
   context,
   new BiquadFilterNode(context, { Q: 1, type: 'lowpass', frequency: 500 }),
   0
);
export const musicMixer = new CosmosMixer(context, [ musicFilter, musicConvolver ]);
export const musicRegistry = new CosmosRegistry(new CosmosDaemon(content.amStory, { context }));

export const soundDelay = new CosmosEffect(context, CosmosAudioUtils.delay(context, 1 / 3, 0.5), 0);
export const soundMixer = new CosmosMixer(context, [ soundDelay ]);
export const soundRegistry = new CosmosRegistry(new CosmosDaemon(content.asNoise, { context }));

export function standardAudio (gain: number, rate: number, loop: boolean, max: number, router: CosmosDaemonRouter) {
   return { context, gain, loop, rate, router, max };
}

export function musicOpts (gain = 0.5, rate = 1, loop = true, max = 1) {
   return standardAudio(gain, rate, loop, max, musicRouter);
}

export function musicRouter (input: AudioNode) {
   input.connect(musicMixer.input);
}

export function soundOpts (gain = 0.5, rate = 1, loop = false, max = 8) {
   return standardAudio(gain, rate, loop, max, input => input.connect(soundMixer.input));
}

export function soundRouter (input: AudioNode) {
   input.connect(soundMixer.input);
}

export function effectSetup (effect: CosmosEffect, router: CosmosDaemonRouter): CosmosDaemonRouter {
   return input => {
      input.connect(effect.input);
      input.connect(effect.throughput);
      router(effect.output);
      router(effect.throughput);
   };
}

export const music = {
   forgespring: new CosmosDaemon(content.amForgespring, musicOpts(0.4)),
   menu: new CosmosDaemon(content.amMenu, musicOpts(0.4)),
   story: new CosmosDaemon(content.amStory, musicOpts(0.25, 0.92, false))
};

export const sounds = {
   appear: new CosmosDaemon(content.asAppear, soundOpts()),
   applause: new CosmosDaemon(content.asApplause, soundOpts()),
   arrow: new CosmosDaemon(content.asArrow, soundOpts()),
   arrow_leap: new CosmosDaemon(content.asArrow, soundOpts(0.5, 1, false, 1)),
   asrielSparkle: new CosmosDaemon(content.asAsrielSparkle, soundOpts()),
   bahbye: new CosmosDaemon(content.asBahbye, soundOpts(0.3)),
   bark: new CosmosDaemon(content.asBark, soundOpts(0.4)),
   bell: new CosmosDaemon(content.asBell, soundOpts(0.24)), // done
   boing: new CosmosDaemon(content.asBoing, soundOpts()),
   bomb: new CosmosDaemon(content.asBomb, soundOpts()),
   bombfall: new CosmosDaemon(content.asBombfall, soundOpts()),
   bookspin: new CosmosDaemon(content.asBookspin, soundOpts()),
   boom: new CosmosDaemon(content.asBoom, soundOpts(0.25)),
   boom_orange: new CosmosDaemon(content.asBoom, soundOpts(0.25, 2, false, 1)),
   boxpush: new CosmosDaemon(content.asLanding, {
      context,
      gain: 0.5,
      router: effectSetup(new CosmosEffect(context, CosmosAudioUtils.delay(context, 0.2, 0.5), 0.6), soundRouter)
   }),
   break: new CosmosDaemon(content.asBreak, soundOpts(1)),
   buhbuhbuhdaadodaa: new CosmosDaemon(content.asBuhbuhbuhdaadodaa, soundOpts()),
   burst: new CosmosDaemon(content.asBurst, soundOpts()),
   cackle: new CosmosDaemon(content.asCackle, soundOpts()),
   cast: new CosmosDaemon(content.asCast, soundOpts()),
   clap: new CosmosDaemon(content.asClap, soundOpts()),
   computer: new CosmosDaemon(content.asComputer, soundOpts(0.5, 1, true)),
   crit: new CosmosDaemon(content.asCrit, soundOpts()),
   cymbal: new CosmosDaemon(content.asCymbal, soundOpts()),
   deeploop2: new CosmosDaemon(content.asDeeploop2, soundOpts(0.5, 1, true)),
   depower: new CosmosDaemon(content.asDepower, soundOpts(0.26)), // done
   destroyed: new CosmosDaemon(content.asDestroyed, soundOpts(0.5, 1, true)), // done
   dimbox: new CosmosDaemon(content.asDimbox, soundOpts(0.21)),
   dog: new CosmosDaemon(content.asDog, soundOpts()),
   dogsword: new CosmosDaemon(content.asDogsword, soundOpts()),
   door: new CosmosDaemon(content.asDoor, soundOpts()),
   doorClose: new CosmosDaemon(content.asDoorClose, soundOpts()),
   drumroll: new CosmosDaemon(content.asDrumroll, soundOpts()),
   dununnn: new CosmosDaemon(content.asDununnn, soundOpts()),
   echostop: new CosmosDaemon(content.asNoise, soundOpts(0.2, 1.4)),
   electrodoor: new CosmosDaemon(content.asElectrodoor, soundOpts()),
   elevator: new CosmosDaemon(content.asElevator, soundOpts(0.34)), // done
   equip: new CosmosDaemon(content.asEquip, soundOpts(0.6)),
   fear: new CosmosDaemon(content.asFear, soundOpts()),
   frypan: new CosmosDaemon(content.asFrypan, soundOpts()),
   glassbreak: new CosmosDaemon(content.asGlassbreak, soundOpts()),
   goner_charge: new CosmosDaemon(content.asGonerCharge, soundOpts()),
   goodbye: new CosmosDaemon(content.asGoodbye, soundOpts(0.3)),
   grab: new CosmosDaemon(content.asGrab, soundOpts()),
   gunshot: new CosmosDaemon(content.asGunshot, soundOpts()),
   heal: new CosmosDaemon(content.asHeal, soundOpts(0.43)),
   hero: new CosmosDaemon(content.asHero, soundOpts()),
   heroDark: new CosmosDaemon(content.asHero, soundOpts(0.5, 0.8)),
   hit: new CosmosDaemon(content.asHit, soundOpts()),
   hurt: new CosmosDaemon(content.asHurt, soundOpts()),
   impact: new CosmosDaemon(content.asImpact, soundOpts(0.7)),
   indicator: new CosmosDaemon(content.asIndicator, soundOpts(0.3)), // done
   jetpack: new CosmosDaemon(content.asJetpack, soundOpts(0.5, 1, true)),
   knock: new CosmosDaemon(content.asKnock, soundOpts()),
   landing: new CosmosDaemon(content.asLanding, soundOpts()),
   menu: new CosmosDaemon(content.asMenu, soundOpts(0.63)),
   meow: new CosmosDaemon(content.asMeow, soundOpts(0.5, 0.9)),
   menuMusic: new CosmosDaemon(content.asMenu, musicOpts(0.63, 1, false)),
   metapproach: new CosmosDaemon(content.asMetapproach, soundOpts()),
   mus_mt_yeah: new CosmosDaemon(content.asMusMtYeah, soundOpts(0.4)),
   mus_ohyes: new CosmosDaemon(content.asMusOhyes, soundOpts()),
   node: new CosmosDaemon(content.asNode, soundOpts()),
   noise: new CosmosDaemon(content.asNoise, soundOpts(0.41)),
   note: new CosmosDaemon(content.asNote, soundOpts()),
   notify: new CosmosDaemon(content.asNotify, soundOpts(0.3)), // done
   pathway: new CosmosDaemon(content.asPathway, soundOpts(0.42)), // done
   phone: new CosmosDaemon(content.asPhone, soundOpts(0.3)), // done
   prebomb: new CosmosDaemon(content.asPrebomb, soundOpts()),
   punch1: new CosmosDaemon(content.asPunch1, soundOpts()),
   punch2: new CosmosDaemon(content.asPunch2, soundOpts()),
   purchase: new CosmosDaemon(content.asPurchase, soundOpts()),
   rainbowbeam: new CosmosDaemon(content.asRainbowbeam, soundOpts(0.5, 1, true)),
   retract: new CosmosDaemon(content.asRetract, soundOpts(0.45)),
   rimshot: new CosmosDaemon(content.asRimshot, soundOpts()),
   rotate: new CosmosDaemon(content.asRotate, soundOpts(0.5, 1, true)),
   run: new CosmosDaemon(content.asRun, soundOpts(0.6)),
   rustle: new CosmosDaemon(content.asRustle, soundOpts()),
   saber3: new CosmosDaemon(content.asSaber3, soundOpts()),
   save: new CosmosDaemon(content.asSave, soundOpts(1 / 3)),
   sega: new CosmosDaemon(content.asSega, soundOpts(0.25)),
   select: new CosmosDaemon(content.asSelect, soundOpts(1)),
   shake: new CosmosDaemon(content.asShake, {
      context,
      gain: 1,
      router: effectSetup(new CosmosEffect(context, convolver, 0.4), soundRouter)
   }),
   shakein: new CosmosDaemon(content.asShakein, soundOpts()),
   shatter: new CosmosDaemon(content.asShatter, soundOpts(1)),
   shock: new CosmosDaemon(content.asShock, soundOpts()),
   singBad1: new CosmosDaemon(content.asSingBad1, soundOpts()),
   singBad2: new CosmosDaemon(content.asSingBad2, soundOpts()),
   singBad3: new CosmosDaemon(content.asSingBad3, soundOpts()),
   singBass1: new CosmosDaemon(content.asSingBass1, soundOpts()),
   singBass2: new CosmosDaemon(content.asSingBass2, soundOpts()),
   singTreble1: new CosmosDaemon(content.asSingTreble1, soundOpts()),
   singTreble2: new CosmosDaemon(content.asSingTreble2, soundOpts()),
   singTreble3: new CosmosDaemon(content.asSingTreble3, soundOpts()),
   singTreble4: new CosmosDaemon(content.asSingTreble4, soundOpts()),
   singTreble5: new CosmosDaemon(content.asSingTreble5, soundOpts()),
   singTreble6: new CosmosDaemon(content.asSingTreble6, soundOpts()),
   slidewhistle: new CosmosDaemon(content.asSlidewhistle, soundOpts()),
   sparkle: new CosmosDaemon(content.asSparkle, soundOpts()),
   specin: new CosmosDaemon(content.asSpecin, soundOpts(0.4, 1.2)),
   specout: new CosmosDaemon(content.asSpecout, soundOpts(0.4, 1.2)),
   speed: new CosmosDaemon(content.asSpeed, soundOpts()),
   spiderLaugh: new CosmosDaemon(content.asSpiderLaugh, soundOpts(0.24)),
   splash: new CosmosDaemon(content.asSplash, soundOpts(1)),
   spooky: new CosmosDaemon(content.asSpooky, soundOpts()),
   squeak: new CosmosDaemon(content.asSqueak, soundOpts(0.12, 1.2)),
   stab: new CosmosDaemon(content.asStab, soundOpts()),
   starfall: new CosmosDaemon(content.asStarfall, soundOpts()),
   static: new CosmosDaemon(content.asStatic, soundOpts()),
   step: new CosmosDaemon(content.asStep, soundOpts()),
   stomp: new CosmosDaemon(content.asStomp, soundOpts()),
   supercymbal: new CosmosDaemon(content.asCymbal, {
      context,
      gain: 0.5,
      router: effectSetup(new CosmosEffect(context, CosmosAudioUtils.delay(context, 0.25, 0.5), 0.6), soundRouter)
   }),
   surge: new CosmosDaemon(content.asGonerCharge, {
      context,
      gain: 0.5,
      router: effectSetup(new CosmosEffect(context, CosmosAudioUtils.delay(context, 0.25, 0.8), 0.6), soundRouter)
   }),
   swallow: new CosmosDaemon(content.asSwallow, soundOpts()),
   swipe: new CosmosDaemon(content.asSwipe, soundOpts()),
   switch: new CosmosDaemon(content.asSwitch, soundOpts()),
   sword: new CosmosDaemon(content.asSword, soundOpts()),
   target: new CosmosDaemon(content.asTarget, soundOpts()),
   textnoise: new CosmosDaemon(content.asTextnoise, soundOpts(0.2)), // done
   twinklyLaugh: new CosmosDaemon(content.asTwinklyLaugh, soundOpts(0.8)),
   upgrade: new CosmosDaemon(content.asUpgrade, soundOpts(0.45, 0.8)),
   upgrade_jusant: new CosmosDaemon(content.asUpgrade, soundOpts(0.45, 0.8)),
   whimper: new CosmosDaemon(content.asWhimper, soundOpts()),
   wind: new CosmosDaemon(content.asWind, soundOpts(0.5, 1, true)),
   xm: new CosmosDaemon(content.asDimbox, {
      context,
      gain: 0.3,
      rate: (2 ** (1 / 12)) ** -10,
      router: effectSetup(new CosmosEffect(context, CosmosAudioUtils.delay(context, 0.4, 0.5), 0.6), soundRouter)
   })
};

musicMixer.output.connect(context.destination);
musicRegistry.register(music);
soundMixer.output.connect(context.destination);
soundRegistry.register(sounds);

CosmosUtils.status(`LOAD MODULE: ASSETS (${Math.floor(performance.now()) / 1000})`, { color: '#07f' });
