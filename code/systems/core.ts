import ComicSans from '../../assets/fonts/ComicSans.png?url';
import CryptOfTomorrow from '../../assets/fonts/CryptOfTomorrow.png?url';
import DeterminationMono from '../../assets/fonts/DeterminationMono.png?url';
import DeterminationSans from '../../assets/fonts/DeterminationSans.png?url';
import DiaryOfAn8BitMage from '../../assets/fonts/DiaryOfAn8BitMage.png?url';
import DotumChe from '../../assets/fonts/DotumChe.png?url';
import MarsNeedsCunnilingus from '../../assets/fonts/MarsNeedsCunnilingus.png?url';
import Papyrus from '../../assets/fonts/Papyrus.png?url';
import fonts$info from '../../assets/fonts/index.json';

import fileDialog from 'file-dialog';
import { Rectangle } from 'pixi.js';
import {
   OutertaleLayerKey,
   OutertaleMap,
   OutertaleRoom,
   OutertaleSpeechPreset
} from './outertale';
import {
   CosmosAtlas,
   CosmosDaemon,
   CosmosDirection,
   CosmosEventHost,
   CosmosHitbox,
   CosmosImage,
   CosmosInstance,
   CosmosKeyboardInput,
   CosmosKeyed,
   CosmosObject,
   CosmosPointSimple,
   CosmosProvider,
   CosmosRegistry,
   CosmosRenderer,
   CosmosSprite,
   CosmosTextUtils,
   CosmosTyper,
   CosmosUtils,
   CosmosValueRandom
} from './storyteller';

export const atlas = new CosmosAtlas();

export const backend = typeof ___spacetime___ === 'object' ? ___spacetime___ : null;

export const dialog = {
   async message (
      error: boolean,
      type: 'alert' | 'confirm',
      display: { buttons: string[]; message: string; title: string }
   ): Promise<number> {
      return backend?.exec('dialog.message', error, display) ?? (window[type](display.message) ? 1 : 0);
   },
   async open (display: { buttonLabel: string; name: string; title: string }): Promise<string | null> {
      return backend?.exec('dialog.open', display) ?? (await fileDialog()).item(0)?.text() ?? null;
   },
   async save (content: string, display: { buttonLabel: string; name: string; title: string }): Promise<boolean> {
      return (
         backend?.exec('dialog.save', content, display) ??
         (() => {
            const link = document.createElement('a');
            link.download = 'universe.json';
            link.href = `data:text/json,${content}`;
            link.click();
            return true;
         })()
      );
   }
};

export function exit () {
   backend === null ? reload_full() : close();
}

export const events = new CosmosEventHost<{
   // right when an item use is triggered
   'pre-consume': [string, number];
   // when an item is "consumed" from the inventory
   consume: [string, number];
   // when the player drops an item
   drop: [string, number];
   // when the player is healed
   heal: [CosmosHitbox, number];
   // fired when the game is initialized
   init: [];
   // fired to wait for any intro sequences
   'init-intro': [];
   // fired after the intro phase and before the overworld phase
   'init-between': [];
   // initializes the overworld
   'init-overworld': [];
   // when the game is loaded fully
   loaded: [];
   // when a mod is loaded fully
   'loaded-mod': [string];
   // when the mods are loaded
   modded: [];
   // after an item is used
   'post-use': [string, number];
   // ready or not, here he comes!!!
   ready: [];
   // when the game is saved
   save: [];
   // when a script is fired
   script: [string, ...string[]];
   // when the current speaker is not spekaing
   shut: [];
   // when the current speaker is not spekaing (multi-typer)
   'shut-multi': [number];
   // when the player first spawns in
   spawn: [];
   // when the current speaker is speaking
   talk: [];
   // fired on every game tick where player movement is enabled
   tick: [];
   // when the player changes rooms
   teleport: [string, string];
   // asynchronous pre-releport event
   'teleport-pre': [string, string];
   // when the player changes rooms (early)
   'teleport-start': [string, string];
   // directly after the room value is changed in a teleport
   'teleport-update': [CosmosDirection, CosmosPointSimple];
   // when an item is used
   use: [string, number];
}>();

export const fonts = {
   ComicSans,
   CryptOfTomorrow,
   DeterminationMono,
   DeterminationSans,
   DiaryOfAn8BitMage,
   DotumChe,
   MarsNeedsCunnilingus,
   Papyrus
};

export const fontLoader = Promise.all(
   fonts$info.map(font => CosmosTextUtils.import(fonts[font.name as keyof typeof fonts], font))
).then(list => {
   for (const { name, font } of list) {
      CosmosTextUtils.register(name, font);
   }
});

export function fullscreen () {
   try {
      document.fullscreenElement
         ? document.exitFullscreen()
         : document.body.requestFullscreen({ navigationUI: 'hide' });
   } catch {}
}

export const game = {
   /** whether the player has loaded into the world or not */
   active: false,
   /** game camera director */
   camera: new CosmosObject(),
   /** force focus */
   focus: false,
   /** game initialized */
   init: false,
   /** enable or disable all input */
   input: true,
   /** allow interactions (automatic value) */
   interact: true,
   /** allow opening overworld menu */
   menu: true,
   /** enable or disable overworld movement */
   movement: false,
   /** current music */
   music: null as CosmosInstance | null,
   /** offset of current music */
   music_offset: 0,
   /** disable player hitbox from interacting with the overworld */
   noclip: false,
   /** get ratio'd */
   ratio: 1,
   /** prepare for standard reload (don't clear params) */
   reload: false,
   /** handle resizing */
   resize () {
      const width = game.width;
      const height = 480;
      const frame = document.querySelector('#frame') as HTMLElement;
      const wrapper = document.querySelector('#wrapper') as HTMLElement;
      let ratio: number;
      if (frame.clientWidth / frame.clientHeight < width / height) {
         ratio = frame.clientWidth / width;
      } else {
         ratio = frame.clientHeight / height;
      }
      game.ratio = ratio;
      wrapper.style.width = `${width}px`;
      wrapper.style.height = `${height}px`;
      wrapper.style.transform = `scale(${ratio})`;
   },
   /** current room */
   room: '',
   /** player can sprint */
   sprint: false,
   /** the current dialoguer text */
   text: '',
   /** game timer */
   timer: false,
   // slow mode
   vortex: false,
   // slow mode speed
   vortex_factor: 1,
   // game width
   width: 640
};

export function init () {
   if (!game.init) {
      game.init = true;
      renderer.start();
      events.fire('init');
   }
}

export const items = new CosmosRegistry<
   string,
   {
      type: 'consumable' | 'armor' | 'weapon' | 'special';
      value: number;
      useSFX?: CosmosDaemon | boolean;
      healSFX?: CosmosDaemon | boolean;
      text: {
         drop: CosmosProvider<string[]>;
         info: CosmosProvider<string[]>;
         name: CosmosProvider<string>;
         use: CosmosProvider<string[]>;
      };
   }
>({
   type: 'consumable',
   value: 0,
   text: { drop: [], info: [], name: '', use: [] }
});

export const keys = {
   altKey: new CosmosKeyboardInput(null, 'AltLeft', 'AltRight', 'Backquote'),
   downKey: new CosmosKeyboardInput(null, 'ArrowDown', 'KeyS', 'Numpad5'),
   interactKey: new CosmosKeyboardInput(null, 'KeyZ', 'Enter', 'Numpad1'),
   leftKey: new CosmosKeyboardInput(null, 'ArrowLeft', 'KeyA', 'Numpad4'),
   menuKey: new CosmosKeyboardInput(null, 'KeyC', 'ControlLeft', 'ControlRight', 'Numpad3'),
   openKey: new CosmosKeyboardInput(null, 'KeyO'),
   quitKey: new CosmosKeyboardInput(null, 'Escape'),
   rightKey: new CosmosKeyboardInput(null, 'ArrowRight', 'KeyD', 'Numpad6'),
   saveKey: new CosmosKeyboardInput(null, 'KeyS'),
   shiftKey: new CosmosKeyboardInput(null, 'ShiftLeft', 'ShiftRight'),
   specialKey: new CosmosKeyboardInput(null, 'KeyX', 'ShiftLeft', 'ShiftRight', 'Numpad2'),
   upKey: new CosmosKeyboardInput(null, 'ArrowUp', 'KeyW', 'Numpad8')
};

export const maps = new CosmosRegistry<string, OutertaleMap>(new OutertaleMap('', new CosmosImage('')));

export function param (key: string, value: string | null = null) {
   value === null ? params.delete(key) : params.set(key, value);
   history.replaceState(null, '', `${location.origin + location.pathname}?${params.toString()}${location.hash}`);
}

export const params = new URLSearchParams(location.search);

export const launch = { intro: !params.has('fast'), overworld: true, timeline: !params.has('namespace') };

/** reload the game */
export function reload (fast = false, invisible = backend === null ? false : fast) {
   param('fast', fast ? '' : null);
   param('invisible', invisible ? '' : null);
   game.reload = true;
   backend?.exec('reload') ?? location.reload();
}

export function reload_full () {
   for (const p of params.keys()) {
      param(p);
   }
   reload();
}

export const renderer = new CosmosRenderer<OutertaleLayerKey>({
   area: new Rectangle(0, 0, 640, 480),
   auto: false,
   wrapper: '#wrapper',
   layers: { base: [ 'fixed' ], below: [], main: [ 'vertical' ], above: [], menu: [ 'fixed' ] },
   width: 640,
   height: 480,
   scale: 2
});

export const rng = {
   dialogue: new CosmosValueRandom(),
   overworld: new CosmosValueRandom()
};

export const rooms = new CosmosRegistry<string, OutertaleRoom>(new OutertaleRoom());

export const spawn = 'f_hub';

export const speech = {
   emoters: {} as CosmosKeyed<CosmosSprite>,
   holders: [] as (() => void)[],
   state: {
      face_value: null as CosmosSprite | null,
      get face () {
         return speech.state.face_value;
      },
      set face (v) {
         speech.state.face_value = v;
         for (const holder of speech.holders) {
            holder();
         }
      },
      get fontName1 () {
         return speech.state.preset.fontName1;
      },
      get fontName2 () {
         return speech.state.preset.fontName2;
      },
      get fontSize1 () {
         return speech.state.preset.fontSize1;
      },
      get fontSize2 () {
         return speech.state.preset.fontSize2;
      },
      preset: new OutertaleSpeechPreset()
   },
   presets: new CosmosRegistry<string, OutertaleSpeechPreset>(new OutertaleSpeechPreset()),
   targets: new Set<CosmosSprite>()
};

export const typer = new CosmosTyper({ renderer });

addEventListener('unload', () => {
   game.reload || (location.href = location.origin + location.pathname);
});

CosmosUtils.status(`LOAD MODULE: CORE (${Math.floor(performance.now()) / 1000})`, { color: '#07f' });
