/////////////////////////////////////////////////////////////////////////////
//                                                                         //
//    ::::::::   ::::::::   ::::::::  ::::::::::::  ::::::::   ::::::::    //
//   :+:    :+: :+:    :+: :+:        :+:  ::  :+: :+:    :+: :+:          //
//   +:+        +:+    +:+ +:+        +:+  ::  +:+ +:+    +:+ +:+          //
//   +#+        +#+    +#+  #++::++#  +#+  ++  +#+ +#+    +#+  #++::++#    //
//   +#+        +#+    +#+        +#+ +#+  ++  +#+ +#+    +#+        +#+   //
//   #+#    #+# #+#    #+#        #+# #+#      #+# #+#    #+#        #+#   //
//    ########   ########   ########  ###      ###  ########   ########    //
//                                                                         //
//// powered by pixi.js /////////////////////////////////////////////////////

import { colord } from '@pixi/colord';
import { AdvancedBloomFilter } from '@pixi/filter-advanced-bloom';
import { DropShadowFilter } from '@pixi/filter-drop-shadow';
import { GlowFilter } from '@pixi/filter-glow';
import { ZoomBlurFilter } from '@pixi/filter-zoom-blur';
import {
   Application,
   BLEND_MODES,
   BaseTexture,
   BitmapFont,
   ColorMatrixFilter,
   Container,
   Extract,
   Filter,
   Graphics,
   Rectangle,
   Renderer,
   Sprite,
   TextMetrics,
   TextStyle,
   Texture,
   UPDATE_PRIORITY
} from 'pixi.js';
import { Polygon, Vector, testPolygonPolygon } from 'sat';

// types
export type CosmosArea = CosmosPointSimple & CosmosDimensions;
export type CosmosBaseEvents = { tick: []; render: []; 'pre-render': [] };
export type CosmosCharacterPreset = { [x in 'walk' | 'talk']: { [y in CosmosDirection]: CosmosSprite } };
export type CosmosColor = [number, number, number, number];
export type CosmosCrop = { [x in CosmosFace]: number };
export type CosmosDaemonRouter = (input: AudioNode) => void;
export type CosmosDefined<A> = Exclude<A, null | undefined | void>;
export type CosmosDirection = 'down' | 'left' | 'right' | 'up';
export type CosmosEventHandler<A extends any, B extends any[] = any> =
   | CosmosEventListener<A, B>
   | { listener: CosmosEventListener<A, B>; priority: number };
export type CosmosEventListener<A extends any, B extends any[]> = (this: A, ...data: B) => any;
export type CosmosFace = 'bottom' | 'left' | 'right' | 'top';
export type CosmosFont = { glyphs: { [x in string]: CosmosGlyph }; shift: CosmosPointSimple; size: number };
export type CosmosFontData = { name: string; glyphs: CosmosGlyphData[]; shift: CosmosPointSimple; size: number };
export type CosmosGlyph = { margin: number; metrics: CosmosArea; texture: Texture };
export type CosmosGlyphData = { area: CosmosArea; code: string; margin: number; metrics: CosmosArea };
export type CosmosKeyed<A = any, B extends string = any> = { [x in B]: A };
export type CosmosMetadata = { [k: string]: any };
export type CosmosNot<A, B extends string> = { [x in Exclude<keyof CosmosDefined<A>, B>]?: CosmosDefined<A>[x] };
export type CosmosPixelShader = (color: CosmosColor, position: CosmosPointSimple, pixels: Uint8Array) => CosmosColor;
export type CosmosProvider<A, B extends any[] = []> = A | ((...args: B) => A);
export type CosmosRegion = [CosmosPointSimple, CosmosPointSimple];
export type CosmosRendererLayerModifier = 'fixed' | 'vertical';
export type CosmosResult<A> = A extends () => Promise<infer B> ? B : never;
export type CosmosTransform = [CosmosPoint, number, CosmosPoint];

// interfaces
export interface CosmosAnchoredObjectProperties<A extends CosmosMetadata = CosmosMetadata>
   extends CosmosObjectProperties<A> {
   anchor?: Partial<CosmosPointSimple> | number;
}

export interface CosmosAnimationInfo {
   frames: {
      duration: number;
      frame: { x: number; y: number; w: number; h: number };
      spriteSourceSize: CosmosPointSimple;
   }[];
   meta: { frameTags: { name: string; from: number; to: number }[] };
}
export interface CosmosAnimationProperties<A extends CosmosMetadata = CosmosMetadata>
   extends CosmosNot<CosmosSpriteProperties<A>, 'crop' | 'frames'> {
   extrapolate?: boolean;
   resources?: CosmosAnimationResources | null;
   subcrop?: Partial<CosmosCrop>;
}

export interface CosmosBaseProperties extends Partial<CosmosStyle> {
   alpha?: number;
   area?: Rectangle | null;
   filters?: Filter[] | null;
   position?: Partial<CosmosPointSimple> | number;
   rotation?: number;
   scale?: Partial<CosmosPointSimple> | number;
}

export interface CosmosCharacterProperties<A extends CosmosMetadata = CosmosMetadata>
   extends CosmosNot<CosmosEntityProperties<A>, 'sprites'> {
   key: string;
   preset: CosmosCharacterPreset;
}

export interface CosmosDaemonProperties {
   context: AudioContext;
   gain?: number;
   loop?: boolean;
   max?: number;
   rate?: number;
   router?: CosmosDaemonRouter;
}

export interface CosmosDimensions {
   height: number;
   width: number;
}

export interface CosmosEntityProperties<A extends CosmosMetadata = CosmosMetadata> extends CosmosSizedObjectProperties {
   face?: CosmosDirection;
   metadata?: A;
   sprites?: { [x in CosmosDirection]?: CosmosSprite };
   step?: number;
}

export interface CosmosObjectProperties<A extends CosmosMetadata = CosmosMetadata> extends CosmosBaseProperties {
   acceleration?: number;
   gravity?: Partial<CosmosPointSimple> | number;
   metadata?: A;
   offsets?: (Partial<CosmosPointSimple> | number)[];
   objects?: CosmosObject[];
   parallax?: Partial<CosmosPointSimple> | number;
   priority?: number;
   spin?: number;
   velocity?: Partial<CosmosPointSimple> | number;
}

export interface CosmosPlayerProperties<A extends CosmosMetadata = CosmosMetadata> extends CosmosEntityProperties<A> {
   extent?: Partial<CosmosPointSimple> | number;
}

export interface CosmosPointSimple {
   x: number;
   y: number;
}

export interface CosmosRendererLayer {
   active: boolean;
   container: Container;
   dirty: boolean;
   modifiers: boolean[];
   objects: CosmosObject[];
}

export interface CosmosRendererProperties<A extends string = string> extends CosmosBaseProperties {
   auto?: boolean;
   freecam?: boolean;
   height?: number;
   layers?: { [x in A]: CosmosRendererLayerModifier[] };
   shake?: number;
   shake_limit?: number;
   speed?: number;
   ticks?: number;
   width?: number;
   wrapper?: HTMLElement | string | null;
   zoom?: number;
}

export interface CosmosSizedObjectProperties<A extends CosmosMetadata = CosmosMetadata>
   extends CosmosAnchoredObjectProperties<A> {
   size?: Partial<CosmosPointSimple> | number;
}

export interface CosmosSpriteProperties<A extends CosmosMetadata = CosmosMetadata>
   extends CosmosAnchoredObjectProperties<A> {
   active?: boolean;
   crop?: Partial<CosmosCrop>;
   duration?: number;
   frames?: (CosmosImage | null)[];
   index?: number;
   reverse?: boolean;
   step?: number;
}

export interface CosmosStyle {
   blend: BLEND_MODES;
   border: number;
   fill: string;
   fontName: string;
   fontSize: number;
   stroke: string;
   tint: number;
}

export interface CosmosTextProperties<A extends CosmosMetadata = CosmosMetadata>
   extends CosmosAnchoredObjectProperties<A> {
   charset?: string;
   content?: string;
   phase?: number;
   plain?: boolean;
   spacing?: Partial<CosmosPointSimple> | number;
}

export interface CosmosTyperProperties {
   chunksize?: number;
   interval?: number;
   magic?: string;
   sounds?: CosmosDaemon[];
   threshold?: number;
   renderer?: CosmosRenderer;
   variables?: CosmosKeyed;
}

export interface CosmosValueSimple {
   value: number;
}

// classes - first level
export class CosmosAsset<A = any> {
   static tracked = new Set<CosmosAsset>();
   static collect () {
      const owners: CosmosAsset[] = [];
      for (const value of this.tracked.values()) {
         CosmosUtils.chain(value, (input, loop) => {
            input.owners.includes(input) && !owners.includes(input) && owners.push(input);
            if (input.owners.length !== 0) {
               for (const owner of input.owners) {
                  owner !== input && owner instanceof CosmosAsset && loop(owner);
               }
            }
         });
      }
      return owners;
   }
   #asset = { destroyed: false, promise: null as Promise<A> | null, value: null as A | null };
   name = 'UNKNOWN';
   owners: any[] = [];
   get loaded () {
      return this.#asset.value !== null;
   }
   get value () {
      if (this.#asset.value === null) {
         queueMicrotask(() => {
            console.trace(this);
            throw `UNLOADED ${this.name}`;
         });
      }
      return this.#asset.value;
   }
   destroy () {
      if (!this.#asset.destroyed) {
         this.#asset.destroyed = true;
         this.owners = [];
         this.unload();
      }
   }
   async load (owner: any = this) {
      if (!this.#asset.destroyed) {
         this.owners.includes(owner) || this.owners.push(owner);
         const promise = (this.#asset.promise ??= this.loader().then(value => {
            this.#asset.promise === promise && (this.#asset.value = value);
            CosmosAsset.tracked.add(this);
            return value;
         }));
         await promise;
      }
   }
   async loader () {
      return null as A;
   }
   unload (owner: any = this) {
      this.owners.includes(owner) && this.owners.splice(this.owners.indexOf(owner), 1);
      if (this.owners.length === 0) {
         this.#asset.value = this.#asset.promise = null;
         CosmosAsset.tracked.delete(this);
         this.unloader();
      }
   }
   unloader () {}
}

export class CosmosAtlas<A extends string = string> {
   navigators = new CosmosRegistry<string, CosmosNavigator<A>>(new CosmosNavigator<A>());
   target: A | null = null;
   constructor (map: { [x in A]: CosmosNavigator<A> } = {} as { [x in A]: CosmosNavigator<A> }) {
      this.navigators.register(map);
   }
   attach<B extends CosmosRenderer<any>> (
      renderer: B,
      layer: B extends CosmosRenderer<infer C> ? C : never,
      ...targets: A[]
   ) {
      for (const target of targets) {
         renderer.attach(layer, ...this.navigators.of(target).objects);
      }
   }
   detach<B extends CosmosRenderer<any>> (
      renderer: B,
      layer: B extends CosmosRenderer<infer C> ? C : never,
      ...targets: A[]
   ) {
      for (const target of targets) {
         renderer.detach(layer, ...this.navigators.of(target).objects);
      }
   }
   next () {
      const nav = this.navigator();
      if (nav) {
         const result = CosmosUtils.provide(nav.next, nav) as A | null | void;
         result === void 0 || this.switch(result);
      }
   }
   prev () {
      const nav = this.navigator();
      if (nav) {
         const result = CosmosUtils.provide(nav.prev, nav) as A | null | void;
         result === void 0 || this.switch(result);
      }
   }
   navigator () {
      return this.target === null ? null : this.navigators.of(this.target);
   }
   seek (direction: CosmosDirection) {
      const nav = this.navigator();
      if (nav) {
         const prev = nav.selection();
         const x = direction === 'left' ? -1 : direction === 'right' ? 1 : 0;
         const y = direction === 'up' ? -1 : direction === 'down' ? 1 : 0;
         const row = CosmosUtils.provide(nav.grid, nav);
         nav.position.x = Math.min(nav.position.x, row.length - 1) + (nav.flip ? y : x);
         if (nav.position.x < 0) {
            nav.position.x = row.length - 1;
         } else if (row.length <= nav.position.x) {
            nav.position.x = 0;
         }
         const column = row[nav.position.x] ?? [];
         nav.position.y = Math.min(nav.position.y, column.length - 1) + (nav.flip ? x : y);
         if (nav.position.y < 0) {
            nav.position.y = column.length - 1;
         } else if (column.length <= nav.position.y) {
            nav.position.y = 0;
         }
         const next = nav.selection();
         prev === next || nav.fire('change', prev, next);
      }
   }
   switch (target: A | null) {
      if (target !== void 0) {
         let next: CosmosNavigator<A> | null = null;
         if (typeof target === 'string') {
            next = this.navigators.of(target);
         }
         const nav = this.navigator();
         nav && nav.fire('to', target, nav);
         const current = this.target;
         this.target = target;
         next && next.fire('from', current, nav);
      }
   }
}

export class CosmosCache<A, B> extends Map<A, B> {
   compute: (key: A) => B;
   constructor (compute: (key: A) => B) {
      super();
      this.compute = compute;
   }
   of (key: A) {
      this.has(key) || this.set(key, this.compute(key));
      return this.get(key)!;
   }
}

export class CosmosDaemon {
   audio: CosmosAudio;
   context: AudioContext;
   gain: number;
   readonly instances = [] as CosmosInstance[];
   loop: boolean;
   max: number;
   rate: number;
   router: CosmosDaemonRouter;
   constructor (
      audio: CosmosAudio,
      {
         context,
         gain = 1,
         loop = false,
         max = Infinity,
         rate = 1,
         router = input => void input.connect(input.context.destination)
      }: CosmosDaemonProperties
   ) {
      this.audio = audio;
      this.context = context;
      this.gain = gain;
      this.loop = loop;
      this.max = max;
      this.rate = rate;
      this.router = router;
   }
   instance (renderer: CosmosRenderer, { offset = 0 } = {}) {
      return new CosmosInstance(this, renderer, { offset });
   }
   stop () {
      for (const instance of this.instances.slice()) {
         instance.stop();
      }
      return this;
   }
}

export class CosmosEventHost<A extends { [x in string]: any[] } = {}> {
   events: { [x in keyof A]?: { handlers: CosmosEventHandler<any, A[x]>[]; priorities: number } } = {};
   fire<B extends keyof A> (name: B, ...data: A[B]) {
      const event = this.events[name];
      if (event && event.handlers.length !== 0) {
         if (event.handlers.length > 1) {
            return event.handlers.slice().map(handler => {
               if (typeof handler === 'function') {
                  return handler.call(this, ...data);
               } else {
                  return handler.listener.call(this, ...data);
               }
            });
         } else {
            const handler = event.handlers[0];
            if (typeof handler === 'function') {
               return [ handler.call(this, ...data) ];
            } else {
               return [ handler.listener.call(this, ...data) ];
            }
         }
      } else {
         return [];
      }
   }
   off<B extends keyof A> (name: B, handler: CosmosEventHandler<this, A[B]>) {
      const event = this.events[name];
      if (event) {
         const index = event.handlers.indexOf(handler);
         if (index > -1) {
            event.handlers.splice(index, 1);
            if (typeof handler === 'object' && handler.priority !== 0) {
               event.priorities--;
            }
         }
      }
      return this;
   }
   on<B extends keyof A>(name: B): Promise<A[B]>;
   on<B extends keyof A>(name: B, priority: number): Promise<A[B]>;
   on<B extends keyof A>(name: B, listener: CosmosEventHandler<this, A[B]>): this;
   on<B extends keyof A> (name: B, a2: number | CosmosEventHandler<this, A[B]> = 0) {
      if (typeof a2 === 'number') {
         return new Promise(resolve => {
            const listener = (...data: A[B]) => {
               this.off(name, singleton);
               resolve(data);
            };
            const singleton = a2 === 0 ? listener : { listener, priority: a2 };
            this.on(name, singleton);
         });
      } else {
         const event = (this.events[name] ??= { handlers: [] as CosmosEventHandler<this, A[B]>[], priorities: 0 });
         event.handlers.push(a2);
         if (typeof a2 === 'object' && a2.priority !== 0) {
            event.priorities++;
         }
         if (event.priorities > 0) {
            event.handlers.sort(
               (handler1, handler2) =>
                  (typeof handler1 === 'function' ? 0 : handler1.priority) -
                  (typeof handler2 === 'function' ? 0 : handler2.priority)
            );
         }
         return this;
      }
   }
}

export class CosmosKeyboardInput extends CosmosEventHost<{ down: [string | null]; up: [string | null] }> {
   #input = { codes: [] as string[], force1: false, force2: false };
   get force1 () {
      return this.#input.force1;
   }
   set force1 (value) {
      if (value !== this.#input.force1) {
         this.#input.force1 = value;
         if (value) {
            this.fire('down', null);
         } else {
            this.fire('up', null);
         }
      }
   }
   get force2 () {
      return this.#input.force2;
   }
   set force2 (value) {
      if (value !== this.#input.force2) {
         this.#input.force2 = value;
         if (value) {
            this.fire('down', null);
         } else {
            this.fire('up', null);
         }
      }
   }
   constructor (target: HTMLElement | null, ...codes: string[]) {
      super();
      ((target ?? window) as HTMLElement).addEventListener('keyup', ({ code }) => {
         if (codes.includes(code) && this.#input.codes.includes(code)) {
            this.#input.codes.splice(this.#input.codes.indexOf(code));
            this.fire('up', code);
         }
      });
      ((target ?? window) as HTMLElement).addEventListener('keydown', ({ code }) => {
         if (codes.includes(code) && !this.#input.codes.includes(code)) {
            this.#input.codes.push(code);
            this.fire('down', code);
         }
      });
   }
   active () {
      return this.#input.force1 || this.#input.force2 || this.#input.codes.length !== 0;
   }
   reset () {
      this.#input.codes = [];
   }
}

export class CosmosRegistry<A extends string, B> extends Map<A, B> {
   placeholder: B;
   constructor (placeholder: B) {
      super();
      this.placeholder = placeholder;
   }
   of (key: A) {
      return this.has(key) ? this.get(key)! : this.placeholder;
   }
   register<C extends B>(key: A, value: C): C;
   register<C extends B>(properties: CosmosKeyed<C, A>): this;
   register<C extends B> (a1: A | CosmosKeyed<C, A>, value?: C) {
      if (typeof a1 === 'string') {
         this.set(a1, value!);
         return value!;
      } else {
         for (const key in a1) {
            this.set(key, a1[key]);
         }
         return this;
      }
   }
}

export class CosmosPoint implements CosmosPointSimple {
   task: (() => void) | undefined = void 0;
   x: number;
   y: number;
   get angle () {
      return this.angleFrom(0);
   }
   set angle (value) {
      this.set(CosmosMath.ray(value, this.extent));
   }
   get extent () {
      return this.extentOf(0);
   }
   set extent (value) {
      this.set(CosmosMath.ray(this.angle, value));
   }
   constructor();
   constructor(a: number, b: number);
   constructor(a: Partial<CosmosPointSimple> | number);
   constructor (a: Partial<CosmosPointSimple> | number = 0, b = a as number) {
      if (typeof a === 'number') {
         this.x = a;
         this.y = b;
      } else {
         this.x = a.x ?? 0;
         this.y = a.y ?? 0;
      }
   }
   abs () {
      return new CosmosPoint(Math.abs(this.x), Math.abs(this.y));
   }
   add(a: number, b: number): CosmosPoint;
   add(a: Partial<CosmosPointSimple> | number): CosmosPoint;
   add (a: Partial<CosmosPointSimple> | number, b = a as number) {
      if (typeof a === 'number') {
         return new CosmosPoint(this.x + a, this.y + b);
      } else {
         return this.add(a.x ?? 0, a.y ?? 0);
      }
   }
   angleFrom(a: number, b: number): number;
   angleFrom(a: Partial<CosmosPointSimple> | number): number;
   angleFrom (a: Partial<CosmosPointSimple> | number, b = a as number) {
      if (typeof a === 'number') {
         if (this.y === b) {
            if (this.x < a) {
               return 180;
            } else {
               return 0;
            }
         } else if (this.x === a) {
            if (this.y < b) {
               return 270;
            } else {
               return 90;
            }
         } else {
            return Math.atan2(this.y - b, this.x - a) / (Math.PI / 180);
         }
      } else {
         return this.angleFrom(a.x ?? 0, a.y ?? 0);
      }
   }
   angleTo(a: number, b: number): number;
   angleTo(a: Partial<CosmosPointSimple> | number): number;
   angleTo (a: Partial<CosmosPointSimple> | number, b = a as number) {
      if (typeof a === 'number') {
         return this.angleFrom(a, b) + 180;
      } else {
         return this.angleTo(a.x ?? 0, a.y ?? 0);
      }
   }
   ceil(): CosmosPoint;
   ceil(a: number, b: number): CosmosPoint;
   ceil(a: Partial<CosmosPointSimple> | number): CosmosPoint;
   ceil (a: Partial<CosmosPointSimple> | number = 1, b = a as number) {
      if (typeof a === 'number') {
         return new CosmosPoint(Math.ceil(this.x * a) / a, Math.ceil(this.y * b) / b);
      } else if (a) {
         return this.ceil(a.x ?? 0, a.y ?? 0);
      }
   }
   clamp (min: Partial<CosmosPointSimple> | number, max: Partial<CosmosPointSimple> | number) {
      return new CosmosPoint(
         Math.min(
            Math.max(this.x, typeof min === 'number' ? min : min.x ?? -Infinity),
            typeof max === 'number' ? max : max.x ?? Infinity
         ),
         Math.min(
            Math.max(this.y, typeof min === 'number' ? min : min.y ?? -Infinity),
            typeof max === 'number' ? max : max.y ?? Infinity
         )
      );
   }
   clone () {
      return new CosmosPoint(this);
   }
   divide(a: number, b: number): CosmosPoint;
   divide(a: Partial<CosmosPointSimple> | number): CosmosPoint;
   divide (a: Partial<CosmosPointSimple> | number, b = a as number) {
      if (typeof a === 'number') {
         return new CosmosPoint(this.x / a, this.y / b);
      } else {
         return this.divide(a.x ?? 0, a.y ?? 0);
      }
   }
   endpoint(a: number, b: number): CosmosPoint;
   endpoint(a: { angle?: number; extent?: number } | number): CosmosPoint;
   endpoint (a: { angle?: number; extent?: number } | number, b = a as number) {
      if (typeof a === 'number') {
         if (b === 0) {
            return this.clone();
         } else {
            switch (a % 360) {
               case 0:
                  return this.add(b, 0);
               case 45:
               case -315:
                  return this.add(b * Math.SQRT1_2, b * Math.SQRT1_2);
               case 90:
               case -270:
                  return this.add(0, b);
               case 135:
               case -225:
                  return this.add(-b * Math.SQRT1_2, b * Math.SQRT1_2);
               case 180:
               case -180:
                  return this.add(-b, 0);
               case 225:
               case -135:
                  return this.add(-b * Math.SQRT1_2, -b * Math.SQRT1_2);
               case 270:
               case -90:
                  return this.add(0, -b);
               case 315:
               case -45:
                  return this.add(b * Math.SQRT1_2, -b * Math.SQRT1_2);
               default:
                  return this.add(CosmosMath.ray(a, b));
            }
         }
      } else {
         return this.endpoint(a.angle ?? 0, a.extent ?? 0);
      }
   }
   extentOf(a: number, b: number): number;
   extentOf(a: Partial<CosmosPointSimple> | number): number;
   extentOf (a: Partial<CosmosPointSimple> | number, b = a as number) {
      if (typeof a === 'number') {
         if (this.x === a) {
            return Math.abs(b - this.y);
         } else if (this.y === b) {
            return Math.abs(a - this.x);
         } else {
            return Math.sqrt((a - this.x) ** 2 + (b - this.y) ** 2);
         }
      } else {
         return this.extentOf(a.x ?? 0, a.y ?? 0);
      }
   }
   floor(): CosmosPoint;
   floor(a: number, b: number): CosmosPoint;
   floor(a: Partial<CosmosPointSimple> | number): CosmosPoint;
   floor (a: Partial<CosmosPointSimple> | number = 1, b = a as number) {
      if (typeof a === 'number') {
         return new CosmosPoint(Math.floor(this.x * a) / a, Math.floor(this.y * b) / b);
      } else if (a) {
         return this.floor(a.x ?? 0, a.y ?? 0);
      }
   }
   async modulate (renderer: CosmosRenderer, duration: number, ...points: (Partial<CosmosPointSimple> | number)[]) {
      let active = true;
      this.task?.();
      this.task = () => (active = false);
      const base = { x: this.x, y: this.y };
      const origin = renderer.ticks;
      const subpointsX = points.map(point => (typeof point === 'number' ? point : point.x ?? base.x));
      const subpointsY = points.map(point => (typeof point === 'number' ? point : point.y ?? base.y));
      const trueDuration = Math.round(duration / CosmosMath.FRAME_2);
      await renderer.when(() => {
         if (active) {
            const elapsed = renderer.ticks - origin;
            if (elapsed < trueDuration) {
               this.x = CosmosMath.bezier(elapsed / trueDuration, base.x, ...subpointsX);
               this.y = CosmosMath.bezier(elapsed / trueDuration, base.y, ...subpointsY);
               return false;
            } else {
               this.task = void 0;
               this.x = CosmosMath.bezier(1, base.x, ...subpointsX);
               this.y = CosmosMath.bezier(1, base.y, ...subpointsY);
            }
         }
         return true;
      });
   }
   multiply(a: number, b: number): CosmosPoint;
   multiply(a: Partial<CosmosPointSimple> | number): CosmosPoint;
   multiply (a: Partial<CosmosPointSimple> | number, b = a as number) {
      if (typeof a === 'number') {
         return new CosmosPoint(this.x * a, this.y * b);
      } else {
         return this.multiply(a.x ?? 0, a.y ?? 0);
      }
   }
   round(): CosmosPoint;
   round(a: number, b: number): CosmosPoint;
   round(a: Partial<CosmosPointSimple> | number): CosmosPoint;
   round (a: Partial<CosmosPointSimple> | number = 1, b = a as number) {
      if (typeof a === 'number') {
         return new CosmosPoint(Math.round(this.x * a) / a, Math.round(this.y * b) / b);
      } else if (a) {
         return this.round(a.x ?? 0, a.y ?? 0);
      }
   }
   set(a: number, b: number): CosmosPoint;
   set(a: Partial<CosmosPointSimple> | number): CosmosPoint;
   set (a: Partial<CosmosPointSimple> | number, b = a as number) {
      if (typeof a === 'number') {
         this.x = a;
         this.y = b;
         return this;
      } else {
         return this.set(a.x ?? 0, a.y ?? 0);
      }
   }
   shift (angle: number, extent = 0, origin: Partial<CosmosPointSimple> | number = 0): CosmosPoint {
      if (angle % 360 === 0 && extent === 0) {
         return this.clone();
      } else if (origin instanceof CosmosPoint) {
         return origin.endpoint(origin.angleTo(this) + angle, origin.extentOf(this) + extent);
      } else {
         return this.shift(angle, extent, new CosmosPoint(origin));
      }
   }
   async step (renderer: CosmosRenderer, speed: number, ...targets: (Partial<CosmosPointSimple> | number)[]) {
      for (const target of targets) {
         if (typeof target === 'number') {
            await this.modulate(renderer, (this.extentOf(target) / speed) * CosmosMath.FRAME, target);
         } else {
            await this.modulate(
               renderer,
               (this.extentOf(target.x ?? this.x, target.y ?? this.y) / speed) * CosmosMath.FRAME,
               target
            );
         }
      }
   }
   subtract(a: number, b: number): CosmosPoint;
   subtract(a: Partial<CosmosPointSimple> | number): CosmosPoint;
   subtract (a: Partial<CosmosPointSimple> | number, b = a as number) {
      if (typeof a === 'number') {
         return new CosmosPoint(this.x - a, this.y - b);
      } else {
         return this.subtract(a.x ?? 0, a.y ?? 0);
      }
   }
   value () {
      return { x: this.x, y: this.y };
   }
}

export class CosmosValue implements CosmosValueSimple {
   task: (() => void) | undefined = void 0;
   value: number;
   constructor (value: CosmosValueSimple | number = 0) {
      if (typeof value === 'number') {
         this.value = value;
      } else {
         this.value = value.value;
      }
   }
   async modulate (renderer: CosmosRenderer, duration: number, ...points: number[]) {
      let active = true;
      this.task?.();
      this.task = () => (active = false);
      const base = this.value;
      const origin = renderer.ticks;
      const trueDuration = Math.round(duration / CosmosMath.FRAME_2);
      await renderer.when(() => {
         if (active) {
            const elapsed = renderer.ticks - origin;
            if (elapsed < trueDuration) {
               this.value = CosmosMath.bezier(elapsed / trueDuration, base, ...points);
               return false;
            } else {
               this.task = void 0;
               this.value = points.length !== 0 ? points[points.length - 1] : base;
            }
         }
         return true;
      });
   }
   set (a: CosmosValueSimple | number): CosmosValue {
      if (typeof a === 'number') {
         this.value = a;
         return this;
      } else {
         return this.set(a.value);
      }
   }
   async step (renderer: CosmosRenderer, speed: number, ...targets: number[]) {
      for (const target of targets) {
         await this.modulate(renderer, (Math.abs(target - this.value) / speed) * CosmosMath.FRAME, target);
      }
   }
}

export class CosmosValueLinked implements CosmosValue {
   task: (() => void) | undefined = void 0;
   target: CosmosValueSimple;
   get value () {
      return this.target.value;
   }
   set value (value) {
      this.target.value = value;
   }
   constructor (target: CosmosValueSimple) {
      this.target = target;
   }
   async modulate (renderer: CosmosRenderer, duration: number, ...points: number[]) {
      let active = true;
      this.task?.();
      this.task = () => (active = false);
      const base = this.value;
      const origin = renderer.ticks;
      const trueDuration = Math.round(duration / CosmosMath.FRAME_2);
      await renderer.when(() => {
         if (active) {
            const elapsed = renderer.ticks - origin;
            if (elapsed < trueDuration) {
               this.value = CosmosMath.bezier(elapsed / trueDuration, base, ...points);
               return false;
            } else {
               this.task = void 0;
               this.value = points.length !== 0 ? points[points.length - 1] : base;
            }
         }
         return true;
      });
   }
   set (a: CosmosValueSimple | number): CosmosValue {
      if (typeof a === 'number') {
         this.value = a;
         return this;
      } else {
         return this.set(a.value);
      }
   }
   async step (renderer: CosmosRenderer, speed: number, ...targets: number[]) {
      for (const target of targets) {
         await this.modulate(renderer, (Math.abs(target - this.value) / speed) * CosmosMath.FRAME, target);
      }
   }
}

export class Vow {
   active = true;
   callbacks: (() => void)[] = [];
   then (callback: () => void) {
      if (this.active) {
         this.callbacks.push(callback);
      } else {
         callback();
      }
   }
   confirm () {
      if (this.active) {
         this.active = false;
         for (const callback of this.callbacks) {
            callback();
         }
      }
   }
}

// classes - second level
export class CosmosAudio extends CosmosAsset<AudioBuffer> {
   static cache = new CosmosCache((key: string) => {
      return new Promise<AudioBuffer>(resolve => {
         const errorListener = function () {
            xhr.removeEventListener('error', errorListener);
            xhr.removeEventListener('load', loadListener);
            resolve(CosmosAudioUtils.EMPTY);
         };
         const loadListener = async function () {
            xhr.removeEventListener('error', errorListener);
            xhr.removeEventListener('load', loadListener);
            if (!CosmosAudio.cache.has(key)) {
               resolve(CosmosAudioUtils.EMPTY);
               return;
            }
            const context = CosmosAudioUtils.getContext();
            const buffer1 = await context.decodeAudioData(xhr.response);
            if (!CosmosAudio.cache.has(key)) {
               resolve(CosmosAudioUtils.EMPTY);
               return;
            }
            const buffer2 = context.createBuffer(buffer1.numberOfChannels + 1, buffer1.length, buffer1.sampleRate);
            let index = buffer2.length;
            const source = new Float32Array(index);
            while (index-- !== 0) {
               source[index] = index;
               if (index !== 0 && index << 16 === 0) {
                  if (!CosmosAudio.cache.has(key)) {
                     resolve(CosmosAudioUtils.EMPTY);
                     return;
                  } else {
                     await new Promise<void>(resolve => requestAnimationFrame(() => resolve()));
                  }
               }
            }
            let channel = buffer1.numberOfChannels;
            buffer2.copyToChannel(source, channel);
            while (channel-- !== 0) {
               buffer2.copyToChannel(buffer1.getChannelData(channel), channel);
            }
            resolve(buffer2);
         };
         const xhr = new XMLHttpRequest();
         xhr.responseType = 'arraybuffer';
         xhr.timeout = 0;
         xhr.addEventListener('error', errorListener);
         xhr.addEventListener('load', loadListener);
         xhr.open('GET', key);
         xhr.send();
      });
   });
   source: string;
   get value () {
      return super.value ?? CosmosAudioUtils.EMPTY;
   }
   constructor (source: string) {
      super();
      this.source = source;
   }
   async loader () {
      return await CosmosAudio.cache.of(this.source);
   }
   unloader () {
      CosmosAudio.cache.delete(this.source);
   }
}

export class CosmosBase<A extends CosmosBaseEvents = CosmosBaseEvents>
   extends CosmosEventHost<A>
   implements CosmosPointSimple, Partial<CosmosStyle>
{
   alpha: CosmosValue;
   blend: BLEND_MODES | undefined;
   border: number | undefined;
   readonly container = new Container();
   fill: string | undefined;
   fontName: string | undefined;
   fontSize: number | undefined;
   position: CosmosPoint;
   rotation: CosmosValue;
   scale: CosmosPoint;
   stroke: string | undefined;
   tint: number | undefined;
   area: Rectangle | null;
   get filters () {
      return this.container.filters;
   }
   set filters (value) {
      this.container.filters = value;
   }
   /** @deprecated */
   get x () {
      return this.position.x as any;
   }
   /** @deprecated */
   get y () {
      return this.position.y as any;
   }
   constructor ({
      alpha = 1,
      area = null,
      blend = void 0,
      border = void 0,
      fill = void 0,
      filters = null,
      fontName = void 0,
      fontSize = void 0,
      position = 0,
      rotation = 0,
      scale = 1,
      stroke = void 0,
      tint = void 0
   }: CosmosBaseProperties = {}) {
      super();
      this.alpha = new CosmosValue(alpha);
      this.area = area;
      this.blend = blend;
      this.border = border;
      this.fill = fill;
      this.filters = filters;
      this.fontName = fontName;
      this.fontSize = fontSize;
      if (typeof position === 'number') {
         this.position = new CosmosPoint(position ?? 0);
      } else {
         this.position = new CosmosPoint(position?.x ?? 0, position?.y ?? 0);
      }
      this.rotation = new CosmosValue(rotation);
      if (typeof scale === 'number') {
         this.scale = new CosmosPoint(scale ?? 1);
      } else {
         this.scale = new CosmosPoint(scale?.x ?? 1, scale?.y ?? 1);
      }
      this.stroke = stroke;
      this.tint = tint;
   }
   updateFilters (filters = this.container.filters) {
      if (filters === null || filters.length === 0) {
         if (this.container.filterArea !== null) {
            this.container.filterArea = null as any;
         }
      } else if (CosmosRenderer.fancy) {
         for (const filter of filters) {
            if (
               filter instanceof AdvancedBloomFilter ||
               filter instanceof DropShadowFilter ||
               filter instanceof GlowFilter ||
               filter instanceof ZoomBlurFilter
            ) {
               filter.enabled = true;
            }
         }
         if (this.container.filterArea !== this.area) {
            this.container.filterArea = this.area as Rectangle;
         }
      } else {
         let area: Rectangle | null = null;
         for (const filter of filters) {
            if (
               filter instanceof AdvancedBloomFilter ||
               filter instanceof DropShadowFilter ||
               filter instanceof GlowFilter ||
               filter instanceof ZoomBlurFilter
            ) {
               filter.enabled = false;
            } else {
               area = this.area;
            }
         }
         if (this.container.filterArea !== area) {
            this.container.filterArea = area as Rectangle;
         }
      }
   }
}

export class CosmosData<A extends any = any> extends CosmosAsset<A> {
   source: string;
   constructor (source: string) {
      super();
      this.source = source;
   }
   async loader () {
      return CosmosUtils.import<A>(this.source);
   }
}

export class CosmosEffect extends CosmosValueLinked {
   context: AudioContext;
   input: GainNode;
   output: AudioNode;
   throughput: GainNode;
   get value () {
      return this.target.value;
   }
   set value (value) {
      this.target.value = value;
      this.throughput.gain.value = 1 - value;
   }
   constructor (context: AudioContext, node: AudioNode, value: number) {
      const input = new GainNode(context);
      super(input.gain);
      this.context = context;
      this.input = input;
      this.output = node;
      this.throughput = new GainNode(context);
      this.input.connect(this.output);
      this.value = value;
   }
   connect (target: CosmosEffect | AudioNode | AudioContext) {
      if (target instanceof AudioContext) {
         this.output.connect(target.destination);
         this.throughput.connect(target.destination);
      } else if (target instanceof AudioNode) {
         this.output.connect(target);
         this.throughput.connect(target);
      } else {
         this.output.connect(target.input);
         this.output.connect(target.throughput);
         this.throughput.connect(target.input);
         this.throughput.connect(target.throughput);
      }
   }
   disconnect (target?: CosmosEffect | AudioNode | AudioContext) {
      if (target instanceof AudioContext) {
         this.output.disconnect(target.destination);
         this.throughput.disconnect(target.destination);
      } else if (target instanceof AudioNode) {
         this.output.disconnect(target);
         this.throughput.disconnect(target);
      } else if (target) {
         this.output.disconnect(target.input);
         this.output.disconnect(target.throughput);
         this.throughput.disconnect(target.input);
         this.throughput.disconnect(target.throughput);
      } else {
         this.output.disconnect();
      }
   }
}

export class CosmosImage extends CosmosAsset<Texture> {
   static cache = new CosmosCache((key: string) => {
      return new Promise<Texture>(resolve => {
         const errorListener = function () {
            xhr.removeEventListener('error', errorListener);
            xhr.removeEventListener('load', loadListener);
            resolve(Texture.EMPTY);
         };
         const loadListener = async function () {
            xhr.removeEventListener('error', errorListener);
            xhr.removeEventListener('load', loadListener);
            const texture = BaseTexture.from(URL.createObjectURL(xhr.response));
            await texture.resource.load();
            resolve(new Texture(texture));
         };
         const xhr = new XMLHttpRequest();
         xhr.responseType = 'blob';
         xhr.timeout = 0;
         xhr.addEventListener('error', errorListener);
         xhr.addEventListener('load', loadListener);
         xhr.open('GET', key);
         xhr.send();
      });
   });
   shader: CosmosPixelShader | null;
   source: string;
   sprites = new Set<Sprite>();
   get value () {
      return super.value ?? Texture.EMPTY;
   }
   constructor (source: string, shader: CosmosPixelShader | null = null) {
      super();
      this.shader = shader;
      this.source = source;
   }
   async loader () {
      let texture = await CosmosImage.cache.of(this.source);
      if (this.shader) {
         const pixels = CosmosImageUtils.texture2pixels(texture);
         let x = 0;
         let y = 0;
         while (y < texture.height) {
            while (x < texture.width) {
               const z = (x + y * texture.width) * 4;
               const color = this.shader([ pixels[z], pixels[z + 1], pixels[z + 2], pixels[z + 3] ], { x, y }, pixels);
               pixels[z] = color[0];
               pixels[z + 1] = color[1];
               pixels[z + 2] = color[2];
               pixels[z + 3] = color[3];
               x++;
            }
            x = 0;
            y++;
         }
         texture = CosmosImageUtils.pixels2texture(pixels, texture);
      }
      for (const sprite of this.sprites) {
         sprite.texture = texture;
      }
      return texture;
   }
   unloader () {
      CosmosImage.cache.delete(this.source);
      for (const sprite of this.sprites) {
         sprite.texture = Texture.EMPTY;
      }
   }
}

export class CosmosInstance extends CosmosEventHost<{ stop: [] }> {
   #instance: {
      gain: GainNode;
      offset: number | null;
      rate: number;
      samples: Float32Array;
      stop: () => void;
      tick: () => void;
   };
   analyser: AnalyserNode;
   daemon: CosmosDaemon;
   gain: CosmosValueLinked;
   rate: CosmosValueLinked;
   renderer: CosmosRenderer;
   source: AudioBufferSourceNode;
   get loop () {
      return this.source.loop;
   }
   set loop (value) {
      this.source.loop = value;
   }
   get position () {
      if (this.source.buffer === null) {
         return 0;
      } else {
         this.analyser.getFloatTimeDomainData(this.#instance.samples);
         const value = this.#instance.samples[0] / this.source.buffer.sampleRate;
         if (value === 0) {
            return this.#instance.offset ?? value;
         } else {
            this.#instance.offset = null;
            return value;
         }
      }
   }
   get stopped () {
      return this.source.buffer === null;
   }
   constructor (daemon: CosmosDaemon, renderer: CosmosRenderer, { offset = 0 } = {}) {
      super();

      const analyser = new AnalyserNode(daemon.context);
      const gain = new GainNode(daemon.context, { gain: daemon.gain });
      const source = new AudioBufferSourceNode(daemon.context, {
         buffer: daemon.audio.value,
         loop: daemon.loop,
         playbackRate: daemon.rate * renderer.speed.value
      });
      const splitter = new ChannelSplitterNode(daemon.context, { numberOfOutputs: source.buffer!.numberOfChannels });
      const merger = new ChannelMergerNode(daemon.context, { numberOfInputs: source.buffer!.numberOfChannels - 1 });

      daemon.router(gain);
      let channel = source.buffer!.numberOfChannels - 1;
      splitter.connect(analyser, channel);
      while (channel-- > 0) {
         splitter.connect(merger, channel, channel);
      }
      source.connect(splitter);
      merger.connect(gain);

      const $instance = {
         gain,
         offset,
         rate: daemon.rate,
         samples: new Float32Array(1),
         stop: () => this.stop(),
         tick: () => this.tick()
      };

      this.#instance = $instance;
      this.analyser = analyser;
      this.daemon = daemon;
      this.gain = new CosmosValueLinked(gain.gain);
      this.rate = new CosmosValueLinked({
         get value () {
            return $instance.rate;
         },
         set value (value) {
            $instance.rate = value;
            source.playbackRate.value = value * renderer.speed.value;
         }
      });
      this.renderer = renderer;
      this.source = source;

      renderer.speeders.push($instance.tick);
      daemon.instances.push(this);
      daemon.instances.length > daemon.max && daemon.instances[0].stop();
      source.addEventListener('ended', $instance.stop);
      source.start(0, offset);
   }
   stop () {
      if (!this.stopped) {
         this.source.removeEventListener('ended', this.#instance.stop);
         this.source.stop();
         this.source.buffer = null;
         this.#instance.gain.disconnect();
         this.source.disconnect();
         this.renderer.speeders.splice(this.renderer.speeders.indexOf(this.#instance.tick), 1);
         this.daemon.instances.splice(this.daemon.instances.indexOf(this), 1);
         this.fire('stop');
      }
   }
   tick () {
      const value = this.#instance.rate * this.renderer.speed.value;
      if (this.source.playbackRate.value !== value) {
         this.source.playbackRate.value = value;
      }
   }
}

export class CosmosInventory<A extends CosmosAsset[] = CosmosAsset[]> extends CosmosAsset<A> {
   #inventory: { assets: A };
   get value () {
      return this.#inventory.assets;
   }
   constructor (...assets: A) {
      super();
      this.#inventory = { assets };
   }
   async loader () {
      await Promise.all(this.#inventory.assets.map(asset => asset.load(this)));
      return this.#inventory.assets;
   }
   unloader () {
      for (const asset of this.#inventory.assets) {
         asset.unload(this);
      }
   }
}

export class CosmosMixer extends CosmosValueLinked {
   #mixer = {
      effects: (() => {
         const value = [] as CosmosEffect[];
         const proxy = new Proxy(value, {
            set: (target, key: `${number}`, value) => {
               const index = +key;
               if (Number.isInteger(index) && index > -1 && index < target.length + 1) {
                  this.update(index, value);
               }
               target[key] = value;
               return true;
            },
            deleteProperty: (target, key: `${number}`) => {
               const index = +key;
               if (Number.isInteger(index) && index > -1 && index < target.length + 1) {
                  this.update(index);
               }
               delete target[key];
               return true;
            }
         });
         return { value, proxy };
      })()
   };
   context: AudioContext;
   input: GainNode;
   output: GainNode;
   get $effects () {
      return this.#mixer.effects.value;
   }
   get effects () {
      return this.#mixer.effects.proxy;
   }
   set effects (value) {
      this.#mixer.effects.proxy.splice(0, this.#mixer.effects.value.length);
      this.#mixer.effects.proxy.push(...value);
   }
   constructor (context: AudioContext, effects: CosmosEffect[] = []) {
      const output = new GainNode(context);
      super(output.gain);
      this.context = context;
      this.input = new GainNode(context);
      this.output = output;
      this.input.connect(this.output);
      this.effects = effects;
   }
   update (index: number, value?: CosmosEffect) {
      const prevValue = (this.#mixer.effects.value[index - 1] ?? this.input) as CosmosEffect | GainNode;
      const currValue = this.#mixer.effects.value[index];
      const nextValue = (this.#mixer.effects.value[index + 1] ?? this.output) as CosmosEffect | GainNode;
      if (currValue) {
         if (prevValue instanceof CosmosEffect) {
            prevValue.disconnect(currValue);
         } else {
            prevValue.disconnect(currValue.input);
            prevValue.disconnect(currValue.throughput);
         }
         currValue.disconnect(nextValue);
      } else {
         prevValue.disconnect(nextValue);
      }
      if (value) {
         if (prevValue instanceof CosmosEffect) {
            prevValue.connect(value);
         } else {
            prevValue.connect(value.input);
            prevValue.connect(value.throughput);
         }
         value.connect(nextValue);
      } else {
         if (prevValue instanceof CosmosEffect) {
            prevValue.connect(nextValue);
         } else if (nextValue instanceof CosmosEffect) {
            prevValue.connect(nextValue.input);
            prevValue.connect(nextValue.throughput);
         } else {
            prevValue.connect(nextValue);
         }
      }
   }
}

export class CosmosNavigator<A extends string = string, B extends any = any> extends CosmosEventHost<
   { [x in 'from' | 'to']: [A | null | void, CosmosNavigator<A, B> | null] } & { change: [B, B] }
> {
   flip: boolean;
   grid: CosmosProvider<B[][], [CosmosNavigator<A, B>]>;
   next: CosmosProvider<A | null | void, [CosmosNavigator<A, B>]>;
   objects: CosmosObject[];
   position: CosmosPointSimple;
   prev: CosmosProvider<A | null | void, [CosmosNavigator<A, B>]>;
   constructor ({
      flip = false,
      grid = [],
      next = void 0,
      objects = [],
      position: { x = 0, y = 0 } = {},
      prev = void 0
   }: {
      flip?: boolean;
      grid?: CosmosProvider<B[][], [CosmosNavigator<A, B>]>;
      next?: CosmosProvider<A | null | void, [CosmosNavigator<A, B>]>;
      objects?: CosmosObject[];
      position?: Partial<CosmosPointSimple>;
      prev?: CosmosProvider<A | null | void, [CosmosNavigator<A, B>]>;
   } = {}) {
      super();
      this.flip = flip;
      this.grid = grid;
      this.next = next;
      this.objects = objects;
      this.position = { x, y };
      this.prev = prev;
   }
   selection () {
      return (CosmosUtils.provide(this.grid)[this.position.x] ?? [])[this.position.y];
   }
}

export class CosmosStringData<A extends string = string> extends CosmosAsset<A> {
   source: string;
   get value () {
      return (super.value ?? '') as A;
   }
   constructor (source: string) {
      super();
      this.source = source;
   }
   loader () {
      return new Promise<A>(resolve => {
         const errorListener = function () {
            xhr.removeEventListener('error', errorListener);
            xhr.removeEventListener('load', loadListener);
            resolve('' as A);
         };
         const loadListener = async function () {
            xhr.removeEventListener('error', errorListener);
            xhr.removeEventListener('load', loadListener);
            resolve(xhr.response);
         };
         const xhr = new XMLHttpRequest();
         xhr.responseType = 'text';
         xhr.timeout = 0;
         xhr.addEventListener('error', errorListener);
         xhr.addEventListener('load', loadListener);
         xhr.open('GET', this.source);
         xhr.send();
      });
   }
}

export class CosmosTyper extends CosmosEventHost<
   { [x in 'char' | 'code' | 'header' | 'text']: [string] } & { [x in 'empty' | 'idle' | 'read' | 'skip']: [] } & {
      inst: [CosmosInstance];
   }
> {
   #typer = {
      autonext: 0,
      autoread: 0,
      display: [] as string[],
      index: 0,
      index_chunk: 0,
      index_line: 0,
      lines: [] as string[],
      sfx: true,
      skippable: true,
      skippable_fast: false
   };
   chunksize = 1;
   content = '';
   interval: number;
   magic: string;
   mode: 'empty' | 'idle' | 'read' = 'empty';
   sounds: CosmosDaemon[] = [];
   task: (() => void) | undefined = void 0;
   threshold: number;
   renderer: CosmosRenderer;
   variables: CosmosKeyed<string, string>;
   constructor ({
      chunksize = 1,
      interval = 0,
      magic = '',
      sounds = [],
      threshold = 0,
      renderer = new CosmosRenderer(),
      variables = {}
   }: CosmosTyperProperties = {}) {
      super();
      this.chunksize = chunksize;
      this.interval = interval;
      this.magic = magic;
      this.sounds = sounds;
      this.threshold = threshold;
      this.renderer = renderer;
      this.variables = variables;
   }
   emit (content = this.#typer.display.join('')) {
      this.content = content;
      this.fire('text', content);
      if (
         this.#typer.sfx &&
         this.sounds.length !== 0 &&
         this.mode === 'read' &&
         content.length !== 0 &&
         !content[content.length - 1].match(/[\s\µ]/)
      ) {
         const instance = this.sounds[Math.floor(Math.random() * this.sounds.length)].instance(this.renderer);
         this.fire('inst', instance);
         this.#typer.sfx = false;
         if (this.threshold > 0) {
            this.renderer.pause(this.threshold * instance.daemon.audio.value!.duration * 1000).then(() => {
               this.#typer.sfx = true;
            });
         } else {
            this.renderer.post().then(async () => {
               await this.renderer.post();
               this.#typer.sfx = true;
            });
         }
      }
   }
   next (skip = false) {
      if (this.mode !== 'read' || this.#typer.lines.length <= this.#typer.index_line) {
         return;
      }
      let read = false;
      let done = false;
      let line = this.#typer.lines[this.#typer.index_line];
      let post = false;
      while (!done && this.#typer.index < line.length) {
         const char = line[this.#typer.index++];
         if (char === '{') {
            const code = line.slice(this.#typer.index, line.indexOf('}', this.#typer.index));
            const data = code.slice(1);
            this.#typer.index += code.length + 1;
            this.fire('code', code);
            switch (code[0]) {
               case '~':
                  line = line.slice(0, this.#typer.index) + this.magic + line.slice(this.#typer.index);
                  this.#typer.lines[this.#typer.index_line] = line;
                  break;
               case '!':
                  if (this.#typer.skippable || code[1] === '!') {
                     skip = true;
                  }
                  break;
               case '@':
                  this.#typer.display.push(`§${data}§`);
                  break;
               case '#':
                  this.fire('header', data);
                  break;
               case '%':
                  if (data.length !== 0) {
                     const time = Math.round(Number(data) * this.interval);
                     if (time > 0) {
                        this.#typer.autoread = time;
                        break;
                     }
                  }
                  skip = true;
                  read = true;
                  break;
               case '^':
                  if (!skip) {
                     const time = Math.round(Number(data) * this.interval);
                     if (time) {
                        done = true;
                        this.#typer.autonext = time;
                     }
                  }
                  break;
               case '&':
                  this.#typer.display.push(String.fromCharCode(parseInt(data, 16)));
                  break;
               case '*':
                  this.#typer.skippable_fast = false;
               case '|':
                  this.#typer.skippable = false;
                  break;
            }
         } else {
            this.fire('char', char);
            this.#typer.display.push(char);
            if (!skip) {
               const index_chunk = this.#typer.index_chunk++;
               if (index_chunk > 0 && (index_chunk % this.chunksize === 0 || char.match(/[\s\µ\.\,\!\?]/))) {
                  this.#typer.index < line.length && this.emit();
               }
               if (this.interval > 0) {
                  this.#typer.autonext = this.interval;
                  done = true;
               }
            }
         }
         this.#typer.index < line.length || (post = true);
         if (done) {
            break;
         }
      }
      if (line.length <= this.#typer.index) {
         this.fire('idle');
         this.mode = 'idle';
         post && this.emit();
         this.#typer.index_line++;
         read && this.read();
      }
   }
   read (force = false) {
      force && this.skip(true);
      if (this.mode === 'idle' && (force || this.#typer.autonext === 0)) {
         if (this.#typer.lines.length <= this.#typer.index_line) {
            this.emit();
            this.reset(true);
            this.fire('empty');
            this.mode = 'empty';
         } else {
            this.reset();
            this.fire('read');
            this.mode = 'read';
            this.next();
         }
      }
   }
   reset (full = false) {
      this.#typer.autoread = 0;
      this.#typer.autonext = 0;
      this.#typer.display = [];
      this.#typer.index = 0;
      this.#typer.index_chunk = 0;
      this.#typer.skippable = true;
      this.#typer.skippable_fast = true;
      if (full) {
         this.#typer.index_line = 0;
         this.#typer.lines = [];
         this.task?.();
      }
   }
   skip (force = false, fastskip = false) {
      if (this.mode === 'read' && (force || (fastskip ? this.#typer.skippable_fast : this.#typer.skippable))) {
         this.fire('skip');
         this.next(true);
      }
   }
   text (...lines: string[]) {
      this.reset(true);
      const trueLines = lines
         .map(line => {
            for (const [ key, value ] of Object.entries(this.variables)) {
               line = line.split(`$(${key})`).join(value);
            }
            if (line[0] === '<') {
               const plain = line[1] === '#';
               const segments = line.slice(plain ? 2 : 1).split('>');
               line = CosmosTextUtils.format(segments.slice(1).join('>'), +segments[0], plain);
            }
            return line.trim();
         })
         .filter(line => line.length !== 0);
      const vow = new Vow();
      if (trueLines.length === 0) {
         vow.confirm();
         return vow;
      }
      this.task = () => vow.confirm();
      this.#typer.lines = trueLines;
      this.renderer.when(() => {
         if (vow.active) {
            if (this.#typer.autoread > 0 && --this.#typer.autoread === 0) {
               this.read(true);
            } else if (this.#typer.autonext > 0 && --this.#typer.autonext === 0) {
               this.next();
            }
            return false;
         }
         return true;
      });
      this.fire('read');
      this.mode = 'read';
      this.next();
      return vow;
   }
}

export class CosmosValueRandom extends CosmosValue {
   compute () {
      let z = this.value;
      z++;
      z ^= z >>> 17;
      z = Math.imul(z, 0xed5ad4bb);
      z ^= z >>> 11;
      z = Math.imul(z, 0xac4c1b51);
      z ^= z >>> 15;
      z = Math.imul(z, 0x31848bab);
      z ^= z >>> 14;
      return (z >>> 0) / 4294967296;
   }
   next () {
      this.value = (this.value + 0x9e3779b9) | 0;
      return this.compute();
   }
   next_void () {
      this.value = (this.value + 0x9e3779b9) | 0;
   }
   int (limit: number) {
      return Math.floor(this.next() * limit);
   }
}

// classes - third level
export class CosmosAnimationResources extends CosmosInventory<[CosmosImage, CosmosData<CosmosAnimationInfo>]> {
   image: CosmosImage;
   data: CosmosData<CosmosAnimationInfo>;
   constructor (image: CosmosImage, data: CosmosData<CosmosAnimationInfo>) {
      super(image, data);
      this.image = image;
      this.data = data;
   }
}

export class CosmosRenderer<
   A extends string = string,
   B extends CosmosBaseEvents = CosmosBaseEvents
> extends CosmosBase<B> {
   static fancy = true;
   static style = {
      blend: BLEND_MODES.NORMAL,
      border: 1,
      fill: '',
      fontName: '',
      fontSize: 0,
      stroke: '',
      tint: 0xffffff
   };
   static suspend = false;
   #renderer = { delta: 0, height: 0, layers: [] as CosmosRendererLayer[], subtick: false, width: 0 };
   application: Application;
   fader = new Graphics();
   freecam: boolean;
   layers: { [x in A]: CosmosRendererLayer };
   posts: (() => void)[] = [];
   region: CosmosRegion = [ new CosmosPoint(-Infinity), new CosmosPoint(Infinity) ];
   shake: CosmosValue;
   shake_limit: number;
   speed: CosmosValueLinked;
   speeders: (() => void)[] = [];
   ticks: number;
   whens: { condition: (subtick: boolean) => boolean; resolve: () => void }[] = [];
   wrapper: HTMLElement | null;
   zoom: CosmosValue;
   get canvas () {
      return this.application.view as HTMLCanvasElement;
   }
   get size () {
      return new CosmosPoint(this.#renderer.width, this.#renderer.height);
   }
   get time () {
      return this.ticks * CosmosMath.FRAME_2;
   }
   constructor (properties: CosmosRendererProperties<A> = {}) {
      super(properties);
      (({
         auto = true,
         freecam = false,
         height = 1,
         layers = {} as { [x in A]: CosmosRendererLayerModifier[] },
         shake = 0,
         shake_limit = Infinity,
         speed = 1,
         ticks = 0,
         width = 1,
         wrapper = null,
         zoom = 1
      }: CosmosRendererProperties<A>) => {
         this.#renderer.height = height;
         this.#renderer.width = width;
         this.application = new Application({ antialias: false, autoStart: auto, backgroundAlpha: 0, height, width });
         this.freecam = freecam;
         this.layers = Object.fromEntries(
            Object.entries(layers).map(([ key, modifiers ]) => {
               const container = new Container();
               container.filterArea = new Rectangle(0, 0, width, height);
               container.sortableChildren = true;
               this.container.addChild(container);
               const layer = {
                  active: true,
                  container,
                  dirty: false,
                  modifiers: [
                     (modifiers as CosmosRendererLayerModifier).includes('fixed'),
                     (modifiers as CosmosRendererLayerModifier).includes('vertical')
                  ],
                  objects: []
               };
               this.#renderer.layers.push(layer as CosmosRendererLayer);
               return [ key, layer ];
            })
         ) as any;
         this.shake_limit = shake_limit;
         this.shake = new CosmosValue(shake);
         const speed_accessor = { speed } as { speed: number; value: number };
         Object.defineProperty(speed_accessor, 'value', {
            get: () => {
               return speed_accessor.speed;
            },
            set: (value: number) => {
               speed_accessor.speed = value;
               for (const speeder of this.speeders) {
                  speeder();
               }
            }
         });
         this.speed = new CosmosValueLinked(speed_accessor);
         this.ticks = ticks;
         this.wrapper = typeof wrapper === 'string' ? (document.querySelector(wrapper) as HTMLElement) : wrapper;
         this.zoom = new CosmosValue(zoom);
         this.container.pivot.set(width / 2, height / 2);
         this.container.position.set(width / 2, height / 2);
         this.fader.beginFill(0, 1).drawRect(0, 0, width, height).endFill();
         this.fader.zIndex = Infinity;
         this.container.addChild(this.fader);
         this.application.stage = this.container;
         this.canvas.style.imageRendering = 'pixelated';
         auto && this.wrapper?.appendChild(this.canvas);
      })(properties);
      this.application.ticker.add(
         delta => {
            if (CosmosRenderer.suspend) {
               return;
            }
            this.#renderer.delta += this.speed.value * delta;
            while (1 <= this.#renderer.delta) {
               this.ticks++;
               this.#renderer.delta -= 1;
               this.#renderer.subtick = !this.#renderer.subtick;
               if (this.whens.length !== 0) {
                  for (const when of this.whens) {
                     if (when.condition(this.#renderer.subtick)) {
                        this.whens.splice(this.whens.indexOf(when), 1);
                        when.resolve();
                     }
                  }
               }
               this.#renderer.subtick || this.tick();
               if (this.posts.length !== 0) {
                  for (const post of this.posts.splice(0, this.posts.length)) {
                     post();
                  }
               }
            }
         },
         void 0,
         UPDATE_PRIORITY.UTILITY
      );
   }
   attach (key: A, ...objects: CosmosObject[]) {
      const layer = this.layers[key];
      const vertical = layer.modifiers[1];
      for (const object of objects) {
         if (!layer.objects.includes(object)) {
            layer.dirty = true;
            layer.objects.push(object);
         }
         object.update(object.priority.value || (vertical ? object.position.y : 0));
      }
   }
   calculate (source: A | CosmosObject, filter: CosmosProvider<boolean, [CosmosHitbox]> = true) {
      const list: CosmosHitbox[] = [];
      for (const object of typeof source === 'string' ? this.layers[source].objects : source.$objects) {
         if (object instanceof CosmosHitbox && CosmosUtils.provide(filter, object)) {
            list.push(object);
            object.calculate();
         }
         list.push(...this.calculate(object, filter));
      }
      return list;
   }
   clear (...keys: A[]) {
      for (const key of keys) {
         const layer = this.layers[key];
         if (layer.objects.length !== 0) {
            layer.dirty = true;
            layer.objects.splice(0, layer.objects.length);
         }
      }
   }
   detach (key: A, ...objects: CosmosObject[]) {
      const layer = this.layers[key];
      for (const object of objects) {
         const index = layer.objects.indexOf(object);
         if (index !== -1) {
            layer.dirty = true;
            layer.objects.splice(index, 1);
         }
      }
   }
   detect (source: CosmosHitbox, ...targets: CosmosHitbox[]) {
      source.calculate();
      return targets.filter(target => source.detect(target));
   }
   async pause (duration = 0) {
      if (duration <= 0) {
         return this.when(() => true);
      } else if (duration === Infinity) {
         return new Promise<void>(() => {});
      } else {
         const ticks = this.ticks + Math.round(duration / CosmosMath.FRAME_2);
         return this.when(() => ticks <= this.ticks);
      }
   }
   post () {
      return new Promise<void>(resolve => this.posts.push(resolve));
   }
   projection (position: CosmosPointSimple, camera = this.position) {
      return this.size
         .divide(this.scale.multiply(2))
         .subtract(this.freecam ? this.position : camera.clamp(...this.region))
         .add(position);
   }
   resolve (position: CosmosPointSimple, camera = this.position) {
      return (this.freecam ? this.position : camera.clamp(...this.region))
         .subtract(this.size.divide(this.scale.multiply(2)))
         .add(position);
   }
   start () {
      this.#renderer.delta = 0;
      this.#renderer.subtick = false;
      this.ticks = 0;
      this.application.start();
      this.wrapper?.appendChild(this.canvas);
   }
   stop () {
      this.application.stop();
      this.canvas.remove();
   }
   style () {
      return {
         blend: this.blend ?? CosmosRenderer.style.blend,
         border: this.border ?? CosmosRenderer.style.border,
         fill: this.fill ?? CosmosRenderer.style.fill,
         fontName: this.fontName ?? CosmosRenderer.style.fontName,
         fontSize: this.fontSize ?? CosmosRenderer.style.fontSize,
         stroke: this.stroke ?? CosmosRenderer.style.stroke,
         tint: this.tint ?? CosmosRenderer.style.tint
      };
   }
   tick () {
      this.fire('tick');
      let filters = this.container.filters?.slice() ?? null;
      for (const layer of this.#renderer.layers) {
         if (layer.active && layer.objects.length !== 0) {
            const dirty = layer.dirty;
            if (dirty) {
               layer.dirty = false;
               layer.container.removeChildren();
            }
            const vertical = layer.modifiers[1];
            for (const object of layer.objects.slice()) {
               object.tick();
               object.update(object.priority.value || (vertical ? object.position.y : 0));
               dirty && layer.container.addChild(object.container);
            }
            const subfilters = layer.container.filters;
            if (subfilters !== null && subfilters.length !== 0) {
               if (filters === null) {
                  filters = subfilters.slice();
               } else {
                  filters.push(...subfilters);
               }
            }
         } else if (layer.container.children.length !== 0) {
            layer.container.removeChildren();
            layer.dirty = true;
         }
      }
      this.fire('pre-render');
      let shakeX = 0;
      let shakeY = 0;
      const style = this.style();
      const camera = this.freecam ? this : this.position.clamp(...this.region);
      const subsize = this.size.divide(this.scale);
      const center = subsize.divide(2);
      if (this.shake.value !== 0) {
         shakeX = this.shake.value * (Math.random() - 0.5) * 2;
         shakeY = this.shake.value * (Math.random() - 0.5) * 2;
         if (this.shake_limit < Infinity) {
            if (shakeX < -this.shake_limit) {
               shakeX = -this.shake_limit;
            } else if (shakeX > this.shake_limit) {
               shakeX = this.shake_limit;
            }
            if (shakeY < -this.shake_limit) {
               shakeY = -this.shake_limit;
            } else if (shakeY > this.shake_limit) {
               shakeY = this.shake_limit;
            }
         }
      }
      this.fader.alpha = 1 - this.alpha.value;
      this.updateFilters(filters);
      for (const layer of this.#renderer.layers) {
         if (layer.active && layer.objects.length !== 0) {
            const fixed = layer.modifiers[0];
            const zoom = fixed ? 1 : this.zoom.value;
            const subcamera = subsize.subtract(subsize.divide(zoom)).divide(2).add(camera);
            const scale = this.scale.multiply(zoom);
            layer.container.rotation = fixed ? 0 : this.rotation.value * (Math.PI / 180);
            layer.container.position.set(
               Math.round(((fixed ? 0 : center.x - subcamera.x) + shakeX) * scale.x),
               Math.round(((fixed ? 0 : center.y - subcamera.y) + shakeY) * scale.y)
            );
            layer.container.scale.set(scale.x, scale.y);
            for (const object of layer.objects) {
               object.render(camera, scale, style);
            }
         }
      }
      this.fire('render');
   }
   when (condition: (subtick: boolean) => boolean) {
      const { promise, resolve } = CosmosUtils.hyperpromise();
      this.whens.push({ condition, resolve });
      return promise;
   }
}

export class CosmosObject<
   A extends CosmosBaseEvents = CosmosBaseEvents,
   B extends CosmosMetadata = CosmosMetadata
> extends CosmosBase<A> {
   #object = {
      dirty: false,
      objects: (() => {
         const value = [] as CosmosObject[];
         const proxy = new Proxy(value, {
            set: (target, key: any, value: CosmosObject) => {
               if (value && typeof value.parent === 'object') {
                  this.#object.dirty = true;
                  value.parent = this;
               }
               target[key] = value;
               return true;
            },
            deleteProperty: (target, key: any) => {
               const value = target[key];
               if (value && typeof value.parent === 'object') {
                  this.#object.dirty = true;
                  value.parent = null;
               }
               delete target[key];
               return true;
            }
         });
         return { value, proxy };
      })(),
      offsets: (() => {
         const value = [] as CosmosPoint[];
         const proxy = new Proxy(value, {
            get (target, key: any) {
               return (target[key] ??= new CosmosPoint());
            }
         });
         return { value, proxy };
      })(),
      subcontainer: new Container(),
      x: 0,
      y: 0
   };
   acceleration: CosmosValue;
   gravity: CosmosPoint;
   metadata: B;
   parallax: CosmosPoint;
   parent: CosmosObject | null = null;
   priority: CosmosValue;
   spin: CosmosValue;
   velocity: CosmosPoint;
   get $objects () {
      return this.#object.objects.value;
   }
   get $offsets () {
      return this.#object.offsets.value;
   }
   get objects () {
      return this.#object.objects.proxy;
   }
   set objects (value) {
      this.#object.objects.proxy.splice(0, this.#object.objects.value.length);
      this.#object.objects.proxy.push(...value);
   }
   get offsets () {
      return this.#object.offsets.proxy;
   }
   set offsets (value) {
      this.#object.offsets.value.splice(0, this.#object.offsets.value.length);
      this.#object.offsets.value.push(...value);
   }
   constructor (properties: CosmosObjectProperties<B> = {}) {
      super(properties);
      (({
         acceleration = 1,
         gravity = 0,
         offsets = [],
         metadata = {} as B,
         objects = [],
         parallax = 0,
         priority = 0,
         spin = 0,
         velocity = 0
      }: CosmosObjectProperties<B>) => {
         this.acceleration = new CosmosValue(acceleration);
         if (typeof gravity === 'number') {
            this.gravity = new CosmosPoint(gravity ?? 0);
         } else {
            this.gravity = new CosmosPoint(gravity?.x ?? 0, gravity?.y ?? 0);
         }
         this.metadata = metadata;
         this.#object.offsets.value.push(
            ...offsets.map(offset => {
               if (typeof offset === 'number') {
                  return new CosmosPoint(offset ?? 0);
               } else {
                  return new CosmosPoint(offset?.x ?? 0, offset?.y ?? 0);
               }
            })
         );
         this.#object.objects.proxy.push(...objects);
         if (typeof parallax === 'number') {
            this.parallax = new CosmosPoint(parallax ?? 0);
         } else {
            this.parallax = new CosmosPoint(parallax?.x ?? 0, parallax?.y ?? 0);
         }
         this.priority = new CosmosValue(priority);
         this.spin = new CosmosValue(spin);
         if (typeof velocity === 'number') {
            this.velocity = new CosmosPoint(velocity ?? 0);
         } else {
            this.velocity = new CosmosPoint(velocity?.x ?? 0, velocity?.y ?? 0);
         }
      })(properties);
      this.#object.subcontainer.sortableChildren = true;
      this.container.addChild(this.#object.subcontainer);
   }
   attach (...objects: typeof this.objects) {
      for (const object of objects) {
         this.#object.objects.value.includes(object) || this.#object.objects.proxy.push(object);
      }
      return this;
   }
   clear () {
      this.#object.objects.value.length !== 0 &&
         this.#object.objects.proxy.splice(0, this.#object.objects.value.length);
      return this;
   }
   detach (...objects: typeof this.objects) {
      for (const object of objects) {
         const index = this.#object.objects.value.indexOf(object);
         index === -1 || this.#object.objects.proxy.splice(index, 1);
      }
      return this;
   }
   draw (style: CosmosStyle) {}
   getBlend (style: CosmosStyle) {
      if (this.blend === void 0) {
         let parent = this.parent;
         while (parent !== null) {
            if (parent.blend === void 0) {
               parent = parent.parent;
            } else {
               return parent.blend;
            }
         }
         return style.blend;
      } else {
         return this.blend;
      }
   }
   getBorder (style: CosmosStyle) {
      if (this.border === void 0) {
         let parent = this.parent;
         while (parent !== null) {
            if (parent.border === void 0) {
               parent = parent.parent;
            } else {
               return parent.border;
            }
         }
         return style.border;
      } else {
         return this.border;
      }
   }
   getFill (style: CosmosStyle) {
      if (this.fill === void 0) {
         let parent = this.parent;
         while (parent !== null) {
            if (parent.fill === void 0) {
               parent = parent.parent;
            } else {
               return parent.fill;
            }
         }
         return style.fill;
      } else {
         return this.fill;
      }
   }
   getFontName (style: CosmosStyle) {
      if (this.fontName === void 0) {
         let parent = this.parent;
         while (parent !== null) {
            if (parent.fontName === void 0) {
               parent = parent.parent;
            } else {
               return parent.fontName;
            }
         }
         return style.fontName;
      } else {
         return this.fontName;
      }
   }
   getFontSize (style: CosmosStyle) {
      if (this.fontSize === void 0) {
         let parent = this.parent;
         while (parent !== null) {
            if (parent.fontSize === void 0) {
               parent = parent.parent;
            } else {
               return parent.fontSize;
            }
         }
         return style.fontSize;
      } else {
         return this.fontSize;
      }
   }
   getStroke (style: CosmosStyle) {
      if (this.stroke === void 0) {
         let parent = this.parent;
         while (parent !== null) {
            if (parent.stroke === void 0) {
               parent = parent.parent;
            } else {
               return parent.stroke;
            }
         }
         return style.stroke;
      } else {
         return this.stroke;
      }
   }
   getTint (style: CosmosStyle) {
      if (this.tint === void 0) {
         let parent = this.parent;
         while (parent !== null) {
            if (parent.tint === void 0) {
               parent = parent.parent;
            } else {
               return parent.tint;
            }
         }
         return style.tint;
      } else {
         return this.tint;
      }
   }
   render (camera: CosmosPointSimple, scale: CosmosPointSimple, style: CosmosStyle) {
      this.fire('pre-render');
      this.#object.x = this.position.x;
      this.#object.y = this.position.y;
      if (this.#object.offsets.value.length !== 0) {
         for (const offset of this.#object.offsets.value) {
            if (offset !== void 0) {
               this.#object.x += offset.x;
               this.#object.y += offset.y;
            }
         }
      }
      this.container.alpha = this.alpha.value;
      this.container.angle = this.rotation.value;
      this.container.position.set(
         this.#object.x + this.parallax.x * camera.x,
         this.#object.y + this.parallax.y * camera.y
      );
      this.container.scale.set(this.scale.x, this.scale.y);
      this.draw(style);
      this.updateFilters();
      if (this.#object.objects.value.length !== 0) {
         for (const object of this.#object.objects.value) {
            object.render(camera, scale, style);
         }
      }
      this.fire('render');
   }
   tick () {
      if (this.#object.dirty) {
         this.#object.dirty = false;
         this.#object.subcontainer.removeChildren();
         for (const object of this.#object.objects.value) {
            this.#object.subcontainer.addChild(object.container);
         }
      }
      this.velocity.x += this.gravity.x;
      this.velocity.y += this.gravity.y;
      this.velocity.x *= this.acceleration.value;
      this.velocity.y *= this.acceleration.value;
      this.spin.value *= this.acceleration.value;
      this.rotation.value += this.spin.value;
      this.position.x += this.velocity.x;
      this.position.y += this.velocity.y;
      if (this.#object.objects.value.length !== 0) {
         for (const object of this.#object.objects.value.slice()) {
            object.tick();
         }
      }
      this.fire('tick');
   }
   transparent () {
      if (this.alpha.value === 0) {
         return true;
      } else {
         let parent = this.parent;
         while (parent !== null) {
            if (parent.alpha.value === 0) {
               return true;
            } else {
               parent = parent.parent;
            }
         }
         return false;
      }
   }
   update (index: number) {
      index === this.container.zIndex || (this.container.zIndex = index);
      for (const object of this.#object.objects.value) {
         object.update(object.priority.value);
      }
   }
}

// classes - fourth level
export class CosmosAnchoredObject<
   A extends CosmosBaseEvents = CosmosBaseEvents,
   B extends CosmosMetadata = CosmosMetadata
> extends CosmosObject<A, B> {
   anchor: CosmosPoint;
   constructor (properties: CosmosAnchoredObjectProperties<B> = {}) {
      super(properties);
      if (typeof properties.anchor === 'number') {
         this.anchor = new CosmosPoint(properties.anchor ?? -1);
      } else {
         this.anchor = new CosmosPoint(properties.anchor?.x ?? -1, properties.anchor?.y ?? -1);
      }
   }
   cast (position: CosmosPointSimple) {
      const size = this.compute();
      if (size.x !== 0 || size.y !== 0) {
         this.anchor.set(
            size
               .multiply(this.anchor)
               .subtract(this.position.multiply(2))
               .add(new CosmosPoint(position).multiply(2))
               .divide(size)
         );
      }
      this.position.set(position.x, position.y);
   }
   compute () {
      return new CosmosPoint();
   }
}

// classes - fifth level
export class CosmosSizedObject<
   A extends CosmosBaseEvents = CosmosBaseEvents,
   B extends CosmosMetadata = CosmosMetadata
> extends CosmosAnchoredObject<A, B> {
   size: CosmosPoint;
   constructor (properties: CosmosSizedObjectProperties<B> = {}) {
      super(properties);
      if (typeof properties.size === 'number') {
         this.size = new CosmosPoint(properties.size ?? 0);
      } else {
         this.size = new CosmosPoint(properties.size?.x ?? 0, properties.size?.y ?? 0);
      }
   }
   compute () {
      return this.size.clone();
   }
}

export class CosmosSprite<
   A extends CosmosBaseEvents = CosmosBaseEvents,
   B extends CosmosMetadata = CosmosMetadata
> extends CosmosAnchoredObject<A, B> {
   #sprite = { crop: [ 0, 0 ], image: null as CosmosImage | null, visible: false };
   active = false;
   crop: CosmosCrop;
   duration: number;
   frames: (CosmosImage | null)[];
   readonly graphics = new Graphics();
   index: number;
   reverse: boolean;
   readonly sprite = new Sprite();
   step: number;
   constructor (properties: CosmosSpriteProperties<B> = {}) {
      super(properties);
      (({
         active = false,
         crop: { bottom = 0, left = 0, right = 0, top = 0 } = {},
         duration = 1,
         frames = [],
         index = 0,
         reverse = false,
         step = 0
      }: CosmosSpriteProperties<B> = {}) => {
         this.active = active;
         this.crop = { bottom, left, right, top };
         this.duration = duration;
         this.frames = frames;
         this.index = index;
         this.reverse = reverse;
         this.step = step;
      })(properties);
   }
   advance () {
      if (this.active && this.duration <= ++this.step) {
         this.step = 0;
         this.index += this.reverse ? -1 : 1;
         if (this.index < 0) {
            this.index = this.frames.length - 1;
         } else if (this.frames.length <= this.index) {
            this.index = 0;
         }
      }
   }
   compute () {
      let index = this.index % this.frames.length;
      while (index < 0) {
         index += this.frames.length;
      }
      const image = this.frames[index];
      if (image) {
         const area = CosmosImageUtils.area(image.value, this.crop);
         return new CosmosPoint(area[2], area[3]);
      } else {
         return super.compute();
      }
   }
   disable () {
      this.active = false;
      return this;
   }
   draw (style: CosmosStyle) {
      if (this.frames.length === 0 || this.transparent()) {
         if (this.#sprite.visible) {
            this.container.removeChildAt(1);
            this.container.removeChildAt(0);
            this.#sprite.visible = false;
         }
      } else {
         const graphics = this.graphics;
         const sprite = this.sprite;
         if (!this.#sprite.visible) {
            this.container.addChildAt(graphics, 0);
            this.container.addChildAt(sprite, 1);
            this.#sprite.visible = true;
         }
         const prev = this.#sprite.image;
         let index = this.index % this.frames.length;
         index < 0 && (index += this.frames.length);
         const next = this.frames[index];
         if (prev !== next) {
            prev?.sprites.delete(sprite);
            next?.sprites.add(sprite);
            if (next?.value) {
               sprite.texture = next.value;
            } else {
               sprite.texture = Texture.EMPTY;
            }
            this.#sprite.image = next;
         }
         if (sprite.texture.baseTexture) {
            const a = (this.anchor.x + 1) / 2;
            const b = (this.anchor.y + 1) / 2;
            if (this.crop.bottom !== 0 || this.crop.left !== 0 || this.crop.right !== 0 || this.crop.top !== 0) {
               const [ x, y, width, height ] = CosmosImageUtils.area(sprite.texture, this.crop);
               graphics.pivot.set(width * a, height * b);
               let update = false;
               if (width !== this.#sprite.crop[0]) {
                  this.#sprite.crop[0] = width;
                  update = true;
               }
               if (height !== this.#sprite.crop[1]) {
                  this.#sprite.crop[1] = height;
                  update = true;
               }
               update && graphics.clear().beginFill(0xff0000, 1).drawRect(0, 0, width, height).endFill();
               sprite.anchor.set((x + width * a) / sprite.texture.width, (y + height * b) / sprite.texture.height);
               sprite.mask = graphics;
            } else {
               sprite.anchor.set(a, b);
               sprite.mask = null;
            }
            sprite.blendMode = this.getBlend(style);
            sprite.tint = this.getTint(style);
         }
      }
   }
   enable (duration = this.duration) {
      this.active = true;
      this.duration = duration;
      return this;
   }
   read (): CosmosColor[][] {
      let index = this.index % this.frames.length;
      while (index < 0) {
         index += this.frames.length;
      }
      const image = this.frames[index];
      if (image) {
         return CosmosImageUtils.texture2colors(image.value, this.crop);
      } else {
         return [ [] ];
      }
   }
   reset () {
      this.active = false;
      this.index = 0;
      this.step = 0;
      return this;
   }
   tick () {
      this.advance();
      super.tick();
   }
}

export class CosmosText<
   A extends CosmosBaseEvents = CosmosBaseEvents,
   B extends CosmosMetadata = CosmosMetadata
> extends CosmosAnchoredObject<A, B> {
   #text = {
      anchor: { x: -1, y: -1 },
      subcontainer: new Container(),
      charset: '',
      content: '',
      dirty: false,
      plain: false,
      spacing: { x: 0, y: 0 },
      blend: null as BLEND_MODES | null,
      border: null as number | null,
      fancy: null as boolean | null,
      fill: null as string | null,
      fontName: null as string | null,
      fontSize: null as number | null,
      stroke: null as string | null,
      tint: null as number | null,
      visible: false
   };
   static charset = '/0123456789=ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
   charset: string;
   content: string;
   phase: number;
   plain: boolean;
   spacing: CosmosPointSimple;
   constructor (properties: CosmosTextProperties<B> = {}) {
      super(properties);
      (({
         charset = CosmosText.charset,
         content = '',
         phase = 0,
         plain = false,
         spacing = 0
      }: CosmosTextProperties<B> = {}) => {
         this.charset = charset;
         this.content = content;
         this.phase = phase;
         this.plain = plain;
         this.spacing = new CosmosPoint(spacing).value();
      })(properties);
   }
   compute () {
      if (this.#text.fontName !== null && this.#text.fontSize !== null) {
         const fontName = this.#text.fontName;
         const font = CosmosTextUtils.storage[fontName];
         if (font !== void 0) {
            const fontScale = this.#text.fontSize / font.size;
            const lines = this.content
               .replace(/§(.*?)§/g, '')
               .split('\n')
               .map(section => {
                  if (section.length !== 0) {
                     return (
                        CosmosTextUtils.metrics(fontName, section, fontScale).x +
                        this.spacing.x * Math.max(section.length - 1, 0)
                     );
                  } else {
                     return 0;
                  }
               });
            const metrics = CosmosTextUtils.metrics(fontName, this.charset, fontScale);
            return new CosmosPoint(Math.max(...lines), metrics.y + (metrics.y + this.spacing.y) * (lines.length - 1));
         }
      }
      return new CosmosPoint();
   }
   tick () {
      this.phase++;
      super.tick();
   }
   draw (style: CosmosStyle) {
      if (this.transparent()) {
         if (this.#text.visible) {
            this.#text.visible = false;
            this.container.removeChildAt(0);
         }
      } else {
         if (!this.#text.visible) {
            this.#text.visible = true;
            this.container.addChildAt(this.#text.subcontainer, 0);
         }
         let update = false;
         if (this.anchor.x !== this.#text.anchor.x) {
            update = true;
            this.#text.anchor.x = this.anchor.x;
         }
         if (this.anchor.y !== this.#text.anchor.y) {
            update = true;
            this.#text.anchor.y = this.anchor.y;
         }
         if (this.charset !== this.#text.charset) {
            update = true;
            this.#text.charset = this.charset;
         }
         if (this.content !== this.#text.content) {
            update = true;
            this.#text.content = this.content;
         }
         if (this.plain !== this.#text.plain) {
            update = true;
            this.#text.plain = this.plain;
         }
         if (this.spacing.x !== this.#text.spacing.x) {
            update = true;
            this.#text.spacing.x = this.spacing.x;
         }
         if (this.spacing.y !== this.#text.spacing.y) {
            update = true;
            this.#text.spacing.y = this.spacing.y;
         }
         const blend = this.getBlend(style);
         if (blend !== this.#text.blend) {
            update = true;
            this.#text.blend = blend;
         }
         const border = this.getBorder(style);
         if (border !== this.#text.border) {
            update = true;
            this.#text.border = border;
         }
         let fill = this.getFill(style);
         if (fill !== this.#text.fill) {
            update = true;
            this.#text.fill = fill;
         }
         const fontName = this.getFontName(style);
         if (fontName !== this.#text.fontName) {
            update = true;
            this.#text.fontName = fontName;
         }
         const fontSize = this.getFontSize(style);
         if (fontSize !== this.#text.fontSize) {
            update = true;
            this.#text.fontSize = fontSize;
         }
         let stroke = this.getStroke(style);
         if (stroke !== this.#text.stroke) {
            update = true;
            this.#text.stroke = stroke;
         }
         const tint = this.getTint(style);
         if (tint !== this.#text.tint) {
            update = true;
            this.#text.tint = tint;
         }
         if (this.#text.dirty) {
            update = true;
            this.#text.dirty = false;
         }
         if (update) {
            let index = 0;
            let mystify1 = [] as string[];
            const mystify2 = [] as string[];
            const offset = { x: 0, y: 0 };
            const phase = this.phase / CosmosMath.FRAME;
            const random = { x: 0, y: 0 };
            const swirl = { h: false, p: 0, r: 0, s: 0 };
            const fontValue = CosmosTextUtils.storage[fontName];
            const spacing = { x: 0, y: 0 }
            if (fontValue) {
               const fontScale = +fontSize / fontValue.size;
               const metrics = CosmosTextUtils.metrics(fontName, this.charset, fontScale);
               const size = this.compute();
               const half = size.divide(2);
               const base = new CosmosPoint(this.container.position.x, this.container.position.y).subtract(
                  half.add(half.multiply(this.anchor))
               );
               this.#text.subcontainer.removeChildren();
               while (index < this.content.length) {
                  let char = this.content[index++];
                  if (char === '\n') {
                     offset.x = 0;
                     offset.y += metrics.y + this.spacing.y + spacing.y;
                  } else if (!this.plain && char === '§') {
                     const code = this.content.slice(index, this.content.indexOf('§', index));
                     const [ key, value ] = code.split(':');
                     index += code.length + 1;
                     switch (key) {
                        case 'fill':
                           fill = value;
                           break;
                        case 'offset':
                           const [ offsetX, offsetY ] = value.split(',').map(value => +value);
                           offset.x = offsetX || 0;
                           offset.y = offsetY || 0;
                           break;
                        case 'random':
                           const [ randomX, randomY ] = value.split(',').map(value => +value);
                           random.x = randomX || 0;
                           random.y = randomY || 0;
                           break;
                        case 'reset':
                           fill = this.getFill(style);
                           random.x = 0;
                           random.y = 0;
                           stroke = this.getStroke(style);
                           swirl.r = 0;
                           swirl.s = 0;
                           swirl.p = 0;
                           mystify1 = [];
                           swirl.h = false;
                           break;
                        case 'stroke':
                           stroke = value;
                           break;
                           case 'spacing':
                              const [ spacingX, spacingY ] = value.split(',').map(value => +value);
                              spacing.x = spacingX || 0;
                              spacing.y = spacingY || 0;
                              break;
                        case 'swirl':
                           const [ swirlR, swirlS, swirlP ] = value.split(',').map(value => +value);
                           swirl.r = swirlR || 0;
                           swirl.s = swirlS || 0;
                           swirl.p = swirlP || 0;
                           break;
                        case 'mystify':
                           mystify1 = value.split('').filter(char => char !== '');
                           mystify1.length !== 0 && (this.#text.dirty = true);
                           break;
                        case 'hue':
                           swirl.h = !swirl.h;
                           break;
                     }
                     if (random.x !== 0 || random.y !== 0 || (swirl.s !== 0 && swirl.r !== 0)) {
                        this.#text.dirty = true;
                     }
                  } else {
                     let h = null as number | null;
                     let x = base.x - border / 2 + offset.x;
                     let y = base.y - border / 2 + offset.y;
                     if (random.x > 0) {
                        x += random.x * (Math.random() - 0.5);
                     }
                     if (random.y > 0) {
                        y += random.y * (Math.random() - 0.5);
                     }
                     if (swirl.s !== 0 && swirl.r !== 0) {
                        const angle = ((phase * 360 * swirl.s) % 360) + index * (360 / swirl.p);
                        const endpoint = new CosmosPoint(x, y).endpoint(angle, swirl.r);
                        swirl.h && (h = angle);
                        x = endpoint.x;
                        y = endpoint.y;
                     }
                     if (fontValue) {
                        if (mystify1.length !== 0) {
                           char = mystify1.splice(Math.floor(Math.random() * mystify1.length), 1)[0];
                           if (mystify1.length === 0) {
                              mystify1.push(...mystify2.splice(0, mystify2.length));
                           } else {
                              mystify2.push(char);
                           }
                        }
                        const glyph = fontValue.glyphs[char.charCodeAt(0)];
                        if (glyph) {
                           const sprite = new Sprite(glyph.texture);
                           sprite.blendMode = blend;
                           if (h !== null) {
                              const cmf = new ColorMatrixFilter();
                              cmf.hue(h, false);
                              sprite.filters = [ cmf ];
                           }
                           sprite.position.set(
                              x + glyph.metrics.x - this.container.position.x + fontValue.shift.x,
                              y + glyph.metrics.y - this.container.position.y + fontValue.shift.y
                           );
                           sprite.scale.set(fontScale);
                           sprite.tint = CosmosImageUtils.color2hex(
                              stroke === '' || stroke === fill
                                 ? CosmosImageUtils.color.of(fill)
                                 : CosmosImageUtils.gradient(
                                      CosmosImageUtils.color.of(fill),
                                      CosmosImageUtils.color.of(stroke),
                                      Math.min(Math.max(offset.x / size.x, 0), 1)
                                   )
                           );
                           this.#text.subcontainer.addChild(sprite);
                        }
                        offset.x += CosmosTextUtils.metrics(fontName, char, fontScale).x + this.spacing.x + spacing.x;
                     }
                  }
               }
            } else {
               this.#text.dirty = true;
            }
         }
      }
   }
}

// classes - sixth level
export class CosmosAnimation<
   A extends CosmosBaseEvents = CosmosBaseEvents,
   B extends CosmosMetadata = CosmosMetadata
> extends CosmosSprite<A, B> {
   #animation = { resources: null as CosmosAnimationResources | null };
   extrapolate: boolean;
   resources: CosmosAnimationResources | null;
   subcrop: CosmosCrop;
   constructor (properties: CosmosAnimationProperties<B> = {}) {
      super(properties);
      (({
         extrapolate = true,
         resources,
         subcrop: { bottom = 0, left = 0, right = 0, top = 0 } = {}
      }: CosmosAnimationProperties<B> = {}) => {
         this.extrapolate = extrapolate;
         this.resources = resources ?? null;
         this.subcrop = { bottom, left, right, top };
      })(properties);
      this.resources?.loaded && this.fix();
   }
   fix (force = false) {
      if (force || this.#animation.resources !== this.resources) {
         this.#animation.resources = this.resources;
         if (this.resources?.data.value) {
            this.frames = CosmosUtils.populate(this.resources.data.value.frames.length, this.resources.image);
         } else {
            this.frames = [];
         }
      }
      if (this.resources?.data.value) {
         const info = this.resources.data.value.frames[this.index];
         if (info) {
            const [ x, y, width, height ] = CosmosImageUtils.area(
               { height: info.frame.h, width: info.frame.w },
               this.subcrop
            );
            this.crop.bottom = -(info.frame.y + y + height);
            this.crop.left = info.frame.x + x;
            this.crop.right = -(info.frame.x + x + width);
            this.crop.top = info.frame.y + y;
            this.extrapolate && (this.duration = Math.round(info.duration / (1000 / 30)));
         }
      }
      return this;
   }
   tick () {
      super.tick();
      this.fix();
   }
   use (resources: CosmosAnimationResources | null) {
      this.resources = resources;
      this.index = 0;
      this.step = 0;
      this.resources?.loaded && this.fix();
      return this;
   }
}

export class CosmosHitbox<
   A extends CosmosBaseEvents = CosmosBaseEvents,
   B extends CosmosMetadata = CosmosMetadata
> extends CosmosSizedObject<A, B> {
   #hitbox = {
      anchorX: 0,
      anchorY: 0,
      positionX: 0,
      positionY: 0,
      rotation: 0,
      scaleX: 0,
      scaleY: 0,
      sizeX: 0,
      sizeY: 0
   };
   polygon = new Polygon(new Vector(), [ new Vector(), new Vector(), new Vector(), new Vector() ]);
   region: CosmosRegion = [
      { x: 0, y: 0 },
      { x: 0, y: 0 }
   ];
   calculate () {
      let parent = this.parent;
      const position = this.position.clone();
      let rotation = this.rotation.value;
      const scale = this.scale.clone();
      while (parent) {
         position.x += parent.position.x;
         position.y += parent.position.y;
         position.set(
            parent.position.endpoint(
               position.angleFrom(parent.position) + parent.rotation.value,
               position.extentOf(parent.position)
            )
         );
         rotation += parent.rotation.value;
         scale.x *= parent.scale.x;
         scale.y *= parent.scale.y;
         parent = parent.parent;
      }
      let update = false;
      if (this.anchor.x !== this.#hitbox.anchorX) {
         this.#hitbox.anchorX = this.anchor.x;
         update = true;
      }
      if (this.anchor.y !== this.#hitbox.anchorY) {
         this.#hitbox.anchorY = this.anchor.y;
         update = true;
      }
      if (this.size.x !== this.#hitbox.sizeX) {
         this.#hitbox.sizeX = this.size.x;
         update = true;
      }
      if (this.size.y !== this.#hitbox.sizeY) {
         this.#hitbox.sizeY = this.size.y;
         update = true;
      }
      if (position.x !== this.#hitbox.positionX) {
         this.#hitbox.positionX = position.x;
         update = true;
      }
      if (position.y !== this.#hitbox.positionY) {
         this.#hitbox.positionY = position.y;
         update = true;
      }
      if (rotation !== this.#hitbox.rotation) {
         this.#hitbox.rotation = rotation;
         update = true;
      }
      if (scale.x !== this.#hitbox.scaleX) {
         this.#hitbox.scaleX = scale.x;
         update = true;
      }
      if (scale.y !== this.#hitbox.scaleY) {
         this.#hitbox.scaleY = scale.y;
         update = true;
      }
      if (update) {
         const size = this.size.multiply(scale);
         const half = size.divide(2);
         this.polygon.pos.x = position.x;
         this.polygon.pos.y = position.y;
         this.polygon.setAngle(rotation * (Math.PI / 180));
         this.polygon.setOffset(new Vector(-(half.x + half.x * this.anchor.x), -(half.y + half.y * this.anchor.y)));
         this.polygon.setPoints([
            new Vector(),
            new Vector(size.x, 0),
            new Vector(size.x, size.y),
            new Vector(0, size.y)
         ]);
         if (this.polygon.calcPoints.length === 4) {
            const { x: x1, y: y1 } = this.polygon.calcPoints[0];
            const { x: x2, y: y2 } = this.polygon.calcPoints[1];
            const { x: x3, y: y3 } = this.polygon.calcPoints[2];
            const { x: x4, y: y4 } = this.polygon.calcPoints[3];
            this.region[0].x = Math.round((position.x + Math.min(x1, x2, x3, x4)) * 1000) / 1000;
            this.region[0].y = Math.round((position.y + Math.min(y1, y2, y3, y4)) * 1000) / 1000;
            this.region[1].x = Math.round((position.x + Math.max(x1, x2, x3, x4)) * 1000) / 1000;
            this.region[1].y = Math.round((position.y + Math.max(y1, y2, y3, y4)) * 1000) / 1000;
         } else {
            const { x, y } = this.polygon.calcPoints[0];
            this.region[0].x = Math.round((position.x + x) * 1000) / 1000;
            this.region[0].y = Math.round((position.y + y) * 1000) / 1000;
            this.region[1].x = Math.round((position.x + x) * 1000) / 1000;
            this.region[1].y = Math.round((position.y + y) * 1000) / 1000;
         }
      }
      return this;
   }
   detect (hitbox: CosmosHitbox) {
      if (testPolygonPolygon(this.polygon, hitbox.polygon)) {
         return (
            this.region[0].x < hitbox.region[1].x &&
            hitbox.region[0].x < this.region[1].x &&
            this.region[0].y < hitbox.region[1].y &&
            hitbox.region[0].y < this.region[1].y
         );
      }
      return false;
   }
}

export class CosmosRectangle<
   A extends CosmosBaseEvents = CosmosBaseEvents,
   B extends CosmosMetadata = CosmosMetadata
> extends CosmosSizedObject<A, B> {
   $rectangle = { graphics: new Graphics(), visible: false };
   draw (style: CosmosStyle) {
      const fill = this.getFill(style);
      const stroke = this.getStroke(style);
      if (
         ((this.size.x === 0 || this.size.y === 0 || fill === '') && (this.border === 0 || stroke === '')) ||
         this.transparent()
      ) {
         if (this.$rectangle.visible) {
            this.$rectangle.visible = false;
            this.container.removeChildAt(0);
         }
      } else {
         if (!this.$rectangle.visible) {
            this.$rectangle.visible = true;
            this.container.addChildAt(this.$rectangle.graphics, 0);
         }
         const graphics = this.$rectangle.graphics.clear();
         const fillVisible = fill !== '';
         const strokeVisible = stroke !== '';
         const half = this.size.divide(2);
         const origin = half.add(half.multiply(this.anchor));
         const base = new CosmosPoint(
            this.size.x < 0 ? origin.x + this.size.x : origin.x,
            this.size.y < 0 ? origin.y + this.size.y : origin.y
         );
         graphics.pivot.set(this.size.x * ((this.anchor.x + 1) / 2), this.size.y * ((this.anchor.y + 1) / 2));
         graphics.position.set(base.x - graphics.pivot.x, base.y - graphics.pivot.y);
         graphics.blendMode = this.getBlend(style);
         if (strokeVisible) {
            const strokeColor = CosmosImageUtils.color.of(stroke);
            if (strokeColor[3] > 0) {
               graphics.lineStyle({
                  alpha: strokeColor[3] * this.alpha.value,
                  color: CosmosImageUtils.color2hex(strokeColor),
                  width: this.getBorder(style)
               });
            }
         }
         if (fillVisible) {
            const fillColor = CosmosImageUtils.color.of(fill);
            if (fillColor[3] > 0) {
               graphics.beginFill(CosmosImageUtils.color2hex(fillColor), fillColor[3] * this.alpha.value);
            } else {
               graphics.beginFill(0, 0);
            }
         }
         graphics.drawRect(0, 0, Math.abs(this.size.x), Math.abs(this.size.y)).endFill();
         graphics.tint = this.getTint(style);
      }
   }
}

// classes - seventh level
export class CosmosEntity<
   A extends CosmosBaseEvents = CosmosBaseEvents,
   B extends CosmosMetadata = CosmosMetadata
> extends CosmosHitbox<A, B> {
   #entity = { walk: null as (() => void) | null };
   face: CosmosDirection;
   sprites: { [x in CosmosDirection]: CosmosSprite };
   step: number;
   get sprite () {
      return this.sprites[this.face];
   }
   constructor (properties: CosmosEntityProperties<B> = {}) {
      super(properties);
      (({
         face = 'down',
         sprites: {
            down = new CosmosSprite(),
            left = new CosmosSprite(),
            right = new CosmosSprite(),
            up = new CosmosSprite()
         } = {},
         step = 1
      }: CosmosEntityProperties<B> = {}) => {
         this.face = face;
         this.sprites = { down, left, right, up };
         this.step = step;
      })(properties);
   }
   move<B extends string> (
      offset: CosmosPointSimple,
      renderer?: CosmosRenderer<B>,
      keys: B[] = [],
      filter?: CosmosProvider<boolean, [CosmosHitbox]>
   ) {
      const source = this.position.value();
      const hitboxes = filter && renderer ? keys.flatMap(key => renderer.calculate(key, filter)) : [];
      for (const axis of [ 'x', 'y' ] as ['x', 'y']) {
         const distance = offset[axis];
         if (distance !== 0) {
            this.position[axis] += distance;
            const hits = renderer ? renderer.detect(this, ...hitboxes) : [];
            if (hits.length !== 0) {
               const single = (distance / Math.abs(distance)) * this.step;
               while (this.position[axis] !== source[axis] && renderer!.detect(this, ...hits).length !== 0) {
                  this.position[axis] -= single;
               }
            }
         }
      }
      if (this.position.x === source.x && this.position.y === source.y) {
         if (offset.y < 0) {
            this.face = 'up';
         } else if (offset.y > 0) {
            this.face = 'down';
         } else if (offset.x < 0) {
            this.face = 'left';
         } else if (offset.x > 0) {
            this.face = 'right';
         }
         for (const sprite of Object.values(this.sprites)) {
            sprite.reset();
         }
         return false;
      } else {
         if (this.position.y < source.y) {
            this.face = 'up';
         } else if (this.position.y > source.y) {
            this.face = 'down';
         } else if (this.position.x < source.x) {
            this.face = 'left';
         } else if (this.position.x > source.x) {
            this.face = 'right';
         }
         this.sprite.enable();
         return true;
      }
   }
   tick () {
      this.objects[0] === this.sprite || (this.objects[0] = this.sprite);
      super.tick();
   }
   async walk (renderer: CosmosRenderer, speed?: number, ...points: Partial<CosmosPointSimple>[]) {
      this.#entity.walk?.();
      if (speed) {
         let active = true;
         this.#entity.walk = () => (active = false);
         let index = 0;
         const duration = Math.round(15 / speed);
         await renderer.when(subtick => {
            if (subtick) {
               return false;
            } else if (active) {
               const { x = this.position.x, y = this.position.y } = points[index];
               const limit = speed * renderer.speed.value;
               const dx = Math.min(Math.max(x - this.position.x, -limit), limit);
               const dy = Math.min(Math.max(y - this.position.y, -limit), limit);
               this.sprite.duration = duration;
               this.move({ x: dx, y: dy });
               if (Math.abs(x - this.position.x) < 0.000001 && Math.abs(y - this.position.y) < 0.000001) {
                  this.move({ x: 0, y: 0 });
                  this.position.set(x, y);
                  if (++index !== points.length) {
                     return false;
                  }
               } else {
                  return false;
               }
            }
            for (const sprite of Object.values(this.sprites)) {
               sprite.reset();
            }
            return true;
         });
      } else {
         for (const sprite of Object.values(this.sprites)) {
            sprite.reset();
         }
      }
   }
}

// classes - eighth level
export class CosmosCharacter<
   A extends CosmosBaseEvents = CosmosBaseEvents,
   B extends CosmosMetadata = CosmosMetadata
> extends CosmosEntity<A, B> {
   key: string;
   preset: CosmosCharacterPreset;
   talk = false;
   get sprite () {
      return (this.talk ? this.preset.talk : this.preset.walk)[this.face];
   }
   constructor (properties: CosmosCharacterProperties<B>) {
      super(properties);
      (({ key, preset }: CosmosCharacterProperties<B>) => {
         this.key = key;
         this.preset = preset;
      })(properties);
   }
   tick () {
      this.sprites = this.talk ? this.preset.talk : this.preset.walk;
      super.tick();
   }
}

export class CosmosPlayer<
   A extends CosmosBaseEvents = CosmosBaseEvents,
   B extends CosmosMetadata = CosmosMetadata
> extends CosmosEntity<A, B> {
   #player = { face: null as CosmosDirection | null };
   readonly funni = new CosmosHitbox();
   extent = new CosmosPoint();
   puppet = false;
   constructor (properties: CosmosPlayerProperties<B> = {}) {
      super(properties);
      this.extent = new CosmosPoint(properties.extent ?? -1);
   }
   tick () {
      if (this.face !== this.#player.face) {
         this.#player.face = this.face;
         this.funni.anchor.set(
            this.face === 'left' ? 1 : this.face === 'right' ? -1 : 0,
            this.face === 'up' ? 1 : this.face === 'down' ? -1 : 0
         );
         this.funni.size.set(
            this.face === 'left' || this.face === 'right' ? this.extent.y : this.extent.x,
            this.face === 'down' || this.face === 'up' ? this.extent.y : this.extent.x
         );
      }
      this.objects[1] = this.funni;
      super.tick();
   }
   async walk (renderer: CosmosRenderer, speed?: number, ...targets: Partial<CosmosPointSimple>[]) {
      this.puppet = true;
      await super.walk(renderer, speed, ...targets);
      this.puppet = false;
   }
}

export const CosmosAudioUtils = {
   EMPTY: new AudioBuffer({ length: 1, numberOfChannels: 2, sampleRate: 8000 }),
   context: null as AudioContext | null,
   delay (context: AudioContext, offset: number, feedback: number, echoes = Infinity) {
      const gain = new GainNode(context, { gain: feedback });
      const delay = new DelayNode(context, { delayTime: offset, maxDelayTime: Math.min(offset * echoes, 60) });
      gain.connect(delay);
      delay.connect(gain);
      return gain;
   },
   convolver (context: AudioContext, duration: number, mapper = (value: number) => 1 - value) {
      const length = context.sampleRate * duration;
      const buffer = new AudioBuffer({ length, numberOfChannels: 2, sampleRate: context.sampleRate });
      let channel = 0;
      while (channel < buffer.numberOfChannels) {
         let index = 0;
         const data = buffer.getChannelData(channel++);
         while (index < length) {
            data[index] = (Math.random() * 2 - 1) * mapper(index / length);
            data[index] = (Math.random() * 2 - 1) * mapper(index / length);
            index++;
         }
      }
      return new ConvolverNode(context, { buffer });
   },
   getContext () {
      return (CosmosAudioUtils.context ??= new AudioContext());
   }
};

export const CosmosImageUtils = {
   area (source: CosmosDimensions, crop: CosmosCrop): [number, number, number, number] {
      const x = (crop.left < 0 ? source.width : 0) + crop.left;
      const y = (crop.top < 0 ? source.height : 0) + crop.top;
      return [
         x,
         y,
         (crop.right < 0 ? 0 : source.width) - crop.right - x,
         (crop.bottom < 0 ? 0 : source.height) - crop.bottom - y
      ];
   },
   async base2texture (base: string) {
      return await CosmosImage.cache.of(base);
   },
   async base2pixels (base: string) {
      return CosmosImageUtils.texture2pixels(await CosmosImageUtils.base2texture(base));
   },
   async base2colors (base: string) {
      return CosmosImageUtils.texture2colors(await CosmosImageUtils.base2texture(base));
   },
   color: new CosmosCache<string, CosmosColor>(key => {
      const color = colord(key).toRgb();
      return [ color.r, color.g, color.b, color.a ];
   }),
   color2hex (color: CosmosColor) {
      return Math.round(color[0]) * 65536 + Math.round(color[1]) * 256 + Math.round(color[2]);
   },
   async colors2base (colors: CosmosColor[][], crop?: CosmosCrop) {
      return await CosmosImageUtils.texture2base(CosmosImageUtils.colors2texture(colors, crop));
   },
   colors2pixels (colors: CosmosColor[][], crop: CosmosCrop = { bottom: 0, left: 0, right: 0, top: 0 }) {
      let x = 0;
      let y = 0;
      const [ ax, ay, width, height ] = CosmosImageUtils.area(
         { width: colors.length, height: colors[0]?.length ?? 0 },
         crop
      );
      if (width > 0 && height > 0) {
         const pixels = new Uint8Array(width * height * 4);
         while (y < height) {
            while (x < width) {
               const z = Math.floor(ax + x + (ay + y) * width) * 4;
               const color = colors[x][y];
               pixels[z] = color[0];
               pixels[z + 1] = color[1];
               pixels[z + 2] = color[2];
               pixels[z + 3] = color[3];
               x++;
            }
            x = 0;
            y++;
         }
         return pixels;
      } else {
         return new Uint8Array();
      }
   },
   colors2texture (colors: CosmosColor[][], crop?: CosmosCrop) {
      return CosmosImageUtils.pixels2texture(CosmosImageUtils.colors2pixels(colors, crop), {
         width: colors.length,
         height: colors[0]?.length ?? 0
      });
   },
   crop (area: CosmosArea): CosmosCrop {
      return { bottom: area.height - area.y, left: area.x, right: area.width - area.x, top: area.y };
   },
   extract: null as Extract | null,
   getExtract () {
      return (CosmosImageUtils.extract ??= new Extract(new Renderer({ antialias: false, backgroundAlpha: 0 })));
   },
   gradient<A extends CosmosColor | number> (c1: A, c2: A, v: number): A {
      if (typeof c1 === 'number') {
         return CosmosImageUtils.color2hex(
            CosmosImageUtils.gradient(CosmosImageUtils.hex2color(c1), CosmosImageUtils.hex2color(c2 as number), v)
         ) as A;
      } else {
         const [ r1, g1, b1, a1 ] = c1;
         const [ r2, g2, b2, a2 ] = c2 as CosmosColor;
         return [
            CosmosMath.remap(v, r1, r2),
            CosmosMath.remap(v, g1, g2),
            CosmosMath.remap(v, b1, b2),
            CosmosMath.remap(v, a1, a2)
         ] as A;
      }
   },
   hex2color (hex: number) {
      return [ Math.floor(hex / 65536) % 256, Math.floor(hex / 256) % 256, hex % 256, 1 ] as CosmosColor;
   },
   merge (items: CosmosColor[][][], cols = Math.round(Math.sqrt(items.length))): [CosmosColor[][], CosmosArea[]] {
      const blockWidth = Math.max(...items.map(item => item.length));
      const blockHeight = Math.max(...items.map(item => item[0]?.length ?? 0));
      if (blockWidth > 0 && blockHeight > 0) {
         const pageWidth = blockWidth * cols;
         const pageHeight = blockHeight * Math.ceil(items.length / cols);
         const page = CosmosUtils.populate(pageWidth, () =>
            CosmosUtils.populate(pageHeight, () => [ 0, 0, 0, 0 ] as CosmosColor)
         );
         const info = CosmosUtils.populate(items.length, () => ({ x: 0, y: 0, width: 0, height: 0 } as CosmosArea));
         for (const [ index, item ] of items.entries()) {
            const itemWidth = item.length;
            const itemHeight = item[0]?.length ?? 0;
            if (itemWidth > 0 && itemHeight > 0) {
               const offsetX = (index % cols) * blockWidth;
               const offsetY = Math.floor(index / cols) * blockHeight;
               for (const [ x, subitem ] of item.entries()) {
                  const subpage = page[offsetX + x];
                  for (const [ y, color ] of subitem.entries()) {
                     subpage[offsetY + y] = color.slice() as CosmosColor;
                  }
               }
               info[index] = { x: offsetX, y: offsetY, width: itemWidth, height: itemHeight };
            }
         }
         return [ page, info ];
      } else {
         return [ [], [] ];
      }
   },
   split (page: CosmosColor[][], info: CosmosArea[]): CosmosColor[][][] {
      return info.map(area =>
         page.slice(area.x, area.x + area.width).map(subpage => subpage.slice(area.y, area.y + area.height))
      );
   },
   async pixels2base (pixels: Uint8Array, dimensions: CosmosDimensions) {
      return await CosmosImageUtils.texture2base(CosmosImageUtils.pixels2texture(pixels, dimensions));
   },
   pixels2colors (
      pixels: Uint8Array,
      dimensions: CosmosDimensions,
      crop: CosmosCrop = { bottom: 0, left: 0, right: 0, top: 0 }
   ) {
      const [ ax, ay, width, height ] = CosmosImageUtils.area(dimensions, crop);
      if (width > 0 && height > 0) {
         const colors: CosmosColor[][] = [];
         let x = 0;
         let y = 0;
         while (x < width) {
            const subcolors = (colors[x] = [] as CosmosColor[]);
            while (y < height) {
               const z = Math.floor(ax + x + (ay + y) * dimensions.width) * 4;
               subcolors.push([ pixels[z], pixels[z + 1], pixels[z + 2], pixels[z + 3] ]);
               y++;
            }
            y = 0;
            x++;
         }
         return colors;
      } else {
         return [] as CosmosColor[][];
      }
   },
   pixels2hexs (
      pixels: Uint8Array,
      dimensions: CosmosDimensions,
      crop: CosmosCrop = { bottom: 0, left: 0, right: 0, top: 0 }
   ) {
      const [ ax, ay, width, height ] = CosmosImageUtils.area(dimensions, crop);
      if (width > 0 && height > 0) {
         const hexs: number[][] = [];
         let x = 0;
         let y = 0;
         while (x < width) {
            const subhexs = (hexs[x] = [] as number[]);
            while (y < height) {
               const z = Math.floor(ax + x + (ay + y) * dimensions.width) * 4;
               subhexs.push(
                  Math.round(pixels[z]) * 65536 + Math.round(pixels[z + 1]) * 256 + Math.round(pixels[z + 2])
               );
               y++;
            }
            y = 0;
            x++;
         }
         return hexs;
      } else {
         return [] as number[][];
      }
   },
   pixels2texture (pixels: Uint8Array, dimensions: CosmosDimensions) {
      return Texture.fromBuffer(pixels, dimensions.width, dimensions.height);
   },
   synthesize (colors: CosmosColor[][]) {
      return createImageBitmap(new ImageData(new Uint8ClampedArray(colors.flat(2)), colors.length));
   },
   async texture2base (texture: Texture) {
      const sprite = Sprite.from(texture);
      const base = await CosmosImageUtils.getExtract().base64(sprite, 'image/png', 1);
      sprite.destroy();
      return base;
   },
   texture2colors (texture: Texture, crop?: CosmosCrop) {
      return CosmosImageUtils.pixels2colors(CosmosImageUtils.texture2pixels(texture), texture, crop);
   },
   texture2hexs (texture: Texture, crop?: CosmosCrop) {
      return CosmosImageUtils.pixels2hexs(CosmosImageUtils.texture2pixels(texture), texture, crop);
   },
   texture2pixels (texture: Texture) {
      const sprite = Sprite.from(texture);
      const pixels = CosmosImageUtils.getExtract().pixels(sprite);
      sprite.destroy();
      return pixels;
   }
};

// constants
export const CosmosMath = {
   COS_ARRAY: (() => {
      const arr = new Float64Array(360);
      let i = 0;
      while (i < 360) {
         arr[i] = Math.cos(i++ * (Math.PI / 180));
      }
      return arr;
   })(),
   FRAME: 100 / 3,
   FRAME_2: 100 / 6,
   SIN_ARRAY: (() => {
      const arr = new Float64Array(360);
      let i = 0;
      while (i < 360) {
         arr[i] = Math.sin(i++ * (Math.PI / 180));
      }
      return arr;
   })(),
   bezier (value: number, ...points: number[]): number {
      switch (points.length) {
         case 0:
            return value;
         case 1:
            // 0
            return points[0];
         case 2:
            if (points[0] === points[1]) {
               // 0, 0
               return points[0];
            } else {
               // 0, 1
               return (value * (points[1] - points[0])) / 1 + points[0];
            }
         case 3:
            if (points[0] === points[1]) {
               if (points[0] === points[2]) {
                  // 0, 0, 0
                  return points[0];
               } else {
                  // 0, 0, 1
                  return value ** 2 * (points[2] - points[0]) + points[0];
               }
            } else if (points[0] === points[2]) {
               // 0, 1, 0
               return (value * (value - 1) * 2 + 1) * (points[0] - points[1]) + points[1];
            } else if (points[1] === points[2]) {
               // 0, 1, 1
               return (1 - (1 - value) ** 2) * (points[1] - points[0]) + points[0];
            }
            break;
         case 4:
            if (points[0] === points[1]) {
               if (points[0] === points[2]) {
                  if (points[0] === points[3]) {
                     // 0, 0, 0, 0
                     return points[0];
                  } else {
                     // 0, 0, 0, 1
                     return value ** 3 * (points[3] - points[0]) + points[0];
                  }
               } else if (points[2] === points[3]) {
                  // 0, 0, 1, 1
                  return value ** 2 * (3 - value * 2) * (points[3] - points[0]) + points[0];
               }
            } else if (points[1] === points[2]) {
               if (points[0] === points[3]) {
                  // 0, 1, 1, 0
                  return (value * (value - 1) * 3 + 1) * (points[0] - points[1]) + points[1];
               } else if (points[1] === points[3]) {
                  // 0, 1, 1, 1
                  return (1 - (1 - value) ** 3) * (points[1] - points[0]) + points[0];
               }
            }
            break;
      }
      let total = points.length - 1;
      while (total > 0) {
         let index = 0;
         while (index < total) {
            points[index] = points[index] * (1 - value) + points[index + 1] * value;
            index++;
         }
         total--;
      }
      return points[0];
   },
   cos (i: number) {
      if (i === Math.floor(i)) {
         while (i < 0) {
            i += 360;
         }
         while (360 <= i) {
            i -= 360;
         }
         return CosmosMath.COS_ARRAY[i];
      } else {
         return Math.cos(i * (Math.PI / 180));
      }
   },
   intersection (a1: CosmosPointSimple, a2: CosmosPointSimple, b1: CosmosPointSimple, b2: CosmosPointSimple) {
      return (
         CosmosMath.rotation(a1, b1, b2) !== CosmosMath.rotation(a2, b1, b2) &&
         CosmosMath.rotation(a1, a2, b1) !== CosmosMath.rotation(a1, a2, b2)
      );
   },
   rotation (a1: CosmosPointSimple, a2: CosmosPointSimple, a3: CosmosPointSimple) {
      return (a3.y - a1.y) * (a2.x - a1.x) > (a2.y - a1.y) * (a3.x - a1.x);
   },
   linear (value: number, ...points: number[]) {
      if (points.length === 0) {
         return value;
      } else if (points.length === 1) {
         return points[0];
      } else if (value <= 0) {
         return CosmosMath.remap(value, points[0], points[1]);
      } else if (1 <= value) {
         return CosmosMath.remap(value, points[points.length - 2], points[points.length - 1]);
      } else {
         const supervalue = value * (points.length - 1);
         const index = Math.floor(supervalue);
         return CosmosMath.remap(supervalue % 1, points[index], points[index + 1]);
      }
   },
   ray (a: number, b: number) {
      return { x: b * CosmosMath.sin(a + 90), y: -b * CosmosMath.cos(a + 90) };
   },
   remap (value: number, min2: number, max2: number, min1 = 0, max1 = 1) {
      return ((value - min1) * (max2 - min2)) / (max1 - min1) + min2;
   },
   remap_clamped (value: number, min2: number, max2: number, min1 = 0, max1 = 1) {
      return Math.min(Math.max(((value - min1) * (max2 - min2)) / (max1 - min1) + min2, min2), max2);
   },
   sin (i: number) {
      if (i === Math.floor(i)) {
         while (i < 0) {
            i += 360;
         }
         while (360 <= i) {
            i -= 360;
         }
         return CosmosMath.SIN_ARRAY[i];
      } else {
         return Math.sin(i * (Math.PI / 180));
      }
   },
   /** output ranges from -size to size */
   spread (size: number, index: number, total: number) {
      if (total <= 1) {
         return 0;
      } else {
         const base = (total - 1) / 2;
         return ((index - base) / base) * size;
      }
   },
   /** output cuts range of -size to size into slices, and returns the center of the slice index */
   spread_quantize (size: number, index: number, total: number) {
      return CosmosMath.spread(size, (index % total) + 0.5, total + 1);
   },
   transform (transform: CosmosTransform, object: CosmosObject, camera = { x: 0, y: 0 }): CosmosTransform {
      let x = object.position.x;
      let y = object.position.y;
      for (const offset of object.$offsets) {
         if (offset !== void 0) {
            x += offset.x;
            y += offset.y;
         }
      }
      return [
         transform[0]
            .add(transform[2].multiply(x, y))
            .shift(transform[1], 0, transform[0])
            .add(object.parallax.multiply(camera)),
         transform[1] + object.rotation.value,
         transform[2].multiply(object.scale)
      ];
   },
   wave (value: number) {
      return CosmosMath.sin(Math.round(((value + 0.5) * 2 - 1) * 180)) / 2 + 0.5;
   },
   weigh<A> (input: CosmosProvider<[A, number][]>, modifier: number) {
      const weights = CosmosUtils.provide(input);
      let total = 0;
      for (const entry of weights) {
         total += entry[1];
      }
      const value = modifier * total;
      for (const entry of weights) {
         if (value > (total -= entry[1])) {
            return entry[0];
         }
      }
   }
};

export const CosmosTextUtils = {
   async create (
      name: string,
      size: number,
      shift: CosmosPointSimple,
      chars: string,
      resolution: number,
      cols = 10,
      preprocessor = (code: string, colors: CosmosColor[][]) => colors
   ) {
      const style = new TextStyle({ fontFamily: name, fontSize: size, fill: '#fff', padding: 20 });
      const font = BitmapFont.from(name, style, {
         chars,
         resolution,
         textureHeight: resolution * 100,
         textureWidth: resolution * 100
      });
      const entries = chars
         .split('')
         .map(char => char.charCodeAt(0).toString())
         .map(code => {
            const value = font.chars[code];
            return [ code, value ] as [typeof code, typeof value];
         });
      const [ page, info ] = CosmosImageUtils.merge(
         entries.map(entry => preprocessor(entry[0], CosmosImageUtils.texture2colors(entry[1].texture))),
         cols
      );
      const data: CosmosFontData = {
         name,
         glyphs: await Promise.all(
            entries.map(async ([ code, char ], index): Promise<CosmosGlyphData> => {
               const submetrics = TextMetrics.measureText(String.fromCharCode(+code), style);
               return {
                  area: info[index],
                  code,
                  margin: char.xAdvance ?? 0,
                  metrics: {
                     height: submetrics.height,
                     width: submetrics.width,
                     x: char.xOffset ?? 0,
                     y: char.yOffset ?? 0
                  }
               };
            })
         ),
         shift,
         size
      };
      font.destroy();
      return { data, source: await CosmosImageUtils.colors2base(page) };
   },
   cjk (char: string) {
      return /[\u3000-\u303F\u4E00-\u9FFF\uAC00-\uD7AF\u3040-\u309F\u30A0-\u30FF]/.test(char);
   },
   cjk_length (text: string) {
      let value = 0;
      for (const char of text) {
         value += CosmosTextUtils.cjk(char) ? 2 : 1;
      }
      return value;
   },
   format (text: string, length = Infinity, plain = false) {
      let output = '';
      const raw = CosmosTextUtils.raw(text);
      const indent = raw[0] === '*';
      if (raw.length > length) {
         let braces = false;
         for (const char of text) {
            output += char;
            switch (char) {
               case '{':
                  braces = true;
                  break;
               case '}':
                  braces = false;
                  break;
               default:
                  if (!braces) {
                     const lines = output.split('\n');
                     const ender = lines[lines.length - 1];
                     if (CosmosTextUtils.raw(ender).length > length) {
                        const words = ender.split(' ');
                        output = `${lines.slice(0, -1).join('\n')}${lines.length > 1 ? '\n' : ''}${words
                           .slice(0, -1)
                           .join(' ')}\n${indent ? '  ' : ''}${words[words.length - 1]}`;
                     }
                  }
            }
         }
      } else {
         output = text;
      }
      return (
         plain
            ? output
            : output
               .replace(
                  /([\u3000-\u303F\u4E00-\u9FFF\uAC00-\uD7AF\u3040-\u309F\u30A0-\u30FF])([^\u3000-\u303F\u4E00-\u9FFF\uAC00-\uD7AF\u3040-\u309F\u30A0-\u30FF]|$)/g,
                  '$1{#i/x1}$2'
               )
               .replace(
                  /(^|[^\u3000-\u303F\u4E00-\u9FFF\uAC00-\uD7AF\u3040-\u309F\u30A0-\u30FF])([\u3000-\u303F\u4E00-\u9FFF\uAC00-\uD7AF\u3040-\u309F\u30A0-\u30FF])/g,
                  '$1{#i/x2}$2'
               )
               .replace(/-/g, '-{^2}')
               .replace(/,([\n ])/g, ',{^3}$1')
               .replace(/，/g, '，{^4}')
               .replace(/~([\n ])/g, '~{^4}$1')
               .replace(/\n\*/g, '{^5}\n*')
               .replace(/([.?!。？！])([\n ])/g, '$1{^5}$2')
               .replace(/:([\n ])/g, ':{^6}$1')
               .replace(/：/g, '：{^6}')
      ).replace(/µ/g, ' ');
   },
   async import (source: string, { name, glyphs, shift, size }: CosmosFontData) {
      const page = await CosmosImageUtils.base2colors(source);
      return {
         name,
         font: {
            glyphs: Object.fromEntries(
               await Promise.all(
                  glyphs.map(async ({ area, code, margin, metrics }) => {
                     return [
                        code,
                        {
                           margin,
                           metrics,
                           texture: CosmosImageUtils.colors2texture(CosmosImageUtils.split(page, [ area ])[0])
                        }
                     ] as [string, CosmosGlyph];
                  })
               )
            ),
            shift,
            size
         }
      };
   },
   metrics (name: string, content: string, scale: number) {
      if (name in CosmosTextUtils.storage) {
         let x = 0;
         let y = 0;
         let index = 0;
         const font = CosmosTextUtils.storage[name];
         while (index < content.length) {
            const info = font.glyphs[content.charCodeAt(index++)];
            if (info) {
               x += (info.metrics.x + info.metrics.width + info.margin) / 2;
               y = Math.max(y, info.metrics.y + info.metrics.height);
            }
         }
         return new CosmosPoint(x, y).multiply(scale);
      } else {
         return new CosmosPoint();
      }
   },
   raw (text: string) {
      return text.replace(/{[^}]*}|[\[\]]/g, '');
   },
   register (name: string, font: CosmosFont) {
      CosmosTextUtils.storage[name] = font;
   },
   storage: {} as { [x in string]: CosmosFont },
   unregister (name: string) {
      delete CosmosTextUtils.storage[name];
   }
};

export const CosmosUtils = {
   chain<A, B> (input: A, handler: (input: A, loop: (input: A) => B) => B) {
      const loop = (input: A) => handler(input, loop);
      return loop(input);
   },
   hyperpromise<A = void> () {
      let hyperresolve: (value: A | PromiseLike<A>) => void;
      const promise = new Promise<A>(resolve => {
         hyperresolve = resolve;
      });
      const state = { active: true, promise, resolve: hyperresolve! };
      promise.then(() => (state.active = false));
      return state;
   },
   import<A = any> (source: string) {
      return new Promise<A>(resolve => {
         const errorListener = function () {
            xhr.removeEventListener('error', errorListener);
            xhr.removeEventListener('load', loadListener);
            resolve(null as A);
         };
         const loadListener = async function () {
            xhr.removeEventListener('error', errorListener);
            xhr.removeEventListener('load', loadListener);
            resolve(xhr.response);
         };
         const xhr = new XMLHttpRequest();
         xhr.responseType = 'json';
         xhr.timeout = 0;
         xhr.addEventListener('error', errorListener);
         xhr.addEventListener('load', loadListener);
         xhr.open('GET', source);
         xhr.send();
      });
   },
   parse<A = any> (text: string | null | void, fallback?: A): A {
      if (text || fallback === void 0) {
         return JSON.parse(text ?? '', (key, value) => {
            if (value === '\x00') {
               return Infinity;
            } else if (value === '\x01') {
               return -Infinity;
            } else if (value === '\x02') {
               return NaN;
            } else {
               return value;
            }
         });
      } else {
         return fallback;
      }
   },
   populate: ((size: number, provider: any) => {
      let index = 0;
      const array: any[] = [];
      while (index < size) {
         array.push(CosmosUtils.provide(provider, index++));
      }
      return array;
   }) as {
      <A extends (index: number) => unknown>(size: number, provider: A): ReturnType<A>[];
      <A>(size: number, provider: A): A[];
   },
   provide<A extends CosmosProvider<unknown, unknown[]>> (
      provider: A,
      ...args: A extends CosmosProvider<infer _, infer C> ? C : never
   ): A extends CosmosProvider<infer B, any[]> ? B : never {
      return typeof provider === 'function' ? provider(...args) : provider;
   },
   serialize (value: any, beautify = false) {
      return JSON.stringify(
         value,
         (key, value) => {
            if (value === Infinity) {
               return '\x00';
            } else if (value === -Infinity) {
               return '\x01';
            } else if (typeof value === 'number' && value !== value) {
               return '\x02';
            } else {
               return value;
            }
         },
         beautify ? 3 : void 0
      );
   },
   status (
      text: string,
      {
         backgroundColor = '#000',
         color = '#fff',
         fontFamily = 'monospace',
         fontSize = '16px',
         fontStyle = 'italic',
         fontWeight = 'bold',
         padding = '4px 8px'
      } = {}
   ) {
      console.log(
         `%c${text}`,
         `background-color:${backgroundColor};color:${color};font-family:${fontFamily};font-size:${fontSize};font-style:${fontStyle};font-weight:${fontWeight};padding:${padding};`
      );
   }
};

CosmosUtils.status(`LOAD MODULE: STORYTELLER (${Math.floor(performance.now()) / 1000})`, { color: '#07f' });
