import { backend, dialog, params } from './core';
import { OutertaleInventory } from './outertale';
import { CosmosKeyed, CosmosUtils } from './storyteller';
import text from './text';

// SAVE data booleans
export type OutertaleDataBooleanBase = typeof dataBoolean;
export interface OutertaleDataBoolean extends OutertaleDataBooleanBase {}

// SAVE flag booleans
export type OutertaleFlagBooleanBase = typeof flagBoolean;
export interface OutertaleFlagBoolean extends OutertaleFlagBooleanBase {}

// SAVE data numbers
export type OutertaleDataNumberBase = typeof dataNumber;
export interface OutertaleDataNumber extends OutertaleDataNumberBase {}

// SAVE flag numbers
export type OutertaleFlagNumberBase = typeof flagNumber;
export interface OutertaleFlagNumber extends OutertaleFlagNumberBase {}

// SAVE data strings
export type OutertaleDataStringBase = typeof dataString;
export interface OutertaleDataString extends OutertaleDataStringBase {}

// SAVE flag strings
export type OutertaleFlagStringBase = typeof flagString;
export interface OutertaleFlagString extends OutertaleFlagStringBase {}

export async function reportError (...identifier: string[]) {
   console.trace(new Error(text.dialog.error_access.replace('$(x)', identifier.join(':'))));
   backend?.exec('devtools', true);
   dialog.message(true, 'alert', {
      buttons: text.dialog.message_alert,
      message: `${text.dialog.error_access.replace('$(x)', identifier.join(':'))}`,
      title: text.dialog.dialog_error_title
   });
}

export function createDataStore<A extends object> (prefix: string, fallback: any, entries: any): A {
   return new Proxy(entries, {
      get (target, key) {
         if (typeof key === 'string') {
            if (!SAVE.ready) {
               reportError('data', prefix, key);
               return fallback;
            }
            return SAVE.state[prefix]?.[key] ?? entries[key] ?? fallback;
         }
      },
      set (target, key, value) {
         if (typeof key === 'string') {
            if (!SAVE.ready) {
               reportError('data', prefix, key);
               return fallback;
            }
            (SAVE.state[prefix] ??= {})[key] = value;
            return true;
         } else {
            return false;
         }
      },
      deleteProperty (target, key) {
         if (typeof key === 'string') {
            if (!SAVE.ready) {
               reportError('data', prefix, key);
               return fallback;
            }
            delete SAVE.state[prefix]?.[key];
            return true;
         } else {
            return false;
         }
      }
   }) as A;
}

export function createFlagStore<A extends object> (prefix: string, fallback: any, entries: any): A {
   return new Proxy(entries, {
      get (target, key) {
         if (typeof key === 'string') {
            if (!SAVE.ready && key[0] !== '$') {
               reportError('flag', prefix, key);
               return fallback;
            }
            const name = `${key[0] === '$' ? SAVE.safespace : SAVE.namespace}:${prefix}:${key}`;
            if (key[0] === '_' && name in SAVE.hostages) {
               return SAVE.hostages[name] ?? entries[key] ?? fallback;
            } else {
               return CosmosUtils.parse(SAVE.manager.getItem(name), entries[key] ?? fallback);
            }
         }
      },
      set (target, key, value) {
         if (typeof key === 'string') {
            if (!SAVE.ready && key[0] !== '$') {
               reportError('flag', prefix, key);
               return fallback;
            }
            const name = `${key[0] === '$' ? SAVE.safespace : SAVE.namespace}:${prefix}:${key}`;
            if (key[0] === '_' && name in SAVE.hostages) {
               SAVE.hostages[name] = value;
            } else {
               SAVE.manager.setItem(name, CosmosUtils.serialize(value));
            }
            return true;
         } else {
            return false;
         }
      },
      deleteProperty (target, key) {
         if (typeof key === 'string') {
            if (!SAVE.ready && key[0] !== '$') {
               reportError('flag', prefix, key);
               return fallback;
            }
            const name = `${key[0] === '$' ? SAVE.safespace : SAVE.namespace}:${prefix}:${key}`;
            if (key[0] === '_' && name in SAVE.hostages) {
               SAVE.hostages[name] = null;
            } else {
               SAVE.manager.removeItem(name);
            }
            return true;
         } else {
            return false;
         }
      }
   }) as A;
}

export function info<A> (defaultValue = null as A): A {
   return defaultValue;
}

export const dataBoolean = {
   placeholder: info<boolean>()
};

export const dataNumber = {
   // random number generator base values
   base_dialogue: info<number>(),
   base_overworld: info<number>(),

   /** current EXP (LV is calculated from this) */
   exp: info<number>(),

   /** current G */
   g: info<number>(),

   /** current HP */
   hp: info<number>(),

   /** masa shop undyne topic */
   shop_masa_undyne: info<number>(),

   /** elapsed time in frames */
   time: info<number>()
};

export const dataString = {
   /** current armor */
   armor: info<string>(),

   /** current room */
   room: info<string>(),

   /** current weapon */
   weapon: info<string>()
};

export const flagBoolean = {
   /** true if epilepsy-safe mode is enabled */
   $option_epilepsy: info<boolean>(),

   /** true if fancy graphics is enabled */
   $option_fancy: info<boolean>(backend !== null),

   /** true if music is disabled */
   $option_music: info<boolean>(),

   /** left-handed mobile */
   $option_right: info<boolean>(),

   /** true if sfx is disabled */
   $option_sfx: info<boolean>()
};

export const flagNumber = {
   /** joystick activation threshold */
   $option_deadzone: info<number>(0.5),

   /** music level */
   $option_music: info<number>(0.2),

   /** sfx level */
   $option_sfx: info<number>(0.2)
};

export const flagString = {
   // gamepad inputs
   $gamepad_input_z: info<string>(),
   $gamepad_input_x: info<string>(),
   $gamepad_input_c: info<string>(),
   $gamepad_input_u: info<string>(),
   $gamepad_input_l: info<string>(),
   $gamepad_input_d: info<string>(),
   $gamepad_input_r: info<string>(),
   $gamepad_input_f: info<string>()
};

export const dataBooleanStore = createDataStore<OutertaleDataBoolean>('b', false, dataBoolean);
export const dataNumberStore = createDataStore<OutertaleDataNumber>('n', 0, dataNumber);
export const dataStringStore = createDataStore<OutertaleDataString>('s', '', dataString);
export const flagBooleanStore = createFlagStore<OutertaleFlagBoolean>('b', false, flagBoolean);
export const flagNumberStore = createFlagStore<OutertaleFlagNumber>('n', 0, flagNumber);
export const flagStringStore = createFlagStore<OutertaleFlagString>('s', '', flagString);

export const SAVE = {
   data: { b: dataBooleanStore, n: dataNumberStore, s: dataStringStore },
   flag: { b: flagBooleanStore, n: flagNumberStore, s: flagStringStore },
   hostages: {} as Partial<CosmosKeyed>,
   internal: {
      dirty: false,
      entries: [] as [string, string][],
      write () {
         backend?.writeSave(SAVE.serialize());
      }
   },
   keys () {
      return CosmosUtils.populate(SAVE.manager.length, index => SAVE.manager.key(index)!);
   },
   manager: ((): Storage => {
      if (backend === null) {
         return localStorage;
      } else {
         return {
            clear () {
               SAVE.internal.entries.splice(0, SAVE.internal.entries.length);
               SAVE.internal.write();
            },
            getItem (key: string) {
               for (const item of SAVE.internal.entries) {
                  if (key === item[0]) {
                     return item[1];
                  }
               }
               return null;
            },
            key (index: number) {
               return SAVE.internal.entries[index]?.[0] ?? null;
            },
            get length () {
               return SAVE.internal.entries.length;
            },
            removeItem (key: string) {
               for (const [ index, item ] of SAVE.internal.entries.entries()) {
                  if (key === item[0]) {
                     SAVE.internal.entries.splice(index, 1);
                     SAVE.internal.write();
                     return;
                  }
               }
            },
            setItem (key: string, value: string) {
               for (const item of SAVE.internal.entries) {
                  if (key === item[0]) {
                     item[1] = value;
                     SAVE.internal.write();
                     return;
                  }
               }
               SAVE.internal.entries.push([ key, value ]);
               SAVE.internal.write();
            }
         };
      }
   })(),
   namespace: 'SPACETIME-F',
   parse (content: string | null | void) {
      return CosmosUtils.parse<[string, any][]>(content, []).map(
         ([ key, value ]) =>
            [ key, typeof value === 'string' && !key.includes(':s') ? value : CosmosUtils.serialize(value) ] as [
               string,
               string
            ]
      );
   },
   ready: false,
   safespace: 'UNIVERSAL-F',
   save (state: CosmosKeyed | null = null) {
      SAVE.manager.setItem(SAVE.namespace, CosmosUtils.serialize(state ?? SAVE.state));
      for (const [ key, value ] of Object.entries(SAVE.hostages)) {
         value === null ? SAVE.manager.removeItem(key) : SAVE.manager.setItem(key, value!);
         delete SAVE.hostages[key];
      }
   },
   saveswap: false,
   serialize (beautify = false) {
      return CosmosUtils.serialize(
         SAVE.keys().map(key => [ key, CosmosUtils.parse(SAVE.manager.getItem(key) ?? '') ]),
         beautify
      );
   },
   state: {} as CosmosKeyed,
   storage: {
      inventory: new OutertaleInventory(8, dataStringStore, index => `inventory_${index}`),
      dimboxA: new OutertaleInventory(10, dataStringStore, index => `dimboxA_${index}`),
      dimboxB: new OutertaleInventory(10, dataStringStore, index => `dimboxB_${index}`)
   },
   strings (contents: string | null | void) {
      return CosmosUtils.parse<{ s?: Partial<OutertaleDataString> }>(contents, {}).s ?? {};
   },
   transfer (target: Storage) {
      if (SAVE.manager !== target) {
         for (const key of SAVE.keys()) {
            target.setItem(key, SAVE.manager.getItem(key)!);
         }
         SAVE.manager = target;
      }
   }
};

SAVE.internal.entries.push(...SAVE.parse(backend?.data()));

CosmosUtils.status(`LOAD MODULE: SAVE (${Math.floor(performance.now()) / 1000})`, { color: '#07f' });
