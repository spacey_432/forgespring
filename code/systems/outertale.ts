import { Graphics, Texture } from 'pixi.js';
import {
   CosmosAnimation,
   CosmosAnimationInfo,
   CosmosAnimationResources,
   CosmosArea,
   CosmosAsset,
   CosmosBaseEvents,
   CosmosDaemon,
   CosmosDirection,
   CosmosHitbox,
   CosmosImage,
   CosmosInventory,
   CosmosKeyed,
   CosmosMetadata,
   CosmosObject,
   CosmosObjectProperties,
   CosmosPoint,
   CosmosPointSimple,
   CosmosProvider,
   CosmosRectangle,
   CosmosSprite,
   CosmosSpriteProperties,
   CosmosUtils
} from './storyteller';

export type OutertaleChildAnimationDecorator = {
   anchor?: Partial<CosmosPointSimple>;
   active?: boolean;
   position?: Partial<CosmosPointSimple>;
   rotation?: number;
   filters?: string[];
   frames?: undefined;
   resources?: string;
   steps?: undefined;
   type: string;
};

export type OutertaleChildBarrierDecorator = {
   anchor?: Partial<CosmosPointSimple>;
   position?: Partial<CosmosPointSimple>;
   rotation?: number;
   size?: Partial<CosmosPointSimple>;
};

export type OutertaleChildObject =
   | CosmosSprite<CosmosBaseEvents, { class: 'attachment'; decorator: OutertaleSpriteDecorator }>
   | CosmosAnimation<CosmosBaseEvents, { class: 'attachment'; decorator: OutertaleChildAnimationDecorator }>
   | CosmosHitbox<
        CosmosBaseEvents,
        | { class: 'barrier'; decorator: OutertaleChildBarrierDecorator }
        | {
             args: string[];
             class: 'interact' | 'trigger';
             decorator: OutertaleChildScriptDecorator;
             name: string;
          }
     >;

export type OutertaleChildScriptDecorator = {
   anchor?: Partial<CosmosPointSimple>;
   args?: string[];
   name?: string;
   position?: Partial<CosmosPointSimple>;
   rotation?: number;
   size?: Partial<CosmosPointSimple>;
};

export type OutertaleEditorContainer = {
   box: CosmosRectangle;
   children: { box: CosmosRectangle; object: OutertaleChildObject; parent: OutertaleEditorContainer }[];
   object: OutertaleParentObject;
};

export type OutertaleLayerKey = 'base' | 'below' | 'main' | 'above' | 'menu';

export type OutertaleSpriteDecorator = {
   anchor?: Partial<CosmosPointSimple>;
   active?: boolean;
   position?: Partial<CosmosPointSimple>;
   rotation?: number;
   filters?: string[];
   frames?: string[];
   resources?: undefined;
   steps?: number;
   type: string;
};

export type OutertaleParentObject = CosmosObject<
   CosmosBaseEvents,
   { class: 'object'; decorator: OutertaleParentObjectDecorator; tags: string[] }
> & {
   objects: OutertaleChildObject[];
};

export type OutertaleParentObjectDecorator = {
   attachments?: (OutertaleChildAnimationDecorator | OutertaleSpriteDecorator)[];
   barriers?: OutertaleChildBarrierDecorator[];
   filters?: string[];
   interacts?: OutertaleChildScriptDecorator[];
   position?: Partial<CosmosPointSimple>;
   rotation?: number;
   tags?: string[];
   triggers?: OutertaleChildScriptDecorator[];
};

export type OutertaleRoomDecorator = {
   background?: string;
   face?: string;
   layers?: Partial<CosmosKeyed<OutertaleParentObjectDecorator[], string>>;
   mixins?: Partial<CosmosKeyed<string, string>>;
   metadata?: CosmosKeyed<any>;
   neighbors?: string[];
   preload?: string[];
   region?: CosmosPointSimple[];
   score?: { filter?: number; gain?: number; music?: string | null; rate?: number; reverb?: number };
   spawn?: CosmosPointSimple;
};

export class OutertaleInventory {
   capacity: number;
   keygen: (index: number) => string;
   store: CosmosKeyed<string>;
   get contents () {
      return CosmosUtils.populate(this.size, index => this.store[this.keygen(index)]);
   }
   get size () {
      let index = 0;
      while (this.store[this.keygen(index)]) {
         index++;
      }
      return index;
   }
   constructor (capacity: number, store: CosmosKeyed<string>, keygen: (index: number) => string) {
      this.capacity = capacity;
      this.keygen = keygen;
      this.store = store;
   }
   add (item: string) {
      const size = this.size;
      if (size < this.capacity) {
         this.store[this.keygen(size)] = item;
         return true;
      } else {
         return false;
      }
   }
   count (item: string) {
      let index = 0;
      let value = 0;
      let match: string;
      while ((match = this.store[this.keygen(index++)])) {
         if (match === item) {
            value++;
         }
      }
      return value;
   }
   has (item: string) {
      let index = 0;
      let match: string;
      while ((match = this.store[this.keygen(index++)])) {
         if (match === item) {
            return true;
         }
      }
      return false;
   }
   remove (target: number | string) {
      if (typeof target === 'number') {
         const size = this.size;
         if (target < size) {
            while (target < size) {
               const next = this.store[this.keygen(target + 1)];
               if (next) {
                  this.store[this.keygen(target++)] = next;
               } else {
                  delete this.store[this.keygen(target)];
                  break;
               }
            }
            return true;
         } else {
            return false;
         }
      } else {
         let index = 0;
         let match: string;
         while ((match = this.store[this.keygen(index)])) {
            if (match === target) {
               void this.remove(index);
               return true;
            } else {
               index++;
            }
         }
         return false;
      }
   }
   of (index: number) {
      if (index < this.size) {
         return this.store[this.keygen(index)];
      } else {
         return null;
      }
   }
   set (index: number, value: string) {
      this.store[this.keygen(index)] = value;
   }
}

export class OutertaleMap extends CosmosAsset<CosmosKeyed<{ area: CosmosArea; offset: CosmosPointSimple }>> {
   image: CosmosImage;
   source: string;
   get value () {
      return super.value ?? {};
   }
   constructor (source: string, image: CosmosImage) {
      super();
      this.image = image;
      this.source = source;
   }
   async loader () {
      const [ data ] = await Promise.all([
         CosmosUtils.import<CosmosAnimationInfo>(this.source),
         this.image.load(this as any)
      ]);
      return Object.fromEntries(
         data.meta.frameTags.map(frameTag => {
            const { frame, spriteSourceSize } = data.frames[frameTag.from];
            return [
               frameTag.name,
               { area: { x: frame.x, y: frame.y, width: frame.w, height: frame.h }, offset: spriteSourceSize }
            ];
         })
      );
   }
   unloader () {
      this.image.unload(this);
   }
}

export class OutertaleMultivisualObject extends CosmosObject {
   sprite: CosmosSprite;
   animation: CosmosAnimation;
   constructor (properties: CosmosObjectProperties = {}, subproperties: CosmosSpriteProperties = {}) {
      super(properties);
      this.sprite = new CosmosSprite(subproperties);
      this.animation = new CosmosAnimation(subproperties);
   }
   use (asset: CosmosImage | CosmosAnimationResources) {
      if (asset.value instanceof Texture) {
         this.sprite.frames = [ asset as CosmosImage ];
         this.objects = [ this.sprite ];
      } else {
         this.animation.use(asset as CosmosAnimationResources);
         this.objects = [ this.animation ];
      }
      return this;
   }
}

export interface OutertaleRoomProperties {
   decorator?: OutertaleRoomDecorator | null;
   face?: CosmosDirection;
   layers?: Partial<CosmosKeyed<CosmosObject[], OutertaleLayerKey>>;
   metadata?: CosmosKeyed<any>;
   neighbors?: string[];
   preload?: CosmosInventory;
   region?: [Partial<CosmosPointSimple>, Partial<CosmosPointSimple>] | [];
   score?: {
      filter?: number;
      gain?: number;
      music?: string | null;
      rate?: number;
      reverb?: number;
   };
   spawn?: Partial<CosmosPointSimple>;
}

export class OutertaleRoom {
   decorator: OutertaleRoomDecorator | null;
   face: CosmosDirection;
   layers: Partial<CosmosKeyed<CosmosObject[], OutertaleLayerKey>>;
   metadata: CosmosKeyed<any>;
   neighbors: string[];
   preload: CosmosInventory;
   region: [Partial<CosmosPointSimple>, Partial<CosmosPointSimple>] | [];
   score: { filter: number; gain: number; music: string | null; rate: number; reverb: number };
   spawn: Partial<CosmosPointSimple>;
   constructor ({
      decorator = null,
      face = 'down',
      layers = {},
      metadata = {},
      neighbors = [],
      preload = new CosmosInventory(),
      region = [],
      score: { filter = 0, gain = 1, music = null, rate = 1, reverb = 0 } = {},
      spawn = {}
   }: OutertaleRoomProperties = {}) {
      this.decorator = decorator;
      this.face = face;
      this.layers = layers;
      this.metadata = metadata;
      this.neighbors = neighbors;
      this.preload = preload;
      this.region = region;
      this.score = { filter, gain, music, rate, reverb };
      this.spawn = spawn;
   }
}

export interface OutertaleShopProperties {
   background?: CosmosSprite;
   handler?: () => Promise<void> | void;
   keeper?: CosmosSprite;
   music?: CosmosDaemon | null;
   options?: CosmosProvider<string[]>;
   persist?: () => boolean;
   preset?: (...indices: number[]) => void;
   price?: CosmosProvider<number>;
   prompt?: CosmosProvider<string>;
   purchase?: (buy: boolean) => void | boolean;
   size?: CosmosProvider<number>;
   status?: CosmosProvider<string>;
   tooltip?: CosmosProvider<string | null>;
   vars?: CosmosKeyed<any>;
}

export class OutertaleShop {
   background: CosmosSprite;
   handler: () => Promise<void> | void;
   keeper: CosmosSprite;
   music: CosmosDaemon | null;
   options: CosmosProvider<string[]>;
   preset: (...indices: number[]) => void;
   price: CosmosProvider<number>;
   prompt: CosmosProvider<string>;
   purchase: (buy: boolean) => void | boolean;
   size: CosmosProvider<number>;
   status: CosmosProvider<string>;
   tooltip: CosmosProvider<string | null>;
   vars: CosmosKeyed<any>;
   constructor ({
      background = new CosmosSprite(),
      handler = () => {},
      keeper = new CosmosSprite(),
      music = null,
      options = [],
      preset = () => {},
      price = 0,
      prompt = '',
      purchase = () => {},
      size = 0,
      status = '',
      tooltip = null,
      vars = {}
   }: OutertaleShopProperties = {}) {
      this.background = background;
      this.handler = handler;
      this.keeper = keeper;
      this.music = music;
      this.options = options;
      this.preset = preset;
      this.price = price;
      this.prompt = prompt;
      this.purchase = purchase;
      this.size = size;
      this.status = status;
      this.tooltip = tooltip;
      this.vars = vars;
   }
}

export interface OutertaleSpeechPresetProperties {
   chunksize?: number;
   faces?: (CosmosSprite | null)[];
   interval?: number;
   voices?: (CosmosDaemon[] | null)[];
   fontName1?: string;
   fontName2?: string;
   fontSize1?: number;
   fontSize2?: number;
   threshold?: number;
}

export class OutertaleSpeechPreset {
   chunksize: number;
   faces: (CosmosSprite | null)[];
   interval: number;
   voices: (CosmosDaemon[] | null)[];
   fontName1: string;
   fontName2: string;
   fontSize1: number;
   fontSize2: number;
   threshold: number;
   constructor ({
      chunksize = 1,
      faces = [],
      interval = 0,
      voices = [],
      fontName1 = 'DeterminationMono',
      fontName2 = 'DotumChe',
      fontSize1 = 16,
      fontSize2 = 9,
      threshold = 0
   }: OutertaleSpeechPresetProperties = {}) {
      this.chunksize = chunksize;
      this.faces = faces;
      this.interval = interval;
      this.voices = voices;
      this.fontName1 = fontName1;
      this.fontName2 = fontName2;
      this.fontSize1 = fontSize1;
      this.fontSize2 = fontSize2;
      this.threshold = threshold;
   }
}

export class OutertaleStat {
   modifiers: ['add' | 'multiply', number, number][] = [];
   provider: CosmosProvider<number>;
   get base () {
      return CosmosUtils.provide(this.provider);
   }
   constructor (provider: CosmosProvider<number>) {
      this.provider = provider;
   }
   compute () {
      let v = this.base;
      for (const mod of this.modifiers) {
         if (mod[0] === 'add') {
            v += mod[1];
         } else {
            v *= mod[1];
         }
      }
      return v;
   }
   elapse () {
      for (const mod of this.modifiers.slice()) {
         --mod[2] <= 0 && this.modifiers.splice(this.modifiers.indexOf(mod), 1);
      }
   }
   reset () {
      this.modifiers = [];
   }
}

CosmosUtils.status(`LOAD MODULE: OUTERTALE (${Math.floor(performance.now()) / 1000})`, { color: '#07f' });
