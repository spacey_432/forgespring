// START-TRANSLATE
export default {
   dialog: {
      dialog_clear_title: 'Clear File',
      dialog_error_title: 'Error',
      dialog_notice_title: 'Notice',
      dialog_open: { buttonLabel: 'Open', name: 'SAVE files', title: 'Open File' },
      dialog_save: { buttonLabel: 'Save', name: 'SAVE files', title: 'Save File' },
      error_access: 'Invalid SAVE file access "$(x)"',
      error_load: 'That file could not be parsed.',
      error_mod: 'The mod "$(x)" could not be loaded.',
      message_alert: [ 'OK' ],
      message_confirm: [ 'Cancel', 'OK' ],
      prompt_clear: 'Clear this file?',
      prompt_demo: 'Your SAVE file from the\nFORGESPRING demo has been\nmoved to a timeline slot.',
      prompt_save: 'Save this file?',
      prompt_open: 'Load this file?'
   },

   extra: {
      credits: [ [ '§fill:#ff0§< DEVELOPER >§fill:#fff§', 'spacey_432' ] ],

      quitText1: 'Quitting',
      quitText2: 'Quitting.',
      quitText3: 'Quitting..',

      restartText1: 'Restarting',
      restartText2: 'Restarting.',
      restartText3: 'Restarting..'
   },

   gamepad: {
      prompt: 'GAMEPAD SETUP',
      prompt_desc:
         'Use an input on your gamepad to assign\nit to the in-game action.\n\nUse the input again to confirm, or use\nother inputs to assign those as well.\n\nPress ESC to skip setup.',
      prompt_counter: 'Inputs Assigned: $(x)',
      z: '[Z OR ENTER] - Confirm',
      x: '[X OR SHIFT] - Cancel',
      c: '[C OR CTRL] - Menu (In-game)',
      u: '[UP or W] - Move Up',
      l: '[LEFT or A] - Move Left',
      d: '[DOWN or S] - Move Down',
      r: '[RIGHT or D] - Move Right',
      f: '[F4] - Fullscreen',
      prompt_done: 'Setup complete.\nPress any button to continue.',
      prompt_done_browser: '\nNote: On the browser, the gamepad may\nnot always be able to enter fullscreen.',
      prompt_load:
         'A gamepad has already been set up.\nPress any button to continue, or press\nany button three times in rapid\nsuccession to restart setup.\n\nPress ESC to skip setup.'
   },

   general: {
      disabled: 'DISABLED',
      enabled: 'ENABLED',
      finish: 'Press [X] to Finish',
      g: 'G',
      hp: 'HP',
      inf: 'INF',
      landing1: '[PRESS Z OR ENTER]',
      lv: 'LV',
      name: 'Chara',
      no: 'No',
      percent: '$(x)%',
      settings: 'Settings',
      unknown: '?',
      yes: 'Yes'
   },

   menu: {
      box1: 'INVENTORY',
      box2: 'BOX',
      key1: 'KEYRING',

      confirm1: 'That password is invalid.',
      confirm2: 'Go back',
      confirm3: {
         // no names
         '': 'You must enter a password.'
      },
      confirm4: [] as string[],

      footer: 'UNDERTALE? VX.00X (C) ASRIEL DREEMURR 2015',

      heal1: '* (HP fully restored.)',
      heal2: '* (You recovered $(x) HP.)',

      item1: 'USE',
      item1_offset: 0,
      item2: 'EQUIP',
      item2_offset: 7,
      item3: 'INFO',
      item4: 'DROP',

      load1: 'Continue',
      load2: 'Password',

      name1: 'Enter a password.',
      name2: 'Quit',
      name3: 'Backspace',
      name4: 'Done',
      name5: [
         [ 'A', 'B', 'C', 'D', 'E', 'F', 'G' ],
         [ 'H', 'I', 'J', 'K', 'L', 'M', 'N' ],
         [ 'O', 'P', 'Q', 'R', 'S', 'T', 'U' ],
         [ 'V', 'W', 'X', 'Y', 'Z' ],
         [ 'a', 'b', 'c', 'd', 'e', 'f', 'g' ],
         [ 'h', 'i', 'j', 'k', 'l', 'm', 'n' ],
         [ 'o', 'p', 'q', 'r', 's', 't', 'u' ],
         [ 'v', 'w', 'x', 'y', 'z' ]
      ],
      name6: (index: number) => {
         const x = (index % 26) % 7;
         const y = Math.floor((index % 26) / 7);
         if (index < 26) {
            return { x: 120 + x * 64, y: 158 + y * 28 };
         } else {
            return { x: 120 + x * 64, y: 278 + y * 28 };
         }
      },

      save1: 'Save',
      save2: 'Return',
      save3: 'File saved.',

      settings1: 'SETTINGS',
      settings2: 'EXIT',
      settings3: 'SOUND FX',
      settings4: 'MUSIC',
      settings5: 'FANCY GRAPHICS',
      settings6: 'FLASHING IMAGERY',
      settings6H: 'NORMAL',
      settings6L: 'REDUCED',
      settings7: 'ALIGN CONTROLS',
      settings7L: 'LEFT',
      settings7R: 'RIGHT',
      settings8: 'DEADZONE',
      settings9: 'OPEN MOD FOLDER',

      sidebar1: 'ITEM',
      sidebar2: 'STAT',
      sidebar3: 'CELL',

      start1: [
         '--- Instruction ---',
         '[Z or ENTER] - Confirm',
         '[X or SHIFT] - Cancel',
         '[C or CTRL] - Menu (In-game)',
         '[F4] - Fullscreen',
         '[Hold ESC] - Restart',
         'When HP is 0, you lose.'
      ],
      start2: 'Begin Game',

      stat1: 'AT',
      stat2: 'DF',
      stat3: 'WEAPON',
      stat4: 'ARMOR',
      stat5: 'GOLD',
      stat6: 'EXP',
      stat7: 'NEXT',

      story1: [ '{#p/twinkly}{#i/3}Once upon a time, {^3}a stupid HUMAN fell down a mountain.{^35}' ],
      story2: [ 'Lost and confused, {^3}they stumbled upon a "friendly" FLOWER...{^35}' ],
      story3: [ '{#v/1}{#i/5}Who tried taking their SOUL.{^35}' ],
      story4: [ 'After all, {^3}the world was KILL or BE killed.{^25}' ],
      story5: [ '{#i/3}However{^5}.{^5}.{^5}.{^7}' ],
      story6: [ 'That IDIOT refused to kill.{^3}' ],
      story7: [ 'Wiping away their worst mistakes, {^3}they fought bloodlust with kindness and MERCY...{^7}' ],
      story8: [ 'And while the Flower was confused at first...', 'He soon used this to his advantage.{^3}' ],
      story9: [
         'Gathering the SOULs of their WORTHLESS friends, {^3}the Flower became a GOD...{^7}',
         'And began to reset time.{^3}'
      ],
      story10: [ 'But something went wrong.{^3}' ],
      story11: [ '{^30}' ],
      story12: [ '{^30}' ],
      story13: [ '{^30}' ],
      story14: [ '{^30}' ],
      story15: [ '{^30}' ]
   }
};
// END-TRANSLATE
