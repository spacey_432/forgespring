import { AdvancedBloomFilter } from '@pixi/filter-advanced-bloom';
import { OutlineFilter } from '@pixi/filter-outline';
import { isMobile } from 'pixi.js';
import {
   content,
   context,
   filters,
   inventories,
   music,
   musicConvolver,
   musicFilter,
   musicMixer,
   musicRegistry,
   soundDelay,
   soundMixer,
   soundRegistry,
   sounds
} from './assets';
import {
   atlas,
   backend,
   events,
   exit,
   fullscreen,
   game,
   items,
   keys,
   maps,
   reload,
   reload_full,
   renderer,
   rng,
   rooms,
   spawn,
   speech,
   typer
} from './core';
import { OutertaleLayerKey, OutertaleMap, OutertaleRoom, OutertaleRoomDecorator, OutertaleShop } from './outertale';
import { SAVE } from './save';
import {
   CosmosAnimation,
   CosmosAnimationResources,
   CosmosAsset,
   CosmosCache,
   CosmosCharacter,
   CosmosCharacterPreset,
   CosmosCharacterProperties,
   CosmosDaemon,
   CosmosDirection,
   CosmosHitbox,
   CosmosImage,
   CosmosInstance,
   CosmosInventory,
   CosmosKeyboardInput,
   CosmosKeyed,
   CosmosMath,
   CosmosNavigator,
   CosmosNot,
   CosmosObject,
   CosmosPlayer,
   CosmosPoint,
   CosmosPointSimple,
   CosmosProvider,
   CosmosRectangle,
   CosmosRegion,
   CosmosRegistry,
   CosmosRenderer,
   CosmosSizedObjectProperties,
   CosmosSprite,
   CosmosText,
   CosmosTextProperties,
   CosmosTextUtils,
   CosmosUtils,
   CosmosValue,
   CosmosValueRandom
} from './storyteller';
import text from './text';

export const namerange = text.menu.name5.flat();

export const buttons = [
   new CosmosSprite({
      alpha: 0.5,
      anchor: 0,
      scale: 0.5,
      metadata: {
         target: 'menuKey' as 'menuKey',
         sides: [
            { x: 292.5, y: 161.5 },
            { x: 107.5, y: 198.5 }
         ],
         size: 36,
         touches: [] as number[]
      },
      frames: [ content.ieButtonC ]
   }),
   new CosmosSprite({
      alpha: 0.5,
      anchor: 0,
      scale: 0.5,
      metadata: {
         target: 'specialKey' as 'specialKey',
         sides: [
            { x: 252.5, y: 180 },
            { x: 67.5, y: 180 }
         ],
         size: 36,
         touches: [] as number[]
      },
      frames: [ content.ieButtonX ]
   }),
   new CosmosSprite({
      alpha: 0.5,
      anchor: 0,
      scale: 0.5,
      metadata: {
         target: 'interactKey' as 'interactKey',
         sides: [
            { x: 212.5, y: 198.5 },
            { x: 27.5, y: 161.5 }
         ],
         size: 36,
         touches: [] as number[]
      },
      frames: [ content.ieButtonZ ]
   }),
   new CosmosSprite({
      alpha: 0.5,
      anchor: 0,
      scale: 0.5,
      metadata: {
         target: null,
         sides: [
            { x: 45, y: 180 },
            { x: 275, y: 180 }
         ],
         size: 72,
         touches: [] as number[],
         joystick: [
            [ 0, 'rightKey', { active: false } ],
            [ 90, 'downKey', { active: false } ],
            [ 180, 'leftKey', { active: false } ],
            [ 270, 'upKey', { active: false } ]
         ] as [number, keyof typeof keys, { active: boolean }][]
      },
      frames: [ content.ieButtonM ],
      objects: [
         new CosmosRectangle({
            alpha: 0.5,
            anchor: 0,
            size: { x: 47, y: 47 },
            stroke: '#000',
            border: 1.5
         }),
         new CosmosRectangle({
            anchor: 0,
            size: { x: 47, y: 47 },
            stroke: '#fff',
            border: 0.5
         })
      ]
   }).on('tick', function () {
      const position = (
         this.metadata.touches.length > 0 ? mobile.state.touches[this.metadata.touches[0]]![1] : this.position
      )
         .subtract(this.position)
         .clamp(-22, 22);
      const distance = position.extent;
      const direction = distance > mobile.deadzone ? (position.angle + 360) % 360 : 0;
      for (const [ angle, key, state ] of this.metadata.joystick) {
         const active =
            distance > mobile.deadzone &&
            (key === 'rightKey'
               ? direction > 360 - mobile.arc || direction < angle + mobile.arc
               : direction > angle - mobile.arc && direction < angle + mobile.arc);
         if (active !== state.active) {
            state.active = active;
            keys[key].force1 = active;
         }
      }
      this.objects[0].position.set(position.multiply(2));
      this.objects[1].position.set(position.multiply(2));
   }),
   new CosmosSprite({
      alpha: 0.5,
      anchor: 0,
      scale: 0.5,
      metadata: {
         target: null,
         sides: [
            { x: 20, y: 20 },
            { x: 20, y: 20 }
         ],
         size: 36,
         touches: [] as number[],
         pressed: false
      },
      frames: [ content.ieButtonF ]
   }).on('tick', function () {
      if (this.metadata.touches.length !== 0) {
         this.metadata.pressed = true;
      } else if (this.metadata.pressed) {
         this.metadata.pressed = false;
         backend?.exec('f4') ?? fullscreen();
      }
   })
];

/** choicer (seperate from battle choice system) */
export const choicer = {
   /** rows available in choicer */
   type: 0,
   /** spacing from the left edge to the left choicer options */
   marginA: 0,
   /** spacing from the center to the right choicer options */
   marginB: 0,
   /** position (row of text where the choicer appears) */
   navigator: null as string | null,
   /** result (what the player selects) */
   result: 0,
   /** create choicer */
   create (
      header: string,
      ...options: [string, string] | [string, string, string, string] | [string, string, string, string, string, string]
   ) {
      let total = 0;
      for (const option of options) {
         total += CosmosTextUtils.cjk_length(CosmosTextUtils.raw(option));
      }
      const margin = Math.round((16 - total / options.length) / 2);
      const segments = [] as string[];
      for (const option of options) {
         segments.push(
            `${CosmosUtils.populate(margin, () => 'µ').join('')}${option}${CosmosUtils.populate(
               16 - margin - CosmosTextUtils.cjk_length(CosmosTextUtils.raw(option)),
               () => 'µ'
            ).join('')}`
         );
      }
      if (options.length === 2) {
         return `<99>{#p/human}${header}{!}\n${header.includes('\n') ? '' : '\n'}${segments[0]}${
            segments[1]
         }{#c/0/${margin}/${margin}}`;
      } else if (options.length === 4) {
         return `<99>{#p/human}${header}{!}\n${segments[0]}${segments[1]}\n${segments[2]}${segments[3]}{#c/1/${margin}/${margin}}`;
      } else {
         return `<99>{#p/human}{!}${segments[0]}${segments[1]}\n${segments[2]}${segments[3]}\n${segments[4]}${segments[5]}{#c/2/${margin}/${margin}}`;
      }
   }
};

export function dialogueObjects () {
   return [
      new CosmosObject({ position: { x: 34, y: 35 } }),
      menuText(0, 0, () => game.text).on('tick', function () {
         this.position.x = speech.state.face ? 69 : 11;
         switch (speech.state.fontName1) {
            case 'Papyrus':
               this.spacing.x = -0.375;
               this.spacing.y = 2;
               this.position.y = 6;
               break;
            default:
               this.spacing.x = 0;
               this.spacing.y = 5;
               this.position.y = 9;
               break;
         }
      })
   ];
}

export const dialogueSession = { active: false, movement: false };

export const expLevels = [
   10, 30, 70, 120, 200, 300, 500, 800, 1200, 1700, 2500, 3500, 5000, 7000, 10000, 15000, 25000, 50000, 65535
];

// front end state
export const frontEnder = {
   // sfx test function (settings menu)
   testMusic () {
      frontEnder.updateMusic();
      sounds.menuMusic.instance(renderer);
   },
   testSFX () {
      frontEnder.updateSFX();
      sounds.menu.instance(renderer);
   },
   // current story panel index
   index: 0,
   // story panel music
   introMusic: null as CosmosInstance | null,
   // impact noise
   impactNoise: null as null | CosmosInstance,
   // menu music
   menuMusic: null as null | CosmosInstance,
   /** skip story panels */
   nostory: false,
   // PAN PANEL UPWARDS
   scroll: new CosmosValue(1),
   /** correct menu music to play */
   get menuMusicResource () {
      return { asset: content.amMenu, daemon: music.menu };
   },
   name: {
      // blacklisted names
      whitelist: text.menu.confirm4,
      // amount of shake to display on name
      shake: new CosmosValue(),
      // currently entered name
      value: '',
      // get name for multimode menus
      get value_true () {
         return frontEnder.name.value;
      }
   },
   createBackground (priority = 0) {
      return new CosmosObject({ priority });
   },
   updateRight () {
      const right = SAVE.flag.b.$option_right;
      for (const button of buttons) {
         button.position.set(button.metadata.sides[right ? 1 : 0]);
      }
   },
   updateMusic () {
      musicMixer.value = ((1 - SAVE.flag.n.$option_music) * (SAVE.flag.b.$option_music ? 0 : 1)) ** 1.5;
   },
   updateSFX () {
      soundMixer.value = ((1 - SAVE.flag.n.$option_sfx) * (SAVE.flag.b.$option_sfx ? 0 : 1)) ** 1.5;
   },
   updateEpilepsy () {
      renderer.shake_limit = SAVE.flag.b.$option_epilepsy ? 1 : Infinity;
   },
   updateFancy () {
      CosmosRenderer.fancy = SAVE.flag.b.$option_fancy;
   },
   updateDeadzone () {
      gamepadder.deadzone = SAVE.flag.n.$option_deadzone;
   }
};

export const gamepadder = {
   deadzone: 0.5,
   get gamepad () {
      return gamepadder.index === null ? null : navigator.getGamepads()[gamepadder.index];
   },
   index: null as number | null,
   mappings: null as { button: { pressed: boolean }[]; input: CosmosKeyboardInput | 'f'; active: boolean }[] | null,
   renderer: null as CosmosRenderer | null,
   async connect (gamepad: Gamepad) {
      gamepadder.disconnect();
      gamepadder.index = gamepad.index;
      const ggRenderer = (gamepadder.renderer = new CosmosRenderer({
         auto: true,
         wrapper: '#wrapper',
         layers: { main: [] },
         width: 640,
         height: 480,
         scale: 2,
         position: { x: 160, y: 120 }
      }));
      ggRenderer.canvas.style.zIndex = '99';
      const bg = new CosmosRectangle({
         fill: '#000000cf',
         size: { x: 320, y: 240 },
         fontName: 'DeterminationSans',
         objects: [
            new CosmosText({
               anchor: 0,
               fontSize: 24,
               fill: '#fff',
               position: { x: 160, y: 30 },
               content: text.gamepad.prompt
            })
         ]
      });
      ggRenderer.attach('main', bg);
      const display1 = new CosmosText({
         anchor: 0,
         fontSize: 16,
         fill: '#fff',
         position: { x: 160, y: 90 }
      });
      const display2 = new CosmosText({
         anchor: 0,
         fontSize: 24,
         fill: '#fff',
         position: { x: 160, y: 180 }
      });
      const display3 = new CosmosText({
         anchor: 0,
         fontSize: 16,
         fill: '#fff',
         position: { x: 160, y: 220 }
      });
      bg.attach(display1, display2, display3);
      let q = false;
      const qk = keys.quitKey.on('down').then(() => (q = true));
      const controls = gamepadder.controls(gamepad);
      let load = SAVE.flag.s.$gamepad_input_f !== '';
      if (load) {
         display1.content = text.gamepad.prompt_load;
         await Promise.race([ qk, gamepadder.down(ggRenderer, controls) ]);
         if (q) {
            gamepadder.disconnect(gamepad);
            return;
         }
         let done = false;
         await Promise.race([
            qk,
            gamepadder.down(ggRenderer, controls),
            ggRenderer.pause(500).then(() => {
               done = true;
            })
         ]);
         if (q) {
            gamepadder.disconnect(gamepad);
            return;
         }
         if (!done) {
            await Promise.race([
               qk,
               gamepadder.down(ggRenderer, controls),
               ggRenderer.pause(500).then(() => {
                  done = true;
               })
            ]);
            if (q) {
               gamepadder.disconnect(gamepad);
               return;
            }
            if (!done) {
               load = false;
               display1.content = '';
               await renderer.pause(500);
            }
         }
      }
      if (!load) {
         display1.content = text.gamepad.prompt_desc;
         const assign = async (line: string) => {
            if (q) {
               return [];
            }
            display2.content = line;
            display3.content = text.gamepad.prompt_counter.replace('$(x)', '0');
            const list = [] as number[];
            while (true) {
               const value = await Promise.race([ qk, gamepadder.down(ggRenderer, controls) ]);
               if (q) {
                  return [];
               }
               if (list.includes(value as number)) {
                  display2.fill = '#0f0';
                  await Promise.race([ qk, renderer.pause(500) ]);
                  if (q) {
                     return [];
                  }
                  display2.fill = '#fff';
                  display3.fill = '#fff';
                  break;
               } else {
                  list.push(value as number);
                  display3.fill = '#0f0';
                  display3.content = text.gamepad.prompt_counter.replace('$(x)', list.length.toString());
               }
            }
            return list;
         };
         const z = await assign(text.gamepad.z);
         const x = await assign(text.gamepad.x);
         const c = await assign(text.gamepad.c);
         const u = await assign(text.gamepad.u);
         const l = await assign(text.gamepad.l);
         const d = await assign(text.gamepad.d);
         const r = await assign(text.gamepad.r);
         const f = await assign(text.gamepad.f);
         if (q) {
            gamepadder.disconnect(gamepad);
            return;
         }
         SAVE.flag.s.$gamepad_input_z = z.join(',');
         SAVE.flag.s.$gamepad_input_x = x.join(',');
         SAVE.flag.s.$gamepad_input_c = c.join(',');
         SAVE.flag.s.$gamepad_input_u = u.join(',');
         SAVE.flag.s.$gamepad_input_l = l.join(',');
         SAVE.flag.s.$gamepad_input_d = d.join(',');
         SAVE.flag.s.$gamepad_input_r = r.join(',');
         SAVE.flag.s.$gamepad_input_f = f.join(',');
         display1.content = text.gamepad.prompt_done + (backend ? '' : text.gamepad.prompt_done_browser);
         display2.content = '';
         display3.content = '';
         await gamepadder.down(ggRenderer, controls);
      }
      gamepadder.stop();
      const buttonZ = SAVE.flag.s.$gamepad_input_z.split(',').map(value => controls[+value]);
      const buttonX = SAVE.flag.s.$gamepad_input_x.split(',').map(value => controls[+value]);
      const buttonC = SAVE.flag.s.$gamepad_input_c.split(',').map(value => controls[+value]);
      const buttonU = SAVE.flag.s.$gamepad_input_u.split(',').map(value => controls[+value]);
      const buttonL = SAVE.flag.s.$gamepad_input_l.split(',').map(value => controls[+value]);
      const buttonD = SAVE.flag.s.$gamepad_input_d.split(',').map(value => controls[+value]);
      const buttonR = SAVE.flag.s.$gamepad_input_r.split(',').map(value => controls[+value]);
      const buttonF = SAVE.flag.s.$gamepad_input_f.split(',').map(value => controls[+value]);
      gamepadder.mappings = [
         { active: buttonZ.find(button => button.pressed) !== void 0, button: buttonZ, input: keys.interactKey },
         { active: buttonX.find(button => button.pressed) !== void 0, button: buttonX, input: keys.specialKey },
         { active: buttonC.find(button => button.pressed) !== void 0, button: buttonC, input: keys.menuKey },
         { active: buttonU.find(button => button.pressed) !== void 0, button: buttonU, input: keys.upKey },
         { active: buttonL.find(button => button.pressed) !== void 0, button: buttonL, input: keys.leftKey },
         { active: buttonD.find(button => button.pressed) !== void 0, button: buttonD, input: keys.downKey },
         { active: buttonR.find(button => button.pressed) !== void 0, button: buttonR, input: keys.rightKey },
         { active: buttonF.find(button => button.pressed) !== void 0, button: buttonF, input: 'f' }
      ];
   },
   controls (gamepad: Gamepad) {
      return [
         ...CosmosUtils.populate(gamepad.buttons.length, i => ({
            get pressed () {
               return gamepadder.gamepad?.buttons[i].pressed ?? false;
            }
         })),
         ...CosmosUtils.populate(gamepad.axes.length, i => ({
            get pressed () {
               return (gamepadder.gamepad?.axes[i] ?? 0) <= -gamepadder.deadzone;
            }
         })),
         ...CosmosUtils.populate(gamepad.axes.length, i => ({
            get pressed () {
               return gamepadder.deadzone <= (gamepadder.gamepad?.axes[i] ?? 0);
            }
         }))
      ];
   },
   down (gr: CosmosRenderer, gc: { pressed: boolean }[]) {
      const state = gc.map(control => control.pressed);
      return new Promise<number>(resolve => {
         const ticker = () => {
            for (const [ index, control ] of gc.entries()) {
               if (control.pressed) {
                  if (!state[index]) {
                     gr.off('tick', ticker);
                     resolve(index);
                     return;
                  }
               } else {
                  state[index] = false;
               }
            }
         };
         gr.on('tick', ticker);
      });
   },
   disconnect (gamepad: Gamepad | null = null) {
      if (gamepad === null || gamepad.index === gamepadder.index) {
         gamepadder.index = null;
         gamepadder.mappings = null;
         gamepadder.stop();
      }
   },
   reset () {
      if (gamepadder.mappings !== null) {
         for (const mapping of gamepadder.mappings) {
            mapping.active = false;
            typeof mapping.input !== 'string' && (mapping.input.force2 = false);
         }
      }
   },
   stop () {
      if (gamepadder.renderer) {
         gamepadder.renderer.stop();
         gamepadder.renderer.canvas.remove();
         gamepadder.renderer = null;
      }
   },
   update () {
      if (gamepadder.mappings !== null) {
         for (const mapping of gamepadder.mappings) {
            if (mapping.button.find(button => button.pressed) !== void 0) {
               mapping.active = true;
            } else if (mapping.active) {
               mapping.active = false;
               typeof mapping.input !== 'string' && (mapping.input.force2 = false);
               switch (mapping.input) {
                  case 'f':
                     backend?.exec('f4') ?? fullscreen();
                     break;
                  default:
                     mapping.input.force2 = true;
               }
            }
         }
      }
   }
};

export const hashes = new CosmosCache((name: string) => {
   let pos = 0;
   let hash1 = 0xdeadbeef ^ 432;
   let hash2 = 0x41c6ce57 ^ 432;
   while (pos !== name.length) {
      const code = name.charCodeAt(pos++);
      hash1 = Math.imul(hash1 ^ code, 2654435761);
      hash2 = Math.imul(hash2 ^ code, 1597334677);
   }
   hash1 = Math.imul(hash1 ^ (hash1 >>> 16), 2246822507) ^ Math.imul(hash2 ^ (hash2 >>> 13), 3266489909);
   hash2 = Math.imul(hash2 ^ (hash2 >>> 16), 2246822507) ^ Math.imul(hash1 ^ (hash1 >>> 13), 3266489909);
   return 4294967296 * (2097151 & hash2) + (hash1 >>> 0);
});

export const keyring = new CosmosRegistry<
   string,
   {
      description: CosmosProvider<string>;
      display: () => boolean;
      name: CosmosProvider<string>;
      priority?: CosmosProvider<number>;
   }
>({ display: () => false, name: '', description: '' });

export const keyState = {
   down: false,
   interact: false,
   left: false,
   menu: false,
   right: false,
   special: false,
   up: false
};

export const mobile = {
   /** joystick directional input angle range */
   arc: 57.5,
   bounds: { x: 0, y: 0 },
   clear (identifier: number) {
      for (const button of mobile.state.buttons) {
         if (button.touches.includes(identifier)) {
            button.touches.splice(button.touches.indexOf(identifier), 1);
            if (button.touches.length === 0) {
               button.active = false;
               button.object.alpha.value = 0.5;
               button.target && (keys[button.target].force1 = false);
            }
            break;
         }
      }
      delete mobile.state.touches[identifier];
   },
   /** joystick deadzone size */
   deadzone: 8,
   gamepad () {
      mobile.reset();
      return new CosmosObject({
         priority: Number.MAX_VALUE,
         objects: buttons,
         metadata: { mobile_gamepad: true }
      }).on('tick', function () {
         this.alpha.value = gamepadder.mappings === null ? 1 : 0;
      });
   },
   reset () {
      for (const button of mobile.state.buttons) {
         button.touches.splice(0, button.touches.length);
         button.active = false;
         button.object.alpha.value = 0.5;
         button.target && (keys[button.target].force1 = false);
      }
      for (const identifier in mobile.state.touches) {
         delete mobile.state.touches[identifier];
      }
   },
   state: {
      buttons: buttons.map(object => {
         const size = new CosmosPoint(object.metadata.size);
         const half = size.divide(2);
         const diff = half.add(half.multiply(object.anchor));
         return {
            active: false,
            get base () {
               return object.position.subtract(diff);
            },
            object,
            size,
            target: object.metadata.target,
            touches: object.metadata.touches
         };
      }),
      taps: 0,
      touches: {} as Partial<CosmosKeyed<[number, CosmosPoint]>>
   },
   target: null as HTMLCanvasElement | null,
   touch (identifier: number, position: CosmosPoint) {
      const touch = mobile.state.touches[identifier];
      if (touch) {
         touch[1].set(position);
      } else {
         for (const [ index, button ] of mobile.state.buttons.entries()) {
            if (
               position.x > button.base.x &&
               position.x < button.base.x + button.size.x &&
               position.y > button.base.y &&
               position.y < button.base.y + button.size.y
            ) {
               button.touches.unshift(identifier);
               mobile.state.touches[identifier] = [ index, position ];
               if (!button.active) {
                  button.active = true;
                  button.object.alpha.value = 1;
                  button.target && (keys[button.target].force1 = true);
               }
               break;
            }
         }
      }
   },
   touches (event: TouchEvent) {
      return CosmosUtils.populate(event.changedTouches.length, index => event.changedTouches.item(index)!);
   }
};

export const mobileLoader = isMobile.any && inventories.mobileAssets.load();

export const pager = {
   /** pager storage */
   storage: [] as { active: boolean; page: number; pages: CosmosProvider<string[]>[] }[],
   /** create pager */
   create<A extends any[]> (type: 0 | 1 | 2, ...pages: CosmosProvider<string[], A>[]): (...args: A) => string[] {
      switch (type) {
         case 0:
            return pager.providerLimit.bind({ active: false, page: 0, pages });
         case 1:
            return pager.providerSequence.bind({ active: false, page: pages.length, pages });
         case 2:
            return pager.providerRandom.bind({ pages: pages as CosmosProvider<string[], any[]>[] });
      }
   },
   providerLimit (this: { active: boolean; page: number; pages: CosmosProvider<string[]>[] }, ...args: any[]) {
      if (!this.active) {
         this.active = true;
         pager.storage.push(this);
      }
      return CosmosUtils.provide(this.pages[this.page === this.pages.length - 1 ? this.page : this.page++], ...args);
   },
   providerSequence (this: { active: boolean; page: number; pages: CosmosProvider<string[]>[] }, ...args: any[]) {
      if (!this.active) {
         this.active = true;
         pager.storage.push(this);
      }
      this.page === this.pages.length && (this.page = 0);
      return CosmosUtils.provide(this.pages[this.page++], ...args);
   },
   providerRandom<A extends any[]> (this: { pages: CosmosProvider<string[]>[] }, ...args: A) {
      return CosmosUtils.provide(this.pages[rng.dialogue.int(this.pages.length)], ...args);
   }
};

export const phone = new CosmosRegistry<
   string,
   {
      display: () => boolean;
      trigger: () => Promise<void> | void;
      priority?: CosmosProvider<number>;
      name: CosmosProvider<string>;
   }
>({ display: () => false, trigger: () => {}, name: '' });

export const tracker = {
   history: CosmosUtils.populate(60, () => [ 'down', { x: 0, y: 0 } ] as [CosmosDirection, CosmosPointSimple]),
   interpolate (distance: number) {
      const floor = Math.floor(distance);
      const floorPos = tracker.retrieve(floor);
      const ceilPos = tracker.retrieve(Math.ceil(distance));
      return [
         tracker.retrieve(Math.round(distance))[0],
         {
            x: CosmosMath.linear(distance - floor, floorPos[1].x, ceilPos[1].x),
            y: CosmosMath.linear(distance - floor, floorPos[1].y, ceilPos[1].y)
         }
      ] as [CosmosDirection, CosmosPointSimple];
   },
   supplant (face = player.face, origin: CosmosPointSimple = player) {
      const vec = { down: { x: 0, y: -3 }, left: { x: 3, y: 0 }, right: { x: -3, y: 0 }, up: { x: 0, y: 3 } }[face];
      for (const [ index, entry ] of tracker.history.entries()) {
         entry[0] = face;
         entry[1].x = origin.x + vec.x * index;
         entry[1].y = origin.y + vec.y * index;
      }
   },
   retrieve (index: number) {
      return tracker.history[Math.min(index, tracker.history.length - 1)] ?? [ player.face, player.position.value() ];
   }
};

export const player = new CosmosPlayer({
   anchor: { x: 0, y: 1 },
   extent: { x: 5, y: 20 },
   size: { x: 20, y: 3 },
   metadata: {
      x: NaN,
      y: NaN,
      face: '',
      reverse: false,
      speed: 1,
      moved: false,
      notracker: false,
      trackerbooster: 0,
      cache: { init: false }
   }
}).on('tick', function () {
   if (this.metadata.notracker) {
      return;
   }
   if (
      this.position.x !== this.metadata.x ||
      this.position.y !== this.metadata.y ||
      this.face !== this.metadata.face ||
      this.metadata.trackerbooster > 0
   ) {
      this.metadata.x = this.position.x;
      this.metadata.y = this.position.y;
      this.metadata.face = this.face;
      this.metadata.trackerbooster > 0 && this.metadata.trackerbooster--;
      tracker.history.unshift(tracker.history.pop()!);
      const node = tracker.history[0];
      node[0] = this.face;
      node[1].x = this.position.x;
      node[1].y = this.position.y;
      this.metadata.moved = true;
   } else {
      this.metadata.moved = false;
   }
});

export const portraits = new CosmosRegistry<string, CosmosSprite>(new CosmosSprite());

export const escText = new CosmosText({
   alpha: 0,
   position: { x: 5, y: 5 },
   fill: '#fff',
   fontName: 'DiaryOfAn8BitMage',
   fontSize: 10,
   metadata: { state: 0, renderer: renderer as CosmosRenderer },
   priority: Infinity,
   stroke: ''
}).on('tick', function () {
   this.content =
      backend === null || SAVE.ready
         ? [ text.extra.restartText1, text.extra.restartText2, text.extra.restartText3 ][
              Math.min(escText.metadata.state, 2)
           ]
         : [ text.extra.quitText1, text.extra.quitText2, text.extra.quitText3 ][Math.min(escText.metadata.state, 2)];
});

// save menu state
export const saver = {
   locations: new CosmosRegistry<string, { name: CosmosProvider<string>; text: CosmosProvider<string[]> }>({
      name: '',
      text: []
   }),
   save (room = game.room) {
      saver.time = SAVE.data.n.time;
      SAVE.data.n.base_dialogue = rng.dialogue.value;
      SAVE.data.n.base_overworld = rng.overworld.value;
      SAVE.data.s.room = room;
      events.fire('save');
      SAVE.save();
   },
   time: -Infinity,
   yellow: false
};

// shop state
export const shopper = {
   get index () {
      return atlas.navigators.of('shop').position.y;
   },
   get listIndex () {
      return atlas.navigators.of('shopList').position.y;
   },
   async open (shop: OutertaleShop, face: CosmosDirection, x: number, y: number) {
      const movementValue = game.movement;
      game.movement = false;
      shopper.value = shop;
      await Promise.all([ game.music?.gain.modulate(renderer, 300, 0), renderer.alpha.modulate(renderer, 300, 0) ]);
      renderer.layers.base.active = false;
      renderer.layers.below.active = false;
      renderer.layers.main.active = false;
      renderer.layers.above.active = false;
      const display = new CosmosObject({ priority: -1000, objects: [ shopper.value.background ] });
      display.attach(shopper.value.keeper);
      renderer.attach('menu', display);
      atlas.switch('shop');
      const mus = shopper.value.music?.instance(renderer);
      if (mus) {
         mus.rate.value *= world.rate(mus.daemon);
         mus.gain.value /= 4;
         mus.gain.modulate(renderer, 300, mus.gain.value * 4);
      }
      renderer.alpha.modulate(renderer, 300, 1);
      await renderer.when(() => !atlas.target);
      player.face = face;
      player.position.set(x, y);
      renderer.detach('menu', display);
      shopper.value = null;
      game.movement = movementValue;
      game.music?.gain.modulate(renderer, 300, world.level);
      renderer.layers.base.active = true;
      renderer.layers.below.active = true;
      renderer.layers.main.active = true;
      renderer.layers.above.active = true;
   },
   async text (...lines: string[]) {
      const prev = atlas.target;
      atlas.switch('shopText');
      await dialogue_primitive(...lines);
      atlas.switch(prev);
   },
   value: null as OutertaleShop | null
};

export const sidebarrer = {
   // current dim box
   dimbox: 'dimboxA' as 'dimboxA' | 'dimboxB',
   // can move after using item
   use_movement: true,
   get item () {
      return SAVE.storage.inventory.of(atlas.navigators.of('sidebarItem').position.y)!;
   },
   get use () {
      return [ 'consumable', 'special' ].includes(items.of(sidebarrer.item).type);
   },
   async openSettings () {
      const background = new CosmosRectangle({ fill: '#000', size: { x: 320, y: 240 } });
      renderer.attach('menu', background);
      atlas.switch('frontEndSettings');
      await atlas.navigators.of('frontEndSettings').on('to');
      renderer.detach('menu', background);
   }
};

// attack span
export const spanMin = 3;
export const spanBase = 22;
export const spanRange = 138;

export const teleporter = {
   attach: true,
   hot: false,
   forcemove: false,
   movement: false,
   timer: false,
   menu: false,
   nomusic: false,
   region: null as CosmosRegion | null
};

export const world = {
   /** ambient pitch level for game music */
   get ambiance () {
      return world.rate(game.room);
   },
   /** any active cutscene spanning multiple rooms */
   cutscene () {
      return false;
   },
   /** force cutscene mode */
   cutscene_override: false,
   /** current room defualt music gain */
   gain (room: string) {
      const score = rooms.of(room).score;
      return (musicRegistry.get(score.music!)?.gain ?? 0) * (score.gain ?? 0);
   },
   get level () {
      return world.gain(game.room);
   },
   rate (mus = '' as string | CosmosDaemon) {
      typeof mus === 'string' && (mus = musicRegistry.of(rooms.of(mus).score.music!));
      return mus?.rate ?? 1;
   }
};

export function activate (source: CosmosHitbox, filter: (hitbox: CosmosHitbox) => boolean) {
   source.calculate();
   for (const key of [ 'below', 'main' ]) {
      for (const { metadata } of renderer.detect(source, ...renderer.calculate(key as OutertaleLayerKey, filter))) {
         if (typeof metadata.name === 'string') {
            events.fire('script', metadata.name, ...((metadata.args ?? []) as string[]));
         }
      }
   }
}

export function autoNav () {
   return renderer.projection(game.camera, game.camera.position).y < 155 ? 'dialoguerBottom' : 'dialoguerTop';
}

export async function autozoom (zoom: number, time: number) {
   const area = new CosmosValue(320 / renderer.zoom.value);
   function zoomerTicker () {
      renderer.zoom.value = 320 / area.value;
   }
   renderer.on('tick', zoomerTicker);
   await area.modulate(renderer, time, 320 / zoom);
   renderer.off('tick', zoomerTicker);
   renderer.zoom.value = zoom;
}

export function backSprite (map: OutertaleMap, name: string) {
   const handler = {
      priority: -Infinity,
      listener (this: CosmosSprite) {
         const {
            area: { x, y, width, height },
            offset
         } = map.value![`r:${name}`];
         this.crop.top = y;
         this.crop.left = x;
         this.crop.right = -x - width;
         this.crop.bottom = -y - height;
         this.position.set(offset);
         this.off('tick', handler);
      }
   };
   return new CosmosSprite({ frames: [ map.image ] }).on('tick', handler);
}

export function calcAT () {
   return calcLV() * 2;
}

export function calcDF () {
   return Math.floor((calcLV() - 1) / 4);
}

export function calcHP () {
   const lv = calcLV();
   if (lv === 20) {
      return 99;
   } else {
      return lv * 4 + 16;
   }
}

export function calcLV () {
   let lv = 0;
   for (const expLevel of expLevels) {
      if (expLevel > SAVE.data.n.exp) {
         break;
      }
      lv++;
   }
   return lv + 1;
}

export function calcNX () {
   const value = expLevels[calcLV() - 1];
   return value ? value - SAVE.data.n.exp : null;
}

/** quick character */
export function character (
   key: string,
   preset: CosmosCharacterPreset,
   position: CosmosPointSimple,
   face: CosmosDirection,
   override = {} as CosmosNot<CosmosCharacterProperties, 'preset' | 'position'>,
   layer = 'main' as OutertaleLayerKey | null
) {
   const instance = new CosmosCharacter(Object.assign({ face, preset, key, position }, override));
   layer === null || renderer.attach(layer, instance);
   return instance;
}

export async function credits (end = true) {
   const creditsText = new CosmosText({
      anchor: 0,
      position: { x: 160, y: 120 },
      fontName: 'DeterminationMono',
      fontSize: 16,
      fill: '#fff'
   });
   renderer.attach('menu', creditsText);
   let page = 0;
   let bbbb = sounds.impact.instance(renderer);
   while (page !== text.extra.credits.length) {
      creditsText.content = text.extra.credits[page++].join('\n');
      await keys.interactKey.on('down');
      bbbb.stop();
      bbbb = sounds.impact.instance(renderer);
   }
   renderer.detach('menu', creditsText);
   await bbbb.on('stop');
   if (end) {
      await renderer.pause(1000);
      reload(false, true);
   }
}

export function displayTime (value: number) {
   const seconds = value / 30;
   return `${Math.floor(seconds / 60)}:${Math.floor(seconds % 60)
      .toString()
      .padStart(2, '0')}`;
}

export function distanceGravity (v: number, d: number) {
   if (d === 0) {
      return 0;
   } else {
      return v ** 2 / (v + d * 2);
   }
}

/** smart dialogue system */
export async function dialogue (nav: string, ...lines: string[]) {
   if (lines.length > 0) {
      if (dialogueSession.active) {
         typer.reset(true);
      } else {
         dialogueSession.movement = game.movement;
      }
      dialogueSession.active = true;
      game.movement = false;
      const trueNavigator = nav === 'auto' ? autoNav() : nav;
      atlas.target === trueNavigator || atlas.switch(trueNavigator);
      game.text = '';
      const t = typer.text(...lines);
      t.then(() => {
         atlas.switch(null);
         dialogueSession.active = false;
         dialogueSession.movement && (game.movement = true);
      });
      await t;
   }
}

export async function dialogue_primitive (...lines: string[]) {
   game.text = '';
   await typer.text(...lines);
}

export function disengageDelay () {
   return soundDelay.modulate(renderer, 500 * (soundDelay.value / 0.5), 0);
}

/** useful for a surprising number of things */
export async function dropShake (simple: CosmosPointSimple, sfx = true) {
   sfx && sounds.noise.instance(renderer);
   const base = simple.x;
   simple.x = base + 3;
   await renderer.pause(66);
   simple.x = base - 3;
   await renderer.pause(66);
   simple.x = base + 2;
   await renderer.pause(66);
   simple.x = base - 1;
   await renderer.pause(66);
   simple.x = base;
}

export function easyRoom (name: string, map: OutertaleMap, decorator: OutertaleRoomDecorator) {
   const extras: CosmosAsset[] = [];
   rooms.register(
      name,
      new OutertaleRoom({
         decorator,
         face: decorator.face as CosmosDirection,
         layers: Object.fromEntries(
            Object.entries(decorator.layers ?? {}).map(([ layer, objects = [] ]) => [
               layer,
               [
                  ...(layer === decorator.background ? [ backSprite(map, name) ] : []),
                  ...(layer in (decorator.mixins ?? {}) ? [ backSprite(maps.of(decorator.mixins![layer]!), name) ] : []),
                  ...objects.map(decorator => {
                     const {
                        attachments = [],
                        barriers = [],
                        filters: filterList = [],
                        interacts = [],
                        position,
                        rotation,
                        tags = [],
                        triggers = []
                     } = decorator;
                     return new CosmosObject({
                        filters: filterList
                           .filter(filter => filter in filters)
                           .map(filter => filters[filter as keyof typeof filters]),
                        metadata: { class: 'object', decorator, tags: tags.slice() },
                        position,
                        rotation,
                        objects: [
                           ...attachments.map(decorator => {
                              const {
                                 anchor,
                                 active = false,
                                 filters: filterList = [],
                                 frames = [],
                                 position,
                                 resources,
                                 rotation,
                                 steps: duration,
                                 type
                              } = decorator;
                              if (type === 'sprite') {
                                 return new CosmosSprite({
                                    anchor,
                                    active,
                                    filters: filterList
                                       .filter(filter => filter in filters)
                                       .map(filter => filters[filter as keyof typeof filters]),
                                    frames: frames.map(frame => {
                                       const extra = content[frame as keyof typeof content] as CosmosImage;
                                       extras.push(extra);
                                       return extra;
                                    }),
                                    metadata: { class: 'attachment', decorator },
                                    position,
                                    rotation,
                                    duration
                                 });
                              } else {
                                 const extra = content[resources as keyof typeof content] as CosmosAnimationResources;
                                 extras.push(extra);
                                 return new CosmosAnimation({
                                    anchor,
                                    active,
                                    filters: filterList
                                       .filter(filter => filter in filters)
                                       .map(filter => filters[filter as keyof typeof filters]),
                                    metadata: { class: 'attachment', decorator },
                                    resources: extra,
                                    position,
                                    rotation
                                 });
                              }
                           }),
                           ...barriers.map(decorator => {
                              const { anchor, position, rotation, size } = decorator;
                              return new CosmosHitbox({
                                 anchor,
                                 metadata: { barrier: true, class: 'barrier', decorator },
                                 position,
                                 rotation,
                                 size
                              });
                           }),
                           ...interacts.map(decorator => {
                              const { anchor, args = [], name, position, rotation, size } = decorator;
                              return new CosmosHitbox({
                                 anchor,
                                 metadata: {
                                    args: args.slice(),
                                    class: 'interact',
                                    decorator,
                                    interact: true,
                                    name
                                 },
                                 position,
                                 rotation,
                                 size
                              });
                           }),
                           ...triggers.map(decorator => {
                              const { anchor, args = [], name, position, rotation, size } = decorator;
                              return new CosmosHitbox({
                                 anchor,
                                 metadata: { args, class: 'trigger', decorator, name, trigger: true },
                                 position,
                                 rotation,
                                 size
                              });
                           })
                        ]
                     });
                  })
               ]
            ])
         ),
         metadata: decorator.metadata,
         neighbors: decorator.neighbors?.slice(),
         preload: new CosmosInventory(
            ...([
               ...[ ...new Set([ map, ...Object.values(decorator.mixins ?? {}).map(key => maps.of(key!)) ]) ],
               ...extras,
               ...(decorator.preload ?? []).map(asset =>
                  asset[0] === '#'
                     ? maps.of(asset.slice(1))
                     : content[asset as keyof typeof content] || inventories[asset as keyof typeof inventories]
               ),
               ...(Object.values(decorator.layers ?? {})
                  .flat(1)
                  .find(x => x?.tags?.includes('spawn'))
                  ? [ content.asSave ]
                  : [])
            ].filter(asset => asset instanceof CosmosAsset) as CosmosAsset[])
         ),
         region: decorator.region?.slice() as CosmosRegion | [],
         score: decorator.score,
         spawn: decorator.spawn
      })
   );
   rooms.of(name).preload.name = `rooms::${name}`;
}

export function engageDelay () {
   return soundDelay.modulate(renderer, 33, 0.5);
}

export function fader (properties: CosmosSizedObjectProperties = {}, layer: OutertaleLayerKey | null = 'menu') {
   const object = new CosmosRectangle(Object.assign({ alpha: 0, fill: '#000', size: { x: 320, y: 240 } }, properties));
   layer && renderer.attach(layer, object);
   return object;
}

export function fastAssets () {
   const assets = [ rooms.of(SAVE.data.s.room).preload ] as CosmosAsset[];
   return assets;
}

/** all loaded characters */
export function fetchCharacters () {
   return renderer.layers.main.objects.filter(object => object instanceof CosmosCharacter) as CosmosCharacter[];
}

export function quickresume (fade = false) {
   resume({ gain: world.level, rate: world.ambiance, fade });
}

/** listen for specific header */
export function header (target: string) {
   return new Promise<void>(resolve => {
      const listener = (header: string) => {
         if (header === target) {
            typer.off('header', listener);
            resolve();
         }
      };
      typer.on('header', listener);
   });
}

export function heal (amount = Infinity, sfx: CosmosDaemon | boolean = true) {
   if (amount < 0) {
      sfx && sounds.hurt.instance(renderer);
      SAVE.data.n.hp = Math.max(SAVE.data.n.hp + amount, 1);
   } else {
      sfx && (sfx === true ? sounds.heal : sfx).instance(renderer);
      SAVE.data.n.hp = Math.max(SAVE.data.n.hp, Math.min(SAVE.data.n.hp + amount, calcHP()));
   }
}

/** get a single instance */
export function instance (layer: OutertaleLayerKey, tag: string) {
   return instances(layer, tag).next().value;
}

/** get instances */
export function * instances (layer: OutertaleLayerKey, tag: string) {
   for (const object of renderer.layers[layer].objects.slice()) {
      const tags = object.metadata.tags;
      if (tags instanceof Array && tags.includes(tag)) {
         yield {
            destroy: () => renderer.detach(layer, object),
            set index (value: number) {
               (object.objects[0] as CosmosSprite).index = value;
            },
            talk: async (
               tag: string,
               provider: ((top: CosmosObject) => CosmosSprite) | null,
               navigator: string,
               ...lines: string[]
            ) => {
               const anim = provider?.(object) ?? null;
               const listener = (header: string) => {
                  if (anim !== null) {
                     if (header === `npc/${tag}`) {
                        speech.targets.add(anim);
                     } else if (header === 'npc') {
                        speech.targets.delete(anim);
                     }
                  }
               };
               typer.on('header', listener);
               await dialogue(navigator, ...lines);
               typer.off('header', listener);
               anim === null || speech.targets.delete(anim);
            },
            tags,
            object
         };
      }
   }
}

export function interactionCheck (target: CosmosHitbox) {
   return (player.objects[1] as CosmosHitbox).calculate().detect(target.calculate());
}

export function keepActive (this: CosmosSprite) {
   this.active = true;
}

export function menuText (
   x: number,
   y: number,
   c: CosmosProvider<string>,
   properties: CosmosTextProperties = {},
   white?: boolean
) {
   return new CosmosText(
      Object.assign(
         { fill: white === false ? void 0 : '#fff', position: { x: x / 2, y: y / 2 }, stroke: '' },
         properties
      )
   ).on('tick', function () {
      this.content = CosmosUtils.provide(c);
   });
}

export function menuBox (
   x: number,
   y: number,
   w: number,
   h: number,
   b: number,
   properties: CosmosSizedObjectProperties = {}
) {
   return new CosmosRectangle({
      fill: '#fff',
      position: { x: x / 2, y: y / 2 },
      size: { x: w / 2 + b, y: h / 2 + b },
      stroke: '',
      objects: [
         new CosmosRectangle({ fill: '#000', position: b / 2, size: { x: w / 2, y: h / 2 } }),
         new CosmosObject(),
         new CosmosObject(Object.assign({ fill: '#000', position: { x: b / 2, y: b / 2 } }, properties))
      ]
   });
}

export function menuFinder (nav: string, navX: CosmosProvider<any>, navY?: CosmosProvider<number>) {
   if (atlas.target === nav) {
      const navigator = atlas.navigator();
      if (navigator) {
         const navYv = CosmosUtils.provide(navY);
         if (typeof navYv === 'number') {
            if (navigator.position.x === CosmosUtils.provide(navX) && navigator.position.y === navYv) {
               return true;
            }
         } else if (navigator.selection() === CosmosUtils.provide(navX)) {
            return true;
         }
      }
   }
}

export function menuSOUL (x: number, y: number, nav: string, navX: CosmosProvider<any>, navY?: CosmosProvider<number>) {
   return new CosmosSprite({
      frames: [ content.ieSOUL ],
      position: { x: x / 2, y: y / 2 },
      priority: 1
   }).on('tick', function () {
      this.alpha.value = menuFinder(nav, navX, navY) ? 1 : 0;
   });
}

/** prime radiant */
export function rand_rad (value = Math.random()) {
   return new CosmosValueRandom(hashes.of(value.toString()));
}

/** resume music */
export function resume ({ gain = 1, rate = 1, fade = true }) {
   game.music?.stop();
   const score = rooms.of(game.room).score;
   const daemon = typeof score.music === 'string' ? musicRegistry.of(score.music) : null;
   game.music = daemon?.instance(renderer, { offset: game.music_offset }) ?? null;
   game.music_offset = 0;
   musicFilter.value = score.filter;
   if (game.music) {
      game.music.gain.value = gain / 4;
      game.music.gain.modulate(renderer, fade ? 300 : 0, gain);
      game.music.rate.value = score.rate * rate;
   }
   musicConvolver.value = score.reverb;
}

/** saw mapper */
export function sawWaver (time: number, period: number, from: number, to: number, phase = 0, ct = renderer.time) {
   return CosmosMath.linear((((ct - time) % period) / period + phase) % 1, from, to, from);
}

export function selectText (input: string[][]) {
   if (input.length < 2) {
      return input[0];
   } else {
      return input[rng.dialogue.int(input.length)];
   }
}

/** sine mapper */
export function sineWaver (time: number, period: number, from: number, to: number, phase = 0, ct = renderer.time) {
   return CosmosMath.remap(CosmosMath.wave(((ct - time + phase * period) % period) / period), from, to);
}

/** quicker shadow func */
export function quickshadow (
   spr: CosmosSprite,
   position: Partial<CosmosPointSimple> | number = spr,
   parent: OutertaleLayerKey | CosmosObject = 'menu',
   alpha = 0.6,
   alphaDecay = 1.5,
   alphaThreshold = 0.2,
   ticker = (spr: CosmosSprite) => false
) {
   const e = CosmosUtils.hyperpromise();
   const s = new CosmosSprite({
      alpha,
      anchor: spr.anchor,
      crop: spr.crop,
      parallax: spr.parallax,
      position,
      priority: spr.priority.value,
      rotation: spr.rotation.value,
      scale: spr.scale,
      frames: [ spr.frames[spr.index] ],
      metadata: { e }
   })
      .on('tick', function () {
         (ticker(this) || (this.alpha.value /= alphaDecay) < alphaThreshold) && e.resolve();
      })
      .on('render', function () {
         SAVE.flag.b.$option_fancy || e.resolve();
      });
   renderer.post().then(() => {
      e.active && (typeof parent === 'string' ? renderer.attach(parent, s) : parent.attach(s));
   });
   e.promise.then(() => {
      typeof parent === 'string' ? renderer.detach(parent, s) : parent.detach(s);
   });
   return s;
}

/** quick screen-shake */
export async function shake (value = 2, runoff = 500, hold = 0, ...points: number[]) {
   await renderer.shake.modulate(renderer, 0, value);
   await renderer.pause(hold);
   await renderer.shake.modulate(renderer, runoff, ...points, 0);
}

/** trivia provider */
export async function trivia (...lines: string[]) {
   game.movement = false;
   atlas.switch(autoNav());
   await dialogue_primitive(...lines);
   atlas.switch(null);
   game.movement = true;
}

/** quick talker filter */
export function talkFinder (index = 0) {
   return (top: CosmosObject) =>
      top.objects.filter(object => object instanceof CosmosAnimation)[index] as CosmosAnimation;
}

/** room teleporter */
export async function teleport (
   dest: string,
   face: CosmosDirection,
   x: number,
   y: number,
   {
      fade = true,
      fast = false,
      gain = 1 as CosmosProvider<number, [string]>,
      rate = 1 as CosmosProvider<number, [string]>,
      cutscene = false as CosmosProvider<boolean, [string]>
   }
) {
   // clear pagers
   for (const instance of pager.storage.splice(0, pager.storage.length)) {
      instance.active = false;
      instance.page = 0;
   }

   // fire preteleport event
   events.fire('teleport-start', game.room, dest);

   // store movement state
   if (teleporter.forcemove) {
      teleporter.forcemove = false;
      teleporter.movement = true;
   } else {
      teleporter.movement = game.movement;
   }
   teleporter.timer = game.timer;

   // disable movement
   game.movement = false;

   // fire actual preteleport event
   await Promise.all(events.fire('teleport-pre', game.room, dest));

   // get next room
   const next = rooms.of(dest);

   // get room music options
   const score = next.score;

   // get previous room
   const prev = rooms.of(game.room);

   // set music options
   if (!fast) {
      const time = renderer.alpha.value * 300;
      if (!CosmosUtils.provide(cutscene, dest)) {
         // check if old and new music is the same
         if (score.music === prev.score.music) {
            // transition values
            musicFilter.modulate(renderer, fade ? time + 300 : 0, score.filter);
            if (score.rate === prev.score.rate) {
               game.music?.gain.modulate(renderer, fade ? time + 300 : 0, CosmosUtils.provide(gain, dest));
               game.music?.rate.modulate(renderer, fade ? time : 0, score.rate * CosmosUtils.provide(rate, dest));
            } else {
               game.music?.gain.modulate(renderer, fade ? time + 300 : 0, 0);
            }
            musicConvolver.modulate(renderer, fade ? time + 300 : 0, score.reverb);
         } else {
            // fade out old music
            game.music?.gain.modulate(renderer, fade ? time : 0, 0);
         }
      }

      // begin & wait for fade out
      await renderer.alpha.modulate(renderer, fade ? time : 0, 0);
   }

   // pause timer during room load
   game.timer = false;

   // remove old objects
   for (const key in prev.layers) {
      renderer.detach(key as keyof typeof prev.layers, ...(prev.layers[key as keyof typeof prev.layers] ?? []));
   }

   // load all needed assets
   await next.preload.load();

   // add new objects
   if (teleporter.attach) {
      for (const key in next.layers) {
         renderer.attach(key as keyof typeof next.layers, ...(next.layers[key as keyof typeof next.layers] ?? []));
      }
   } else {
      teleporter.attach = true;
   }

   // load new preloads
   for (const neighbor of next.neighbors.map(neighbor => rooms.of(neighbor))) {
      neighbor.preload.load();
   }

   // store previous key for event
   const prevkey = game.room;

   // set room
   game.room = dest;
   events.fire('teleport-update', face, { x, y });

   // wait for listeners to update
   await renderer.on('render');

   // unload old preloads (not including assets in new preload set)
   const safezone = [ next, ...next.neighbors.map(neighbor => rooms.of(neighbor)) ];
   for (const neighbor of [ prev, ...prev.neighbors.map(neighbor => rooms.of(neighbor)) ]) {
      safezone.includes(neighbor) || neighbor.preload.unload();
   }

   // fire teleport event
   events.fire('teleport', prevkey, dest);

   // update camera bounds
   if (teleporter.region !== null) {
      renderer.region = teleporter.region;
      teleporter.region = null;
   } else {
      renderer.region[0].x = next.region[0]?.x ?? renderer.region[0].x;
      renderer.region[0].y = next.region[0]?.y ?? renderer.region[0].y;
      renderer.region[1].x = next.region[1]?.x ?? renderer.region[1].x;
      renderer.region[1].y = next.region[1]?.y ?? renderer.region[1].y;
   }

   // resume timer
   game.timer = teleporter.timer;
   game.movement = teleporter.movement;

   if (!fast) {
      // begin fade in
      renderer.alpha.modulate(renderer, fade ? 300 : 0, 1);

      // start new music if applicable
      if (teleporter.nomusic) {
         teleporter.nomusic = false;
      } else if (
         !CosmosUtils.provide(cutscene, dest) &&
         (score.music !== prev.score.music || (score.music && !game.music))
      ) {
         resume({ gain: CosmosUtils.provide(gain, dest), rate: CosmosUtils.provide(rate, dest) });
      } else if (score.rate !== prev.score.rate) {
         game.music && (game.music.rate.value = score.rate * CosmosUtils.provide(rate, dest));
         game.music?.gain.modulate(renderer, fade ? 300 : 0, CosmosUtils.provide(gain, dest));
      }
   }

   // room tick
   events.fire('tick');
}

export function temporary<X extends CosmosObject> (
   object: X,
   parent: OutertaleLayerKey | CosmosObject,
   callback = () => {}
) {
   typeof parent === 'string' ? renderer.attach(parent, object) : parent.attach(object);
   events.on('teleport').then(() => {
      typeof parent === 'string'
         ? renderer.detach(parent, object)
         : parent.objects.splice(parent.objects.indexOf(object), 1);
      callback();
   });
   return object;
}

export function ultimaFacer (
   { position, size }: { position: CosmosPointSimple; size: { x: number } },
   away = false
): CosmosDirection {
   if (Math.abs(position.x - player.position.x) < Math.abs(size.x) / 2 + 5) {
      return player.position.y < position.y === away ? 'down' : 'up';
   } else {
      return player.position.x < position.x === away ? 'right' : 'left';
   }
}

export function ultraPosition (room: string) {
   const roomValue = rooms.of(room);
   let face: CosmosDirection = 'down';
   let position = roomValue.decorator?.spawn;
   if (!position) {
      const spawn = Object.values((roomValue.decorator ?? {}).layers ?? {})
         .flat(1)
         .find(x => x?.tags?.includes('spawn'));
      if (spawn) {
         position = { x: spawn.position?.x ?? 0, y: (spawn.position?.y ?? 0) + 7 };
      } else {
         const script = roomValue.neighbors
            .map(neighbor => rooms.of(neighbor))
            .map(x => x.decorator ?? {})
            .map(x => Object.values(x.layers ?? {}))
            .flat(2)
            .map(x => x?.triggers ?? [])
            .flat(1)
            .find(x => x.name === 'teleport' && x.args!.includes(room));
         if (script) {
            face = script.args![1] as CosmosDirection;
            position = { x: +script.args![2], y: +script.args![3] };
         } else {
            position = {
               x: ((roomValue.region?.[0]?.x ?? 0) + (roomValue.region?.[1]?.x ?? 0)) / 2,
               y: ((roomValue.region?.[0]?.y ?? 0) + (roomValue.region?.[1]?.y ?? 0)) / 2
            };
         }
      }
   }
   return { face, position };
}

export function updateArmor (armor: string) {
   SAVE.data.s.armor = armor;
}

/** item handling */
export async function use (key: string, index: number) {
   await Promise.all(events.fire('pre-consume', key, index));
   const item = items.of(key);
   if (item.type === 'consumable') {
      item.useSFX && (item.useSFX === true ? sounds.swallow : item.useSFX).instance(renderer);
      SAVE.storage.inventory.remove(index);
      typer.variables.x = Math.abs(item.value).toString();
   } else {
      item.useSFX && (item.useSFX === true ? sounds.equip : item.useSFX).instance(renderer);
      if (item.type === 'armor') {
         SAVE.storage.inventory.set(index, SAVE.data.s.armor || 'spacesuit');
         updateArmor(key);
      } else if (item.type === 'weapon') {
         SAVE.storage.inventory.set(index, SAVE.data.s.weapon || 'spanner');
         SAVE.data.s.weapon = key;
      }
   }
   await Promise.all(events.fire('consume', key, index));
   const lines = CosmosUtils.provide(item.text.use).slice();
   if (item.type === 'consumable') {
      renderer.pause(300).then(() => heal(item.value, item.healSFX));
      if (lines.length > 0) {
         if (item.value < calcHP() - SAVE.data.n.hp) {
            lines[0] += `\n${text.menu.heal2}`;
         } else {
            lines[0] += `\n${text.menu.heal1}`;
         }
      }
   }
   await dialogue_primitive(...lines);
   await Promise.all(events.fire('use', key, index));
   item.type === 'consumable' && item.value < 0 && heal(item.value, item.healSFX);
   await Promise.all(events.fire('post-use', key, index));
}

atlas.navigators.register({
   choicer: new CosmosNavigator({
      flip: true,
      grid: () => CosmosUtils.populate(choicer.type + 1, index => [ index * 2, index * 2 + 1 ]),
      next: self => {
         choicer.result = self.selection() as number;
         choicer.navigator === void 0 || atlas.switch(choicer.navigator);
         atlas.detach(renderer, 'menu', 'choicer');
         typer.read();
      },
      objects: CosmosUtils.populate(6, index => {
         return menuSOUL(0, 0, 'choicer', index).on('tick', function () {
            const row = Math.floor(index / 2) + (2 - choicer.type);
            if (row < 3) {
               this.position.set(
                  19 + (index % 2 === 0 ? choicer.marginA : choicer.marginB + 16) * 8,
                  (choicer.navigator === 'dialoguerTop' ? 19 : 174) + row * 19
               );
            }
         });
      })
   }).on('from', function () {
      this.position = { x: 0, y: 0 };
   }),
   dialoguerBase: new CosmosNavigator({
      next: () => void typer.read(),
      prev: () => void typer.skip()
   }),
   dialoguerBottom: new CosmosNavigator({
      next: () => void typer.read(),
      prev: () => void typer.skip(),
      objects: [
         menuBox(32, 320, 566, 140, 6, { objects: dialogueObjects() }).on('tick', function () {
            this.fontName = speech.state.fontName1;
            this.fontSize = speech.state.fontSize1;
         })
      ]
   }),
   dialoguerTop: new CosmosNavigator({
      next: () => void typer.read(),
      prev: () => void typer.skip(),
      objects: [
         menuBox(32, 10, 566, 140, 6, { objects: dialogueObjects() }).on('tick', function () {
            this.fontName = speech.state.fontName1;
            this.fontSize = speech.state.fontSize1;
         })
      ]
   }),
   frontEnd: new CosmosNavigator<string>({
      next: 'frontEndLanding',
      objects: [
         new CosmosAnimation({
            alpha: 0,
            anchor: { x: 0 },
            position: { x: 160, y: 31 },
            resources: content.ieStory
         }).on('tick', function () {
            this.index = frontEnder.index;
         }),
         menuText(120, 328 - 4, () => game.text, {
            fontName: 'DeterminationMono',
            fontSize: 16,
            spacing: { x: 1, y: 5 }
         })
      ]
   }).on('from', async function () {
      const panel = this.objects[0] as CosmosAnimation;
      atlas.attach(renderer, 'menu', 'frontEnd');
      frontEnder.introMusic = music.story.instance(renderer);
      while (atlas.target === 'frontEnd') {
         panel.alpha.modulate(renderer, 500, 1);
         const idle = typer.on('idle');
         dialogue_primitive(
            ...[
               text.menu.story1,
               text.menu.story2,
               text.menu.story3,
               text.menu.story4,
               text.menu.story5,
               text.menu.story6,
               text.menu.story7,
               text.menu.story8,
               text.menu.story9,
               text.menu.story10,
               text.menu.story11,
               text.menu.story12,
               text.menu.story13,
               text.menu.story14,
               text.menu.story15
            ][frontEnder.index - 1].map(text => CosmosTextUtils.format(text, 24, true))
         );
         await idle;
         const last = frontEnder.index === 15;
         if (last) {
            await frontEnder.scroll.modulate(renderer, 4000, 0);
            if (atlas.target !== 'frontEnd') {
               break;
            }
         }
         await renderer.pause(2000);
         if (atlas.target !== 'frontEnd') {
            break;
         }
         await panel.alpha.modulate(renderer, 500, 0);
         if (atlas.target !== 'frontEnd') {
            break;
         }
         if (last) {
            atlas.switch('frontEndLanding');
         } else {
            frontEnder.index++;
         }
      }
   }),
   frontEndLanding: new CosmosNavigator<string>({
      next: 'frontEndLoad',
      objects: [
         frontEnder.createBackground(),
         new CosmosSprite({
            frames: [ content.ieSplashForeground ]
         }),
         menuText(240, 360 - 2, () => text.general.landing1, {
            alpha: 0,
            fill: '#808080',
            fontName: 'CryptOfTomorrow',
            fontSize: 8
         })
      ]
   })
      .on('from', async function () {
         game.input = false;
         const text = this.objects[2];
         text.alpha.value = 0;
         typer.reset(true);
         game.text = '';
         await Promise.all([
            frontEnder.introMusic!.gain.modulate(renderer, 1000, 0),
            atlas.navigators.of('frontEnd').objects[0].alpha.modulate(renderer, 1000, 0, 0, 0)
         ]);
         game.input = true;
         frontEnder.introMusic?.stop();
         frontEnder.impactNoise = sounds.impact.instance(renderer);
         atlas.detach(renderer, 'menu', 'frontEnd');
         atlas.attach(renderer, 'menu', 'frontEndLanding');
         await renderer.pause(3e3);
         text.alpha.value = 1;
         await renderer.pause(16e3);
         if (atlas.target === 'frontEndLanding') {
            frontEnder.index = 1;
            frontEnder.scroll.task?.();
            frontEnder.scroll.value = 1;
            atlas.switch('frontEnd');
         } else {
            content.amStory.unload();
            content.ieStory.unload();
         }
      })
      .on('to', () => {
         atlas.detach(renderer, 'menu', 'frontEndLanding');
      }),
   frontEndLoad: new CosmosNavigator({
      flip: true,
      grid: [
         [ 'continue', 'reset' ],
         [ 'settings', 'settings' ]
      ],
      next (self) {
         switch (self.selection()) {
            case 'continue':
               frontEnder.menuMusic?.stop();
               events.fire('spawn');
               break;
            case 'reset':
               return 'frontEndName';
            case 'settings':
               return 'frontEndSettings';
         }
      },
      objects: [
         frontEnder.createBackground(),
         new CosmosObject({
            area: renderer.area,
            position: { y: 220 },
            filters: [
               new OutlineFilter(2, 0xffffff, 0.1, 0.1, false),
               new OutlineFilter(1, 0xffffff, 0.1, 0.1, false),
               new AdvancedBloomFilter({ quality: 10, threshold: 0, brightness: 1, bloomScale: 0.1 })
            ],
            objects: [
               // TODO: menu characters
            ]
         }),
         new CosmosObject({
            fontName: 'DeterminationSans',
            fontSize: 16,
            objects: [
               menuText(140, 132 - 4, () => text.general.name),
               menuText(280, 132 - 4, () => `${text.general.lv} ${calcLV()}`),
               menuText(498, 132 - 4, () => displayTime(SAVE.data.n.time), {
                  anchor: { x: 1 }
               }),
               menuText(140, 168 - 4, () => CosmosUtils.provide(saver.locations.of(SAVE.data.s.room).name)),
               menuText(
                  170,
                  218 - 4,
                  () => `§fill:${menuFinder('frontEndLoad', 'continue') ? '#ff0' : '#fff'}§${text.menu.load1}`
               ),
               menuText(
                  360,
                  218 - 4,
                  () => `§fill:${menuFinder('frontEndLoad', 'reset') ? '#ff0' : '#fff'}§${text.menu.load2}`
               ),
               menuText(
                  264,
                  258 - 4,
                  () => `§fill:${menuFinder('frontEndLoad', 'settings') ? '#ff0' : '#fff'}§${text.general.settings}`
               ),
               menuText(320, 464 - 2, () => text.menu.footer, {
                  anchor: { x: 0 },
                  fill: '#808080',
                  fontName: 'CryptOfTomorrow',
                  fontSize: 8
               })
            ]
         })
      ]
   }),
   frontEndName: new CosmosNavigator({
      flip: true,
      grid: [ ...text.menu.name5, [ 'quit', 'backspace', 'done' ] ],
      next (self) {
         const selection = self.selection() as string;
         switch (selection) {
            case 'quit':
               frontEnder.name.value = '';
               return 'frontEndLoad';
            case 'backspace':
               frontEnder.name.value.length > 0 && (frontEnder.name.value = frontEnder.name.value.slice(0, -1));
               break;
            case 'done':
               return 'frontEndNameConfirm';
            default:
               if (frontEnder.name.value.length < 6) {
                  frontEnder.name.value += selection;
               } else {
                  frontEnder.name.value = frontEnder.name.value.slice(0, 5) + selection;
               }
         }
      },
      prev () {
         frontEnder.name.value.length > 0 && (frontEnder.name.value = frontEnder.name.value.slice(0, -1));
      },
      objects: [
         frontEnder.createBackground(),
         new CosmosObject({
            fontName: 'DeterminationSans',
            fontSize: 16,
            objects: [
               menuText(168, 68 - 4, () => text.menu.name1),
               menuText(280, 118 - 4, () => frontEnder.name.value),
               ...namerange.map((letter, index) => {
                  const { x, y } = text.menu.name6(index);
                  return menuText(
                     x,
                     y - 4,
                     () =>
                        `§offset:0.5,0.5§§random:1,1§§fill:${
                           menuFinder('frontEndName', letter) ? '#ff0' : '#fff'
                        }§${letter}`
                  );
               }),
               menuText(
                  120,
                  408 - 4,
                  () => `§fill:${menuFinder('frontEndName', 'quit') ? '#ff0' : '#fff'}§${text.menu.name2}`
               ),
               menuText(
                  240,
                  408 - 4,
                  () => `§fill:${menuFinder('frontEndName', 'backspace') ? '#ff0' : '#fff'}§${text.menu.name3}`
               ),
               menuText(
                  440,
                  408 - 4,
                  () => `§fill:${menuFinder('frontEndName', 'done') ? '#ff0' : '#fff'}§${text.menu.name4}`
               )
            ]
         })
      ]
   }),
   frontEndNameConfirm: new CosmosNavigator<string>({
      flip: true,
      grid: () => [
         !frontEnder.name.whitelist.includes(frontEnder.name.value_true.toLowerCase()) ? [ 'no' ] : [ 'no', 'yes' ]
      ],
      next (self) {
         if (self.selection() === 'no') {
            return 'frontEndName';
         } else {
            // TODO: password handler
         }
      },
      prev: 'frontEndLoad',
      objects: [
         frontEnder.createBackground(),
         new CosmosObject({
            fontName: 'DeterminationSans',
            fontSize: 16,
            objects: [
               menuText(180, 68 - 4, () => {
                  const lower = frontEnder.name.value.toLowerCase();
                  if (lower in text.menu.confirm3) {
                     return CosmosTextUtils.format(
                        text.menu.confirm3[lower as keyof typeof text.menu.confirm3],
                        24,
                        true
                     );
                  } else {
                     return text.menu.confirm1;
                  }
               }).on('tick', function () {
                  this.alpha.value = game.input ? 1 : 0;
               }),
               new CosmosObject({
                  objects: [
                     menuText(0, 0, () => frontEnder.name.value_true).on('tick', function () {
                        this.position.x = Math.random() / 2;
                        this.position.y = Math.random() / 2;
                        this.rotation.value = (Math.random() - 0.5) * frontEnder.name.shake.value;
                     })
                  ]
               }),
               menuText(
                  146,
                  408 - 4,
                  () =>
                     `§fill:${menuFinder('frontEndNameConfirm', 'no') ? '#ff0' : '#fff'}§${
                        !frontEnder.name.whitelist.includes(frontEnder.name.value_true.toLowerCase())
                           ? text.menu.confirm2
                           : text.general.no
                     }`
               ).on('tick', function () {
                  this.alpha.value = game.input ? 1 : 0;
               }),
               menuText(
                  460,
                  408 - 4,
                  () =>
                     `§fill:${menuFinder('frontEndNameConfirm', 'yes') ? '#ff0' : '#fff'}§${
                        !frontEnder.name.whitelist.includes(frontEnder.name.value_true.toLowerCase())
                           ? ''
                           : text.general.yes
                     }`
               ).on('tick', function () {
                  this.alpha.value = game.input ? 1 : 0;
               })
            ]
         }),
         new CosmosRectangle({
            alpha: 0,
            fill: '#fff',
            stroke: '',
            size: { x: 320, y: 240 }
         })
      ]
   }).on('from', function () {
      const name = this.objects[1].objects[1];
      name.scale.x = 1;
      name.scale.y = 1;
      name.position.x = 140;
      name.position.y = 55;
      frontEnder.name.shake.value = 0;
      name.scale.modulate(renderer, 4000, { x: 3.4, y: 3.4 });
      name.position.modulate(renderer, 4000, { x: 100, y: 115 });
      frontEnder.name.shake.modulate(renderer, 4000, 2);
   }),
   frontEndSettings: new CosmosNavigator<string>({
      grid: () => [
         [
            'exit',
            'sfx',
            'music',
            'fancy',
            'epilepsy',
            ...(isMobile.any ? [ 'right' ] : []),
            ...(SAVE.flag.s.$gamepad_input_f === '' ? [] : [ 'deadzone' ]),
            ...(backend === null ? [] : [ 'mods' ])
         ]
      ],
      next (self) {
         const selection = self.selection() as string;
         switch (selection) {
            case 'exit':
               return game.active ? null : 'frontEndLoad';
            case 'music':
               SAVE.flag.b.$option_music = !SAVE.flag.b.$option_music;
               frontEnder.testMusic();
               break;
            case 'sfx':
               SAVE.flag.b.$option_sfx = !SAVE.flag.b.$option_sfx;
               frontEnder.testSFX();
               break;
            case 'fancy':
               SAVE.flag.b.$option_fancy = !SAVE.flag.b.$option_fancy;
               frontEnder.updateFancy();
               break;
            case 'epilepsy':
               SAVE.flag.b.$option_epilepsy = !SAVE.flag.b.$option_epilepsy;
               frontEnder.updateEpilepsy();
               break;
            case 'right':
               SAVE.flag.b.$option_right = !SAVE.flag.b.$option_right;
               frontEnder.updateRight();
               break;
            case 'mods':
               backend?.exec('mods');
               break;
         }
      },
      prev () {
         return game.active ? null : 'frontEndLoad';
      },
      objects: [
         frontEnder.createBackground(Number.MAX_SAFE_INTEGER),
         new CosmosObject({
            fontName: 'DeterminationSans',
            fontSize: 16,
            priority: Number.MAX_SAFE_INTEGER,
            objects: [
               // 60px base indent, +28px for each line (2 lines would have 88 px indent)
               menuText(208, 36 - 4, () => text.menu.settings1, {
                  fontName: 'DeterminationSans',
                  fontSize: 32
               }),
               menuText(
                  40,
                  84,
                  () => `§fill:${menuFinder('frontEndSettings', 'exit') ? '#ff0' : '#fff'}§${text.menu.settings2}`
               ),
               menuText(
                  40,
                  84 + 52,
                  () => `§fill:${menuFinder('frontEndSettings', 'sfx') ? '#ff0' : '#fff'}§${text.menu.settings3}`
               ),
               menuText(360, 84 + 52, () => {
                  return `§fill:${menuFinder('frontEndSettings', 'sfx') ? '#ff0' : '#fff'}§${
                     SAVE.flag.b.$option_sfx ? text.general.disabled : text.general.percent
                  }`.replace('$(x)', Math.round((1 - SAVE.flag.n.$option_sfx) * 100).toString());
               }),
               menuText(
                  40,
                  84 + 52 * 2,
                  () => `§fill:${menuFinder('frontEndSettings', 'music') ? '#ff0' : '#fff'}§${text.menu.settings4}`
               ),
               menuText(360, 84 + 52 * 2, () => {
                  return `§fill:${menuFinder('frontEndSettings', 'music') ? '#ff0' : '#fff'}§${
                     SAVE.flag.b.$option_music ? text.general.disabled : text.general.percent
                  }`.replace('$(x)', Math.round((1 - SAVE.flag.n.$option_music) * 100).toString());
               }),
               menuText(
                  40,
                  84 + 52 * 3,
                  () => `§fill:${menuFinder('frontEndSettings', 'fancy') ? '#ff0' : '#fff'}§${text.menu.settings5}`
               ),
               menuText(
                  360,
                  84 + 52 * 3,
                  () =>
                     `§fill:${menuFinder('frontEndSettings', 'fancy') ? '#ff0' : '#fff'}§${
                        SAVE.flag.b.$option_fancy ? text.general.enabled : text.general.disabled
                     }`
               ),
               menuText(
                  40,
                  84 + 52 * 4,
                  () => `§fill:${menuFinder('frontEndSettings', 'epilepsy') ? '#ff0' : '#fff'}§${text.menu.settings6}`
               ),
               menuText(
                  360,
                  84 + 52 * 4,
                  () =>
                     `§fill:${menuFinder('frontEndSettings', 'epilepsy') ? '#ff0' : '#fff'}§${
                        SAVE.flag.b.$option_epilepsy ? text.menu.settings6L : text.menu.settings6H
                     }`
               ),
               menuText(
                  40,
                  84 + 52 * 5,
                  () => `§fill:${menuFinder('frontEndSettings', 'right') ? '#ff0' : '#fff'}§${text.menu.settings7}`,
                  { alpha: isMobile.any ? 1 : 0 }
               ),
               menuText(
                  360,
                  84 + 52 * 5,
                  () =>
                     `§fill:${menuFinder('frontEndSettings', 'right') ? '#ff0' : '#fff'}§${
                        SAVE.flag.b.$option_right ? text.menu.settings7R : text.menu.settings7L
                     }`,
                  { alpha: isMobile.any ? 1 : 0 }
               ),
               menuText(
                  40,
                  84 + 52 * (isMobile.any ? 6 : 5),
                  () => `§fill:${menuFinder('frontEndSettings', 'deadzone') ? '#ff0' : '#fff'}§${text.menu.settings8}`
               ).on('tick', function () {
                  this.alpha.value = SAVE.flag.s.$gamepad_input_f === '' ? 0 : 1;
               }),
               menuText(
                  360,
                  84 + 52 * (isMobile.any ? 6 : 5),
                  () =>
                     `§fill:${
                        menuFinder('frontEndSettings', 'deadzone') ? '#ff0' : '#fff'
                     }§${text.general.percent.replace(
                        '$(x)',
                        (Math.round(SAVE.flag.n.$option_deadzone * 100) / 100).toString()
                     )}`
               ).on('tick', function () {
                  this.alpha.value = SAVE.flag.s.$gamepad_input_f === '' ? 0 : 1;
               }),
               menuText(
                  40,
                  0,
                  () => `§fill:${menuFinder('frontEndSettings', 'mods') ? '#ff0' : '#fff'}§${text.menu.settings9}`,
                  { alpha: backend === null ? 0 : 1 }
               ).on('tick', function () {
                  this.position.y =
                     42 +
                     26 *
                        (isMobile.any && SAVE.flag.s.$gamepad_input_f
                           ? 7
                           : isMobile.any || SAVE.flag.s.$gamepad_input_f
                           ? 6
                           : 5);
               })
            ]
         })
      ]
   }).on('from', async function () {
      this.position = { x: 0, y: 0 };
   }),
   save: new CosmosNavigator({
      flip: true,
      grid: () => [ saver.yellow ? [ 'done', 'done' ] : [ 'save', 'return' ] ],
      next (self) {
         if (self.selection() === 'save') {
            saver.save();
            saver.yellow = true;
            content.asSave.loaded && sounds.save.instance(renderer);
         } else {
            return null;
         }
      },
      prev: null,
      objects: [
         menuBox(108, 118, 412, 162, 6, {
            fontName: 'DeterminationSans',
            fontSize: 16,
            objects: [
               new CosmosObject({
                  objects: [
                     menuText(26, 24 - 4, () => text.general.name, {}, false),
                     menuText(158, 24 - 4, () => `${text.general.lv} ${calcLV()}`, {}, false),
                     menuText(
                        384,
                        24 - 4,
                        () => displayTime(saver.yellow ? saver.time : SAVE.data.n.time),
                        { anchor: { x: 1 } },
                        false
                     ),
                     menuText(
                        26,
                        64 - 4,
                        () => CosmosUtils.provide(saver.locations.of(SAVE.data.s.room).name),
                        {},
                        false
                     ),
                     menuSOUL(28, 124, 'save', 'save'),
                     menuText(56, 124 - 4, () => (saver.yellow ? text.menu.save3 : text.menu.save1), {}, false),
                     menuSOUL(208, 124, 'save', 'return'),
                     menuText(236, 124 - 4, () => (saver.yellow ? '' : text.menu.save2), {}, false)
                  ]
               }).on('tick', function () {
                  this.fill = saver.yellow ? '#ff0' : '#fff';
               })
            ]
         })
      ]
   })
      .on('to', () => {
         saver.yellow = false;
         game.movement = true;
      })
      .on('from', function () {
         this.position = { x: 0, y: 0 };
      }),
   shop: new CosmosNavigator({
      grid: () => [ CosmosUtils.populate(CosmosUtils.provide(shopper.value!.size), index => index) ],
      next: () => void shopper.value!.handler(),
      prev: () => void typer.skip(),
      objects: [
         menuBox(420, 78 + 162, 206, 154, 8, {
            metadata: {
               mode: 0
            },
            fontName: 'DeterminationSans',
            fontSize: 16,
            objects: [
               menuText(30, 20 - 4, () => CosmosUtils.provide(shopper.value!.tooltip) || '', {
                  spacing: { y: 3 }
               })
            ]
         }).on('tick', function () {
            this.metadata.mode ??= 0;
            const tooltip = CosmosUtils.provide(shopper.value!.tooltip);
            if (this.metadata.mode === 0 && tooltip !== null) {
               this.metadata.mode = 1;
               this.position.modulate(
                  renderer,
                  500 * ((this.position.y - 39) / 120),
                  { x: 210, y: 39 },
                  { x: 210, y: 39 }
               );
            } else if (this.metadata.mode === 1 && tooltip === null) {
               this.metadata.mode = 0;
               this.position.modulate(
                  renderer,
                  500 * (1 - (this.position.y - 39) / 120),
                  { x: 210, y: 120 },
                  { x: 210, y: 120 }
               );
            }
            const diff = (this.position.y - 120) * 2;
            for (const object of this.objects[1].objects as CosmosSprite[]) {
               if (object instanceof CosmosAnimation) {
                  object.subcrop.top = 248 + diff;
                  object.subcrop.bottom = -402 + diff;
               } else {
                  object.crop.top = 248 + diff;
                  object.crop.bottom = -402 + diff;
               }
            }
         }),
         menuBox(-2, 240, 414, 226, 8, {
            fontName: 'DeterminationSans',
            fontSize: 16,
            objects: [
               menuText(34, 20 - 4, () => game.text, {
                  fontName: 'DeterminationMono',
                  fontSize: 16,
                  spacing: { y: 5 }
               }).on('tick', function () {
                  this.alpha.value = atlas.target === 'shop' ? 1 : 0;
               }),
               ...CosmosUtils.populate(10, index => {
                  const row = Math.floor(index / 2);
                  if (index % 2) {
                     return new CosmosSprite({
                        frames: [ content.ieSOUL ],
                        position: { x: 24 / 2, y: (20 + row * 40) / 2 }
                     }).on('tick', function () {
                        this.alpha.value = menuFinder(
                           'shopList',
                           0,
                           row +
                              Math.min(
                                 Math.max(atlas.navigators.of('shopList').position.y - 2, 0),
                                 Math.max(CosmosUtils.provide(shopper.value!.size) - 5, 0)
                              )
                        )
                           ? 1
                           : 0;
                     });
                  } else {
                     return menuText(
                        54,
                        20 + row * 40 - 4,
                        () =>
                           CosmosUtils.provide(shopper.value!.options)[
                              row +
                                 Math.min(
                                    Math.max(atlas.navigators.of('shopList').position.y - 2, 0),
                                    Math.max(CosmosUtils.provide(shopper.value!.size) - 5, 0)
                                 )
                           ] || ''
                     ).on('tick', function () {
                        this.alpha.value = [ 'shopList', 'shopPurchase' ].includes(atlas.target!) ? 1 : 0;
                     });
                  }
               })
            ]
         }),
         menuBox(420, 240, 206, 226, 8, {
            fontName: 'DeterminationSans',
            fontSize: 16,
            objects: [
               menuText(30, 20 - 4, () => game.text, {
                  fontName: 'DeterminationMono',
                  fontSize: 16,
                  spacing: { y: 5 }
               }).on('tick', function () {
                  this.alpha.value = atlas.target === 'shopList' ? 1 : 0;
               }),
               menuText(
                  32,
                  180 - 4,
                  () => `${SAVE.data.n.g === Infinity ? text.general.inf : SAVE.data.n.g}${text.general.g}`
               ),
               menuText(132, 180 - 4, () => `${SAVE.storage.inventory.size}/8`),
               ...CosmosUtils.populate(10, index => {
                  const row = Math.floor(index / 2);
                  if (index % 2) {
                     return menuSOUL(22, 20 + row * 40, 'shop', 0, row);
                  } else {
                     return menuText(
                        52,
                        20 + row * 40 - 4,
                        () => CosmosUtils.provide(shopper.value!.options)[row] || ''
                     ).on('tick', function () {
                        this.alpha.value = atlas.target === 'shop' ? 1 : 0;
                     });
                  }
               })
            ]
         })
      ]
   })
      .on('from', async from => {
         from === null && atlas.attach(renderer, 'menu', 'shop');
         dialogue_primitive(CosmosUtils.provide(shopper.value!.status));
      })
      .on('to', () => {
         typer.reset(true);
         game.text = '';
      }),
   shopList: new CosmosNavigator({
      grid: () => [ CosmosUtils.populate(CosmosUtils.provide(shopper.value!.size), index => index) ],
      next: () => void shopper.value!.handler(),
      prev: 'shop'
   })
      .on('from', async () => {
         dialogue_primitive(CosmosUtils.provide(shopper.value!.status));
      })
      .on('to', () => {
         typer.reset(true);
         game.text = '';
      }),
   shopText: new CosmosNavigator({
      next: () => void typer.read(),
      prev: () => void typer.skip(),
      objects: [
         menuBox(-2, 240, 628, 226, 8, {
            objects: [
               menuText(54, 20 - 4, () => game.text, {
                  fontName: 'DeterminationMono',
                  fontSize: 16,
                  spacing: { y: 5 }
               })
            ]
         })
      ]
   }).on('to', to => {
      typer.reset(true);
      game.text = '';
      if (to === null) {
         atlas.detach(renderer, 'menu', 'shop');
         shopper.value!.vars = {};
      }
   }),
   shopPurchase: new CosmosNavigator({
      flip: true,
      grid: [ [ 0, 1 ] ],
      next (self) {
         return shopper.value!.purchase(self.selection() === 0) ? void 0 : 'shopList';
      },
      prev () {
         return shopper.value!.purchase(false) ? void 0 : 'shopList';
      },
      objects: [
         new CosmosObject({
            fontName: 'DeterminationSans',
            fontSize: 16,
            objects: [
               menuText(
                  460,
                  268 - 4,
                  () =>
                     CosmosUtils.provide(shopper.value!.prompt).replace(
                        '$(x)',
                        CosmosUtils.provide(shopper.value!.price).toString()
                     ),
                  { spacing: { y: 2 } }
               ),
               menuSOUL(450, 378, 'shopPurchase', 0, 0),
               menuText(480, 378 - 4, text.general.yes),
               menuSOUL(545, 378, 'shopPurchase', 0, 1),
               menuText(575, 378 - 4, text.general.no)
            ]
         })
      ]
   }),

   sidebar: new CosmosNavigator({
      grid: () => [ [ 'item', 'stat', 'cell' ] ],
      next (self) {
         switch (self.selection()) {
            case 'item':
               if (SAVE.storage.inventory.size > 0) {
                  return 'sidebarItem';
               } else {
                  break;
               }
            case 'stat':
               return 'sidebarStat';
            case 'cell':
               return 'sidebarCell';
         }
      },
      prev: null,
      objects: [
         menuBox(32, 52, 130, 98, 6, {
            fontName: 'CryptOfTomorrow',
            fontSize: 8,
            objects: [
               menuText(8, 10 - 4, () => text.general.name, {
                  fontName: 'DeterminationSans',
                  fontSize: 16
               }),
               menuText(8, 42 - 2, () => text.general.lv),
               menuText(8, 60 - 2, () => text.general.hp),
               menuText(8, 78 - 2, () => text.general.g),
               menuText(44, 42 - 2, () => calcLV().toString()),
               menuText(
                  44,
                  60 - 2,
                  () => `${SAVE.data.n.hp === Infinity ? text.general.inf : SAVE.data.n.hp}${`/${calcHP()}`}`
               ),
               menuText(44, 78 - 2, () => (SAVE.data.n.g === Infinity ? text.general.inf : SAVE.data.n.g).toString())
            ]
         }),
         menuBox(32, 168, 130, 136, 6, {
            fontName: 'DeterminationSans',
            fontSize: 16,
            objects: [
               menuSOUL(18, 22, 'sidebar', 'item'),
               menuSOUL(18, 58, 'sidebar', 'stat'),
               menuSOUL(18, 94, 'sidebar', 'cell'),
               menuText(
                  46,
                  22 - 4,
                  () => `${SAVE.storage.inventory.size > 0 ? '' : '§fill:#808080§'}${text.menu.sidebar1}`
               ),
               menuText(46, 58 - 4, () => text.menu.sidebar2),
               menuText(46, 94 - 4, () => text.menu.sidebar3)
            ]
         })
      ]
   })
      .on('from', key => {
         switch (key) {
            case null:
               atlas.attach(renderer, 'menu', 'sidebar');
               sounds.menu.instance(renderer);
               break;
            case 'sidebarItem':
            case 'sidebarStat':
            case 'sidebarCell':
               atlas.detach(renderer, 'menu', key);
               break;
         }
      })
      .on('to', key => {
         switch (key) {
            case null:
               atlas.detach(renderer, 'menu', 'sidebar');
               game.movement = true;
               break;
            case 'sidebarItem':
            case 'sidebarStat':
            case 'sidebarCell':
               atlas.attach(renderer, 'menu', key);
               break;
         }
      }),
   sidebarItem: new CosmosNavigator({
      grid: () => [ [ ...CosmosUtils.populate(SAVE.storage.inventory.size, index => index) ] ],
      next: 'sidebarItemOption',
      prev: 'sidebar',
      objects: [
         menuBox(188, 52, 334, 350, 6, {
            fontName: 'DeterminationSans',
            fontSize: 16,
            objects: [
               ...CosmosUtils.populate(16, index => {
                  const row = Math.floor(index / 2);
                  if (index % 2) {
                     return menuSOUL(14, 30 + row * 32, 'sidebarItem', 0, row);
                  } else {
                     return menuText(38, 30 + row * 32 - 4, () => {
                        const key = SAVE.storage.inventory.of(row);
                        if (key) {
                           return CosmosUtils.provide(items.of(key).text.name);
                        } else {
                           return '';
                        }
                     });
                  }
               }),
               menuSOUL(14, 310, 'sidebarItemOption', 0, 0),
               menuSOUL(110, 310, 'sidebarItemOption', 0, 1).on('tick', function () {
                  this.offsets[0].x = sidebarrer.use ? text.menu.item1_offset : text.menu.item2_offset;
               }),
               menuSOUL(224, 310, 'sidebarItemOption', 0, 2),
               menuText(38, 310 - 4, () => (sidebarrer.use ? text.menu.item1 : text.menu.item2)),
               menuText(134, 310 - 4, () => text.menu.item3).on('tick', function () {
                  this.offsets[0].x = sidebarrer.use ? text.menu.item1_offset : text.menu.item2_offset;
               }),
               menuText(248, 310 - 4, () => text.menu.item4)
            ]
         })
      ]
   }),
   sidebarItemOption: new CosmosNavigator<string>({
      flip: true,
      grid: [ [ 'use', 'info', 'drop' ] ],
      next (self) {
         const index = atlas.navigators.of('sidebarItem').position.y;
         const item = SAVE.storage.inventory.of(index)!;
         const selection = self.selection() as 'use' | 'info' | 'drop';
         sidebarrer.use_movement = true;
         atlas.switch('dialoguerBottom');
         (selection === 'use'
            ? use(item, index)
            : dialogue_primitive(...CosmosUtils.provide(items.of(item).text[selection]))
         ).then(async () => {
            atlas.switch(null);
            atlas.detach(renderer, 'menu', 'sidebar');
            if (selection === 'drop') {
               SAVE.storage.inventory.remove(index);
               await Promise.all(events.fire('drop', item, index));
            }
            game.movement = sidebarrer.use_movement;
         });
      },
      prev: 'sidebarItem'
   }).on('to', function (key) {
      this.position = { x: 0, y: 0 };
      if (key === 'dialoguerBottom') {
         atlas.detach(renderer, 'menu', 'sidebarItem');
      }
   }),
   sidebarStat: new CosmosNavigator({
      prev: 'sidebar',
      objects: [
         menuBox(188, 52, 334, 406, 6, {
            fontName: 'DeterminationSans',
            fontSize: 16,
            objects: [
               menuText(22, 34 - 4, () => `"${text.general.name}"`),
               menuText(22, 94 - 4, () => `${text.general.lv} \xa0\xa0\xa0${calcLV()}`),
               menuText(
                  22,
                  126 - 4,
                  () =>
                     `${text.general.hp} \xa0\xa0\xa0${
                        SAVE.data.n.hp === Infinity ? text.general.inf : SAVE.data.n.hp
                     }${` / ${calcHP()}`}`
               ),
               menuText(
                  22,
                  190 - 4,
                  () => `${text.menu.stat1} \xa0\xa0\xa0${calcAT()} (${items.of(SAVE.data.s.weapon).value})`
               ),
               menuText(
                  22,
                  222 - 4,
                  () => `${text.menu.stat2} \xa0\xa0\xa0${calcDF()} (${items.of(SAVE.data.s.armor).value})`
               ),
               menuText(22, 282 - 4, () => `${text.menu.stat3}: ${items.of(SAVE.data.s.weapon).text.name}`),
               menuText(22, 314 - 4, () => `${text.menu.stat4}: ${items.of(SAVE.data.s.armor).text.name}`),
               menuText(
                  22,
                  354 - 4,
                  () => `${text.menu.stat5}: ${SAVE.data.n.g === Infinity ? text.general.inf : SAVE.data.n.g}`
               ),
               menuText(
                  190,
                  190 - 4,
                  () => `${text.menu.stat6}: ${SAVE.data.n.exp === Infinity ? text.general.inf : SAVE.data.n.exp}`
               ),
               menuText(190, 222 - 4, () => `${text.menu.stat7}: ${calcNX() ?? 0}`)
            ]
         })
      ]
   }),
   sidebarCell: new CosmosNavigator<string>({
      grid () {
         if (atlas.target === null) {
            return [ [ 0 ] ];
         } else {
            return [
               [
                  ...[ ...phone.entries() ]
                     .filter(entry => entry[1].display())
                     .sort((a, b) => CosmosUtils.provide(a[1].priority ?? 0) - CosmosUtils.provide(b[1].priority ?? 0))
                     .map(entry => entry[0]),
                  null
               ]
            ];
         }
      },
      next: self => {
         const selection = self.selection();
         if (selection === null) {
            sidebarrer.openSettings();
         } else if (typeof selection === 'number') {
            sidebarrer.dimbox = [ 'dimboxA', 'dimboxB' ][selection] as 'dimboxA' | 'dimboxB';
            return 'sidebarCellBox';
         } else {
            phone.of(selection).trigger();
         }
      },
      prev: 'sidebar',
      objects: [
         menuBox(188, 52, 334, 302, 6, {
            fontName: 'DeterminationSans',
            fontSize: 16,
            objects: [
               ...CosmosUtils.populate(14, index => {
                  const row = Math.floor(index / 2);
                  if (index % 2 === 0) {
                     return new CosmosSprite({
                        frames: [ content.ieSOUL ],
                        position: { x: 14 / 2, y: (30 + row * 32) / 2 },
                        priority: 1
                     });
                  } else {
                     return new CosmosText({
                        fill: '#fff',
                        position: { x: 38 / 2, y: (30 + row * 32 - 4) / 2 },
                        stroke: ''
                     });
                  }
               }),
               new CosmosSprite({
                  frames: [ content.ieSOUL ],
                  position: { x: 14 / 2, y: (30 + 7 * 32) / 2 },
                  priority: 1
               }),
               menuText(38, 30 + 7 * 32 - 4, () => {
                  return text.general.settings;
               })
            ]
         }).on('tick', function () {
            const list = CosmosUtils.provide(atlas.navigators.of('sidebarCell').grid)[0].slice(0, -1) as string[];
            for (const [ index, obj ] of this.objects[2].objects.entries()) {
               if (index < 14) {
                  const row = Math.floor(index / 2);
                  if (index % 2 === 0) {
                     if (row < list.length) {
                        obj.alpha.value = menuFinder('sidebarCell', 0, row) ? 1 : 0;
                     } else {
                        obj.alpha.value = 0;
                     }
                  } else {
                     (obj as CosmosText).content = CosmosUtils.provide(
                        phone.of(CosmosUtils.provide(atlas.navigators.of('sidebarCell').grid)[0][row]).name
                     );
                  }
               } else if (index === 14) {
                  obj.alpha.value = menuFinder('sidebarCell', 0, list.length) ? 1 : 0;
               }
            }
         })
      ]
   }).on('to', key => {
      if (key !== 'sidebar') {
         atlas.detach(renderer, 'menu', 'sidebarCell', ...((key === 'dialoguerBottom' ? [] : [ 'sidebar' ]) as string[]));
      }
   }),
   sidebarCellBox: new CosmosNavigator({
      next (self) {
         const dimbox = SAVE.storage[sidebarrer.dimbox];
         const source = [ SAVE.storage.inventory, dimbox ][self.position.x];
         const item = source.of(self.position.y);
         if (item && [ dimbox, SAVE.storage.inventory ][self.position.x].add(item)) {
            source.remove(self.position.y);
         }
      },
      prev: null,
      grid: () => [ CosmosUtils.populate(8, index => `i${index}`), CosmosUtils.populate(10, index => `b${index}`) ],
      objects: [
         menuBox(16, 16, 598, 438, 6, {
            fontName: 'DeterminationSans',
            fontSize: 16,
            objects: [
               menuText(82 + 5.5, 16 - 4, () => text.menu.box1),
               menuText(426 + 2.5, 16 - 4, () => text.menu.box2),
               menuText(178 + 5, 392 - 4, () => text.general.finish)
            ]
         }),
         ...CosmosUtils.populate(54, index => {
            const x = Math.floor(index / 3);
            const y = x < 8 ? x : x - 8;
            switch (index % 3) {
               case 0:
                  return menuSOUL(x < 8 ? 40 : 342, 80 + y * 32, 'sidebarCellBox', x < 8 ? 0 : 1, y);
               case 1:
                  return menuText(
                     x < 8 ? 68 : 370,
                     80 - 4 + y * 32,
                     () => {
                        const container = x < 8 ? SAVE.storage.inventory : SAVE.storage[sidebarrer.dimbox];
                        if (container.size > y) {
                           return CosmosUtils.provide(items.of(container.of(y)!).text.name);
                        } else {
                           return '';
                        }
                     },
                     { fontName: 'DeterminationSans', fontSize: 16 }
                  );
               default:
                  return new CosmosRectangle({
                     alpha: 0,
                     fill: '#f00',
                     position: { x: (x < 8 ? 78 : 380) / 2, y: (90 + y * 32) / 2 },
                     size: { x: 180 / 2, y: 1 / 2 }
                  }).on('tick', function () {
                     this.alpha.value =
                        (x < 8 ? SAVE.storage.inventory : SAVE.storage[sidebarrer.dimbox]).size > y ? 0 : 1;
                  });
            }
         }),
         new CosmosRectangle({ fill: '#fff', position: { x: 320 / 2, y: 92 / 2 }, size: { x: 1 / 2, y: 300 / 2 } }),
         new CosmosRectangle({ fill: '#fff', position: { x: 322 / 2, y: 92 / 2 }, size: { x: 1 / 2, y: 300 / 2 } })
      ]
   })
      .on('from', function () {
         sounds.dimbox.instance(renderer);
         this.position = { x: 0, y: 0 };
      })
      .on('to', () => (game.movement = true)),
   sidebarCellKey: new CosmosNavigator({
      prev: null,
      grid: () => [ [ ...keyring.values() ].filter(entry => entry.display()).map((_, i) => i + 1) ],
      objects: [
         menuBox(16, 16, 598, 438, 6, {
            fontName: 'DeterminationSans',
            fontSize: 16,
            objects: [
               menuText(300, 16 - 4, text.menu.key1, { anchor: { x: 0 } }),
               menuText(178 + 5, 392 - 4, () => text.general.finish)
            ]
         }),
         ...CosmosUtils.populate(2 * 3, index => {
            const r = Math.floor(index / 2);
            const y = 100 - 4 + r * 100;
            function idx (r: number) {
               return (
                  [ ...keyring.values() ].filter(entry => entry.display()).length -
                  (atlas.navigators.of('sidebarCellKey').selection() as number) -
                  r
               );
            }
            function info (r: number) {
               return [ ...keyring.values() ]
                  .filter(entry => entry.display())
                  .sort((a, b) => CosmosUtils.provide(b.priority ?? 0) - CosmosUtils.provide(a.priority ?? 0))[idx(r)];
            }
            if (index % 2 === 0) {
               return menuText(
                  68,
                  y,
                  () => {
                     const i = info(r);
                     if (i) {
                        return CosmosUtils.provide(i.name);
                     } else {
                        return '';
                     }
                  },
                  { fontName: 'CryptOfTomorrow', fontSize: 8, fill: '#808080' }
               );
            } else {
               return menuText(
                  68,
                  y + 15,
                  () => CosmosTextUtils.format(CosmosUtils.provide(info(r)?.description ?? ''), 36, true),
                  { fontName: 'DeterminationSans', fontSize: 16, fill: '#fff' }
               );
            }
         }),
         new CosmosObject({ position: { x: 287 }, fill: '#fff' }).on('tick', function () {
            const len = [ ...keyring.values() ].filter(entry => entry.display()).length;
            if (this.metadata.len !== len) {
               this.metadata.len = len;
               const dots = Math.ceil(len / 3);
               if (dots < 2) {
                  this.objects = [];
               } else {
                  const dotSize = 10;
                  const totalHeight = dots * dotSize;
                  const origin = 120 - totalHeight / 2 + dotSize / 2;
                  this.objects = CosmosUtils.populate(dots, index =>
                     new CosmosRectangle({
                        anchor: 0,
                        position: { y: origin + index * dotSize },
                        size: 4
                     }).on('tick', function () {
                        if (index === Math.floor((atlas.navigators.of('sidebarCellKey').selection() - 1) / 3)) {
                           this.alpha.value = 1;
                        } else {
                           this.alpha.value = 0.5;
                        }
                     })
                  );
               }
            }
         })
      ]
   })
      .on('from', () => {
         atlas.navigators.of('sidebarCellKey').position = { x: 0, y: 0 };
         atlas.attach(renderer, 'menu', 'sidebarCellKey');
         sounds.dimbox.instance(renderer);
      })
      .on('to', () => {
         atlas.detach(renderer, 'menu', 'sidebarCellKey');
         game.movement = true;
      })
      .on('change', () => {
         sounds.menu.instance(renderer).rate.value = 1.5;
      })
});

// basic attach/detach
for (const key of [
   'dialoguerBottom',
   'dialoguerTop',
   'frontEndLoad',
   'frontEndName',
   'frontEndNameConfirm',
   'frontEndSettings',
   'save',
   'shopText',
   'shopPurchase',
   'sidebarCellBox'
] as string[]) {
   atlas.navigators
      .of(key)
      .on('from', () => atlas.attach(renderer, 'menu', key))
      .on('to', () => atlas.detach(renderer, 'menu', key));
}

// menu navigation sounds
for (const key of [
   'choicer',
   'save',
   'shop',
   'shopList',
   'sidebar',
   'sidebarItem',
   'sidebarItemOption',
   'sidebarCell'
]) {
   atlas.navigators.of(key).on('change', () => sounds.menu.instance(renderer));
}

// positional reset on navigate from, select on navigate to
for (const [ nav1, nav2, ...others ] of [
   [ 'shop', null ],
   [ 'shopList', 'shop' ],
   [ 'sidebar', null, 'frontEndSettings' ],
   [ 'sidebarItem', 'sidebar' ],
   [ 'sidebarCell', 'sidebar', 'sidebarCellBox', 'sidebarCellPms', 'sidebarCellKey', 'frontEndSettings' ]
] as [string, string | null, ...string[]][]) {
   atlas.navigators.of(nav1).on('from', from => {
      from === nav2 && (atlas.navigators.of(nav1).position = { x: 0, y: 0 });
   });
   atlas.navigators.of(nav1).on('to', to => {
      [ nav2, ...others ].includes(to!) || sounds.select.instance(renderer);
   });
}

// ghostface fix
for (const key of [ 'dialoguerBottom', 'dialoguerTop' ]) {
   const nav = atlas.navigators.of(key);
   speech.holders.push(() => {
      nav.objects[0].objects[2].objects[0].objects = [ ...(speech.state.face ? [ speech.state.face ] : []) ];
   });
}

events.on('modded').then(() => {
   SAVE.flag.s.$gamepad_input_f === '' || frontEnder.updateDeadzone();
   isMobile.any && frontEnder.updateRight();
});

events.on('loaded').then(() => {
   frontEnder.updateEpilepsy();
   frontEnder.updateFancy();
   frontEnder.updateMusic();
   frontEnder.updateSFX();
   if (isMobile.any) {
      renderer.attach('menu', mobile.gamepad());
      mobile.target = renderer.canvas;
   }
});

events.on('teleport-update', (direction, position) => {
   player.position.set(position);
   player.face = direction;
   player.metadata.x = player.position.x;
   player.metadata.y = player.position.y;
   player.metadata.face = player.face;
   for (const entry of tracker.history) {
      entry[0] = direction;
      entry[1].x = position.x;
      entry[1].y = position.y;
   }
});

keys.downKey.on('down', () => {
   keyState.down = true;
});

keys.interactKey.on('down', () => {
   keyState.interact = true;
   if (!game.input || !game.movement) {
      return;
   }
   if (!game.noclip && game.interact) {
      game.interact = false;
      activate(player.objects[1] as CosmosHitbox, hitbox => !!(hitbox.metadata.interact && hitbox.metadata.name));
      renderer.on('render').then(() => {
         game.interact = true;
      });
      return;
   }
   atlas.target === 'dialoguerBase' && atlas.next();
});

keys.leftKey.on('down', () => {
   keyState.left = true;
   if (atlas.target === 'frontEndSettings') {
      const selection = atlas.navigators.of('frontEndSettings').selection() as string;
      if (selection === 'music' && !SAVE.flag.b.$option_music) {
         SAVE.flag.n.$option_music = Math.min(Math.max(0, (SAVE.flag.n.$option_music += 0.05)), 1);
         frontEnder.testMusic();
      } else if (selection === 'sfx' && !SAVE.flag.b.$option_sfx) {
         SAVE.flag.n.$option_sfx = Math.min(Math.max(0, (SAVE.flag.n.$option_sfx += 0.05)), 1);
         frontEnder.testSFX();
      } else if (selection === 'deadzone') {
         SAVE.flag.n.$option_deadzone = Math.min(Math.max(0.1, (SAVE.flag.n.$option_deadzone -= 0.05)), 0.9);
      }
   }
});

keys.menuKey.on('down', async () => {
   keyState.menu = true;
   if (game.input) {
      if (game.movement && game.menu) {
         switch (atlas.target) {
            case null:
               game.movement = false;
               atlas.switch('sidebar');
               break;
            case 'sidebar':
               atlas.switch(null);
               break;
         }
      }
   }
});

keys.quitKey.on('down', async () => {
   let active = true;
   const r = escText.metadata.renderer;
   r.attach('menu', escText);
   keys.quitKey.on('up').then(() => {
      active = false;
      escText.metadata.state = 0;
      escText.alpha.modulate(r, escText.alpha.value * 300, 0).then(() => {
         r.detach('menu', escText);
      });
   });
   await escText.alpha.modulate(r, 300, 1);
   while (active && escText.metadata.state++ !== 2) {
      await r.pause(300);
   }
   active && (backend === null || SAVE.ready ? reload_full() : exit());
});

keys.rightKey.on('down', () => {
   keyState.right = true;
   if (atlas.target === 'frontEndSettings') {
      const selection = atlas.navigators.of('frontEndSettings').selection() as string;
      if (selection === 'music' && !SAVE.flag.b.$option_music) {
         SAVE.flag.n.$option_music = Math.min(Math.max(0, (SAVE.flag.n.$option_music -= 0.05)), 1);
         frontEnder.testMusic();
      } else if (selection === 'sfx' && !SAVE.flag.b.$option_sfx) {
         SAVE.flag.n.$option_sfx = Math.min(Math.max(0, (SAVE.flag.n.$option_sfx -= 0.05)), 1);
         frontEnder.testSFX();
      } else if (selection === 'deadzone') {
         SAVE.flag.n.$option_deadzone = Math.min(Math.max(0.1, (SAVE.flag.n.$option_deadzone += 0.05)), 0.9);
      }
   }
});

keys.specialKey.on('down', () => {
   if (atlas.target === 'dialoguerBase' && game.movement) {
      atlas.prev();
   }
});

keys.upKey.on('down', () => {
   keyState.up = true;
});

renderer.on('tick', { priority: -Infinity, listener: gamepadder.update });

renderer.on('tick', () => {
   game.timer && SAVE.data.n.time++;

   if (keyState.menu && game.interact && game.input && !game.movement) {
      switch (atlas.target) {
         case 'dialoguerBase':
         case 'dialoguerBottom':
         case 'dialoguerTop':
         case 'shopText':
            typer.read();
            typer.skip(false, true);
            break;
      }
   }

   if (!player.puppet) {
      if (game.input && game.movement) {
         const up = keyState.up;
         const left = keyState.left;
         const right = keyState.right;
         const down = keyState.down;
         const base = player.position.value();
         const mSpeed = (game.sprint && keyState.special ? 3 : 1) * player.metadata.speed;
         player.move(
            new CosmosPoint(
               left ? -3 : right ? 3 : 0,
               player.metadata.reverse ? (down ? 3 : up ? -3 : 0) : up ? -3 : down ? 3 : 0
            ).multiply(mSpeed),
            renderer,
            [ 'below', 'main' ],
            game.noclip ? false : hitbox => hitbox.metadata.barrier === true
         );
         if (player.metadata.reverse) {
            if (down) {
               player.face = 'up';
               mSpeed === 0 || player.sprite.enable();
            } else if (up) {
               player.face = 'down';
               mSpeed === 0 || player.sprite.enable();
            } else if (left) {
               player.face = 'right';
               mSpeed === 0 || player.sprite.enable();
            } else if (right) {
               player.face = 'left';
               mSpeed === 0 || player.sprite.enable();
            }
         }
         if (mSpeed === 0) {
            if (up) {
               player.face = player.metadata.reverse ? 'down' : 'up';
            } else if (down) {
               player.face = player.metadata.reverse ? 'up' : 'down';
            } else if (left) {
               player.face = player.metadata.reverse ? 'right' : 'left';
            } else if (right) {
               player.face = player.metadata.reverse ? 'left' : 'right';
            }
         } else if (up && down && player.position.y === base.y) {
            if (player.metadata.reverse) {
               player.position.y -= 1;
            } else {
               player.position.y += 1;
            }
            player.face = 'down';
         }
         if (!game.noclip) {
            activate(player, hitbox => !!(hitbox.metadata.trigger && hitbox.metadata.name));
            events.fire('tick');
         }
      } else {
         player.move({ x: 0, y: 0 }, renderer);
      }
   }
});

typer.on('empty', () => {
   for (const instance of fetchCharacters()) {
      instance.talk = false;
      for (const sprite of Object.values(instance.preset.talk)) {
         speech.targets.delete(sprite);
      }
   }
});

typer.on('header', header => {
   const [ key, delimiter, ...args ] = header.split('');
   if (delimiter === '/') {
      const value = args.join('');
      switch (key) {
         case 'c':
            const [ type, mA, mB ] = value.split('/').map(subvalue => +subvalue);
            choicer.marginA = mA;
            choicer.marginB = mB;
            choicer.navigator = atlas.target;
            choicer.result = 0;
            choicer.type = type;
            atlas.target = 'choicer';
            atlas.attach(renderer, 'menu', 'choicer');
            atlas.navigators.of('choicer').position = { x: 0, y: 0 };
            break;
         case 'e':
            const [ identifier, emote ] = value.split('/');
            identifier in speech.emoters && (speech.emoters[identifier].index = +emote);
            break;
         case 'f':
            speech.state.face = speech.state.preset.faces[+value];
            speech.state.face?.reset();
            break;
         case 'g':
            speech.state.face = portraits.of(value);
            speech.state.face?.reset();
            break;
         case 'i':
            if (value[0] === 'x') {
               typer.interval = Math.round(speech.state.preset.interval * +value.slice(1));
            } else {
               typer.interval = +value;
            }
            break;
         case 'k':
            shopper.value!.preset(...value.split('/').map(subvalue => +subvalue));
            break;
         case 'p':
            const preset = (speech.state.preset = speech.presets.of(value));
            speech.state.face = preset.faces[0];
            speech.state.face?.reset();
            typer.chunksize = preset.chunksize ?? 1;
            typer.interval = preset.interval;
            typer.threshold = preset.threshold ?? 0;
            typer.sounds = preset.voices[0] ?? [];
            game.text = '';
            for (const instance of fetchCharacters()) {
               if (instance.key === value) {
                  instance.talk = true;
                  for (const sprite of Object.values(instance.preset.talk)) {
                     speech.targets.add(sprite);
                  }
               } else {
                  instance.talk = false;
                  for (const sprite of Object.values(instance.preset.talk)) {
                     speech.targets.delete(sprite);
                  }
               }
            }
            break;
         case 's':
            soundRegistry.of(value).instance(renderer);
            break;
         case 'v':
            typer.sounds = speech.state.preset.voices[+value] ?? [];
            break;
      }
   }
});

typer.on('idle', () => {
   speech.state.face?.reset();
   for (const speaker of speech.targets) {
      speaker.reset();
   }
   events.fire('shut');
});

typer.on('text', content => {
   game.text = content;
   if (typer.mode === 'read' && content.length > 0) {
      if (content[content.length - 1].match(/[\.\!\?]/)) {
         speech.state.face?.reset();
         for (const speaker of speech.targets) {
            speaker.reset();
         }
         events.fire('shut');
      } else {
         speech.state.face?.enable();
         for (const speaker of speech.targets) {
            speaker.enable();
         }
         events.fire('talk');
      }
   }
});

addEventListener('blur', () => {
   gamepadder.reset();
   mobile.reset();
   if (!game.focus) {
      context.suspend();
      CosmosRenderer.suspend = true;
   }
   for (const key of Object.values(keys)) {
      key.reset();
   }
});

addEventListener('focus', () => {
   context.resume();
   CosmosRenderer.suspend = false;
});

addEventListener('gamepadconnected', event => {
   gamepadder.connect(event.gamepad);
});

addEventListener('gamepaddisconnected', event => {
   gamepadder.disconnect(event.gamepad);
});

if (isMobile.any) {
   addEventListener('touchcancel', event => {
      event.preventDefault();
      for (const touch of mobile.touches(event)) {
         mobile.clear(touch.identifier);
      }
   });

   addEventListener('touchend', event => {
      event.preventDefault();
      for (const touch of mobile.touches(event)) {
         mobile.clear(touch.identifier);
      }
   });

   addEventListener('touchmove', event => {
      event.preventDefault();
      mobile.target && (mobile.bounds = mobile.target.getBoundingClientRect());
      for (const touch of mobile.touches(event)) {
         mobile.touch(
            touch.identifier,
            new CosmosPoint(touch.clientX, touch.clientY)
               .subtract(mobile.bounds)
               .divide(game.ratio)
               .divide(renderer.scale)
         );
      }
   });

   addEventListener('touchstart', event => {
      event.preventDefault();
      mobile.target && (mobile.bounds = mobile.target.getBoundingClientRect());
      for (const touch of mobile.touches(event)) {
         mobile.clear(touch.identifier);
         mobile.touch(
            touch.identifier,
            new CosmosPoint(touch.clientX, touch.clientY)
               .subtract(mobile.bounds)
               .divide(game.ratio)
               .divide(renderer.scale)
         );
      }
   });
}

CosmosUtils.status(`LOAD MODULE: FRAMEWORK (${Math.floor(performance.now()) / 1000})`, { color: '#07f' });
